---
title: "Devocional: Você primeiro! | Revista Comunhão"
author: "John Doe"
dates:
  - created: "December 8, 2022"
  - modified: "December 9, 2022"
tags: [" DEVOCIONAIS/revista_comunhao RELIGIAO/Cristianismo "]
aliases: ["submissão a Cristo", "sujeitai-vos a Cristo", "amar ao próximo"]
abstract: "Sujeição não é simplesmente balançar a cabeça como vaquinhas de presépio, assentindo tudo. A Bíblia diz é que todos nós devemos ter para com o próximo uma atitude de humildade, ajudando e servindo, sempre pensando no outro em primeiro lugar."
link: "https://comunhao.com.br/domingo-08-de-dezembro-2/"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DEVOCIONAL: VOCÊ PRIMEIRO! | REVISTA COMUNHÃO ~ CRIADO EM 2022/12/08 ~ ATUALIZADO EM 2022/12/09 -->

## Devocional: Você primeiro!

<p>&nbsp;</p>

<!-- IFRAME CENTRALIZADO ~ YOUTUBE -->

<!-- <p>&nbsp;</p>

<span class="text-center meio" align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/eALofC1UkCc" title="Devocional - 08 de Dezembro 2019 [Mensagem Motivacional]" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
    <small>_“[Devocional - 08 de Dezembro 2019 [Mensagem Motivacional]](https://www.youtube.com/watch?v=eALofC1UkCc)”_ • YouTube</small>
</span>

<p>&nbsp;</p> -->

<span class="text-center" align="center">![[Devocional - 08 de Dezembro 2019 [Mensagem Motivacional].mp4]]  
<small>_“[Devocional - 08 de Dezembro 2019 [Mensagem Motivacional]](https://www.youtube.com/watch?v=eALofC1UkCc)”_ • YouTube</small></span>

<p>&nbsp;</p>

- **Versículo do dia:**  
    > [!bible] [[Efésios]] 5:21 • VFL
    > Sujeitem-se uns aos outros por causa do respeito que vocês têm por Cristo.

Sujeição não é simplesmente balançar a cabeça como vaquinhas de presépio, assentindo tudo. A Bíblia diz é que todos nós devemos ter para com o próximo uma atitude de humildade, ajudando e servindo, sempre pensando no outro em primeiro lugar. Isso é sujeitar-se, submeter-se, cumprir o mandamento bíblico. Se agirmos assim, principalmente em épocas do ano em que o foco não está mais voltado para a fraternidade universal, as relações humanas ficarão mais fáceis de serem levadas.

- **Lição do dia:**  
    _“Submissão nada mais é do que colocar o outro em primeiro lugar.”_

- **Oração:**  
    _Submeta-se primeiro a Deus e assuma o compromisso de se sujeitar-se ao próximo._

- **Leia hoje:**  
    - [x] [[Efésios]] 4–6

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Devocional: Você primeiro!](https://comunhao.com.br/domingo-08-de-dezembro-2/)”_ | Revista Comunhão
- December 8, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/revista_comunhao #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

