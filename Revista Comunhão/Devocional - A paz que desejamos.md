---
title: "Devocional: A paz que desejamos | Revista Comunhão"
author: "John Doe"
dates:
  - created: "December 9, 2022"
  - modified: "December 9, 2022"
tags: [" DEVOCIONAIS/revista_comunhao RELIGIAO/Cristianismo "]
aliases: ["verdadeira paz", "Príncipe da Paz"]
abstract: "O mundo sempre ansiou pela paz, porque desde a criação existe o espírito beligerante entre os homens. Quando pensamos que estamos nos aproximando dela, surge alguém para começar uma nova guerra."
link: "https://comunhao.com.br/segunda-feira-09-de-dezembro-2/"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DEVOCIONAL: A PAZ QUE DESEJAMOS ~ CRIADO EM 2022/12/09 ~ ATUALIZADO EM 2022/12/09 -->

## Devocional: A paz que desejamos

<p>&nbsp;</p>

<!-- IFRAME CENTRALIZADO ~ YOUTUBE -->

<!-- <p>&nbsp;</p>

<span class="text-center meio" align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/uvy9KGltgz4" title="Devocional - 09 de Dezembro 2019 [Mensagem Motivacional]" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
    <small>_“[Devocional - 09 de Dezembro 2019 [Mensagem Motivacional]](https://www.youtube.com/watch?v=uvy9KGltgz4)”_ • YouTube</small>
</span>

<p>&nbsp;</p> -->

<span class="text-center" align="center">![[Devocional - 09 de Dezembro 2019 [Mensagem Motivacional].mp4]]  
<small>_“[Devocional - 09 de Dezembro 2019 [Mensagem Motivacional]](https://www.youtube.com/watch?v=uvy9KGltgz4)”_ • YouTube</small></span>

<p>&nbsp;</p>

- **Versículo do dia:**  
    > [!bible] [[Filipenses]] 4:7
    > E a paz de Deus, que está além da compreensão humana, guardará os corações e as mentes de vocês em Cristo Jesus.

O mundo sempre ansiou pela paz, porque desde a criação existe o espírito beligerante entre os homens. Quando pensamos que estamos nos aproximando dela, surge alguém para começar uma nova guerra. A mesma coisa acontece nas relações familiares. Quantas famílias destruídas por conflitos internos, das quais mesmo a tão propagada “paz” desta época do ano passa longe! Deus promete a verdadeira paz a todas as famílias.

- **Lição do dia:**  
    _“Somente Jesus, o Príncipe da Paz, pode trazer a verdadeira paz à sua família.”_

- **Oração:**  
    _Peça a Deus para que a paz verdadeira habite no seu coração e no de sua família._

- **Leia hoje:**  
    - [x] [[Filipenses]] 1–4

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Devocional: A paz que desejamos](https://comunhao.com.br/segunda-feira-09-de-dezembro-2/)”_ | Revista Comunhão
- December 9, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/revista_comunhao #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

