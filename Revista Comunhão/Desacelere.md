---
title: "Desacelere | Revista Comunhão"
author: "Sânnie Rocha"
dates:
  - created: "August 24, 2022"
  - modified: "October 7, 2022"
tags: [" ReadItLater desacelerar ansiedade RELIGIAO/Cristianismo REVISTA_COMUNHAO/sannie_rocha "]
aliases: ["Sânnie Rocha", "Revista Comunhão", "ansiedade", "desacelere", "desacelerar", "confiança em Deus"]
abstract: "A ansiedade pode resultar em sérios problemas emocionais, físicos e espirituais. Por isso, por mais difícil, cuide-se e confie em Deus."
link: "https://comunhao.com.br/desacelere-2/"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- Desacelere! ~ CRIADO EM 2022/08/24 ~ ATUALIZADO EM 2022/10/07 -->

## Desacelere!

**Por Sânnie Rocha**{.text-dir}

<p>&nbsp;</p>

> [!info]+ Contexto  
> A ansiedade pode resultar em sérios problemas emocionais, físicos e espirituais. Por isso, por mais difícil, cuide-se e confie em Deus.

<p>&nbsp;</p>

Vivemos num mundo cheio de incertezas. E são elas as responsáveis por provocar a ansiedade, um mal que assola a humanidade neste século, com resultados devastadores para a saúde psicológica, física e espiritual. Preocupação excessiva, medo de falar em público, indigestão crônica, problemas do sono, perfeccionismo, temores irracionais, tensão muscular, pânico, flashback's e comportamento compulsivo indicam que o indivíduo está sofrendo de Transtorno de Ansiedade Generalizada (TAG), como explica Israel Maia, psicanalista e pastor da Assembleia de Deus, autor do livro _“Enfermidades da Alma”_, da editora Betel.

A ansiedade pode ter origem genética, isto é, herda-se dos antepassados uma pré-disposição para desenvolvê-la. Se este for o caso, há risco de que esse quadro surja já nos primeiros anos de vida (0 a 3), faixa em que criança poderá apresentar comportamento hiperativo. Outra possibilidade é de desencadeamento a partir de uma infância carente e problemática, decorrente da dificuldade dos pais, e principalmente da mãe, em transmitir afeto ao filho.

<p>&nbsp;</p>

> [!tip]- Confira também…
> - [Abuso espiritual: Quando o perigo está no púlpito da Igreja](https://comunhao.com.br/abuso-espiritual-quando-o-perigo-esta-no-pulpito-da-igreja/)
> - Acesse nossas redes sociais:
>     + **Instagram:** [@revistacomunhao](https://instagram.com/revistacomunhao)
>     + **Facebook:** [/revistacomunhao](https://www.facebook.com/revistacomunhao)

<p>&nbsp;</p>

Este é o século da saturação de conteúdos. Textos, dados, livros, revistas, jornais, blog's, sites, fanpages, seriados, palestras, cursos, apostilas, treinamentos, receitas, músicas, videoaulas, vídeos amadores… Tudo isso, acessível somente por um clique, surge diretamente na tela do smartphone, na smart TV, no tablet, no notebook. E é justamente esse excesso de informações que é abordado pelo Pr. Pedro Noia no livro _“Ansiedade: Um mal causado pela intoxicação e pelo congestionamento das ideias na era digital”_, obra prestes a ser lançada pela WS Editora.

“Usando de neologismo,[^1] podemos chamar de ‘intoxicação’ o congestionamento nas vias do nosso pensar lógico. Por conta dessa síndrome, nos tornamos pessoas ainda mais ansiosas. Exceder-se no trabalho, nos estudos ou mesmo na vida ministerial, ainda que alcançando o topo do sucesso, nunca será o melhor caminho para a satisfação pessoal. Não há uma relação direta entre o sucesso e a paz, a conquista e a [saúde emocional](obsidian://search?query=tag:%23saude_emocional), o poder e a alegria, o acesso e a satisfação, a riqueza e a realização. Muito pelo contrário, por vezes, a relação é inversamente proporcional. Quanto mais se tem, menos se deleita, quanto mais se conquista, menos espaço para se viver, quanto mais sucesso e fama, menos privacidade, quanto mais evidência, menos paz interior”, alerta Noia.


[^1]: **NE.O.LO.GIS.MO** _Substantivo_  
    1. `Linguística` Palavra formada recentemente, utilizando elementos clássicos e/ou populares, ou adaptando um termo de língua estrangeira, por ex.: _dolarizar_, _dolarização_, no português e, _zebra_, como “resultado inesperado”.  
    2. `Linguística` Novo sentido adquirido por uma palavra já existente em um vocabulário.  
    **Antônimo:** _arcaísmo_.  
    <small>\[Nota de Samej Spenser\]</small> 

<p>&nbsp;</p>

### Tem solução?

Ansiedade pode ser controlada? Para os especialistas, em alguns casos é possível tentar relaxar e conseguir amenizar os sintomas sozinho, mas em outros é necessário buscar ajuda especializada.

“Controlar as expectativas quanto ao amanhã. Lembrar-se de que a vida, a família e a relação com Deus são mais importantes do que tudo que se possa ter ou conquistar neste mundo. Saúde física e mental deve ser prioridade, e não a aparência exterior ou roupas, posses e riquezas. A ansiedade não ajuda ninguém a resolver problemas, pelo contrário, pode se tornar a grande vilã. Deus conhece nossas necessidades e não deixará faltar o [[Senhor e Rei que cuida de nós|pão nosso]] de cada dia. O melhor é encher a mente das coisas do alto, evitando o excesso de informações negativas e fúteis”, orienta Pedro Noia.

O Pr. Israel Maia, como psicanalista, sugere a busca por um especialista já nos primeiros sinais de que há algo errado, pois a situação pode se agravar muito rapidamente. “A ciência nos ensina que existe uma cadeia crescente em relação às enfermidades da alma em que a ansiedade gera estresse; o estresse gera a angústia; a angústia gera a depressão; a depressão gera a melancolia, que é o estágio mais profundo e perigoso de depressão, conhecido popularmente como ‘fundo do poço’ e que se não for devidamente tratado poderá gerar a morte. Sendo assim, do ponto de vista científico, quando os primeiros sintomas de ansiedade forem identificados, o indivíduo deve imediatamente procurar um profissional qualificado para ajudá-lo na busca pela cura”, ressalta.

> [!quote] Eduardo Biazi  
> “Sofro muito por antecipação, tanto que na maioria das vezes não consigo me concentrar nas atividades do dia a dia.”
>
> — **Eduardo Biazi**, membro da Igreja Apostólica Hebrom

Entretanto, explica, não se pode descartar o componente espiritual na vida do cristão que sofre diariamente com os ataques constantes vindos da mente de Satanás. “Realmente ser cristão não nos livra de sofrermos com as enfermidades da alma, mas para nós existe um diferencial que é justamente a [[O valor da Palavra de Deus em nossa vida|Palavra de Deus]]. O próprio Cristo nos dá um ensinamento que, se observado, é capaz de nos tranquilizar e nos ajudar a superar as crises de ansiedade. No [[O que é o Sermão da Montanha (estudo)|sermão da montanha]], Jesus deixa claro que a ansiedade só consegue encontrar espaço naquele que não reconhece o poder do Pai celestial na sua vida. Ao lermos o texto em Mateus 6:25-34, temos plena certeza de que existe alguém que realmente cuida e cuidará de nós, e isso é mais do suficiente para nos tranquilizar”, relativiza.

Para ele, muitos veem o cristão como um ser imune às enfermidades que atingem os sentimentos e as emoções, mas isso não é uma realidade, pois somos humanos e podemos sofrer. “Quando aceitamos a Cristo, recebemos uma dose especial de segurança potencializada pela nossa fé, ou seja, passamos a estar seguros em Jesus. Entretanto, as dificuldades cotidianas corroboram para que surjam em nós sentimentos que nos levam a perder a certeza de que estamos seguros e de que tudo quanto nos é prometido pelo Pai na pessoa do Filho irá se cumprir. A ansiedade nos faz perder o equilíbrio, e o indivíduo desequilibrado perde o foco que o direciona para o alvo.

Para o cristão, grande parte do que acontece na sua vida tem um cunho espiritual. Não quero aqui dizer que ansiedade é algo que venha do mundo das trevas, mas explicar que, quando se vive em um ambiente ou atmosfera de acontecimentos espirituais, se é facilmente atingido por aquele que está pronto para matar, roubar e destruir. O inimigo de nossas almas se utiliza, em muitos casos, de nossas emoções para nos desequilibrar e assim conseguir colocar em um processo de ansiedade. Quando isso acontece, os sintomas começam a se desencadear”, enfatiza.

<p>&nbsp;</p>

### Desacelerando e confiando

Vamos encontrar na Bíblia alguns personagens que refletem esse estado da alma. Talvez [[Davi]] seja o mais transparente deles, segundo observa o psicólogo e pastor da Igreja Batista Erasmo Vieira. “A ansiedade pode se manifestar por conta de erros do passado, tensões do presente e incertezas do futuro, assim como foi com [[Davi]]. De qualquer modo, seja qual for a causa, a [[O valor da Palavra de Deus em nossa vida|Palavra de Deus]] tem algumas recomendações para nós e que sabemos ser as mais corretas para enfrentar esse mal”, orienta.

Os sintomas do TAG ou da síndrome do pensamento acelerado atordoaram por algum tempo a vida de Priscilla Leite de Andrade Ramos, membro da Igreja Cristã Maranata. Até que foi necessário recorrer a remédios para conseguir realizar suas atividades cotidianas. “Eu tomo uma medicação para estabilizar os sintomas, é um antidepressivo que ajuda vários tipos de transtornos como o TOC e o TAG. Mas dependendo do grau do meu estresse, e quando algo foge do meu controle, tenho uma crise, algo mais leve, nada comparado com a forma que eu ficava antes de saber o que tinha.

A ansiedade que sinto é uma doença, e seus sintomas são muito limitantes até para tarefas cotidianas, porque não consigo desligar meu cérebro para determinados assuntos. E pior, tenho preocupações constantes com coisas que nem vão acontecer ou que não são reais. Por exemplo, eu tenho muita dor de cabeça desde pequena; a mente de uma pessoa com TAG pensa assim: se eu tenho tanta dor de cabeça, estou com um tumor no cérebro e vou morrer. Para melhorar, além da medicação diária, eu procuro contradizer os pensamentos que não correspondem à realidade e atento mais para a minha respiração”, relata.

> [!quote] Israel Maia  
> “Quando os primeiros sintomas de ansiedade forem identificados, o indivíduo deve imediatamente procurar um profissional qualificado para ajudá-lo na busca pela cura.”
>
> — **Israel Maia**, pastor e autor do livro “Enfermidades da Alma”

Tentar ocupar a mente com algo alheio ao que está causando a crise foi uma tática que Priscilla aprendeu, mas como serva, apesar de todos os recursos humanos que usa para amenizar a doença, a estratégia bíblica não foi abandonada.

“Eu acredito que a oração é a base para tudo, porque é o acesso direto ao Senhor, e somos dependentes dEle. Tenho uma experiência marcante de quando estava no auge das crises e nem sabia o que estava acontecendo comigo. Em algumas noites, sem conseguir dormir, minha mente não parava de pensar repetidamente em uma reportagem que vi na TV e, mesmo com muito sono, ao cochilar, acordava com qualquer ruído, com o coração disparado de susto. Então, comecei a cantar um louvor mentalmente que dizia: ‘Minha força é o Senhor, nada tenho que temer, meu refúgio e fortaleza’. Então consegui dormir. Para mim foi uma grande bênção, mas outra bênção foi encontrar o diagnóstico e a medição correta. Se for propósito do Senhor me curar, amém, mas devemos procurar assistência médica, sim. Se não for proposito dEle a cura, sei que continuará me ajudando a vencer um dia de cada vez”, diz, esperançosa.

Mas é normal um crente sucumbir aos sintomas da ansiedade? Muitos podem pensar que quem serve a Deus está imune a esse tipo doença, mas não é bem assim. Seres humanos são passíveis a fraquezas, e este é um mal que envolve inquietação, algo que não acontece apenas com não cristãos.

> [!quote] Israel Maia  
> “Quando os primeiros sintomas de ansiedade forem identificados, o indivíduo deve imediatamente procurar um profissional qualificado para ajudá-lo na busca pela cura.”
>
> — **Israel Maia**, pastor e autor do livro “Enfermidades da Alma”

O rei [[Davi]] escreveu: “Até quando terei ansiedades e preocupações, tristeza no meu coração todo dia?” ([[Salmos|Salmo]] 13:2). O que o ajudou a lidar com a ansiedade? Ele abriu seu coração ao Senhor em oração, confiando plenamente no amor leal de Deus ([[Salmos|Salmo]] 13:5; 62:8). De fato, o Altíssimo quer que falemos com Ele sobre nossas preocupações. Isso vai nos trazer alívio. Em 1 Pedro 5:7, a Bíblia diz que devemos lançar sobre o Pai toda a nossa ansiedade, porque Ele cuida de nós.

<p>&nbsp;</p>

### O descanso da alma

Para quem serve a Jesus Cristo, a ansiedade é resultado da incredulidade. É o que explica o reverendo da [[Igreja Presbiteriana]] e conferencista [[Hernandes Dias Lopes]]. “Jesus Cristo diz que os gentios que não conhecem a Deus é que ficam ansiosos quanto ao que comer, beber e vestir. Ficamos ansiosos porque duvidamos de que Deus é suficientemente bom e sábio para cuidar de nossa vida. Ficamos ansiosos porque tentamos tomar o destino de nossa vida em nossas próprias mãos. Só temos duas opções: descansamos na bondosa providência de Deus ou seremos estrangulados emocionalmente pela ansiedade”, destaca.

Rhayza Avelino de Oliveira, da Comunidade Batista Cristã, sofre bastante com sua ansiedade, a ponto de ter sido preciso procurar ajuda para melhorar as crises. “Eu tive que reconhecer que precisava mudar a minha percepção de futuro e sobre as aflições desta vida. Precisei de um acompanhamento profissional (psicólogo) para me ajudar a reorganizar meus pensamentos e emoções, e de um líder/tutor/amigo que pudesse ministrar a [[O valor da Palavra de Deus em nossa vida|Palavra de Deus]] sobre a minha vida, porque sozinha eu não tinha forças para buscar as respostas; minha reação era sempre o desespero, o pânico, adoecer! Foi preciso aprender a confiar no Deus que tem o controle e me sentir segura nEle! Renovação da mente! ([[Romanos]] 12:2). Confiar que Ele já está no meu futuro e por isso eu posso descansar! Essa confiança é adquirida através do relacionamento íntimo com Ele e da meditação na Palavra; Crer de verdade que Alguém maior que você, mais poderoso e que o ama muito, está cuidando de tudo! Hoje, quando me percebo perturbada e ansiosa com alguma situação, me retiro por uns minutos no silêncio do meu quarto, medito na [[O valor da Palavra de Deus em nossa vida|Palavra de Deus]] e oro ao Senhor confessando minha ansiedade, a minha incapacidade de confiar, e peço para que Ele me ajude a me apropriar daquela verdade contida na Palavra,” explica.

> [!quote] Priscilla Leite de Andrade Ramos  
> “A ansiedade que sinto é uma doença e seus sintomas são muito limitantes até para tarefas cotidianas, porque não consigo desligar meu cérebro para determinados assuntos.”
>
> — **Priscilla Leite de Andrade Ramos**, membro da Igreja Cristã Maranata

Assim também é com Eduardo Biazi, da Igreja Apostólica Hebrom. “Sofro muito por antecipação, tanto que na maioria das vezes não consigo me concentrar nas atividades do dia a dia. Perco o foco devido à ansiedade. Um motivo fútil, pequeno, toma grande proporção. Outro sintoma da ansiedade é a forte angústia, que não passa. Cada dia oro a Deus para que possa descansar nEle, porque oração é a base de tudo. Há casos mais leves que a oração resolve; há casos mais graves que já se tornam doenças clínicas e em que é preciso procurar médico especialista que indicará remédios para amenizá-los, não tem jeito”, salienta.

O Pr. Israel Maia finaliza que, para quem serve a Deus, o melhor é sempre pensar que o fim para toda ansiedade será a volta de Jesus. “Para o crente fiel, a esperança de ficar livre de todo mal está firmada na vinda do Filho de Deus, na qual todos os que estiverem em Cristo receberão um corpo glorificado livre de todo ataque do diabo, de toda enfermidade da alma e do corpo. Quando esse momento chegar, estaremos completamente livres de todo tipo de sofrimento. ‘E Deus limpará de seus olhos toda a lágrima; e não haverá mais morte, nem pranto, nem clamor, nem dor; porque já as primeiras coisas são passadas’ ([[Apocalipse]] 21:4)”, completa.

> [!quote] Erasmo Vieira  
> “A ansiedade pode se manifestar por conta de erros do passado, tensões do presente e incertezas do futuro, assim como foi com [[Davi]].”
>
> — **Erasmo Vieira**, pastor da Igreja Batista e psicólogo

<p>&nbsp;</p>

### Ansiedade sob controle

- Controle suas expectativas quanto ao amanhã.
- Lembre-se de que sua vida, sua família e sua relação com Deus são mais importantes do que tudo que você possa ter ou conquistar neste mundo.
- Sua saúde física e mental deve ser sua prioridade, e não sua aparência exterior ou suas vestes, posses e riquezas.
- A ansiedade não ajuda ninguém a resolver problemas, pelo contrário, ela pode se tornar a grande vilã.
- Deus conhece nossas necessidades e não deixará faltar o [[Senhor e Rei que cuida de nós|pão nosso]] de cada dia
- Encha sua mente das coisas do alto, evite o excesso de informações negativas e fúteis.
- Caso não consiga controlar seus sentimentos, busque ajuda especializada, porque o caso pode se agravar.

**Fonte:** pastores Israel Maia (Psicanalista), Pedro Noia e Erasmo Vieira (Psicólogo)

<p>&nbsp;</p>

> [!alert] Atenção…  
> Esta matéria foi publicada originalmente na edição 228 da Revista Comunhão, de Agosto de 2016. As pessoas ouvidas e/ou citadas podem não estar mais nas situações, cargos e instituições que ocupavam na época, assim como suas opiniões e os fatos narrados referem-se às circunstâncias e ao contexto de então.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Desacelere!](https://comunhao.com.br/desacelere-2/)”_ | por Sânnie Rocha • Revista Comunhão
- August 24, 2022

<p>&nbsp;</p>

###### tags:

#ReadItLater #sannie_rocha #desacelerar #ansiedade #RELIGIAO/Cristianismo #revista_comunhao

<p>&nbsp;</p>
