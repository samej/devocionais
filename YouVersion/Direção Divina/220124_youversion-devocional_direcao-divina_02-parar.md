---
title: "Direção Divina - Dia 2: Parar | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 24, 2022"  
update: "January 25, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 2: PARAR” ~ CRIADO EM 2022/01/24 ~ ATUALIZADO EM 2022/01/25 -->

<center><span align="center meio">

# Direção Divina <br />DIA 2: PARAR

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

Uma das melhores decisões que você pode tomar quando está sentindo um impulso ou enfrentando um dilema de alto risco, é a decisão de _parar_. Dê um tempo. Ore pedindo orientação. Pense mais um pouco. Obtenha alguma [[Entendendo a sabedoria de Deus|sabedoria divina]] de pessoas que você confia e imagine os possíveis resultados. Então, pergunte a si mesmo: “Isso é algo que eu deveria parar completamente?”.

A maioria de nós têm boas intenções ou pelo menos algum tipo de justificativa para o que fazemos. E, no entanto, muitos de nós ficamos surpresos quando nos encontramos muito longe da direção que queremos seguir. As grandes mudanças em nossas vidas – tanto negativas quanto positivas – raramente acontecem sem uma série de decisões que se acumulam umas sobre as outras como dominós sem fim.

Você vê como parar pode ser uma das coisas mais produtivas que fazemos? Quando você para e faz um balanço de onde está e para onde quer ir, _então_ você pode decidir como avançar para o seu destino.

**O que você pode parar a fim de se aproximar da direção divina de Deus?**

Você está fazendo algo que o está levando a uma direção que você não quer ir (ou que Deus não quer que você vá)? O que você precisa parar completamente? Um vício em mídias sociais, álcool, pornografia, aprovação ou trabalho? Um relacionamento doentio? Uma atitude de julgamento? O que você pode parar a fim de se aproximar da direção divina de Deus? Trate cada escolha como se fosse o próximo passo em direção ao seu destino.

Quando um comportamento ou relacionamento nos leva em uma direção que sabemos estar distante da história que queremos contar, precisamos pausar não apenas para considerar as consequências, mas também para escolher parar de viajar na direção errada. Você provavelmente já ouviu a palavra “arrepender-se”. Um de seus significados literais é virar-se. Quando você se arrepende, para de se dirigir em uma direção e retorna para Deus e o caminho Dele para você.

Nesse sentido, parar de fato, significa caminhar em uma nova direção. Você pode precisar dar um passo em direção à responsabilidade, ao perdão, aos amigos certos ou a um novo lugar para morar.

<p>&nbsp;</p>

**Pergunte a si mesmo:**

1. Se eu fizer a escolha que estou considerando onde ela pode me levar?

2. O que posso parar a fim de me aproximar da direção divina de Deus?

<p>&nbsp;</p>

### Dia 2: Referências

> “<small>^1^</small> Portanto, também nós, uma vez que estamos rodeados por tão grande nuvem de testemunhas, livremo-nos de tudo o que nos atrapalha e do pecado que nos envolve e corramos com perseverança a corrida que nos é proposta,  
> <small>^2^</small> tendo os olhos fitos em Jesus, autor e consumador da nossa fé. Ele, pela alegria que lhe fora proposta, suportou a cruz, desprezando a vergonha, e assentou-se à direita do trono de Deus.”  
> [Hebreus 12:1-2](https://my.bible.com/pt/bible/129/HEB.12.1-2.NVI) • NVI.

> “<small>^19^</small> Arrependam-se, pois, e voltem-se para Deus, para que os seus pecados sejam cancelados,  
> <small>^20^</small> para que venham tempos de descanso da parte do Senhor, e ele mande o Cristo, o qual lhes foi designado, Jesus.”  
> [Atos 3:19-20](https://my.bible.com/pt/bible/129/ACT.3.19-20.NVI) • NVI.

>“<small>^13^</small> Quem esconde os seus pecados não prospera, mas quem os confessa e os abandona encontra misericórdia.”  
> [Provérbios 28:13](https://my.bible.com/pt/bible/129/PRO.28.13.NVI) • NVI.

> “ <small>^3^</small> Os mestres da lei e os fariseus trouxeram-lhe uma mulher surpreendida em adultério. Fizeram-na ficar em pé diante de todos  
> <small>^4^</small> e disseram a Jesus: “Mestre, esta mulher foi surpreendida em ato de adultério.  
> <small>^5^</small> Na Lei, Moisés nos ordena apedrejar tais mulheres. E o senhor, que diz?”  
> <small>^6^</small> Eles estavam usando essa pergunta como armadilha, a fim de terem uma base para acusá-lo. Mas Jesus inclinou-se e começou a escrever no chão com o dedo.  
> <small>^7^</small> Visto que continuavam a interrogá-lo, ele se levantou e lhes disse: “Se algum de vocês estiver sem pecado, seja o primeiro a atirar pedra nela”.  
> <small>^8^</small> Inclinou-se novamente e continuou escrevendo no chão.  
> <small>^9^</small> Os que o ouviram foram saindo, um de cada vez, começando pelos mais velhos. Jesus ficou só, com a mulher em pé diante dele.  
> <small>^10^</small> Então Jesus pôs-se em pé e perguntou-lhe: “Mulher, onde estão eles?  
> <small>^11^</small> Ninguém a condenou?” “Ninguém, Senhor”, disse ela. Declarou Jesus: “Eu também não a condeno. Agora vá e abandone sua vida de pecado”.”  
> [João 8:3-11](https://my.bible.com/pt/bible/129/JHN.8.3-11.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 2 - Começar | Dia 1: Começar]]

- [[220124_youversion-devocional_direcao-divina_03-permanecer | Dia 3: Permanecer]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

 