---
title: "Direção Divina - Dia 5: Servir | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 25, 2022"  
update: "January 25, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 5: SERVIR” ~ CRIADO EM 2022/01/25 ~ ATUALIZADO EM 2022/01/25 -->

<center><span align="center meio">

# Direção Divina <br />DIA 5: SERVIR

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

Servir aos outros não é natural para mim. Eu sou uma pessoa egocêntrica. Gosto das coisas do meu jeito. Não é algo de que tenha orgulho, mas infelizmente, é verdade.

Eu não sou o único. Todos nós podemos ser um pouco egocêntricos. Por natureza, somos pessoas egoístas. Pense nisso: você não precisa ensinar uma criança a ser egoísta. De acordo com Jesus, a vida não é toda sobre nós, mas ainda assim tudo em nossa cultura tenta nos diz para fazermos as coisas do nosso jeito.

Uma das maneiras mais rápidas de esquecer sobre Deus é ser consumido com o “eu”. Jesus tinha palavras bem diretas para aqueles que queriam segui-Lo. Ele disse: _“Se alguém quiser acompanhar-me, negue-se a si mesmo, tome a sua cruz e siga-me.”_ Mateus 16:24 NVI.

Deus quer que trilhemos o _Seu_ caminho. E ele não está falando sobre um pedaço de carne extra, nem sobre alface.

Falando em comida, Jesus fez uma declaração que deveria nos fazer parar antes de pedirmos o próximo hambúrguer. _"A minha comida... é fazer a vontade daquele que me enviou e concluir a sua obra."_ João 4:34 NVI.

Imagine poder dizer: "A minha comida é servir a Deus. Minha comida é agradá-lo. Minha comida é completar a tarefa que Deus me deu para fazer. Minha comida é fazer a vontade do meu Pai e concluir a Sua obra”. Esse é um tipo diferente de alimento. Isso é viver com direção divina.

**Quando toda a cultura diz: "Encha-se", Deus nos diz para encher outras pessoas.**

Quando as pessoas ao nosso redor dizem: “Consiga tudo o que puder! É tudo sobre você”, Deus quer que contribuamos em vez de consumirmos. Quando toda a cultura diz: "Encha-se", Deus nos diz para encher os outros. Deus não nos criou para sermos compradores. Ele nos criou para sermos doadores. Em vez de nos concentrarmos nos desejos, somos chamados a nos concentrar nas necessidades dos outros. Em vez de furar a fila, somos chamados a esperar no final. Deus nos criou para servir.

Esse tipo de vida vai mudar a sua história.

Pense nisso. As histórias que você ama relembrar são aquelas em que você ajudou o vizinho, se envolveu na igreja ou doou alguma coisa. Isso é porque fomos feitos para servir como Jesus fez na Terra. A decisão de servir pode não parecer natural para você. Nem sempre foi para mim. Mas percebi que servir não é algo que _fazemos_. Somos chamados para _ser_ servos. Porque quando servimos, nos tornamos como Cristo.

**Ore:** Deus, como o Senhor está me chamando para servir? Quem o Senhor está me chamando para servir? Onde o Senhor está me chamando para servir?

<p>&nbsp;</p>

### Dia 5: Referências

> “<small>^24^</small> Então Jesus disse aos seus discípulos: “Se alguém quiser acompanhar-me, negue-se a si mesmo, tome a sua cruz e siga-me.”  
> [Mateus 16:24](https://my.bible.com/pt/bible/129/MAT.16.24.NVI) • NVI.

> “<small>^32^</small> Mas ele lhes disse: “Tenho algo para comer que vocês não conhecem”.  
> <small>^33^</small> Então os seus discípulos disseram uns aos outros: “Será que alguém lhe trouxe comida?”  
> <small>^34^</small> Disse Jesus: “A minha comida é fazer a vontade daquele que me enviou e concluir a sua obra.”  
> [João 4:32-34](https://my.bible.com/pt/bible/129/JHN.4.32-34.NVI) • NVI.

> “<small>^11^</small> O maior entre vocês deverá ser servo.”  
> [Mateus 23:11](https://my.bible.com/pt/bible/129/MAT.23.11.NVI) • NVI.

> “<small>^35^</small> Em tudo o que fiz, mostrei a vocês que mediante trabalho árduo devemos ajudar os fracos, lembrando as palavras do próprio Senhor Jesus, que disse: ‘Há maior felicidade em dar do que em receber’.”  
> [Atos 20:35](https://my.bible.com/pt/bible/129/ACT.20.35.NVI) • NVI.

> “<small>^10^</small> Cada um exerça o dom que recebeu para servir os outros, administrando fielmente a [[O que a Bíblia diz sobre a bondade de Deus|graça de Deus]] em suas múltiplas formas.  
> <small>^11^</small> Se alguém fala, faça-o como quem transmite a palavra de Deus. Se alguém serve, faça-o com a força que Deus provê, de forma que em todas as coisas Deus seja glorificado mediante Jesus Cristo, a quem sejam a glória e o poder para todo o sempre. Amém.”  
> [1 Pedro 4:10-11](https://my.bible.com/pt/bible/129/1PE.4.10-11.NVI) • NVI.

> “<small>^13^</small> Irmãos, vocês foram chamados para a liberdade. Mas não usem a liberdade para dar ocasião à vontade da carne; ao contrário, sirvam uns aos outros mediante o amor.  
> <small>^14^</small> Toda a Lei se resume num só mandamento: “Ame o seu próximo como a si mesmo”.  
> [Gálatas 5:13-14](https://my.bible.com/pt/bible/129/GAL.5.13-14.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 2 - Começar | Dia 1: Começar]]

- [[220124_youversion-devocional_direcao-divina_02-parar | Dia 2: Parar]]

- [[220124_youversion-devocional_direcao-divina_03-permanecer | Dia 3: Permanecer]]

- [[220124_youversion-devocional_direcao-divina_04-ir | Dia 4: Ir]]

- [[220125_youversion-devocional_direcao-divina_06-conectar-se | Dia 6: Conectar-se]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

