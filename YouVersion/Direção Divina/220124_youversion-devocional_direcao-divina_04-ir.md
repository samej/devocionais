---
title: "Direção Divina - Dia 4: Ir | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 24, 2022"  
update: "January 25, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 4: IR” ~ CRIADO EM 2022/01/24 ~ ATUALIZADO EM 2022/01/25 -->

<center><span align="center meio">

# Direção Divina <br />DIA 4: IR

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

Você sente algo novo acontecendo em sua vida? Você quer algo diferente? Mesmo se não sentir isso agora, é sempre uma boa ideia manter seu coração preparado para a mudança, porque ela é uma garantia.

Nós conversamos sobre _começar_, _parar_, e _permanecer_. Às vezes, a melhor decisão que você pode tomar para encontrar a direção de Deus é _ir_.

Muitas vezes somos chamados a manter a posição quando a pressão aumenta, mas muitas vezes precisamos arriscar. Você se sente inquieto onde você está? Deus pode ter plantado um desejo divino em você para servi-Lo de uma maneira surpreendente. Talvez Ele tenha te inspirado com um grupo específico de pessoas, uma ideia, um problema ou um lugar. Talvez ele esteja chamando você para ir. Siga esse palpite e veja onde Ele levará você. Aceite a aventura. A melhor maneira de dar um salto de fé é ter um bom começo de corrida.

Há uma história ótima no [[O Que é o Antigo Testamento|Antigo Testamento]] sobre Abrão e Sarai (que mais tarde tiveram os nomes mudados para Abraão e Sara) que descreve o "ir" perfeitamente. Em [[Gênesis]] 12, Deus falou com Abrão. Na época, ele estava morando em uma cidade chamada Harã, mas ele era de uma cidade chamada Ur dos caldeus. Na cidade natal de Abrão, Ur, o povo adorava um falso deus da lua chamado Nannar.

O importante é que o único Deus verdadeiro escolheu revelar-se a Abrão. Deus deu a ele um comando muito simples e direto: afaste-se de tudo que você já conheceu _“**Saia** da sua terra, do meio dos seus parentes e da casa de seu pai, e **vá** para a terra que eu lhe mostrarei.”_ [[Gênesis]] 12:1 NVI, ênfase minha.

Saia e vá.

**Para avançar em direção ao seu destino, você pode ter que se afastar da segurança.**

Para ir a outro lugar, você tem que sair de onde você está. Você tem que deixar o que é conhecido, confortável, previsível e fácil. Para avançar em direção ao seu destino, você pode ter que se afastar da segurança.

Quem sabe onde Deus vai conduzir sua história se você simplesmente deixar Ele te conduzir? Um dia, daqui a alguns anos, você vai olhar para trás e ver toda a história. O que vai ser? "Eu senti como se Deus estivesse me chamando, mas estava com medo, então não fiz nada." Ou você terá uma aventura cheia de fé para contar? A diferença é se você vai ou não quando Deus disser: "Vá."

**Pergunte:** O que Deus está me chamando para deixar? Onde ele está me chamando para ir?

<p>&nbsp;</p>

### Dia 4: Referências

> “<small>^1^</small> Então o Senhor disse a Abrão: “Saia da sua terra, do meio dos seus parentes e da casa de seu pai, e vá para a terra que eu lhe mostrarei.  
> <small>^2^</small> “Farei de você um grande povo, e o abençoarei. Tornarei famoso o seu nome, e você será uma bênção.  
> <small>^3^</small> Abençoarei os que o abençoarem e amaldiçoarei os que o amaldiçoarem; e por meio de você todos os povos da terra serão abençoados”.  
> <small>^4^</small> Partiu Abrão, como lhe ordenara o Senhor, e Ló foi com ele. Abrão tinha setenta e cinco anos quando saiu de Harã.”  
> [Gênesis 12:1-4](https://my.bible.com/pt/bible/129/GEN.12.1-4.NVI) • NVI.

> “<small>^15^</small> E disse-lhes: “Vão pelo mundo todo e preguem o evangelho a todas as pessoas.”  
> [Marcos 16:15](https://my.bible.com/pt/bible/129/MRK.16.15.NVI) • NVI.

> “<small>^17^</small> Portanto, pensem nisto: Quem sabe que deve fazer o bem e não o faz comete pecado.”  
> [Tiago 4:17](https://my.bible.com/pt/bible/129/JAS.4.17.NVI) • NVI.

> “<small>^8^</small> Eu o instruirei e o ensinarei no caminho que você deve seguir; eu o aconselharei e cuidarei de você.”  
> [Salmos 32:8](https://my.bible.com/pt/bible/129/PSA.32.8.NVI) • NVI.

> “<small>^13^</small> Irmãos, não penso que eu mesmo já o tenha alcançado, mas uma coisa faço: esquecendo-me das coisas que ficaram para trás e avançando para as que estão adiante,  
> <small>^14^</small> prossigo para o alvo, a fim de ganhar o prêmio do chamado celestial de Deus em Cristo Jesus.”  
[Filipenses 3:13-14](https://my.bible.com/pt/bible/129/PHP.3.13-14.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 2 - Começar | Dia 1: Começar]]

- [[220124_youversion-devocional_direcao-divina_02-parar | Dia 2: Parar]]

- [[220124_youversion-devocional_direcao-divina_03-permanecer | Dia 3: Permanecer]]

- [[220125_youversion-devocional_direcao-divina_05-servir | Dia 5: Servir]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

