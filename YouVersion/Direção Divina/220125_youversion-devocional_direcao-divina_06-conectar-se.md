---
title: "Direção Divina - Dia 6: Conectar-se | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 25, 2022"  
update: "January 25, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 6: CONECTAR-SE” ~ CRIADO EM 2022/01/25 ~ ATUALIZADO EM 2022/01/25 -->

<center><span align="center meio">

# Direção Divina <br />DIA 6: CONECTAR-SE

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

Quando percebi que fui feito não só para servir na igreja, mas para servir aos outros como igreja, a conexão com as pessoas tornou-se primordial. Você não pode servir sem se conectar. E suas conexões mudarão as histórias que você contará amanhã. Isso tem sido verdade ao longo da história. Basta considerar o homem que escreveu mais de um terço do [[Novo Testamento]], o apóstolo Paulo.

Paulo nem sempre foi cristão. Antes de ser um seguidor de Jesus, ele era Saulo de uma cidade chamada Tarso, um sujeito furioso que perseguia e matava cristãos. Se você não gosta dos seguidores de Jesus, teria amado Saulo. Mas depois de tirar a vida daqueles que acreditavam que Jesus foi ressuscitado dentre os mortos, Paulo tornou-se um deles.

Sua mudança foi tão grande, tão radical, tão transformadora que Saulo (que teve o nome mudado para Paulo) imediatamente quis contar aos outros sobre Jesus. O problema era que nenhum cristão confiava nele e por razões óbvias.

O livro de Atos coloca isso de forma simples: _Quando \[Saulo\] chegou a Jerusalém, tentou reunir-se aos discípulos, mas todos estavam com medo dele, não acreditando que fosse realmente um discípulo._ Atos 9:26 NVI. Você não pode culpar os discípulos por seu ceticismo. Eu não gostaria que o cara que matou os cristãos no mês passado liderasse meu estudo bíblico! Você gostaria?

Paulo tinha um problema. Ele não tinha credibilidade com os outros cristãos. Então, ele procurou alguém que lhe desse a chance de compartilhar sua nova paixão. A decisão de Paulo de se conectar não mudou apenas a história dele; mudou a história. Veja bem, Paulo só precisava de um amigo para alterar o curso de seu destino. E esse amigo era um homem chamado [[Barnabé]].

**Você só precisa de uma amizade para mudar o curso da sua história.**

Hoje você lerá sobre quando [[Barnabé]] arriscou levar Paulo aos apóstolos, que eram os líderes do povo que Paulo estava tentando matar anteriormente. O que aconteceu? O novo amigo de Paulo, [[Barnabé]], apostou sua reputação nele. E por causa de [[Barnabé]], os outros discípulos deram a Paulo uma chance. O resto é história. Você só precisa de uma amizade para mudar o curso da sua história.

Você só precisa de amizade para ter um casamento melhor. Você só precisa confessar para superar um vício. Você só precisa de uma conversa para ficar em melhor forma. Você só precisa de um mentor para entender seus talentos e tornar-se um líder melhor.

**Pergunte a si mesmo:** O que preciso fazer para me conectar com as pessoas certas? Existe alguém de quem eu precise me desconectar?

<p>&nbsp;</p>

### Dia 6: Referências

> “<small>^26^</small> Quando chegou a Jerusalém, tentou reunir-se aos discípulos, mas todos estavam com medo dele, não acreditando que fosse realmente um discípulo.  
> <small>^27^</small> Então [[Barnabé]] o levou aos apóstolos e lhes contou como, no caminho, Saulo vira o Senhor, que lhe falara, e como em Damasco ele havia pregado corajosamente em nome de Jesus.  
> <small>^28^</small> Assim, Saulo ficou com eles e andava com liberdade em Jerusalém, pregando corajosamente em nome do Senhor.”  
> [Atos 9:26-28](https://my.bible.com/pt/bible/129/ACT.9.26-28.NVI) • NVI.

> “<small>^24^</small> Quem tem muitos amigos pode chegar à ruína, mas existe amigo mais apegado que um irmão.”  
> [Provérbios 18:24](https://my.bible.com/pt/bible/129/PRO.18.24.NVI) • NVI.

> “<small>^20^</small> Aquele que anda com os sábios será cada vez mais sábio, mas o companheiro dos tolos acabará mal.”  
> [Provérbios 13:20](https://my.bible.com/pt/bible/129/PRO.13.20.NVI) • NVI.

> “<small>^42^</small> Eles se dedicavam ao ensino dos apóstolos e à comunhão, ao partir do pão e às orações.  
> <small>^43^</small> Todos estavam cheios de temor, e muitas maravilhas e sinais eram feitos pelos apóstolos.  
> <small>^44^</small> Os que criam mantinham-se unidos e tinham tudo em comum.  
> <small>^45^</small> Vendendo suas propriedades e bens, distribuíam a cada um conforme a sua necessidade.  
> <small>^46^</small> Todos os dias, continuavam a reunir-se no pátio do templo. Partiam o pão em casa e juntos participavam das refeições, com alegria e sinceridade de coração,  
> <small>^47^</small> louvando a Deus e tendo a simpatia de todo o povo. E o Senhor lhes acrescentava diariamente os que iam sendo salvos.”  
> [Atos 2:42-47](https://my.bible.com/pt/bible/129/ACT.2.42-47.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 2 - Começar | Dia 1: Começar]]

- [[220124_youversion-devocional_direcao-divina_02-parar | Dia 2: Parar]]

- [[220124_youversion-devocional_direcao-divina_03-permanecer | Dia 3: Permanecer]]

- [[220124_youversion-devocional_direcao-divina_04-ir | Dia 4: Ir]]

- [[220125_youversion-devocional_direcao-divina_05-servir | Dia 5: Servir]]

- [[220125_youversion-devocional_direcao-divina_07-confiar | Dia 7: Confiar]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

