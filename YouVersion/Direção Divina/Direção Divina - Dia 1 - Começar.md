---
title: "Direção Divina - Dia 1: Começar | YouVersion Devocional"
author: "Craig Groeschel"
dates:
  - created: "January 24, 2022"
  - modified: "December 13, 2022"
tags: [" DEVOCIONAIS/YouVersion direcao_divina craig_groeschel "]
aliases: ["direção divina"]
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."
links:
  - "https://gist.github.com/24dac054c0b10999c47cfc06f60f272f"
  - "https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f"
  - "https://hackmd.io/@SamejSpenser/HytTIUU6t"
  - "https://my.bible.com/pt/reading-plans/3670-divine-direction"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 1: COMEÇAR” ~ CRIADO EM 2022/01/16 ~ ATUALIZADO EM 2022/01/25 -->

# Direção Divina

<p>&nbsp;</p>

## **Dia 1:** Começar

<!-- <span class="text-center" align="center">![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)</span> -->

<span class="text-center" align="center">![[Direção Divina - Banner.png|BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”]]</span>

<p>&nbsp;</p>

Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você deixasse os princípios divinos guiarem essas escolhas? Na próxima semana, começaremos a explorar sete princípios do meu livro _Direção Divina_ para ajudá-lo a encontrar a [[Entendendo a sabedoria de Deus|sabedoria de Deus]] para as decisões diárias.

Se alguém pedisse a você para contar a história de sua vida, o que você diria?

Você pode começar com o lugar onde nasceu e como foi criado. Pode mencionar seu primeiro amor. Talvez você falasse sobre a grande mudança que sua família fez ou quando se formou na faculdade. Se você é casado, pode descrever como conheceu seu cônjuge. Se não é casado, pode descrever o motivo. Se você é pai ou mãe, pode buscar algumas fotos em seu telefone e mostrar sua família. Ou talvez você descreva sua carreira. O que há na sua história?

A maioria de nós têm capítulos que preferimos não compartilhar com ninguém. Talvez você tenha acabado em algum lugar onde nunca quis estar. Você não queria explodir, mas explodiu. Tomou decisões que te levaram mais longe do que pretendia ir. Você fez algumas coisas que lhe custaram mais do que pensou que teria que pagar. Você machucou as pessoas. Comprometeu seus valores. Quebrou promessas. Você fez coisas que acha que não pode desfazer.

**Sua história não acabou. Não é tarde demais para mudar a história que você contará um dia.**

Há boas notícias: sua história ainda não acabou. Não é tarde demais para mudar a história que você contará um dia. Independentemente do que você fez (ou não fez), seu futuro não está escrito. Você tem mais vitórias para vencer, mais amigos para conhecer, mais uma diferença a fazer, mais da [[O que a Bíblia diz sobre a bondade de Deus|bondade de Deus]] para experimentar. Quer goste ou não do enredo até agora, com a ajuda de Deus, você pode transformar sua história em algo que você tenha orgulho de compartilhar.

Aqui está uma maneira de mudar a sua história: _comece_ algo novo.

Não importa o quanto incerto, com medo ou preso você possa se sentir agora, sua história continua hoje. O que você vai começar hoje? Orar diariamente com seu cônjuge? Ler um plano bíblico YouVersion todos os dias? Ir para o aconselhamento para lidar com um problema não resolvido? Viver com mais generosidade? Servir na igreja ou na comunidade? Agora é um bom momento para escrever. Abra suas anotações e registre os pensamentos. Não pense demais. Mas reserve um momento para colocá-los no papel. Apenas uma frase ou duas.

**Pergunte a si mesmo:** O que preciso começar a fazer para avançar em direção a história de vida que quero contar?

<p>&nbsp;</p>

### Referências

> [!bible] [[Coríntios#2 Coríntios]] 5:17 • NVI
> Portanto, se alguém está em Cristo, é nova criação. As coisas antigas já passaram; <abbr title="5:17 Vários manuscritos dizem “eis que tudo se fez novo!”.">eis que surgiram coisas novas!</abbr>[^1]

> [!bible] [[Isaías]] 62:4 • NVI
> Não mais a chamarão abandonada, nem desamparada à sua terra. Você, porém, será chamada <abbr title="62:4 Hefzibá significa o meu prazer está nela.">Hefzibá</abbr>,[^2] e a sua terra, <abbr title="62:4 Beulá significa casada.">Beulá</abbr>,[^3] pois o S<small>ENHOR</small> terá prazer em você, e a sua terra estará casada.

> [!bible] [[Efésios]] 4:22-24 • NVI
> <small><sup>22</sup></small> Quanto à antiga maneira de viver, vocês foram ensinados a despir-se do velho <abbr title="4:22 Isto é, da velha vida dos não regenerados.">homem</abbr>,[^4] que se corrompe por desejos enganosos,  
> <small><sup>23</sup></small> a serem renovados no modo de pensar e  
> <small><sup>24</sup></small> a revestir-se do novo homem, criado para ser semelhante a Deus em justiça e em santidade provenientes da verdade.

> [!bible] [[Isaías]] 43:18-19 • NVI
> <small><sup>18</sup></small> Esqueçam o que se foi; não vivam no passado.  
> <small><sup>19</sup></small> Vejam, estou fazendo uma coisa nova! Ela já está surgindo! Vocês não a reconhecem? Até no deserto vou abrir um caminho e riachos no ermo.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Direção Divina - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

<a id="notas"></a>
## NOTAS

[^1]: 5:17 Vários manuscritos dizem _“eis que tudo se fez novo!”_. 

[^2]: 62:4 _Hefzibá_ significa _o meu prazer está nela_. 

[^3]: 62:4 _Beulá_ significa _casada_. 

[^4]: 4:22 Isto é, da velha vida dos não regenerados. 

[^5]: 6:27 Ou _um único côvado à sua altura?_ O côvado era uma medida linear de cerca de 45 centímetros. 