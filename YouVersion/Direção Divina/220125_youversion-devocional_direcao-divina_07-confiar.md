---
title: "Direção Divina - Dia 7: Confiar | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 25, 2022"  
update: "January 25, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 7: CONFIAR” ~ CRIADO EM 2022/01/25 ~ ATUALIZADO EM 2022/01/25 -->

<center><span align="center meio">

# Direção Divina <br />DIA 7: CONFIAR

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

Moro em Oklahoma onde o clima pode mudar muito rápido e drasticamente. Um ano em março, tivemos o mais belo dia ensolarado de primavera e um aumento de 28°C. No dia seguinte, nevou três centímetros. Por mais dramática que essa mudança tenha sido, não é nada comparado à temporada de tornados. As tempestades parecem rodopiar do nada.

Assim como elas fazem em nossas vidas.

Conversei recentemente com uma senhora no jogo de futebol do meu filho que tem lutado contra alguns problemas de saúde. Ela explicou como esteve perto de Deus anos atrás e costumava estar muito envolvida em nossa igreja. Mas quando ela começou a passar por algumas provações muito sérias, ela questionou por que Deus permitia isso. Ela lutou contra as lágrimas quando disse: “Como posso adorar um Deus em quem não posso confiar?”.

**Vamos confiar que Deus é bom, mesmo quando a vida não é?**

A pergunta desta senhora atinge o centro de uma das maiores decisões da vida. Vamos confiar que Deus é bom mesmo quando a vida não é? Nossa resposta à dor e aos desafios diz muito sobre o futuro.

Por sua própria natureza, a fé exige confiança em algo -- ou em alguém -- que nem sempre é previsível ou compreensível pelos padrões humanos. Se formos honestos, a maioria de nós quer uma prova irrefutável da presença benevolente de Deus em nossas vidas.

Isso não é novo. Lembra-se da dúvida de Tomé? Depois que Jesus morreu na cruz e ressuscitou dos mortos, Tomé disse que não acreditaria a menos que visse a prova. Em vez de ficar furioso e deixá-lo de lado pela [[O cristão pode ir ao psicólogo ou isso é falta de fé O que a Bíblia diz?|falta de fé]], Jesus graciosamente mostrou a Tomé Suas mãos perfuradas.

E lembra-se dos discípulos na tempestade? _Levantou-se um forte vendaval, e as ondas se lançavam sobre o barco, de forma que este ia se enchendo de água._ Marcos 4:37 NVI. No meio da tempestade, os discípulos não estavam sozinhos. Marcos nos lembra no versículo seguinte que Jesus estava dormindo na popa do barco.

**Com Jesus no barco, as tempestades ainda podem te sacudir, mas você não vai afundar.**

Pessoas como você e eu, a senhora no jogo de futebol, Tomé e os discípulos tendem a pensar que não estaríamos passando por uma tempestade se Deus estivesse realmente conosco. Mas não é assim. Com Jesus no barco, as tempestades ainda podem sacudir, mas você não vai afundar. Ele está com você, tanto em um banho de primavera quanto no pior tornado imaginável.

Ele não está apenas com você, Ele é por você. E se Ele está com você, quem pode ser contra você? Confie em Deus na batalha que você está travando. Confie a Ele o futuro cônjuge. Confie a Ele os filhos. Confie a Ele a sua carreira. Confie a Ele a saúde. Confie a Ele as finanças.

Confie Nele sem reservas.

Ponto final.

**Ore:** Pai Celestial confio em você com o que vou começar e parar. Confio em você com onde eu vou ficar e ir. Confio em você o suficiente para dar a minha vida para servir e me conectar com as pessoas. E confio que você está presente com propósito no meio das tempestades da minha vida. Obrigado por estar comigo, guiando meus passos e me dando orientação divina. Amém.

<p>&nbsp;</p>

### Dia 7: Referências

> “<small>^24^</small> Tomé, chamado Dídimo, um dos Doze, não estava com os discípulos quando Jesus apareceu.  
> <small>^25^</small> Os outros discípulos lhe disseram: “Vimos o Senhor!” Mas ele lhes disse: “Se eu não vir as marcas dos pregos nas suas mãos, não colocar o meu dedo onde estavam os pregos e não puser a minha mão no seu lado, não crerei”.  
> <small>^26^</small> Uma semana mais tarde, os seus discípulos estavam outra vez ali, e Tomé com eles. Apesar de estarem trancadas as portas, Jesus entrou, pôs-se no meio deles e disse: “Paz seja com vocês!”  
> <small>^27^</small> E Jesus disse a Tomé: “Coloque o seu dedo aqui; veja as minhas mãos. Estenda a mão e coloque-a no meu lado. Pare de duvidar e creia”.  
> <small>^28^</small> Disse-lhe Tomé: “Senhor meu e Deus meu!”  
> <small>^24^</small> Então Jesus lhe disse: “Porque me viu, você creu? Felizes os que não viram e creram”.”  
> [João 20:24-29](https://my.bible.com/pt/bible/129/JHN.20.24-29.NVI) • NVI.

> “<small>^35^</small> Naquele dia, ao anoitecer, disse ele aos seus discípulos: “Vamos para o outro lado”.  
> <small>^36^</small> Deixando a multidão, eles o levaram no barco, assim como estava. Outros barcos também o acompanhavam.  
> <small>^37^</small> Levantou-se um forte vendaval, e as ondas se lançavam sobre o barco, de forma que este ia se enchendo de água.  
> <small>^38^</small> Jesus estava na popa, dormindo com a cabeça sobre um travesseiro. Os discípulos o acordaram e clamaram: “Mestre, não te importas que morramos?”  
> <small>^39^</small> Ele se levantou, repreendeu o vento e disse ao mar: “Aquiete-se! Acalme-se!” O vento se aquietou, e fez-se completa bonança.  
> <small>^40^</small> Então perguntou aos seus discípulos: “Por que vocês estão com tanto medo? Ainda não têm fé?”  
> <small>^41^</small> Eles estavam apavorados e perguntavam uns aos outros: “Quem é este que até o vento e o mar lhe obedecem?”.”  
> [Marcos 4:35-41](https://my.bible.com/pt/bible/129/MRK.4.35-41.NVI) • NVI.

> “<small>^9^</small> Não fui eu que ordenei a você? Seja forte e corajoso! Não se apavore nem desanime, pois o S<small>ENHOR</small>, o seu Deus, estará com você por onde você andar”.”  
> [Josué 1:9](https://my.bible.com/pt/bible/129/JOS.1.9.NVI) • NVI.

> “<small>^31^</small> Que diremos, pois, diante dessas coisas? Se Deus é por nós, quem será contra nós?”  
> [Romanos 8:31](https://my.bible.com/pt/bible/129/ROM.8.31.NVI) • NVI.

> “<small>^25^</small> Portanto eu digo: Não se preocupem com sua própria vida, quanto ao que comer ou beber; nem com seu próprio corpo, quanto ao que vestir. Não é a vida mais importante que a comida, e o corpo mais importante que a roupa?  
> <small>^26^</small> Observem as aves do céu: não semeiam nem colhem nem armazenam em celeiros; contudo, o Pai celestial as alimenta. Não têm vocês muito mais valor do que elas?  
> <small>^27^</small> Quem de vocês, por mais que se preocupe, pode acrescentar uma hora que seja à sua vida?[^5]  
> <small>^28^</small> “Por que vocês se preocupam com roupas? Vejam como crescem os lírios do campo. Eles não trabalham nem tecem.  
> <small>^29^</small> Contudo, eu digo que nem Salomão, em todo o seu esplendor, vestiu-se como um deles.  
> <small>^30^</small> Se Deus veste assim a erva do campo, que hoje existe e amanhã é lançada ao fogo, não vestirá muito mais a vocês, homens de pequena fé?  
> <small>^31^</small> Portanto, não se preocupem, dizendo: ‘Que vamos comer?’ ou ‘Que vamos beber?’ ou ‘Que vamos vestir?’  
> <small>^32^</small> Pois os pagãos é que correm atrás dessas coisas; mas o Pai celestial sabe que vocês precisam delas.  
> <small>^33^</small> Busquem, pois, em primeiro lugar o Reino de Deus e a sua justiça, e todas essas coisas serão acrescentadas a vocês.”  
> [Mateus 6:25-33](https://my.bible.com/pt/bible/129/MAT.6.25-33.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 2 - Começar | Dia 1: Começar]]

- [[220124_youversion-devocional_direcao-divina_02-parar | Dia 2: Parar]]

- [[220124_youversion-devocional_direcao-divina_03-permanecer | Dia 3: Permanecer]]

- [[220124_youversion-devocional_direcao-divina_04-ir | Dia 4: Ir]]

- [[220125_youversion-devocional_direcao-divina_05-servir | Dia 5: Servir]]

- [[220125_youversion-devocional_direcao-divina_06-conectar-se | Dia 6: Conectar-se]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

<a id="notas"></a>
## NOTA

[^1]: 5:17 Vários manuscritos dizem _“eis que tudo se fez novo!”_. 

[^2]: 62:4 _Hefzibá_ significa _o meu prazer está nela_. 

[^3]: 62:4 _Beulá_ significa _casada_. 

[^4]: 4:22 Isto é, da velha vida dos não regenerados. 

[^5]: 6:27 Ou _um único côvado à sua altura?_ O côvado era uma medida linear de cerca de 45 centímetros. 