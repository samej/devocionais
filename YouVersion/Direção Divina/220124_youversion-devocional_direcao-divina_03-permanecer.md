---
title: "Direção Divina - Dia 3: Permanecer | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 24, 2022"  
update: "January 25, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA - DIA 3: PERMANECER” ~ CRIADO EM 2022/01/24 ~ ATUALIZADO EM 2022/01/24 -->

<center><span align="center meio">

# Direção Divina <br />DIA 3: PERMANECER

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

Pensei muitas vezes em como minha vida seria diferente se desistisse quando quisesse. Minha história poderia se tornar algo como: “Sim, eu costumava pensar que deveria ser pastor, mas dei uma chance e as coisas não deram certo. É assim que acontece”.

Tenho certeza de que você terá que lutar com desafios ocasionais em diferentes épocas de sua vida: um chefe que você não acha que pode aguentar nem mais um dia, um relacionamento que de repente está te machucando, um sonho que está ficando sem recursos, uma mudança que falhou em suas expectativas. Quando você enfrenta dificuldades, é natural reconsiderar as grandes decisões que alteram a vida. Você pode fazer perguntas como essas.

- _Eu deveria me arriscar, sair do emprego e procurar outra coisa?_
- _Depois de minha esposa ter um caso -- é hora de seguir em frente?_
- _Eu realmente sou capaz de administrar um negócio? Devo cortar o mal pela raiz antes que as coisas piorem?_

Em cada um desses exemplos -- e com a maioria das principais escolhas da vida -- você está numa bifurcação crucial na estrada, e é hora de decidir: eu deveria _permanecer_ ou ir embora?

**Estou escolhendo desistir porque é a coisa certa ou porque parece que sair seria mais fácil?**

Muitas vezes, a melhor e mais recompensadora decisão que você pode tomar é seguir a rota mesmo quando seria mais simples se virar e ir embora. Não estou dizendo que você nunca precisará ir embora. Mas antes de decidir, pergunte a si mesmo: “Estou escolhendo desistir porque é a coisa certa ou porque parece que sair seria mais fácil?” Às vezes, o maior ato de fé é a fidelidade, permanecer onde você está plantado. Daqui a alguns anos você pode olhar para trás e agradecer a Deus por ter decidido ficar quando seria mais fácil ir embora.

Lembre-se, Deus fez você à Sua imagem e semelhança e Ele é o autor e consumador de sua história. Você não é um desistente, é um finalizador.

**Ore:** Deus há alguma coisa que eu esteja tentando deixar de lado? O Senhor quer que eu permaneça e termine? O Senhor vai me dar forças para prosseguir? Amém.

<p>&nbsp;</p>

### Dia 3: Referências

> “<small>^16^</small> Rute, porém, respondeu: “Não insistas comigo que te deixe e que não mais te acompanhe. Aonde fores irei, onde ficares ficarei! O teu povo será o meu povo e o teu Deus será o meu Deus!”  
> [Rute 1:16](https://my.bible.com/pt/bible/129/RUT.1.16.NVI) • NVI.

>“<small>^30^</small> Até os jovens se cansam e ficam exaustos, e os moços tropeçam e caem;  
> <small>^31^</small> mas aqueles que esperam no Senhor renovam as suas forças. Voam alto como águias; correm e não ficam exaustos, andam e não se cansam.”  
> [Isaías 40:30-31](https://my.bible.com/pt/bible/129/ISA.40.30-31.NVI) • NVI.

> “<small>^7^</small> Portanto, irmãos, sejam pacientes até a vinda do Senhor. Vejam como o agricultor aguarda que a terra produza a preciosa colheita e como espera com paciência até virem as chuvas do outono e da primavera.  
> <small>^7^</small> Sejam também pacientes e fortaleçam o seu coração, pois a vinda do Senhor está próxima.”  
> [Tiago 5:7-8](https://my.bible.com/pt/bible/129/JAS.5.7-8.NVI) • NVI.

> “<small>^9^</small> E não nos cansemos de fazer o bem, pois no tempo próprio colheremos, se não desanimarmos.”  
> [Gálatas 6:9](https://my.bible.com/pt/bible/129/GAL.6.9.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 2 - Começar | Dia 1: Começar]]

- [[220124_youversion-devocional_direcao-divina_02-parar | Dia 2: Parar]]

- [[220124_youversion-devocional_direcao-divina_04-ir | Dia 4: Ir]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

