---
title: "Direção Divina: Apresentação | YouVersion Devocional"
author: "Craig Groeschel"
dates:
  - created: "January 24, 2022"
  - modified: "December 13, 2022"
tags: [" DEVOCIONAIS/YouVersion direcao_divina craig_groeschel "]
aliases: ["direção divina"]
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."
links:
  - "https://gist.github.com/24dac054c0b10999c47cfc06f60f272f"
  - "https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f"
  - "https://hackmd.io/@SamejSpenser/HytTIUU6t"
  - "https://my.bible.com/pt/reading-plans/3670-divine-direction"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DEVOCIONAL: “DIREÇÃO DIVINA: APRESENTAÇÃO” ~ CRIADO EM 2022/01/24 ~ ATUALIZADO EM 2022/12/13 -->

## ÍNDICE

- [[Direção Divina - Apresentação | Apresentação]]

- [[Direção Divina - Dia 1 - Começar | Dia 1: Começar]]

- [[Direção Divina - Dia 2 Parar | Dia 2: Parar]]

- [[Direção Divina - Dia 3 - Permanecer | Dia 3: Permanecer]]

- [[Direção Divina - Dia 4 - Ir | Dia 4: Ir]]

- [[Direção Divina - Dia 5 - Servir | Dia 5: Servir]]

- [[Direção Divina - Dia 6 - Conectar-se | Dia 6: Conectar-se]]

- [[Direção Divina - Dia 7 - Confiar | Dia 7: Confiar]]

<!-- [DIA 1: COMEÇAR](#dia-1-come%C3%87ar)
    + [Dia 1: Referências](#dia-1-refer%C3%AAncias)
- [DIA 2: PARAR](#dia-2-parar)
    + [Dia 2: Referências](#dia-2-refer%C3%AAncias)
- [DIA 3: PERMANECER](#dia-3-permanecer)
    + [Dia 3: Referências](#dia-3-refer%C3%AAncias)
- [DIA 4: IR](#dia-4-ir)
    + [Dia 4: Referências](#dia-4-refer%C3%AAncias)
- [DIA 5: SERVIR](#dia-5-servir)
    + [Dia 5: Referências](#dia-5-refer%C3%AAncias)
- [DIA 6: CONECTAR-SE](#dia-6-conectar-se)
    + [Dia 6: Referências](#dia-6-refer%C3%AAncias)
- [DIA 7: CONFIAR](#dia-7-confiar)
    + [Dia 7: Referências](#dia-7-refer%C3%AAncias) -->

# Direção Divina

<p>&nbsp;</p>

## APRESENTAÇÃO

<!-- <span class="text-center" align="center">![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)</span> -->

<span class="text-center" align="center">![[Direção Divina - Banner.png|BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”]]</span>

<p>&nbsp;</p>

Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life\.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a [[Entendendo a sabedoria de Deus|sabedoria de Deus]] para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 25, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

<!-- <a id="notas"></a>
## NOTAS

[^1]: 5:17 Vários manuscritos dizem _“eis que tudo se fez novo!”_. 

[^2]: 62:4 _Hefzibá_ significa _o meu prazer está nela_. 

[^3]: 62:4 _Beulá_ significa _casada_. 

[^4]: 4:22 Isto é, da velha vida dos não regenerados. 

[^5]: 6:27 Ou _um único côvado à sua altura?_ O côvado era uma medida linear de cerca de 45 centímetros.  -->