---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 8 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 23, 2023"
  - modified: "July 23, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os perseguidos por causa da justiça", "Felizes os perseguidos por causa da justiça", "Bem-aventurados os que sofrem perseguição", "Felizes os que sofrem perseguição", "Bem-aventurados os que são perseguidos", "Felizes os que são perseguidos", "Felizes são aqueles que são perseguidos por fazerem a vontade de Deus"]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/8"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/17 ~ ATUALIZADO EM 2023/07/23 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 8

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os perseguidos por causa da justiça

Essa última bem-aventurança se restringe aos que sofrem perseguição _por causa da justiça_ (cf. [[Pedro#1 Pedro]] 3:13-14; 4:12-16). Os crentes descritos nessa passagem são os que estão determinados a viver como Jesus viveu.

A perseguição pode assumir muitas formas. Não precisa limitar-se às variadas experiências rigorosas que nossos irmãos cristãos sofrem em alguns países repressores. Um cristão do mundo ocidental que pratique a justiça talvez seja ridicularizado pela família, marginalizado pelos parentes. Mas até o cristão criado em um lar seguro e compreensivo terá de enfrentar oposição e críticas em algum lugar.

A recompensa por ser perseguido por causa da justiça é o reino do céu. Em outras palavras, essa bem-aventurança serve de teste para todas as outras. Assim como uma pessoa deve ser pobre em espírito para entrar no reino (5:3), ela também será perseguida por causa da justiça para entrar no reino. Essa última bem-aventurança vem a ser uma das mais perscrutadoras de todas e liga todo o resto;<small>[^1]</small> pois, se o discípulo de Jesus nunca enfrenta nenhum tipo de perseguição, pode-se muito bem perguntar onde se mostra a justiça em sua vida. Se não há justiça, não há conformidade com a vontade de Deus, como ele pode entrar no reino?

[^1]: **PERS·CRU·TAR:** _v. tran._
    
    1. Examinar minuciosamente, com toda a atenção.
    2. Investigar.
    3. Sondar, estudar.
    4. Penetrar.
    
    <small>ETIMOLOGIA</small>: latim _perscruto(-are)_, de _perscrutor(-ari)_, procurar com cuidado, investigar, pesquisar. <small>\[Nota de Samej Spenser\]</small> 

Esse princípio fundamental reaparece repetidamente no [[O que é o Novo Testamento|Novo Testamento]]. O cristão vive em um mundo caído. Por isso, se ele demonstrar justiça transparente e genuína, será rejeitado por muitos. A justiça genuína condena as pessoas implicitamente, e não é de admirar que elas quase sempre reajam atacando, em represália. Desse modo, pelo viver justo e reto, os discípulos de Cristo dividem as pessoas em dois grupos: as que são afastadas do nosso precioso Salvador e as que são atraídas para Ele.

<p>&nbsp;</p>

> [!info] Adaptado do livro _“[O sermão do monte: exposição de Mateus 5–7](https://bit.ly/3GVr9yK)”_, Vida Nova.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:10 • VFL
> — Felizes são aqueles que são perseguidos por fazerem a vontade de Deus, pois a eles pertence o reino de Deus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 7|« Dia 7]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 8](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/8)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

