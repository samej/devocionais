---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 3 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 16, 2023"
  - modified: "July 16, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os mansos", "Felizes são os humildes", ""]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/3"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/16 ~ ATUALIZADO EM 2023/07/16 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 3

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os mansos

Mansidão é o desejo consciente de pôr os interesses do outro na frente dos nossos. Pense na consideração de [[Quem foi Abraão|Abraão]] por [[Quem foi Ló|Ló]]: isso é mansidão. De acordo com [[Números]] 12:3, [[Quem foi Moisés — Respostas Bíblicas|Moisés]] foi o homem mais manso que já existiu, e sua mansidão é demonstrada de forma excepcional nesse capítulo pela sua recusa em se defender, por seu firme compromisso com o Senhor quando sua pessoa e seus privilégios estavam ameaçados. Mas Jesus é o único que pode dizer com total propriedade:

> [!bible] [[Mateus]] 11:28-29 • VFL
> ²⁸ Venham a mim todos vocês que estão cansados ou sobrecarregados e eu lhes darei descanso.
> 
> ²⁹ Aceitem o meu ensino e aprendam de mim, pois eu sou paciente e tenho espírito humilde. Dessa forma vocês encontrarão descanso para as suas almas.

À medida que a mansidão é praticada entre nós — na mesma proporção, podemos ter certeza — um mundo ostensivamente materialista se oporá a ela. O materialista diz: _“Agarre o que puder; o forte chega primeiro, o resto é cada um por si”_. Isso se aplica tanto a indivíduos que estejam à direita quanto aos que estejam à esquerda do espectro político. Individualmente, o ser humano costuma presumir, sem raciocinar, que está no centro do universo. Por isso, cada um de nós se relaciona mal com os outros sete bilhões e meio de seres humanos que têm essa mesma ilusão. Contudo, o homem manso enxerga a si mesmo e aos outros debaixo do domínio de Deus. Como é pobre em espírito, ele não tem opinião mais elevada de si mesmo do que deveria. Por isso, é capaz de se relacionar bem com os outros.

E os mansos herdarão a terra! Essa afirmação, citada do [[Salmos|Salmo]] 37:11, contraria drasticamente o materialismo filosófico tão predominante em nossa época. Mas essa bênção de herança é verdadeira em pelo menos dois aspectos. Em primeiro lugar, só alguém genuinamente manso se sente satisfeito; seu ego não é inflado a ponto de achar que tem de ter sempre mais. Além disso, ele já se vê “possuindo tudo” em Cristo ([[Coríntios#2 Coríntios]] 6:10; cf. [[Coríntios#1 Coríntios]] 3:21-23).<small>[^1]</small> Tendo em vista essa perspectiva eterna, ele pode se permitir ser manso. Além do mais, um dia ele entrará na plenitude de sua herança, quando verá a bem-aventurança cumprida de fato. Cinquenta bilhões de trilhões de anos eternidade adentro (se é que eu posso falar de eternidade da perspectiva do tempo), o povo de Deus estará ainda se regozijando por essa bem-aventurança ser absolutamente verdadeira. Em um novo céu e uma nova terra, essas pessoas serão gratas porque, pela graça, aprenderam a ser mansas durante seus primeiros setenta anos.

[^1]: > [!bible] [[Coríntios#2 Coríntios|2 Coríntios]] 6:10 • VFL
    > Temos muita tristeza, mas estamos sempre alegres. Somos pobres, mas enriquecemos a muitos. Não temos nada, mas possuímos tudo.
    
    > [!bible] [[Coríntios#1 Coríntios|1 Coríntios]] 3:21-23 • VFL
    > ²¹ Por isso vocês não devem se orgulhar em nenhum homem, pois tudo é de vocês:
    > 
    > ²² [[Paulo de Tarso|Paulo]], [[Quem foi Apolo|Apolo]], [[Quem foi Cefas|Cefas]]; o mundo, a vida, a morte, o presente e o futuro. Tudo isto é de vocês.
    > 
    > ²³ E vocês são de Cristo, e Cristo é de Deus.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:5 • VFL
> — Felizes são os humildes, pois eles herdarão a terra que Deus prometeu.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 2|« Dia 2]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 4|Dia 4 »]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 3](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/3)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

