---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 1 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 11, 2023"
  - modified: "July 16, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os pobres de espírito", ""]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/1"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/11 ~ ATUALIZADO EM 2023/07/16 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 1

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os pobres de espírito

Pobreza de espírito é o reconhecimento pessoal da falência espiritual. É a confissão consciente da própria indignidade diante de Deus. Portanto, é a mais profunda forma de arrependimento. Ela é exemplificada pelo publicano que reconhece sua culpa, num canto do Templo: “Ó Deus, tem misericórdia de mim, um pecador!”.

Jesus insiste na pobreza de espírito: o reconhecimento total, sincero, factual, consciente e consciencioso da nossa falta de valor moral diante de Deus. A pobreza de espírito é a forma mais profunda de arrependimento.

Não é de surpreender, portanto, que o reino do céu pertença aos pobres de espírito. Já no início do [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], descobrimos que não temos os recursos espirituais para pôr em prática nenhum dos preceitos do Sermão. Não podemos atingir os padrões de Deus por nós mesmos. Temos de nos apresentar diante de Deus e reconhecer nossa falência espiritual, esvaziando-nos de nosso senso de justiça própria, autoestima moral e vanglória pessoal. Uma vez esvaziados disso tudo, estamos prontos para que ele nos encha. Grande parte do restante do Sermão do Monte tem o propósito de retirar de nós esses autoenganos e promover a pobreza genuína de espírito. A sinceridade e a profundidade desse arrependimento são requisitos primordiais para entrar na vida.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:3 • VFL
> — Felizes são os que reconhecem que precisam de Deus, pois é esse tipo de pessoa que fará parte do reino de Deus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Apresentação|« Apresentação]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 2|Dia 2 »]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 1](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/1)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

