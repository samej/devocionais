---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 2 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 16, 2023"
  - modified: "July 16, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os que choram", "Felizes são os que choram", "Felizes os que choram"]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/2"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/16 ~ ATUALIZADO EM 2023/07/16 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 2

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os que choram

O mundo em que vivemos gosta de rir. Os agentes do prazer vendem alegria e risadas, tudo em troca de lucro. O _summum bonum_ (sumo bem; bem maior) da vida torna-se a diversão, e o objetivo imediato é chegar ao próximo pico de euforia. O mundo não gosta de pessoas chorosas; elas são desmancha-prazeres.

Contudo, o Filho de Deus insiste: _“Felizes são os que choram, pois Deus os consolará”_. Isso não significa que o cristão deva ser sempre triste, como se estivesse em luto constante. Esse choro é o pesar que ele sente por seu próprio pecado. É a tristeza de alguém que começa a reconhecer o negrume de seu pecado quanto mais toma conhecimento da pureza de Deus. Isso aconteceu com [[Quem foi Isaías|Isaías]] quando lhe foi concedida uma visão da Divindade, em que até os próprios anjos do céu cobriam o rosto e clamavam em solene adoração: “Santo, santo, santo”. A reação de Isaías foi de total devastação ([[Isaías]] 6:5). Esse é o pranto de um homem que tenta alcançar a pureza por seus próprios esforços e descobre que não consegue atingi-la, por isso lamenta:

> [!bible] [[Romanos]] 7:24 • VFL
> Que homem tão miserável que sou! Quem poderá me salvar deste corpo que me conduz para a morte?

Contudo, pode haver também um choro estimulado por considerações mais gerais. Às vezes, o pecado deste mundo, a falta de integridade, a injustiça, a crueldade, a mesquinhez, o egoísmo, tudo isso [[Falando à Consciência|se acumula na consciência]] de uma pessoa sensível e a faz chorar. A maioria de nós prefere simplesmente condenar. Estamos preparados para andar com Jesus através de [[Mateus]] 23 e repetir Seus pronunciamentos de juízo; mas paramos antes de chegar ao fim do capítulo e não nos juntamos a Ele quando chora pela cidade. Os grandes luminares da história da igreja aprenderam a chorar — homens do calibre de [[João Calvino|Calvino]], [[Whitefield]], [[John Wesley|Wesley]], [[Shaftesbury]] e [[Wilberforce]].

O cristão deve ser um realista autêntico. Ele chora pelos pecados e blasfêmias de sua nação. Chora pela erosão do conceito de verdade. Chora pela avareza, pelo ceticismo, pela falta de integridade. Chora por haver tão poucos que choram. Mas ele será consolado. E que consolo! Não há consolo ou alegria que se compare com o que Deus dá aos que choram. Eles trocam o pano de saco do lamento por vestes de louvor, as cinzas do luto pelo óleo da alegria.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:4 • VFL
> — Felizes são os que choram, pois Deus os consolará.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 1|« Dia 1]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 3|Dia 3 »]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 2](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/2)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

