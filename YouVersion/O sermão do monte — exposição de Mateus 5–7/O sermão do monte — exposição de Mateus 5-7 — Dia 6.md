---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 6 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 17, 2023"
  - modified: "July 17, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os puros de coração", "Felizes os puros de coração", "Felizes são os que têm coração puro", "Felizes os que têm coração puro"]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/6"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/17 ~ ATUALIZADO EM 2023/07/17 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 6

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os puros de coração

A sexta bem-aventurança insiste em que a pureza de coração é o pré-requisito indispensável para a comunhão com Deus, para “ver” a Deus. “Quem subirá ao monte do Senhor? Quem permanecerá no seu santo lugar? Aquele que tem as mãos limpas _e o coração puro_, que não entrega a alma à vaidade, nem jura enganosamente” ([[Salmos|Salmo]] 24:3-4; cf. [[Salmos|Salmo]] 73:1).<small>[^1]</small> Deus é santo. Por isso, o [[Quem escreveu o livro de Hebreus|autor da epístola aos Hebreus]] insiste:<!-- “Esforcem-se \[…\] para serem santos. Sem santidade ninguém verá o Senhor” ([[Hebreus]] 12:14). -->

> [!bible] [[Hebreus]] 12:14 • VFL
> Esforcem-se para viver em paz com todos e ter uma vida santa; pois sem a santidade ninguém verá o Senhor.

[^1]: > [!bible] [[Salmos|Salmo]] 73:1 • VFL
    > Deus é realmente bom para Israel, bom para com os puros de coração.

Não se deve confundir pureza de coração com observância de regras manifestas exteriormente.

> [!question] Como o coração é que precisa ser puro, essa bem-aventurança nos faz perguntas desconcertantes, tais como:
> - Em que você pensa quando sua mente fica em ponto morto?
> 
> - Quando fica sabendo de um caso em que uma pessoa passou a perna em outra, em que medida isso lhe causa satisfação, independentemente do grau de astúcia e engenhosidade do engodo?
> 
> - Você gosta de humor negro, ainda que seja muito engraçado?
> 
> - A que você se dedica com fidelidade?
> 
> - O que você deseja mais do que qualquer outra coisa?
> 
> - Que coisas e que pessoas você ama?
> 
> - Até que ponto suas ações e palavras são o reflexo preciso do que há em seu coração?
> 
> - Até que ponto suas ações e palavras são uma fachada para esconder o que há em seu coração?

Nosso coração tem de ser puro, limpo e sem mancha.

> [!bible] [[1-3 João|1 João]] 3:2
> Naquele dia, quando o reino do céu se consumar, quando houver novo céu e nova terra onde habita somente a justiça, quando Jesus Cristo se manifestar, nós seremos como ele.

Essa é a nossa expectativa de longo prazo, nossa esperança. Com base nisso, [[Quem foi João Evangelista|João]] afirma:

> [!bible] [[1-3 João|1 João]] 3:3
> Todo aquele que tem esperança nele \[isto é, em Cristo\] se purifica, assim como ele é puro.

Em outras palavras, segundo [[Quem foi João de Patmos|João]], o cristão se purifica agora porque puro é o que ele há de ser no final. Seu empenho presente condiz com sua esperança futura. O mesmo tema se repete de várias formas em todo o [[O que é o Novo Testamento|Novo Testamento]]. Em certo sentido, é claro, as exigências do reino não mudam: perfeição é requisito permanente (5:48). Mas disso se conclui que o discípulo de Jesus que anseia pela consumação do reino em sua perfeição já está determinado a se preparar para ele agora. Sabendo que já está no reino, ele se preocupa com a pureza porque reconhece que o Rei é puro, e o reino em sua forma perfeita só admitirá pureza.

Os puros de coração são abençoados porque verão a Deus. Embora isso só se concretizará completamente quando surgirem o novo céu e a nova terra, já é verdade mesmo agora. Nossa percepção de Deus e de Seu agir, assim como nossa comunhão com Ele, depende de nossa pureza de coração. A _visio Dei_ (visão de Deus) — que incentivo à pureza!

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:8 • VFL
> — Felizes são os que têm coração puro, pois verão a Deus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 5|« Dia 5]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 7|Dia 7 »]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 6](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/6)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

