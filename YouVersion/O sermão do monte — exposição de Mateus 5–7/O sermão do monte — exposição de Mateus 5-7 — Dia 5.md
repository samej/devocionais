---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 5 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 17, 2023"
  - modified: "July 17, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os misericordiosos", "Felizes os misericordiosos", "Felizes são os que têm misericórdia", "Felizes os que têm misericórdia"]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/5"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/17 ~ ATUALIZADO EM 2023/07/17 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 5

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os misericordiosos

Nessa bem-aventurança, Jesus diz que nós temos de ser misericordiosos. Temos de ser compassivos e bondosos, sobretudo para com os aflitos e desamparados. Se não formos misericordiosos, não veremos a misericórdia. Mas como pode alguém que não é misericordioso receber misericórdia? Quem não é misericordioso inevitavelmente é tão ignorante de seu próprio estado que acha que não precisa de misericórdia. Essa pessoa não consegue se ver como pobre e desgraçada. Portanto, como Deus pode ser misericordioso com essa pessoa? Ela é como aquele fariseu no templo, que não teve misericórdia do publicano encolhido num canto ([[Lucas]] 18:10-14).<small>[^1]</small> Em contrapartida, o homem cuja vida reflete essas bem-aventuranças tem plena consciência de sua miséria espiritual ([[Mateus]] 5:3), chora por causa disso (5:4) e tem sede de justiça (5:6). Ele é misericordioso para com os aflitos porque reconhece que é um deles; demonstrando misericórdia, ele também alcançará misericórdia.

[^1]: > [!bible] [[Lucas]] 18:10-14 • VFL
    > ¹⁰ — Dois homens subiram ao templo para orar; um era fariseu e o outro cobrador de impostos.
    > 
    > ¹¹ O fariseu se levantou e orou consigo mesmo: “Meu Deus! Eu lhe agradeço por não ser como as outras pessoas: ladrões, desonestos, adúlteros, ou até mesmo como este cobrador de impostos.
    > 
    > ¹² Jejuo duas vezes por semana e dou dez por cento de tudo o que ganho”.
    > 
    > ¹³ O cobrador de impostos, porém, de longe, nem sequer olhava para o céu e, batendo no peito, dizia: “Meu Deus! Tenha compaixão de mim, pois sou pecador!”
    > 
    > ¹⁴ Eu lhes digo que este homem, e não o outro, foi para casa em paz com Deus. Pois Deus fará com que todo aquele que se exalte seja humilhado, e aquele que se humilhe seja exaltado.

O cristão, além disso, está numa posição intermediária. Ele deve perdoar os outros porque antes Cristo já o perdoou (cf. [[Efésios]] 4:32; [[Colossenses]] 3:13).<small>[^2]</small> Ao mesmo tempo, reconhece sua constante necessidade de mais perdão e se torna também perdoador por causa dessa perspectiva (cf. [[Mateus]] 6:14, e especialmente 18:21-35).<small>[^3]</small> O cristão perdoa porque foi perdoado; ele perdoa porque precisa de perdão. Precisamente da mesma maneira e pela mesma razão o discípulo de Jesus é misericordioso.

[^2]: > [!bible] [[Efésios]] 4:32 • VFL
    > Antes sejam bons e tenham compaixão uns dos outros. Perdoem uns aos outros como também Deus, em Cristo, perdoou a vocês.
    
    > [!bible] [[Colossenses]] 3:13 • VFL
    > Tolerem uns aos outros e também perdoem uns aos outros se algum de vocês tem alguma queixa contra alguém. Assim como o Senhor lhes perdoou, vocês também devem perdoar uns aos outros.

[^3]: > [!bible] [[Mateus]] 6:14 • VFL
    > Pois se vocês perdoarem as ofensas que as outras pessoas lhes fazem, o Pai de vocês que está no céu também lhes perdoará.
    
    > [!bible] [[Mateus]] 18:21-35 • VFL
    > **Quantas vezes se deve perdoar a um irmão**
    > 
    > ²¹ [[Quem foi Pedro|Pedro]], então, se aproximou de Jesus e lhe perguntou: — Senhor, se meu irmão continuar pecando contra mim, até quantas vezes eu devo perdoar-lhe? Até sete vezes?
    > 
    > ²² Jesus, porém, lhe respondeu: — Eu não lhe digo que até sete vezes, mas até setenta vezes sete.
    > 
    > **A parábola do mau empregado**
    > 
    > ²³ — Por isso o reino de Deus pode ser comparado a um rei que decidiu acertar as contas com os seus servos.
    > 
    > ²⁴ Assim que ele começou, um homem que lhe devia milhões de moedas de prata foi levado até ele.
    > 
    > ²⁵ O homem, entretanto, não tinha como pagar a dívida. O rei, então, mandou que ele fosse vendido, juntamente com sua mulher, seus filhos e tudo o que possuía. Dessa forma a sua dívida seria paga.
    > 
    > ²⁶ O devedor, porém, se ajoelhou aos pés do rei e lhe implorou: “Tenha paciência comigo e eu lhe pagarei tudo que lhe devo”.
    > 
    > ²⁷ — O rei sentiu pena daquele servo e lhe perdoou a dívida, deixando-o ir embora.
    > 
    > ²⁸ Quando o servo saiu, encontrou um outro servo que lhe devia cem moedas de prata. Ele agarrou este outro servo pelo pescoço e, sufocando-o, lhe dizia: “Pague-me o que você me deve”.
    > 
    > ²⁹ Este outro servo, ajoelhando-se aos pés dele, lhe implorou: “Tenha paciência comigo e eu pagarei tudo o que lhe devo”.
    > 
    > ³⁰ — Ele, entretanto, não concordou. Ao contrário, jogou o servo na prisão até que ele pagasse o que lhe devia.
    > 
    > ³¹ Quando os outros servos viram o que tinha acontecido, ficaram muito tristes e foram à procura do seu senhor para lhe contar o que tinha acontecido.
    > 
    > ³² Então o senhor chamou o primeiro servo e lhe disse: “Você é um mau servo! Você me implorou e eu perdoei toda a sua dívida para comigo.
    > 
    > ³³ Por que você não teve pena do outro servo assim como eu tive pena de você?”
    > 
    > ³⁴ O senhor ficou com muita raiva e mandou que aquele servo fosse castigado até que lhe pagasse toda a dívida.
    > 
    > ³⁵ É assim que meu Pai que está no céu fará com vocês, se cada um não perdoar de coração ao seu irmão.

Essa bem-aventurança força todo aquele que se declara discípulo de Jesus Cristo a fazer a si mesmo algumas perguntas difíceis.

> [!question] Eu sou misericordioso ou arrogante com os menos afortunados?

> [!question] Sou bondoso ou grosseiro quando lido com os marginalizados?

> [!question] Sou solícito ou insensível com os desvalidos?

> [!question] Sou compassivo ou impaciente com os oprimidos?

Tenho plena convicção de que, se o Espírito de Deus trouxer outro período de avivamento ao mundo ocidental, um dos primeiros sinais será o reconhecimento da falência espiritual que encontra satisfação em Deus e Sua justiça e que resulta em plena misericórdia para com os outros.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:7 • VFL
> — Felizes são os que têm misericórdia dos outros, pois Deus terá misericórdia deles.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 4|« Dia 4]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 6|Dia 6 »]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 5](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/5)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

