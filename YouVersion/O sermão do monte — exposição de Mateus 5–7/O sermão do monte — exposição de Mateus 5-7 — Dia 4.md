---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 4 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 16, 2023"
  - modified: "July 17, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os que têm fome e sede de justiça", "Bem-aventurados os que têm fome de justiça", "Bem-aventurados os que têm sede de justiça", "Felizes são os que têm fome e sede de justiça", "Felizes são os que têm fome de justiça", "Felizes são os que têm sede de justiça"]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/4"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/16 ~ ATUALIZADO EM 2023/07/17 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 4

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os que têm fome e sede de justiça

Quem tem fome e sede de justiça tem fome e sede de viver conforme a vontade de Deus. É alguém que não está à deriva num mar de religiosidade vazia, muito menos à toa na vida distraído com banalidades. Em vez disso, todo o seu ser ecoa a oração de um santo escocês que clamava: _“Ó, Deus, faz-me tão justo quanto um pecador perdoado pode ser!”_. O seu prazer é a [[O valor da Palavra de Deus em nossa vida|Palavra de Deus]], pois onde mais se encontra claramente expressa a vontade de Deus à qual esse indivíduo tem fome de se conformar? Ele quer ser justo não apenas porque teme a Deus, mas porque a justiça é seu mais elevado desejo neste mundo.

E qual é o resultado? Os que têm fome e sede de justiça serão fartos. O contexto exige que entendamos que a bênção significa: “serão fartos com justiça”. O Senhor concede a essa pessoa faminta o desejo do coração dela.

Aquele que tem fome e sede de justiça é abençoado e saciado por Deus, mas a justiça com que é saciado é tão maravilhosa que ele tem fome e sede de receber mais. Esse ciclo natural de crescimento é fácil de entender tão logo nos lembramos de que nesse texto justiça não se refere a obedecer a certas regras, mas à conformidade com toda a vontade de Deus. Quanto mais se busca conformidade com a vontade de Deus, mais atraente essa meta se torna para nós e maiores são os avanços em sua direção.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:6 • VFL
> — Felizes são os que têm fome e sede de justiça, pois Deus fará com que fiquem satisfeitos.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 3|« Dia 3]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 5|Dia 5 »]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 4](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/4)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

