---
title: "O sermão do monte: exposição de Mateus 5–7 • Dia 7 | YouVersion"
author: "Vida Nova"
dates:
  - created: "July 17, 2023"
  - modified: "July 17, 2023"
tags: [" RELIGIAO/Cristianismo/devocional RELIGIAO/Cristianismo/devocionais RELIGIAO/Cristianismo/sermao_do_monte "]
aliases: ["Bem-aventurados os pacificadores", "Felizes são os que promovem a paz", "Felizes são os pacificadores"]
abstract: "Quanto mais lemos o sermão do monte, mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em Mateus e seja edificado."
link: "https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/7"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- O SERMÃO DO MONTE: EXPOSIÇÃO DE MATEUS 5–7 ~ CRIADO EM 2023/07/17 ~ ATUALIZADO EM 2023/07/17 -->

# O sermão do monte: exposição de Mateus 5–7

<p>&nbsp;</p>

## Dia 7

<p>&nbsp;</p>

<span class="text-center" align="center">![O sermão do monte: exposição de Mateus 5–7](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/35684/1280x720.jpg)</span>

<p>&nbsp;</p>

> [!info]+ Contexto
> Quanto mais lemos o [[Sermão da Montanha — Poderosas lições de Jesus no Monte|Sermão do Monte]], mais podemos nos sentir atraídos e ao mesmo tempo envergonhados. As exigências incontornáveis para que o crente leve uma vida cristã pura e dedicada são apresentadas por Jesus de forma clara e incisiva. Prepare-se para explorar os ensinamentos de Jesus em seu famoso sermão em [[Evangelho de Mateus — Estudo do Evangelho Segundo Mateus|Mateus]] e seja edificado.

<p>&nbsp;</p>

### Bem-aventurados os pacificadores

Dentro de toda a estrutura da Bíblia, o maior pacificador é Jesus Cristo — o Príncipe da Paz. Ele estabelece a paz entre Deus e o homem removendo o pecado, que é o motivo do afastamento. Ele traz a paz entre o homem e seu semelhante removendo o pecado e conduzindo as pessoas à uma relação direta com Deus (veja especialmente [[Efésios]] 2:11-22).<small>[^1]</small> Jesus deu ao tradicional cumprimento judaico um significado mais profundo quando, _após sua morte e ressurreição_, saudou [[Quem foram os 12 apóstolos|Seus discípulos]] com estas palavras: “Paz seja com vocês” ([[Lucas]] 24:36; [[João]] 20:19). Assim, as Boas-Novas de Jesus Cristo são a maior mensagem de paz, e o cristão que dá testemunho e fala de sua fé é, basicamente, um arauto da paz, um pacificador. Não admira que [[Paulo de Tarso|Paulo]] use a linguagem figurada de [[Quem foi Isaías|Isaías]], que retrata mensageiros correndo pelas trilhas da região montanhosa da Judeia: “Como são belos sobre os montes os pés dos que anunciam boas-novas, que proclamam a paz, que anunciam coisas boas, que proclamam a salvação, que dizem a Sião: O seu Deus reina!” ([[Isaías]] 52:7; [[Romanos]] 10:15).<small>[^2]</small>

[^1]: > [!bible] [[Efésios]] 2:11-22 • VFL
    > ¹¹ No passado vocês, que não nasceram judeus, eram chamados de “incircuncisos” por aqueles judeus que chamavam a si mesmos de “circuncisos”, referindo-se a uma cirurgia no corpo feita por mãos humanas.
    > 
    > ¹² Portanto, lembrem-se de que naquele tempo vocês estavam sem Cristo e excluídos da comunidade de Israel. Vocês não tinham parte nas alianças relacionadas com a promessa de Deus. Vocês estavam sem esperança e sem Deus no mundo.
    > 
    > ¹³ Antigamente vocês estavam longe de Deus, mas agora, em Cristo Jesus, vocês foram aproximados de Deus pelo sangue de Cristo.
    > 
    > ¹⁴ Por causa de Cristo nós agora temos paz. Ele fez dos dois povos um só e, pelo seu próprio corpo, destruiu a parede de ódio que os separava.
    > 
    > ¹⁵ Cristo fez isso quando aboliu a lei com os seus mandamentos e suas regras. O seu propósito era fazer a paz ao criar em si mesmo um novo povo daqueles dois povos.
    > 
    > ¹⁶ O que ele queria era acabar com o ódio por meio de sua morte na cruz e, por meio dela, reunir ambos os povos com Deus num só corpo.
    > 
    > ¹⁷ E ele veio e anunciou as Boas Novas de paz para todos vocês: tanto para os que estavam longe de Deus como também para os que estavam perto dele.
    > 
    > ¹⁸ E é por meio de Cristo que todos nós podemos ir ao Pai em um Espírito.
    > 
    > ¹⁹ Portanto, vocês não são mais estrangeiros nem estranhos. Agora vocês são cidadãos que pertencem ao povo de Deus; vocês são da família de Deus.
    > 
    > ²⁰ Vocês são como um edifício que está construído sobre um alicerce que são os apóstolos e profetas. E o próprio Jesus Cristo é a pedra fundamental desse edifício.
    > 
    > ²¹ É ele que mantém o edifício todo bem ajustado e o faz crescer como templo consagrado ao Senhor.
    > 
    > ²² E em Cristo, vocês também estão sendo edificados juntamente com os outros, para se tornarem uma casa onde Deus habite por meio do Espírito.

[^2]: > [!bible] [[Romanos]] 10:15 • VFL
    > E como alguém pode proclamá-lo se não for enviado? Como está escrito: “Como é linda a chegada daqueles que proclamam Boas Novas!”

O papel de pacificador do cristão implica não só propagar o evangelho, mas também acalmar tensões, buscar soluções, garantir que a comunicação seja eficiente. Talvez essa missão seja mais difícil nas situações em que há envolvimento pessoal. Nesse caso, é preciso lembrar que “a ira humana não produz a justiça que Deus deseja” ([[Tiago]] 1:20) e que “a resposta branda desvia o furor” ([[Provérbios]] 15:1). O cristão nunca deve confundir problemas, mesmo os importantes, com sua autoimagem; e, para não ser culpado de gerar mais calor do que luz, ele deve aprender a baixar seu tom de voz quanto maior for a intensidade da discussão, e ser mais bem humorado.

A recompensa do pacificador, portanto, é que ele será chamado de filho de Deus. Ele reflete o maravilhoso caráter pacificador de seu Pai celestial. Em nossos dias ainda existe um sentido em que os cristãos intuitivamente reconhecem esse aspecto divino no caráter do pacificador. Por exemplo, quando, em alguma convenção ou reunião da igreja, os cristãos se envolvem em um debate acalorado, o irmão que permanece calmo, escuta cada ponto de vista com respeito, equidade e educação, acalmando os ânimos à flor da pele, é considerado espiritual pelos outros irmãos, ainda que ninguém manifeste isso verbalmente. Mas essa conduta deveria ser considerada normal entre discípulos de Jesus Cristo, pois o próprio Jesus instituiu essa norma. Ela é aspecto essencial do ser filho de Deus.

<p>&nbsp;</p>

### Referência bíblica

> [!bible] [[Mateus]] 5:9 • VFL
> — Felizes são os que promovem a paz, pois Deus os chamará de “filhos”.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">[[O sermão do monte — exposição de Mateus 5-7 — Dia 6|« Dia 6]] • [[O sermão do monte — exposição de Mateus 5-7 — Dia 8|Dia 8]]</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[O sermão do monte: exposição de Mateus 5–7 • Dia 7](https://www.bible.com/pt/reading-plans/35684-o-sermao-do-monte-exposicao-de-mateus-57/day/7)”_ | por [[YouVersion]]
- July 11, 2023

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocional #RELIGIAO/Cristianismo/devocionais #RELIGIAO/Cristianismo/sermao_do_monte 

<p>&nbsp;</p>

