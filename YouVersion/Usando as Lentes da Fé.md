---
title: "Usando as Lentes da Fé | Devocional • YouVersion"
author: "Richard Mannich"
dates:
  - created: "September 20, 2022"
  - modified: "November 15, 2022"
tags: [" RELIGIAO/Cristianismo/devocionais YouVersion/richard_mannich RELIGIAO/feh "]
aliases: ["Fé", "devocionais YouVersion"]
abstract: "A fé é a única maneira pela qual podemos agradar a Deus. Nos próximos três dias abordaremos maneira pelas quais colocamos nossa fé em prática."
link: "https://my.bible.com/pt/reading-plans/32144-usando-as-lentes-da-fe"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- USANDO AS LENTES DA FÉ ~ CRIADO EM 2022/09/20 ~ ATUALIZADO EM 2022/11/15 -->

# Usando as Lentes da Fé

<p>&nbsp;</p>

> [!info]+ Contexto
> A fé é a única maneira pela qual podemos agradar a Deus. Nos próximos três dias abordaremos maneira pelas quais colocamos nossa fé em prática. Praticamos a fé ao compartilhar as vitórias. Exercemos a fé para recebermos o que pedimos a Deus e desenvolvemos a fé com a ajuda da religião. Serão dias que você terá uma nova perspectiva [[sobre a fé]] e a presença dela no dia-a-dia do cristão!

<p>&nbsp;</p>

<span class="text-center" align="center">![figura: ao fundo, semitransparente, a imagem de um exame oftalmológico e em primeiro plano, a figura de um par de óculos e sobre ele a frase: “usando as lentes da fé”; no canto inferior esquerdo, o logotipo do Ministério Vale da Honra.](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/32144/720x405.jpg)</span>

<p>&nbsp;</p>

> [!info]- Sobre o Editor
> Gostaríamos de agradecer ao **Richard Mannich** por fornecer este plano. Para mais informações, visite: [ministeriovaledahonra.org](https://ministeriovaledahonra.org/).
> 
> [Mais informações…](https://ministeriovaledahonra.org/sobre/)

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 1 de 3: Fé para receber o que foi pedido

Esse é um momento crítico da vida de Eliseu. Ele caminhou ao lado de Elias, como seu aprendiz desde o momento em que foi recrutado. O que tinha acabado de acontecer foi uma separação. <!-- Ao mesmo tempo algo sobrenatural, pois Eliseu viu Elias sendo levado por uma carruagem de fogo para o céu. -->Ao mesmo tempo algo sobrenatural, pois Eliseu viu Elias sendo levado ao céu por um redemoinho.[^1] Se não bastasse isso, Eliseu havia feito um último pedido a Elias e a resposta que recebeu seria através de um sinal. O sinal iria acontecer de acordo com a fé de Eliseu e a [[Palavra de Deus para mim hoje — Ele tem uma resposta|resposta de Deus]].

[^1]: > [!bible] [[1-2 Reis|2 Reis]] 2:1 e 11
    > ¹ Quando o Senhor estava para tomar Elias ao céu num redemoinho, Elias partiu de Gilgal com Eliseu.
    > ¹¹ E, indo eles caminhando e conversando, eis que um carro de fogo, com cavalos de fogo, os separou um do outro; e Elias subiu ao céu num redemoinho.

Eliseu recebeu o sinal confirmando o seu pedido e no momento em que ele se encontra agora, a sua fé é, digamos assim, ativada. Ele coloca à prova a sua própria fé e a confirmação do sinal que recebera. Deus respondeu à sua pergunta operando o sobrenatural, confirmando o chamado de Eliseu. Não foi somente para Eliseu que a confirmação veio, como também a todos os profetas que estavam aguardando do outro lado da margem do rio, e viram o sinal maravilhoso que Deus operou por Eliseu, tal como havia feito logo antes com Elias. Esse sinal sela o início do ministério profético de Eliseu. Para constar, Eliseu fez um pedido audacioso a Elias. Como era algo fora do “alcance” de Elias, a resposta foi dada por meio de um sinal que seria confirmado ou não por Deus: Eliseu pediu o dobro da unção que Elias tinha. A ousadia do pedido foi atendida por Deus, mas a resposta positiva precisou ser também aceita pela fé por Eliseu.

<p>&nbsp;</p>

### Aplicação

Quantas vezes estou em uma situação semelhante a de Eliseu, na qual tenho a oportunidade de pedir qualquer coisa a Deus? Muitas vezes isso já aconteceu. Quantas vezes, porém, eu não recebi o que havia pedido porque simplesmente “abandonei” a fé para tomar posse do presente. Aquela fé que exerci para fazer o pedido eu deixei de lado quando o pedido me foi atendido. O que aconteceu? Tenho algumas possíveis respostas para isso: não me senti “digno” de ser atendido (como se Deus me atendesse de acordo com o mérito que tenho); talvez tenha me sentido intimidado pelo que as pessoas pensariam a respeito de mim depois disso; talvez o medo da responsabilidade que viria junto com o pedido atendido…

Lembro agora de um versículo que diz que Deus não nos deu um espírito de covardia, mas de poder, amor e equilíbrio.[^2] Com isso eu quero rejeitar todo tipo de intimidação. Seja a intimidação que vem dos meus pensamentos, das pessoas à minha em volta. Rejeito o travamento que vem por medo. Seguirei com a mesma ousadia, coragem e intrepidez que exerci ao fazer o meu pedido. Eu creio que Deus é poderoso para cumprir e também que o Espírito Santo me capacitará com discernimento e coragem para agir de acordo com a resposta ao meu pedido. Eu já recebi pela fé o que pedi e agora aguardo o tempo que levará para que chegue até mim! Toda a Glória é de Deus!

[^2]: > [!bible] [[Timóteo#2 Timóteo]] 1:7
    > Porque Deus não nos deu o espírito de covardia, mas de poder, de amor e de moderação.

<p>&nbsp;</p>

### Oração

Obrigado Senhor porque atendeste ao meu pedido de libertação, o milagre de me ver livre de todas as dívidas, de qualquer coisa que me amarra a alguém e que pode frear o ritmo do desenvolvimento do ministério que o Senhor confiou a mim e à minha família! Obrigado Deus, porque não precisaremos mais tomar emprestado, que poderemos contar com o recurso do mês, que poderemos ser mais generosos nas ofertas, que poderemos ajudar as pessoas que precisam. A Ti toda a honra! A Ti toda a Glória! Em nome de Jesus, amém!

<p>&nbsp;</p>

### Versículo Base: 2 Reis 2:14

> [!bible] [[1-2 Reis|2 Reis]] 2:14
> Então, pegando da capa de Elias, que dele caíra, feriu as águas e disse: Onde está o Senhor, o Deus de Elias? Quando feriu as águas, estas se dividiram de uma à outra banda, e Eliseu passou.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 2 de 3: Vou falar para todos do livramento que Deus nos deu!

Quem está falando isso são 4 leprosos que estavam saqueando o exército inimigo, que deixou tudo para trás porque fugiu de medo do exército de Israel. Para começar, eu me impressiono com a riqueza de detalhes que encontro em certos relatos na Bíblia. Não somente isso, mas me impressiono com o conteúdo desses relatos. Aqui são 4 leprosos que foram os primeiros a saquearem os despojos do exército inimigo. Esses 4 eram excluídos da sociedade, estavam desarmados e desprotegidos. Num último intento de continuarem vivos iriam se entregar ao exército inimigo. Se fossem mortos, não perderiam nada porque morreriam durante o ataque ou morreriam de fome, como o restante do povo. Deus permitiu que justamente esses excluídos fossem os primeiros a contemplarem e usufruírem do livramento! Eles tiveram a escolha de guardarem tudo para si, mas decidiram compartilhar com os demais. Justamente pelos excluídos, a mensagem de livramento e salvação foi entregue à cidade. Eles são tão improváveis que o rei ainda espera que espiões confirmem o que os mensageiros disseram. De quem os mensageiros tinham medo? Porque foi por causa disso que eles foram compartilhar a notícia. Para mim, a própria consciência os motivou. Eles sabiam o que era certo e o que era errado.

<p>&nbsp;</p>

### Aplicação

Tantas lições em um trecho tão sucinto das escrituras… o valor dos excluídos, a dúvida dos poderosos, o livramento em meio a uma guerra que parecia perdida, a provisão abundante de Deus em meio à escassez. Temos esse relato da ação de Deus à nossa disposição e custamos a acreditar tantas vezes que Ele pode nos livrar de nossos momentos difíceis também! Quantas foram as vezes que recebemos [[As promessas de Deus para nossa vida|promessas de Deus]] através de uma revelação durante a leitura bíblica, ou através de uma profecia vinda de um(a) servo(a) de Deus e custamos a acreditar ou mesmo ignoramos?

A mesma fé que usamos ao orar e clamar por socorro deve ser usada para tomar posse da promessa que é lançada a nós. E mais, quando alcançamos aquilo pelo qual clamamos, precisamos compartilhar com todos, para que o nome do Senhor seja glorificado! Jamais cair na tentação de que fomos atendidos porque somos merecedores disso! Somos atendidos em nossos clamores e petições segundo a fé que exercemos e por [[O que significa a minha graça te basta|graça de Deus]], somente assim!

<p>&nbsp;</p>

### Oração

Querido Pai, que está no céu e ao mesmo tempo caminha ao meu lado todo o tempo, obrigado por Se revelar a mim. Aumenta a minha fé! Quero, sem sombra de dúvida, receber aquilo que o Senhor já me deu em profecia, a resposta ao meu clamor, o livramento da batalha que está difícil. Muito obrigado pelo que o Senhor já me deu e pelo que já aconteceu no ambiente espiritual! Aguardo agora a concretização também no ambiente físico, em nome de Jesus, amém!

<p>&nbsp;</p>

### Versículo Base: 2 Reis 7:9

> [!bible] [[1-2 Reis|2 Reis]] 7:9
> Então disseram uns aos outros: Não fazemos bem; este dia é dia de boas novas, e nós nos calamos. Se esperarmos até a luz da manhã, algum castigo nos sobrevirá; vamos, pois, agora e o anunciemos à casa do rei.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 3 de 3: A religião é só um meio. O fim é Jesus!

Fazer de conta que é religioso e rejeitar o poder da religião é mais comum do que eu pensava. Andei observando as pessoas e também reavaliando minhas próprias atitudes. Quantas vezes participei de um ato religioso pensando que isso me tornaria apto a alguma outra coisa? Muitas vezes a ignorância, a falta de compromisso e a forma fútil de viver são mascaradas por essa religiosidade. Não cabe a nós julgarmos aos outros, mas a nós mesmos podemos e devemos para que não tenhamos uma visão maior ou menor de nós como somos. Precisamos nos julgar justamente.

A religião tem o poder de nos fortalecer na disciplina, na fé, mas não se limita a ela mesma. A religião é um meio e não o fim nela mesma.

<p>&nbsp;</p>

### Aplicação

Não cabe a mim, nem a ninguém julgar quem está ou não vivendo limitado à religião. Obviamente frutos desse estilo de vida aparecem, mas o que cabe a nós é não nos deixar envolver por essas pessoas. Muito pelo contrário, devemos, pelo nosso exemplo, mostrar que a vida pode e deveria ser vivida de maneira mais intensa através da religião, ao desenvolver a espiritualidade e nos aproximar de Deus. É para isso que serve a religião, para nada mais.

<p>&nbsp;</p>

### Oração

Querido Deus, meu Pai, não me deixe confundir o alvo que é ser como Jesus, do meio pelo qual posso alcançar isso, ao praticar a piedade por meio da religião. Obrigado porque o Senhor se revela e se deixa encontrar por todo aquele que Te busca de todo o coração! Em nome de Jesus, amém!

<p>&nbsp;</p>

### Versículo Base: 2 Timóteo 3:5

> [!bible] [[Timóteo#2 Timóteo]] 3:5 • VFL
> Elas farão de conta que são religiosas, mas rejeitarão o poder da religião. Afaste-se dessas pessoas!

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Usando as Lentes da Fé](https://my.bible.com/pt/reading-plans/32144-usando-as-lentes-da-fe)”_ | Richard Mannich • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- September 20, 2022

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocionais #YouVersion/richard_mannich #RELIGIAO/feh 

<p>&nbsp;</p>

