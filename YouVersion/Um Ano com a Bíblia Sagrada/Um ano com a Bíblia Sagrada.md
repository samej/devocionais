---
title: "Um ano com a Bíblia Sagrada | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "SBB - Sociedade Bíblica do Brasil"
create: "January 2, 2023"
update: "January 2, 2023"
tags: ["  "]
aliases: ["", ""]
abstract: "Precisamos de um tempo diário para reflexão e contemplação na Palavra de Deus. Com este plano você entenderá melhor os fatos da Bíblia. Em um ano você vai ler passagens-chave das Escrituras, que vão de Gênesis a Apocalipse. Que tal passar um ano com a Bíblia? Todo dia você fará uma leitura breve e poderá refletir sobre o seu significado para a sua vida."
link: "https://my.bible.com/pt/reading-plans/10808-um-ano-com-a-biblia-sagrada"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM ANO COM A BÍBLIA SAGRADA ~ CRIADO EM 2023/01/02 ~ ATUALIZADO EM 2023/01/02 -->

## Um ano com a Bíblia Sagrada

<p>&nbsp;</p>

<span class="text-center" align="center">![Capa do Plano Devocional “Um Ano com a Bíblia Sagrada” da SBB no app YouVersion](https://imageproxy.youversionapi.com/https://s3.amazonaws.com/yvplans/10808/720x405.jpg)</span>

<p>&nbsp;</p>

366 Dias

Precisamos de um tempo diário para reflexão e contemplação na Palavra de Deus. Com este plano você entenderá melhor os fatos da Bíblia. Em um ano você vai ler passagens-chave das Escrituras, que vão de Gênesis a Apocalipse. Que tal passar um ano com a Bíblia? Todo dia você fará uma leitura breve e poderá refletir sobre o seu significado para a sua vida.

### Editor

Gostaríamos de agradecer ao _Bible Society Of Brazil_ (SBB — Sociedade Bíblica do Brasil) por fornecer esse plano. Para mais informações, visite: [http://www.sbb.org.br/apoieacausadabiblia/](http://www.sbb.org.br/apoieacausadabiblia/)

<!-- [Sobre o Editor](http://www.sbb.org.br/apoieacausadabiblia/) -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

#### Dias concluídos:

- [x] **Dia 1:** 2023/01/01 - Domingo  
      - [Devocional](https://my.bible.com/users/SamejSpenser884/reading-plans/10808-um-ano-com-a-biblia-sagrada/subscription/872878012/day/1/segment/0)
      - [Gênesis 1](https://my.bible.com/users/SamejSpenser884/reading-plans/10808-um-ano-com-a-biblia-sagrada/subscription/872878012/day/1/segment/1)
      - [Gênesis 2:1-4](https://my.bible.com/users/SamejSpenser884/reading-plans/10808-um-ano-com-a-biblia-sagrada/subscription/872878012/day/1/segment/2)

- [x] **Dia 2:** 2023/01/02 - Segunda-feira  
      - [Gênesis 2:4-25](https://my.bible.com/users/SamejSpenser884/reading-plans/10808-um-ano-com-a-biblia-sagrada/subscription/872878012/day/2/segment/0)

- [ ] **Dia 3:** 2023/01/03 - Terça-feira  
      - [Gênesis 3](https://my.bible.com/users/SamejSpenser884/reading-plans/10808-um-ano-com-a-biblia-sagrada/subscription/872878012/day/3/segment/0)


<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um ano com a Bíblia Sagrada](https://my.bible.com/pt/reading-plans/10808-um-ano-com-a-biblia-sagrada)”_ | por SBB — Sociedade Bíblica do Brasil • Plano de Leitura Devocional — Bíblia da YouVersion
- January 2, 2023

<p>&nbsp;</p>

###### tags:

# #

<p>&nbsp;</p>

