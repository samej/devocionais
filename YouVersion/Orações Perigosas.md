---
title: "Orações Perigosas | YouVersion"
author: "Craig Groeschel"
dates:
  - created: "September 19, 2023"
  - modified: "September 20, 2023"
tags: [" craig_groeschel devocional YouVersion RELIGIAO/Cristianismo/oracao "]
aliases: ["orações seguras", "orações egoístas", "orações cristãs"]
abstract: "Devocional de 7 dias sobre orações."
link: "https://my.bible.com/pt/reading-plans/18063-dangerous-prayers"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="/home/samej/Sync/Obsidian/GERAIS/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://gitlab.com/samej/samej/-/blob/fa32cbc9864c98d87395841d4df505e7ed5929ef/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/7acsnogog4njf2o/stylesheet-geral.css" />

<!-- LINK EXTERNO PARA O CSS NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="/storage/emulated/0/Obsidian/.obsidian/snippets/stylesheet-geral.css" />

<!-- ORAÇÕES PERIGOSAS ~ CRIADO EM 2023/09/19 ~ ATUALIZADO EM 2023/09/20 -->

# Orações Perigosas

<p>&nbsp;</p>

<span class="text-center" align="center">![Orações Perigosas](https://telegra.ph/file/115cb59df5fd00ac40e34.jpg)</span>

<p>&nbsp;</p>

Você está cansado de ser cauteloso demais com a sua fé? Você está pronto para enfrentar seus medos, desenvolver sua fé e liberar o seu potencial? Este plano bíblico de 7 dias do livro “Orações Perigosas”, do pastor Craig Groeschel da Life\.Church, lhe desafia a orar perigosamente — porque seguir Jesus nunca teve nada a ver com segurança.

<p>&nbsp;</p>

### Dia 1: Por que suas Orações Precisam ser Perigosas

Como muitas pessoas lutei para orar efetiva e consistentemente por anos. Mesmo com boas intenções, com frequência ficava distraído ou até entediado quando orava. Quando eu era um jovem pastor, um amigo me ajudou a perceber que era hora de mudar. Por muito tempo eu havia tolerado orações sem fé, mas sabia que Deus queria mais para mim, e eu queria conhecê-Lo mais intimamente.

— Ei, Craig, você acredita que Deus ainda faz milagres?

— É claro, eu disse.

— Que bom — porque as suas orações são muito fracas.

Tentei rir com ele, mas a piada do meu amigo doeu — principalmente porque ele estava certo.

Sem nada para dizer, não ofereci defesa da minha parte enquanto processava a verdade de sua observação. Eu não conseguia negar que ele falou em voz alta um segredo que eu já sabia mas não queria admitir: minhas orações eram patéticas.

Esse plano é para qualquer um que se sente preso em uma rotina de oração, fazendo orações repetitivas, previsíveis e seguras.

Servimos a um Deus que pode fazer mais do que possamos pedir ou imaginar. Então é hora de parar de ser cauteloso. Não fomos criados para uma vida de conforto. Somos apaixonados e poderosos, encarregados para mudar o mundo de formas radicais! Acredito que este plano lhe encorajará a quebrar os limites e lhe inspirará a orar perigosamente e a viver corajosamente.

Quanto mais eu estudava a Bíblia, mais me maravilhava com a variedade de orações feitas pelo povo de Deus. Eles não apenas oravam sobre coisas que eram incrivelmente pessoais — por exemplo, conceber um filho ([[Samuel#1 Samuel]] 1:27) — mas, também, eram orações frequentemente práticas, por comida e provisão ([[Mateus]] 6:11) e livramento dos inimigos ([[Salmos|Salmo]] 59:1-2). Às vezes eles pareciam sussurrar gentilmente para um Deus amoroso. Outras vezes, eles berravam para Ele em agonia e frustração.

As orações deles eram honestas. Desesperadas. Ardentes. Vigorosas. Verdadeiras. E lá estava eu orando para que Deus me mantivesse seguro e abençoasse meu hambúrguer com fritas.

Meu amigo estava certo.

As minhas orações eram fracas e bobas.

Talvez você possa se identificar. Não é que você não acredite na oração. Você acredita. Mas está preso em um ciclo. Você ora sobre os mesmos conflitos, pelos mesmos pedidos. Sempre da mesma forma, na mesma hora. Isto é, se você ao menos tenta orar. Você provavelmente sabe que deveria orar mais. E com mais paixão. Mais fé. Você quer falar com Deus e ouvi-Lo, compartilhar uma conversa íntima com Ele assim como você faria com o seu cônjuge ou um melhor amigo. Você realmente quer, mas não tem certeza como. Então as suas orações ficam na zona de segurança.

Rasas. Chatas. Previsíveis. Sem graça. Sem novidades. Tediosas.

O alerta do meu amigo me convenceu de que era hora para uma mudança na minha vida de oração. Por muito tempo tolerei orações sem brilho, sem fé, e na maioria, vazias. Eu sabia que Deus queria mais para mim, e eu queria conhecê-Lo intimamente, apesar da minha hesitação sobre o que isso exigiria de mim.

Quando buscamos nos comunicar com Deus em uma oração verdadeira, vulnerável e íntima, Ele não vai nos envolver em uma bolha de segurança espiritual. Em vez disso, Ele estoura a nossa bolha do “o que ganho com isso”, e nos convida a confiar nEle quando não sabemos o que Ele fará a seguir. Nos sentimos abençoados em alguns dias. Em outros, enfrentamos desafios, oposições e perseguição. Mas todo momento de oração perigosa será preenchido com a Sua presença.

Você está pronto para mais? Você está cansado de ser cauteloso? Você está pronto para fazer orações audaciosas, cheias de fé, que honram a Deus, que mudam a vida e transformam o mundo?

Se sim, então este plano é para você.

Mas fique avisado. Haverá colisões. Quando começar a fazer orações como “examina-me, quebranta-me, envia-me” você pode experimentar os vales, ataques, testes, dor, sofrimento, desencorajamento, e até mesmo mágoa. Mas também haverá a alegria da fé, a maravilha dos milagres, o alívio da entrega, e o prazer de agradar a Deus.

É hora de parar de orar com cautela.

É hora de começar a conversar, conversar de verdade — e realmente ouvir — a Deus.

É hora de fazer orações perigosas.

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Samuel#1 Samuel]] 1:27 • VFL
> Orei por este filho, e o SENHOR respondeu à minha oração, dando-me este filho.

> [!bible]- [[Mateus]] 6:9-13 • VFL
> ⁹ Quando vocês orarem, orem assim: “Pai nosso, que está no céu, que o seu nome sempre permaneça santo.
>
> ¹⁰ Que o seu reino venha a nós. Que a sua vontade seja feita aqui na terra como no céu.
>
> ¹¹ Dê-nos hoje o alimento que precisamos.
>
> ¹² Perdoe os nossos pecados assim como nós perdoamos aos pecam contra nós.
>
> ¹³ Não nos deixe cair em tentação, mas livre-nos do maligno”.

> [!bible]- [[Salmos|Salmo]] 59:1-2 • VFL
> ¹ Ó meu Deus, salve-me dos meus inimigos; proteja-me daqueles que me atacam.
>
> ² Livre-me dos maus, salve-me dos assassinos.

> [!bible]- [[Salmos|Salmo]] 34:17 • VFL
> Os justos oram ao SENHOR e serão ouvidos. Ele os salvará de todos os perigos.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Dia 2: Seja Feita a Tua Vontade

Em vez de orações longas, altas e extravagantes, as orações que movem Deus são simples, autênticas e sinceras. Mas simples não é o mesmo que seguro. E essa é a razão pela qual sou compelido a escrever isso. O maior erro que cometi na minha vida de oração, a razão pela qual minhas orações eram tão fracas, é porque eu orava de uma forma protegida demais. Eu estava numa zona de conforto com Deus. Eu não estava quente, nem frio. As minhas orações eram mornas. Mas orações seguras e mornas não nos aproximam de Deus nem nos ajudam a revelar o Seu amor para o mundo.

As orações são inerentemente perigosas. Percebi esse aspecto da oração enquanto lia sobre Jesus falando com o Seu Pai no jardim do Getsêmani, um pouco antes dEle entregar a Sua vida na cruz. Sabendo o que estava por vir, Jesus perguntou a Deus se havia alguma outra maneira. Então Jesus, (não um discípulo ou uma pessoa normal na Bíblia, mas _J-E-S-U-S_, **o Filho de Deus**), fez uma oração de submissão vulnerável e perigosa: _“Porém que não seja feito o que eu quero, mas o que tu queres”_ ([[Lucas]] 22:42).

Jesus nunca nos pediu para fazer algo que Ele mesmo não faria. Ele nos chama para uma vida de fé, não uma vida de conforto. Em vez de irmos a Ele para um estilo de vida mais seguro, mais fácil e livre de estresse, o Filho de Deus nos desafia a arriscarmos amar aos outros mais do que a nós mesmos. Em vez de ceder aos nossos desejos cotidianos, Ele nos chama a negá-los por algo eterno. Em vez de viver pelo que queremos, Ele nos diz para pegarmos a nossa cruz diariamente e seguir o Seu exemplo.

Me preocupo que, para muitas pessoas, a oração é como comprar um bilhete de loteria, uma chance para uma vida aqui na Terra que seja livre de problemas, estresse e dor. Para outras, a oração é meramente uma rotina sentimental, como recitar a letra da sua música favorita ou uma canção de ninar da sua infância. E ainda, outras oram porque se sentem muito mais culpadas se não o fazem.

Mas nenhuma dessas orações refletem a vida que Jesus veio para nos dar. Em vez disso, Ele nos chamou para deixarmos tudo e segui-Lo.

Jesus não somente desafiou os outros a deixarem suas próprias vontades para trás. **Ele mesmo viveu uma fé perigosa.** Ele tocou os leprosos. Mostrou graça às prostitutas. E se posicionou corajosamente frente ao perigo. E então, Ele nos disse que podemos fazer o que Ele fez (e mais).

E é por isso que não podemos nos acomodar em simplesmente pedir a Deus para abençoar a nossa comida ou “ficar conosco hoje”.

A Bíblia nos diz que podemos nos aproximar _“com toda confiança do trono da graça”_ ([[Hebreus]] 4:16a • NVT). Não temos que nos aproximar com timidez ou nos sentirmos estranhos — podemos ir a Ele com confiança, certeza e ousadia. Quando oramos dessa forma, então _“receberemos misericórdia e encontraremos graça para nos ajudar quando for preciso”_ ([[Hebreus]] 4:16b • NVT).

As suas orações importam.

Como você ora importa.

O que você ora importa.

As. suas. Orações. Movem. a. Deus.

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Mateus]] 6:5-8 • VFL
> ⁵ — E quando vocês orarem, não façam como os hipócritas, que gostam de orar de pé nas sinagogas e nas esquinas para poderem ser vistos pelo povo. Digo a verdade a vocês: Eles já receberam a recompensa deles.
>
> ⁶ Você, entretanto, quando orar, vá para o quarto, feche a porta e ore ao seu Pai, que não pode ser visto. E seu Pai, que vê o que você faz em segredo, lhe dará a recompensa.
>
> ⁷ — Quando vocês orarem, não repitam palavras que não significam nada, como os que não creem no nosso Deus; pois eles pensam que por causa das suas muitas palavras Deus os ouvirá.
>
> ⁸ Portanto, não sejam como eles, pois o Pai de vocês sabe o que vocês precisam antes mesmo de vocês pedirem.

> [!bible]- [[Apocalipse]] 3:15-16 • VFL
> ¹⁵ “Eu sei o que vocês fazem e sei que vocês não são nem frios nem quentes. Quem dera vocês fossem ou frios ou quentes!
>
> ¹⁶ Assim, porque vocês são mornos e não são nem quentes nem frios, estou a ponto de vomitá-los da minha boca.

> [!bible]- [[Lucas]] 22:42 • VFL
> — Pai, o senhor pode afastar de mim este cálice de sofrimento, se quiser. Mas, que seja feita a sua vontade, e não a minha.

> [!bible]- [[Hebreus]] 4:14-16 • VFL
> ¹⁴ Nós temos a Jesus, o Filho de Deus, como o nosso Sumo Sacerdote, o qual entrou nos céus. Por isso devemos conservar firme a fé que temos.
>
> ¹⁵ O nosso Sumo Sacerdote não é alguém que não possa se compadecer de nossas fraquezas. Ao contrário, ele foi tentado de todas as maneiras, assim como nós também somos tentados, mas nunca pecou.
>
> ¹⁶ Por isso devemos nos aproximar com confiança do trono do nosso Pai, que é cheio de amor, a fim de recebermos misericórdia e encontrarmos graça para nos ajudar na hora em que precisarmos.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Dia 3: Examina-me

Você ousaria orar de uma maneira que nunca orou antes?

Com todo o seu coração, alma, mente, e toda a extensão do seu ser? O que aconteceria na sua vida e na daqueles que estão à sua volta se você começasse a fazer orações perigosas?

Você ousaria descobrir?

O [[Quem foi Saul|rei Saul]] acusou falsamente a [[Quem foi Davi|Davi]] de traição e enviou todas as suas tropas atrás dele, em várias tentativas de tirar a sua vida. Com todo o seu coração, Davi queria agradar a Deus. Ele lutou contra a sua raiva a fim de proteger e mostrar honra ao rei. E, sabendo que suas intenções não eram sempre perfeitas, Davi entregou seu coração diante de Deus e fez uma das orações mais vulneráveis, transparentes e perigosas que você ouvirá. Buscando honrar a Deus com todos os aspectos de seu ser, Davi orou:

> [!bible] [[Salmos|Salmo]] 139:23-24
> Ó Deus, examina-me e conhece o meu coração! Prova-me e conhece os meus pensamentos. Vê se há em mim algum pecado e guia-me pelo caminho eterno.

Esta não é apenas uma oração difícil, mas é ainda mais difícil de ser vivida e aplicada. Pois se você tiver a coragem de fazer essa oração, deverá exercitar a coragem de viver o que Deus lhe mostrar em resposta. Então não faça essa oração se não estiver falando sério.

Esteja ciente que essa oração tem o potencial de trazer à luz a sua culpa. De corrigi-lo. De redirecionar a sua vida. De mudar a maneira que você vê a si mesmo. De mudar como os outros veem você.

Talvez você ainda imagine que isso não é grande coisa. Talvez esteja se perguntando por que deveria pedir para Deus examinar o seu coração quando Ele já sabe tudo que há dentro de você. Você sabe o que está lá. Ele sabe o que está lá. Então por que perguntar algo tão óbvio?

É aqui que fica complicado. Na superfície, parece que conhecemos nosso próprio coração. Certo? _Conheço meus motivos. Eu sei o que é mais importante; sei porque faço o que faço._ Além disso, você pode dizer a si mesmo: _Eu tenho um bom coração. Não estou tentando machucar as pessoas. Quero fazer o que é certo. Meu coração é bom. Estou orando, não estou?_

Mas a [[O valor da Palavra de Deus em nossa vida|Palavra de Deus]] revela exatamente o oposto. Pode ser um choque quando você ouve isso pela primeira vez, mas [[Quem foi Jeremias|Jeremias]] nos diz umas verdades bem diretas. Jeremias era filho de um sacerdote levítico nascido por volta de 650 a.C. Durante o reinado do [[Quem foi Josias|rei Josias]], Deus levantou esse jovem profeta para levar a palavra de Deus para Israel e as nações. Jeremias diz sem rodeios que você — e eu e todos nós — não tem um bom coração. Na verdade, o seu coração não apenas não é bom, mas o seu coração é mau e pecaminoso de todas as formas. O profeta disse:

> [!bible] [[Jeremias]] 17:9 • ARA
> Enganoso é o coração, mais do que todas as coisas, e desesperadamente corrupto; quem o conhecerá?

Sem Cristo, o seu coração é enganoso. Quanto mais próximo ficamos de Jesus, mais temos de enfrentar as nossas imperfeições. Orgulho. Egoísmo. Luxúria. Vícios. Um espírito crítico.

Fazer essa oração perigosa pode abrir um canal de comunicação com Deus. Em vez de simplesmente pedir a Deus para fazer algo _para_ você, peça a Ele para revelar algo _em_ você. Este momento de verdade com Deus pode não te mudar instantaneamente, mas te ajudará a reconhecer a sua necessidade espiritual e redirecionar a sua vida.

É por isso que essa oração de [[Quem foi Davi|Davi]] é loucamente perigosa.

_“Examina o meu coração, Senhor.”_

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Salmos|Salmo]] 139:23-24 • VFL
> ²³ Examine-me, ó Deus, e conheça o meu coração; ponha-me à prova e conheça os meus pensamentos.
>
> ²⁴ Veja se ando atrás dos ídolos, e guie-me no caminho eterno.

> [!bible]- [[Jeremias]] 17:9 • VFL
> “Não há nada mais enganoso do que o coração; não tem esperança. Quem o conhece?

> [!bible]- [[Samuel#1 Samuel]] 24:4-8 • VFL
> ⁴ Os homens disseram a Davi: — Este é o dia ao qual o SENHOR se referia quando lhe disse: “Entregarei a você o seu inimigo para que faça com ele o que melhor desejar”. Então Davi se aproximou de Saul sem fazer barulho e cortou a borda do manto de Saul sem que ele se desse conta.
>
> ⁵ Mas Davi ficou com peso na consciência pelo que havia feito,
>
> ⁶ e disse aos seus homens: — Que o SENHOR me livre de fazer alguma coisa ao rei! Saul é quem o SENHOR escolheu como rei. Eu não devo fazer nada contra ele porque ele foi escolhido pelo SENHOR.
>
> ⁷ Davi disse isto a seus homens para contê-los e não permitiu que fizessem nenhum mal a Saul. Saul saiu da caverna e foi embora.
>
> ⁸ Davi o seguiu, gritando: — Sua Majestade! Saul olhou para trás e viu Davi inclinado, rosto em terra.

> [!bible]- [[1-3 João|1 João]] 1:9 • VFL
> Se confessarmos os nossos pecados, Deus nos perdoará e nos purificará de toda injustiça, pois ele é fiel e justo.

> [!bible]- [[Romanos]] 3:23-24 • VFL
> ²³ pois todas \[as pessoas\] pecaram e portanto estão afastadas da glória de Deus.
>
> ²⁴ Mas elas são declaradas justas de um modo gratuito, pela graça de Deus. Isto acontece porque, por meio de Jesus Cristo, elas são libertadas do pecado.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Dia 4: Revele os Meus Medos

O que te deixa ansioso? Nervoso? Inquieto? Com medo?

Não estou falando sobre medos externos e normais, como cobras, aranhas, ou medo de voar. Mas sobre o que te deixa acordado à noite, aquelas coisas que rodeiam em sua mente e se recusam a ficar quietas. Coisas como perder o emprego. Não se casar. Ou ficar preso em um casamento ruim. Ficar doente. Ver sua poupança indo embora pra poder sobreviver.

Não sabemos exatamente quais medos estavam passando pela mente de [[Quem foi Davi|Davi]], mas claramente ele estava angustiado com sua segurança e talvez com seu futuro. Porque depois de pedir a Deus para examinar o seu coração, Davi orou: “conhece os meus pensamentos” ([[Salmos|Salmo]] 139:23). Ele queria compartilhar os seus piores medos com Deus. Enfrentá-los e nomeá-los. Confiar que Deus era maior do que qualquer medo que Davi poderia sonhar.

> [!question] Você está disposto a fazer uma oração dessas?
> Senhor, revela o que mantém a minha mente refém. Me mostre do quê mais tenho medo. Vá em frente, me ajude a enfrentar o que me aterroriza.

#### O que tememos importa

Anos atrás, tive uma revelação sobre esse assunto que me tocou de uma maneira muito pessoal. Deus me mostrou que o que eu mais temia revelava onde eu confiava menos em Deus. Depois do nascimento de nossa terceira filha, Anna, Amy começou a ter problemas de saúde. No começo, pensamos que era apenas fadiga; mas quando metade de seu corpo ficou dormente, tememos que fosse algo muito pior. Nenhum médico nos dava respostas. À medida que seus sintomas começaram a piorar, a minha confiança em Deus começou a enfraquecer.

Esse medo levou a outros, e à noite, meus pensamentos viravam uma bola de neve fora de controle. E se Amy estiver perigosamente doente? E se eu perdê-la? Não vou conseguir criar nossos filhos sem ela. Não conseguiria continuar liderando a igreja. Não quereria continuar. E então percebi. As coisas que me mantinham acordado à noite eram as coisas às quais eu não confiava que Deus cuidaria. Eu estava me segurando a elas, ruminando, tentando achar uma maneira de ter controle sobre elas, de resolver todos os meus problemas, planejar para cada circunstância. Felizmente, pela graça de Deus, Amy melhorou e gradativamente voltou às suas forças, mas seus desafios evidenciaram uma das minhas piores fraquezas. O medo havia me consumido.

> [!question] E você? A quais áreas você está se apegando e permitindo que te aterrorizem? Quais medos você está escondendo de Deus?

Pense nisso. Se você está agarrado ao medo sobre o futuro do seu casamento, isso é um indício de que você não confia completamente em Deus com o seu casamento. Se você está sobrecarregado de preocupação sobre como pagará suas contas, isso pode revelar que você não tem confiado em Deus para ser o seu provedor. Se está paralisado de medo com a segurança de seus filhos, será que você não tem confiado em Deus para mantê-los em segurança?

À medida que Deus revela os seus medos, Ele também desenvolverá sua fé. Você precisa dEle. Você precisa da Sua presença, do Seu poder. [[O que é o Espírito Santo na Bíblia|Do Espírito]] guiando você. Da [[O valor da Palavra de Deus em nossa vida|Sua Palavra]] te fortalecendo.

O que você mais teme mostra onde você precisa crescer com Deus. Do que você tem medo? Quais são os seus pensamentos ansiosos?

O que Deus está te mostrando?

Em qual área você precisa crescer em sua fé?

Confie nEle.

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Salmos|Salmo]] 139:23-24 • VFL
> ²³ Examine-me, ó Deus, e conheça o meu coração; ponha-me à prova e conheça os meus pensamentos.
>
> ²⁴ Veja se ando atrás dos ídolos, e guie-me no caminho eterno.

> [!bible]- [[Filipenses]] 4:6-7 • VFL
> ⁶ Não vivam ansiosos por causa de nada. Ao contrário, orem a Deus, em qualquer circunstância e peçam a ele o que vocês precisam. E façam isso sempre com ações de graça.
> 
> ⁷ E a paz de Deus, que está além da compreensão humana, guardará os corações e as mentes de vocês em Cristo Jesus.

> [!bible]- [[Salmos|Salmo]] 34:4 • VFL
> Eu pedi ajuda ao SENHOR e ele me respondeu, ele me salvou de todos os meus medos.

> [!bible]- [[Pedro#1 Pedro]] 5:7 • VFL
> Lancem sobre ele toda a sua ansiedade, pois ele cuida de vocês.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Dia 5: Quebranta-me

Está tudo bem em orar por segurança e bênçãos, mas e se você quiser mais? E se você desejar poder do [[O que é o Espírito Santo na Bíblia|Espírito Santo]], força do céu, uma fé inabalável, intimidade genuína com o seu Pai?

Em vez de apenas pedir a Deus para lhe manter em segurança, lhe dar mais, e proteger a sua vida, você pode ter que pedir a Deus que lhe quebrante.

Quando penso sobre fazer essa oração: “Senhor, quebranta-me”, penso sobre a experiência que Amy e eu tivemos certa vez em nosso pequeno grupo. Em uma quarta-feira tempestuosa e fria de Janeiro, sentamos em uma sala quentinha e aconchegante com sete ou oito casais falando exatamente sobre esta oração perigosa.

Concordamos que todos _queríamos_ fazer essa oração — e fazê-la de coração — mas não podíamos negar o medo que tínhamos das consequências. A primeira mulher que falou levou à sério a possibilidade, mas reconheceu sua dificuldade. Uma esposa amorosa e mãe de quatro filhos, ela seguia a Jesus fielmente desde seu segundo ano do colegial. Ela servia no ministério de crianças na igreja, era dizimista fiel, ajudava crianças em abrigos, ia semanalmente ao estudo bíblico, e frequentemente se voluntariava para orar em voz alta nos grupos.

Mas quando confrontada com a opção de pedir a Deus para quebrantá-la, ela se recusou. “Desculpe, mas tenho que ser honesta”, ela disse. “Não quero pedir a Deus para me quebrantar. Tenho medo do que irá acontecer. Sou mãe de quatro filhos. Eu os amo demais. Pedir a Deus para me quebrantar é simplesmente assustador demais para mim. E se eu ficar doente ou deprimida ou for afastada da minha família?”.

A maioria das pessoas no pequeno grupo acenou concordando.

Mas a minha pergunta na ocasião permanece a mesma para todos nós hoje:

> [!question] O que estamos perdendo ao nos apegar ao nosso conforto?

O que estamos perdendo por sermos tão comprometidos em evitar dor e desconforto?

Jesus disse:

> [!bible] [[Mateus]] 16:25
> Pois quem põe os seus próprios interesses em primeiro lugar nunca terá a vida verdadeira; mas quem esquece a si mesmo por minha causa terá a vida verdadeira.

Jesus não está nos convidando para uma vida de conforto e facilidade, mas a uma vida de rendição e sacrifício. Nosso maior desejo não deveria ser para que a nossa vontade seja feita, mas para que a vontade dEle seja feita. E Jesus está nos convidando para morrermos para as nossas próprias vidas, para que possamos viver cada momento, cada dia — para Ele. E a deixarmos as nossas salas aconchegantes e orações seguras para descobrirmos o que significa sermos quebrantados pelo bem dos outros.

Quando somos cautelosos, arriscamos perder algo muito mais precioso do que nossa segurança e conforto. Não percebemos as bênçãos que podem ser encontradas do outro lado do quebrantamento de Deus.

[[Quem foi Lucas|Lucas]] disse que Jesus:

> [!bible] [[Lucas]] 22:19
> (…) pegou o pão e deu graças a Deus. Em seguida partiu o pão e o deu aos apóstolos, dizendo: — Isto é o meu corpo que é entregue em favor de vocês. Façam isto em memória de mim.

Quase em toda a Bíblia os estudiosos concordam que a instrução de Jesus que diz “façam isto” concede aos cristão uma maneira de lembrar, honrar, e celebrar a Sua morte e ressurreição. Mas alguns acreditam que a frase de Jesus “façam isto” também se refere a como devemos viver. E se Jesus não estava falando somente sobre um ritual que fazemos de vez em quando na igreja? E se Ele também estava nos convidando a sermos quebrados, quebrantados e derramados diariamente? E se tivéssemos a coragem, a audácia, a fé para orar: “Deus, quebranta-me”?

Não nos lembramos de Jesus apenas durante a Santa Ceia na igreja. Nos lembramos dEle na maneira pela qual vivemos nossas vidas diariamente. Porque o corpo de Jesus foi partido, porque o Seu sangue foi derramado por nós, também devemos viver diariamente por Ele, quebrantados, partidos e derramados.

Isso pode não soar muito atrativo no começo. Quem quer ser “partido”, “quebrantado” e “derramado”? Isso soa doloroso na melhor das hipóteses, e miserável na pior delas. Mas é no doar de nossas vidas que encontramos a verdadeira alegria. Em vez de perseguir a nossa vontade, nos entregamos à dEle. Em vez de tentar preencher nossas vidas com tudo que desejamos, esvaziamos nossas vidas para fazer a diferença na vida dos outros.

O verdadeiro quebrantamento perante Deus não é algo que se faz apenas uma vez na vida; é uma decisão diária. [[Paulo de Tarso|Paulo]] disse:

> [!bible] [[Coríntios#1 Coríntios]] 15:31
> Eu enfrento a morte todos os dias.

O que isso significa? Todos os dias, ele escolheu crucificar os seus próprios desejos para que pudesse viver inteiramente para os desejos de Deus. Se você tem coragem para fazer essa oração, se prepare. Se prepare para conhecer a Deus e ser conhecido por Ele de uma maneira que jamais experimentou antes.

Você pode ser cauteloso. Mas minha intuição diz que você quer mais do que isso. Eu escolho diferente. Sou uma pessoa cheia de fé, disposto a arriscar tudo. Nunca insultarei a Deus pensando pequeno ou vivendo de maneira segura. Se há bênçãos do outro lado do quebrantamento, então quebranta-me.

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Mateus]] 16:24-25 • VFL
> ²⁴ E Jesus, então, disse aos seus discípulos: — Se alguém quiser vir comigo, tem que negar a si mesmo, pegar a sua cruz e me seguir.
> 
> ²⁵ Digo isto pois todo aquele que quiser salvar a sua vida a todo custo, irá perdê-la; e todo aquele que perder a sua vida por minha causa, irá salvá-la.

> [!bible]- [[Mateus]] 26:26 • VFL
> Enquanto comiam, Jesus pegou o pão e deu graças a Deus por ele. Depois, partindo-o, o deu a seus discípulos, dizendo: — Peguem e comam; isto é o meu corpo.

> [!bible]- [[Marcos]] 14:3-9 • VFL
> ³ Jesus estava na cidade de Betânia, à mesa na casa de Simão, o leproso, quando chegou uma mulher. Ela entrou com um frasco de alabastro cheio de um perfume muito caro feito de nardo puro. Quebrou o frasco e derramou o perfume sobre a cabeça de Jesus.
> 
> ⁴ Algumas das pessoas que estavam presentes ficaram indignadas e diziam umas para as outras: — Que desperdício! Por que ela fez isso?
> 
> ⁵ Esse perfume poderia ter sido vendido por mais de 300 moedas de prata e o dinheiro distribuído entre os pobres! E começaram a criticá-la severamente.
> 
> ⁶ Mas Jesus lhes disse: — Deixem-na em paz! Por que vocês a estão incomodando? Ela me fez uma coisa boa!
> 
> ⁷ Os pobres estarão sempre com vocês e poderão ajudá-los quando quiserem. Eu, no entanto, não estarei sempre com vocês.
> 
> ⁸ Ela fez o que pôde; derramou perfume sobre o meu corpo antes do tempo e assim o preparou para o enterro.
> 
> ⁹ Digo a verdade a vocês: Em todos os lugares do mundo onde as Boas Novas forem proclamadas, o que ela acabou de fazer será contado em memória dela.

> [!bible]- [[Lucas]] 22:14-19 • VFL
> ¹⁴ Quando chegou a hora, Jesus tomou seu lugar à mesa com os apóstolos e
> 
> ¹⁵ lhes disse: — Eu tenho desejado muito comer este jantar da Páscoa junto com vocês, antes do meu sofrimento.
> 
> ¹⁶ Pois eu lhes digo que nunca mais o comerei até que ele receba o significado completo no reino de Deus.
> 
> ¹⁷ Então, pegando o cálice, Jesus agradeceu a Deus e disse: — Peguem isto e dividam entre vocês,
> 
> ¹⁸ pois eu lhes digo: Nunca mais beberei vinho até que chegue o reino de Deus.
> 
> ¹⁹ E, pegando o pão, agradeceu a Deus, o partiu em pedaços e os deu a seus discípulos, dizendo: — Isto é o meu corpo, que é dado por vocês. Façam isto para se lembrar de mim.

> [!bible]- [[Salmos|Salmo]] 34:18 • VFL
> O SENHOR está pronto para ajudar os que sofrem, ele salva os que perderam a esperança.

> [!bible]- [[Salmos|Salmo]] 51:17 • VFL
> Os sacrifícios que agradam a Deus são um espírito de humildade; um coração humilde e arrependido, nunca rejeitará, ó Deus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Dia 6: Envia-me

Como pastor há muitos anos, já vi, em primeira mão, os pedidos de oração mais íntimos de milhares de pessoas. Toda semana, centenas de necessidades inundam a nossa igreja, desde cartões de oração preenchidos em nossos cultos a ligações recebidas ao longo da semana, ou pedidos online através das mídias sociais ou do nosso aplicativo. Então, você não ficará surpreso ao saber que a frase mais comum que escuto toda semana é uma que tenho prazer em executar: _“Pastor, poderia por favor orar por (…)?”_.

Considero um privilégio, uma honra, e uma alegre responsabilidade parar e levar uma necessidade perante o trono de Deus, pedindo a Ele para ter misericórdia, para mover, guiar, prover, agir, fazer um milagre para o povo que conheço e amo. Toda semana alguém pede que Deus cure seu ente querido do câncer, ajude um vizinho a encontrar um emprego ou restaure um casamento machucado. Estudantes pedem para entrar na faculdade que desejam, para ajudar a pagar pela faculdade ou lidar com a dor do divórcio de seus pais. Algumas pessoas oram por um cônjuge. Outras pedem ajuda para perdoar alguém que as magoou.

Apesar de os pedidos variarem, as pessoas estão pedindo para Deus fazer algo por elas ou por alguém que amam. Deus, me ajude. Deus, ajude alguém que amo. Senhor, eu preciso… Pai, o Senhor poderia…

Deus, faça algo para _mim_.

Por favor, entenda… com certeza devemos orar dessa maneira. Devemos sempre convidar a presença de Deus, o poder de Deus, a paz de Deus para intervir em nossas vidas. Devemos pedir a Deus para fazer milagres em nosso favor. Devemos colocar em oração os nossos entes queridos e nos lembrarmos de como Deus pode mover a vida deles. Devemos buscar o Senhor para todas as nossas necessidades.

Mas não devemos parar por aí.

E se em vez de pedir a Deus para somente fazer algo por nós, fizéssemos uma oração perigosa e altruísta de disponibilidade para o nosso Pai celestial?

E se fizéssemos, talvez, a oração mais perigosa de todas?

“Envia-me, Senhor. Usa-me.”

[[Quem foi Isaías|Isaías]] fez tal oração de disponibilidade sem reservas na presença de Deus. O profeta do [[O que é o Antigo Testamento|Antigo Testamento]] conta sobre o seu encontro com o Santo quando Deus perguntou: “Quem é que eu vou enviar? Quem será o nosso mensageiro?” ([[Isaías]] 6:8a). E sem saber os detalhes, sem saber quando ou onde, Isaías fez essa esplêndida oração que alteraria a sua vida: “Aqui estou eu. Envia-me a mim!” ([[Isaías]] 6:8b).

Note que Isaías não pediu por qualquer detalhe. Ele não perguntou a Deus sobre onde. Ou quando. Ou o que aconteceria. É por isso que essa oração pode parecer tão perigosa.

> [!important] Deus, me envie. Usa-me. Não estou pedindo detalhes. Não preciso saber dos benefícios. Ou se será fácil. Ou se vou gostar. Por quem Tu és — meu Deus, meu Rei, meu Salvador — confio em Ti. Porque Tu és soberano sobre o universo, entrego a minha vontade a Ti, cada parte de mim. Toma a minha mente, meus olhos, minha boca, meus ouvidos, meu coração, minhas mãos, e meus pés e guia-me em direção a Tua vontade. Confio em Ti. Senhor, minha resposta é sim. Qual é a pergunta?

Imagine se você orasse dessa forma. Você está cansado de fazer orações seguras? Você está cansado de viver por coisas que não importam? Você despreza o cristianismo morno e indiferente? Então faça a oração perigosa.

Aqui estou, Senhor.

Envia-me.

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Isaías]] 6:1-8 • VFL
> ¹ No ano em que o rei Uzias morreu, eu vi o Senhor sentado num trono muito alto. A orla do seu manto enchia todo o templo.
> 
> ² Acima dele estavam umas criaturas voadoras; cada uma delas tinha seis asas: com duas cobriam o rosto, com duas cobriam os pés e com as outras duas voavam.
> 
> ³ E diziam em voz alta umas para as outras: “Santo, santo, santo é o SENHOR Todo-Poderoso. Toda a terra está cheia da sua glória”.
> 
> ⁴ Os suportes das portas tremeram ao som das suas vozes e o templo ficou cheio de fumaça.
> 
> ⁵ Então eu disse: — Ai de mim! Estou perdido! Porque os meus lábios são impuros, vivo no meio de um povo de lábios impuros e os meus olhos viram o Rei, o SENHOR Todo-Poderoso.
> 
> ⁶ Então uma das criaturas voou até mim. Ela tinha na mão uma brasa ardente, que tinha tirado do altar com uma tenaz.
> 
> ⁷ Ela tocou os meus lábios com a brasa e disse: — Esta brasa tocou os seus lábios, agora está limpo da sua culpa e perdoado dos seus pecados.
> 
> ⁸ E ouvi a voz do Senhor perguntar: — Quem vou enviar? Quem irá por nós? E eu respondi: — Aqui estou eu. Envie-me a mim.

> [!bible]- [[Romanos]] 12:1-2 • VFL
> ¹ Irmãos, Deus mostrou por nós uma grande misericórdia. Por isso, peço-lhes que ofereçam a ele as suas vidas em sacrifício, isto é, um sacrifício vivo, puro e que lhe seja agradável. Esta é a maneira espiritual de vocês o adorarem.
> 
> ² Não sejam mais moldados por este mundo mas, pela nova maneira de vocês pensarem, vivam uma vida diferente. Então vão descobrir a vontade de Deus, isto é, o que é bom, agradável a ele, e perfeito.

> [!bible]- [[João]] 12:25-26 • VFL
> ²⁵ Quem ama a sua própria vida vai perdê-la. Mas quem odeia a sua vida neste mundo, vai conservá-la para a vida eterna.
> 
> ²⁶ Aquele que é meu servo, tem que me seguir; e onde quer que eu esteja, meu servo também estará. E todo aquele que me serve, será honrado por meu Pai.

> [!bible]- [[Gálatas]] 2:20 • VFL
> portanto, já não sou eu quem vive, mas Cristo vive em mim. E essa vida que agora vivo no meu corpo, vivo pela fé no Filho de Deus, que me amou e se entregou por mim.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Dia 7: Incomoda-me

O que oramos é importante. E, não apenas importante, é também revelador.

O conteúdo das nossas orações nos diz mais sobre nós e nosso relacionamento com Deus do que a maioria das pessoas imagina. Pelo quê oramos reflete o que acreditamos sobre Deus. Se a maioria das nossas orações são sobre “nós mesmos” ou “o que é importante para nós”, então o conteúdo das nossas orações comunica que acreditamos, lá no fundo, que Deus existe essencialmente para nós.

Então, tire um momento e faça uma auditoria da oração. Pense sobre como você tem orado recentemente — não ao longo de toda a sua vida, apenas nos últimos sete dias. Considere escrever em um bloco de anotações ou em seu celular e liste todas as coisas diferentes pelas quais você pediu para Deus fazer na última semana. Tire um momento para pensar sobre isso. Você se lembra? Sobre o que você orou? O que pediu para Deus fazer?

> [!question] Agora, responda honestamente:
> Se Deus dissesse sim para cada oração que você fez nos últimos sete dias, quão diferente estaria o mundo?

Se as suas orações foram aquelas normais e seguras, então, talvez, você teria um bom dia, chegaria com segurança ou aproveitaria um _cheeseburger_ duplo com fritas e Coca Diet abençoado.

Por anos, se eu fizesse uma auditoria da oração, os resultados teriam sido desanimadores. Se Deus tivesse feito tudo que eu havia pedido para Ele no período de uma semana, o mundo não teria ficado nem um pouco diferente. Honestamente, em algumas semanas eu não orava por nada. Em outras, eu posso ter orado, mas elas eram todas sobre mim, e isso não muda muito no grande esquema das coisas.

As minhas orações eram muito seguras.

Eu tinha acesso ao Criador e Sustentador do universo. O Grande Eu Sou. O Alfa e o Ômega. O Princípio e o Fim. O Deus onipotente, onipresente, onisciente, que pode enviar fogo do céu, fechar a boca de leões famintos e acalmar terríveis tempestades. E tudo que eu pedia a Ele era para me manter em segurança e me ajudar a ter um bom dia.

Por anos, nunca quis ser interrompido. Mas depois de fazer orações mais perigosas, descobri que as suaves sugestões de Deus regularmente interromperiam meus planos egocêntricos e que Ele me direcionaria à Sua eterna vontade.

Minha fé é mais forte.

Minha vida é mais rica.

Meu coração é mais cheio.

Pense sobre o que poderia ser diferente se você orasse com mais transparência. Se arriscasse mais. Se fosse mais aberto ao que Deus pode fazer _em_ você em vez de apenas esperar que Ele faça algo _para_ você. E se você fizesse orações mais corajosas? Sonhasse mais alto? Buscasse a Jesus implacavelmente, com uma fé audaciosa e abnegada?

É hora de mudar a maneira que você ora. É hora de abandonar as orações seguras, confortáveis, previsíveis e fáceis de orar. É hora de orar com coragem, de arriscar, de se abrir para um caminho diferente rumo a um destino melhor. É hora de começar a fazer orações perigosas. É hora de ficar incomodado.

Se você realmente quer fazer a diferença na Terra, você precisa de poder do Céu. Se deseja que sua vida tenha significado, chegou a hora de fazer orações grandes, corajosas e audaciosas.

Busque a Deus e sonhe alto. Se recuse a ter medo de falhar. É hora de se aventurar. De confiar. De ousar. De acreditar. Nem sempre a sua vida fará você se sentir seguro. E a fé será sempre necessária. Mas sem fé é impossível agradar a Deus.

O que você está esperando?

<p>&nbsp;</p>

#### Referências bíblicas

> [!bible]- [[Apocalipse]] 1:8 • VFL
> “Eu sou o Alfa e o Ômega”, diz o Senhor Deus, aquele que é, que era e que virá, o Todo-Poderoso.

> [!bible]- [[Colossenses]] 1:9-14 • VFL
> ⁹ Por esta razão nós também, desde o dia em que ouvimos a respeito disso, não paramos de orar por vocês. Estamos sempre pedindo a Deus o seguinte: que vocês venham a conhecer toda a sua vontade e que, com este conhecimento, também tenham todo o tipo de sabedoria e entendimento espiritual.
> 
> ¹⁰ Pedimos que vocês possam viver de maneira que traga honra ao Senhor e que lhe seja em tudo agradável. Pedimos também que façam todo tipo de coisas boas e que cresçam no conhecimento a respeito de Deus.
> 
> ¹¹ Pedimos ainda que Deus os fortaleça com todo o seu poder glorioso, para que não desistam diante das dificuldades e para que vocês sejam pacientes e alegres.
> 
> ¹² Assim darão graças ao Pai, que os tornou dignos de participar da herança que ele preparou para o seu povo santo que vive na luz.
> 
> ¹³ Deus nos livrou do poder das trevas e nos trouxe para o reino do seu amado Filho,
> 
> ¹⁴ por meio do qual temos liberdade e perdão de nossos pecados.

> [!bible]- [[Efésios]] 1:17-23 • VFL
> ¹⁷ Oro sempre ao Deus do nosso Senhor Jesus Cristo, o Pai glorioso, e peço que ele lhes dê o Espírito, que os tornará sábios e revelará a verdade de Deus para que possam vir a conhecê-lo melhor.
> 
> ¹⁸ Também peço que suas mentes sejam abertas para que vejam a luz, e assim conheçam a esperança para a qual Deus os chamou. Peço que saibam como é rica a gloriosa herança que ele dará para todo o seu povo santo,
> 
> ¹⁹ e como incomparavelmente grande é o seu poder para nós, os que cremos. Esse poder é o mesmo que Deus mostrou com poderosa força
> 
> ²⁰ quando ressuscitou a Jesus dos mortos e o fez sentar à sua direita nos céus.
> 
> ²¹ Deus colocou a Cristo acima de todos os poderes, autoridades, forças e reis e acima de todo título de poder que possa ser dado, tanto nesta época como também na que vai chegar.
> 
> ²² Deus colocou tudo sob o controle de Cristo e o fez o cabeça de todas as coisas para a igreja.
> 
> ²³ A igreja é o corpo de Cristo e a totalidade dele mesmo completa todas as coisas em todos os lugares.

> [!bible]- [[Hebreus]] 11:6 • VFL
> De fato, sem fé é impossível agradar a Deus, porque é necessário que aquele que se aproxima de Deus creia que ele existe e que ele recompensa aqueles que o buscam.

> [!bible]- [[Salmos|Salmo]] 9:10 • VFL
> Os que o conhecem, SENHOR, confiam no Senhor, pois nunca abandona aqueles que procuram pela sua ajuda.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### Editor

Gostaríamos de agradecer ao Pastor Craig Groeschel e à [Life.Church](https://www.life.church/) por fornecerem este plano. Para mais informações \[em inglês\], acesse <https://www.craiggroeschel.com>.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Orações Perigosas](https://my.bible.com/pt/reading-plans/18063-dangerous-prayers)”_ | por _Craig Groeschel_ • [[YouVersion]]
- September 19, 2023

<p>&nbsp;</p>

###### tags:

#craig_groeschel #devocional #YouVersion #RELIGIAO/Cristianismo/oracao

<p>&nbsp;</p>
