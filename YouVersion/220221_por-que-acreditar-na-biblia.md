---
title: "Por Que Acreditar na Bíblia? | YouVersion"
author: "Mike Novotny"
dates:
  - created: "February 21, 2022"
  - modified: "January 24, 2023"
tags: [" BIBLIA crenca acreditar ceticismo cetico contexto cristo RELIGIAO/Cristianismo YouVersion/mike_novotny "]
aliases: 
abstract: "Existem muitos céticos no mundo hoje. Como você responde quando eles o questionam sobre a Bíblia, sua fé, ou por que você acredita no que acredita? Este plano de leitura vai ajudá-lo."
link: "http://bible.com/r/7kp"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- POR QUE ACREDITAR NA BÍBLIA? ~ CRIADO EM 2022/02/21~ ATUALIZADO EM 2023/01/24 -->

# Por Que Acreditar na Bíblia?

<p>&nbsp;</p>

![[cover_por-que-acreditar-na-biblia.jpg|IMAGEM: ao fundo e desfocado, a foto de uma Bíblia aberta. Em primeiro plano, a pergunta “Por Que Acreditar na Bíblia?”. Na borda inferior, uma faixa azul onde se lê em letras brancas: “TIME OF GRACE. Mike Novotny”.]]


<!-- IMAGEM CENTRALIZADA

<div align="center" class="center meio">
    <figure>
        <img width="100%" height="auto" alt="IMAGEM: ao fundo e desfocado, a foto de uma Bíblia aberta. Em primeiro plano, a pergunta “Por Que Acreditar na Bíblia?”. Na borda inferior, uma faixa azul onde se lê em letras brancas: “TIME OF GRACE. Mike Novotny”." src="home/samej/pCloudDrive/Documents/Obsidian/Devocionais/media/cover_por-que-acreditar-na-biblia.jpg" />
    </figure>
</div> -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Apresentação

<p>&nbsp;</p>

Existem muitos céticos no mundo hoje. Como você responde quando eles o questionam sobre a Bíblia, sua fé, ou por que você acredita no que acredita? Este plano de leitura vai ajudá-lo.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## **Dia 1:** Por que acreditar na Bíblia?

<p>&nbsp;</p>

> [!question] “Por que você acredita na Bíblia?” pergunta o cético.
> “Porque é a Palavra de Deus”, responde o cristão.
> “Quem disse?” retruca o cético.
> “A Bíblia disse”, responde o cristão.

<p>&nbsp;</p>

_Humm…_ Você pode ver por quê a lógica que os cristãos amam pode não convencer um amigo cético. O raciocínio parece tão pouco convincente quanto o pai que responde às perguntas do filho com “Porque sim!”.

Felizmente, existem argumentos poderosos para sustentar nossa crença na Bíblia. Um dos meus preferidos é como os profetas do [[Antigo Testamento]] sabiam o que os apóstolos do [[Novo Testamento]] viram. Pegue Isaías, por exemplo. Isaías sabia que o Messias escolhido de Deus nasceria de uma virgem ([[Isaías]] 7). Ele sabia que o Cristo viria da família do [[Quem foi Davi|Rei Davi]] ([[Isaías]] 11). Ele sabia que o Salvador seria perfurado por pecados, sofreria em silêncio, seria enterrado com os ricos, e veria a luz da vida novamente ([[Isaías]] 53). Como é possível que Isaías, vivendo setecentos anos [[A história real de Jesus Cristo|antes do nascimento de Jesus]], sabia de tudo isso?

O apóstolo Pedro tem um motivo: _“Pois jamais a profecia teve origem na vontade humana, mas homens falaram da parte de Deus, impelidos pelo Espírito Santo”_ ([[Pedro|2 Pedro]] 1:21). As profecias do [[Antigo Testamento]] foram, de fato, escritas por homens. Mas esses homens tiveram ajuda. O Espírito Santo. É por isso que eles sabiam de coisas que nenhum outro homem poderia saber. O conhecimento dos profetas, dado pelo [[O que é o Espírito Santo na Bíblia|Espírito Santo]], é uma das razões convincentes pelas quais nós cristãos acreditamos na Bíblia “Sagrada”.

<p>&nbsp;</p>

### Referência do dia 1

> [!bible]- [[Isaías]] 7 • VFL
> ¹ Acaz, filho de Jotão e neto de Uzias, era rei de Judá. Nesse tempo o rei Rezim governava a Síria. O rei Peca, filho de Remalias, governava Israel. Os reis Rezim e Peca decidiram atacar Jerusalém mas não conseguiram conquistá-la.<small><sup>[^1]</sup></small>
> 
[^1]: _O rei Peca (…) Israel_ Isso aconteceu por volta do ano 735 a.C. O propósito da guerra era obrigar Acaz, rei de Judá, a unir-se a Peca, rei das dez tribos do norte (Israel), e à Síria para pararem o avanço da Assíria. Ver [2 Reis 16:5-9](https://bolls.life/VFL/12/16/5-9). 
> 
> ² Quando Acaz, o herdeiro de Davi, foi informado de que a Síria e Efraim tinham feito uma aliança, ele e todo o seu povo ficaram tremendo, como árvores da floresta agitadas pelo vento.
> 
> ³ Então o SENHOR disse a Isaías: — Vá com o seu filho Sear-Jasube ao encontro de Acaz. Ele está no lugar onde termina o canal do reservatório superior, no caminho que leva ao Campo do Lavandeiro.<small><sup>[^2]</sup></small>
> 
[^2]: _Sear-Jasube_. O nome significa “Um resto há de voltar”. 
> 
> ⁴ Diga a ele para ter cuidado e calma! Não deve desanimar, nem ter medo do que resta desses dois pedaços de lenha fumegantes: não deve ter medo da fúria de Rezim, o sírio, nem do filho de Remalias.
> 
> ⁵ A Síria e Israel fizeram planos contra o rei Acaz. Eles disseram:
> 
> ⁶ “Vamos invadir, apavorar e dividir o reino de Judá entre nós. Depois vamos instalar o filho de Tabeel como rei”.
> 
> ⁷ Mas isto é o que eu, o SENHOR Deus, digo: “Esse plano não será bem-sucedido, isso não irá acontecer.
> 
> ⁸ Quem manda na Síria é Damasco, e quem manda em Damasco é Rezim.  
Dentro de sessenta e cinco anos Efraim será destruído e deixará de existir.
> 
> ⁹ Quem manda em Efraim é Samaria, e quem manda em Samaria é o filho de Remalias. Acreditem nisto, ou vocês não sobreviverão”.
> 
> ¹⁰ O SENHOR enviou outra mensagem a Acaz:
> 
> ¹¹ — Peça ao SENHOR, seu Deus, para lhe enviar um sinal. Pode vir do fundo do mundo dos mortos ou do mais alto céu.
> 
> ¹² Mas Acaz disse: — Não vou pedir isso! Não vou pôr o SENHOR à prova.
> 
> ¹³ Então Isaías disse: — Escutem, herdeiros de Davi! Não basta testarem a paciência dos homens? Querem agora também testar a paciência de Deus?
> 
> ¹⁴ Pois bem! O próprio Senhor lhes dará um sinal: “Uma jovem ficará grávida  
e dará à luz um filho. Ela lhe dará o nome de Emanuel.<small><sup>[^3]</sup></small>
> 
[^3]: _Emanuel_. Este nome significa “Deus está conosco”.  
>    _jovem_. Segundo o TM. [[O que é a septuaginta|A LXX]] tem: “virgem”. No [[Novo Testamento]], [Mateus 1:23](https://bolls.life/VFL/40/1/23) segue [[O que é a septuaginta|a LXX]]. 
> 
> ¹⁵ Ele comerá creme de leite e mel, até ter idade de rejeitar o mal e escolher o bem.<small><sup>[^4]</sup></small>
> 
[^4]: _creme de leite_. Aqui se refere a algo parecido com o iogurte. 
> 
> ¹⁶ Mas antes dele saber fazer essa escolha, a terra desses dois reis que você teme estará desolada.
> 
> ¹⁷ “Mas o SENHOR trará um tempo de angústia contra você, contra o seu povo e contra a família do seu pai. Esse sofrimento será o pior que já houve desde o tempo em que Efraim se separou de Judá. Isso vai acontecer quando Deus trouxer o rei da Assíria contra vocês.<small><sup>[^5]</sup></small>
> 
[^5]: _Efraim_. Aqui se refere a Israel, o reino do norte. 
> 
> ¹⁸ “Naquele dia, o SENHOR assobiará para que os egípcios venham, como se fossem moscas, dos distantes rios do Egito. Ele também assobiará para que os assírios, como se fossem abelhas, da terra da Assíria.
> 
> ¹⁹ Eles virão e pousarão nos vales profundos, nas fendas das rochas, nos matagais espinhosos e em todos os lugares onde o gado bebe água.
> 
> ²⁰ Naquele dia, o SENHOR vai contratar o rei da Assíria, que vive no outro lado do rio Eufrates, e o utilizará como uma navalha, para cortar o cabelo, a barba e os pelos do corpo de todas as pessoas.
> 
> ²¹ “Naquele dia, quem tiver uma vaca e duas cabras
> 
> ²² terá leite para fazer e comer creme de leite. Todos os sobreviventes comerão creme de leite e mel.
> 
> ²³ Naquele dia, nos lugares onde havia mil videiras que custavam mil moedas de prata, só haverá mato e espinheiros.
> 
> ²⁴ Só poderão entrar lá caçadores com arco e flechas, pois todo o país estará coberto de mato e espinheiros.
> 
> ²⁵ Já não se poderá ir aos montes onde as pessoas cultivavam com enxada, porque estarão cheios de mato e espinheiros; nesses montes os bois andarão pastando e as ovelhas correrão livremente”.

<p>&nbsp;</p>

> [!bible]- [[Isaías]] 11 • VFL
> ¹ Um novo ramo sairá do tronco de Jessé, das suas raízes brotará um rebento.<small><sup>[^1]</sup></small>
> 
[^1]: _do tronco de Jessé_. Isto é, da família do [[Quem foi Davi|Rei Davi]], de quem [[Quem era o pai de Davi|Jessé é pai]]. Ver [1 Samuel 16:1](https://bolls.life/VFL/9/16/1); [2 Samuel 7:5-16](https://bolls.life/VFL/10/7/5-16). 
> 
> ² O Espírito do SENHOR estará sempre sobre ele: o Espírito lhe dará sabedoria e entendimento, o Espírito o aconselhará e lhe dará força, o Espírito o encherá de conhecimento e respeito pelo SENHOR.
> 
> ³ O respeito pelo SENHOR será todo o seu prazer. Não julgará segundo as aparências, nem decidirá a sentença pelo que ouve dizer.
> 
> ⁴ Mas julgará com justiça os pobres, e defenderá honestamente os direitos dos indefesos. As suas ordens serão como uma vara para castigar os violentos, e as suas palavras condenarão os maus à morte.
> 
> ⁵ A justiça será a faixa do seu peito, e a honestidade, o seu cinturão.
> 
> ⁶ O lobo viverá em paz com o cordeiro, e o leopardo dormirá ao lado do cabrito. O bezerro e o leão pastarão juntos, e serão guiados por um menino.
> 
> ⁷ A vaca pastará com o urso, e as suas crias descansarão juntas. O leão comerá erva como o boi.
> 
> ⁸ O bebê brincará em paz junto à toca da cobra, e a criança não terá medo de meter a mão no buraco da víbora.
> 
> ⁹ Não existirá nem o mal nem a destruição em todo o meu monte santo, porque a terra se encherá do conhecimento do SENHOR, assim como as águas cobrem o mar.
> 
> ¹⁰ Naquele dia, haverá alguém especial vindo da descendência de Jessé. Ele será como uma bandeira levantada para os povos: as nações virão procurá-lo e a sua morada será gloriosa.
> 
> ¹¹ Naquele dia, o Senhor voltará a estender a sua mão para resgatar o resto do seu povo da Assíria, do Egito, de Patros, de Etiópia, de Elam, da Suméria, de Hamate e de outros países distantes.
> 
> ¹² Ele levantará uma bandeira para que as nações saibam que ele vai reunir os exilados de Israel, que ele vai juntar o povo de Judá que está disperso pelos quatro cantos da terra.
> 
> ¹³ Efraim deixará de ter inveja e Judá não será mais o seu inimigo. Efraim já não terá inveja de Judá, e Judá deixará de estar irado com Efraim.
> 
> ¹⁴ Juntos se lançarão contra os filisteus, os quais moram ao oeste. Unidos saquearão os povos do leste, os edomitas, os moabitas e os amonitas se submeterão a eles.
> 
> ¹⁵ O SENHOR secará o golfo do Egito e ameaçará o Eufrates com a sua mão erguida. Com um vento muito forte, dividirá o rio em sete riachos, de modo que as pessoas poderão atravessá-lo com as sandálias calçadas.
> 
> ¹⁶ Assim haverá um caminho para o resto do seu povo, o qual ficou na Assíria, como houve um caminho para Israel quando saiu do Egito.

<p>&nbsp;</p>

> [!bible]- [[Isaías]] 53 • VFL
> ¹ Quem acreditou naquilo que ouvimos? Quem viu nele o grande poder do SENHOR?
> 
> ² Ele sempre esteve com Deus. Ele cresceu como um rebento, como uma raiz em terra seca. Ele não tinha beleza ou majestade para atrair a nossa atenção; não havia nada na sua aparência para que gostássemos dele.
> 
> ³ Ele foi desprezado pelas pessoas, e até os seus amigos o abandonaram. Ele foi um homem que sofreu muita dor. Nós o tratamos como se fosse alguém sem importância, como se fosse alguém que as pessoas evitam ou nem olham para ela.
> 
> ⁴ Na verdade, foram os nossos sofrimentos que ele levou sobre si mesmo; ele sofreu as nossas dores. Embora nós pensássemos que Deus o tinha castigado, ferido e humilhado pelo que ele tinha feito,
> 
> ⁵ na realidade ele foi castigado por causa daquilo que nós tínhamos feito. Ele foi esmagado pelas nossas maldades. Ele recebeu o castigo que nós merecíamos, e isso nos trouxe a paz. Nós fomos curados pelas suas feridas!
> 
> ⁶ Todos nós andávamos perdidos como ovelhas; cada um seguia o seu próprio caminho. Mas o SENHOR fez cair sobre ele o castigo que nós merecíamos.
> 
> ⁷ Ele foi tratado cruelmente e torturado, mas ele não protestou. Ele ficou calado como um cordeiro que é levado para o matadouro. Ele era como uma ovelha que fica calada diante dos seus tosquiadores. Ele nunca abriu a sua boca para se defender.
> 
> ⁸ Depois de ter sido preso e condenado injustamente, ele foi levado para ser morto sem que ninguém se importasse com isso! Mas na realidade ele foi morto por causa dos pecados do seu povo.<small><sup>[^1]</sup></small>
> 
[^1]: _seu_. De acordo com Qumran. O TM e [[O que é a septuaginta|a LXX]] têm: “meu”. 
> 
> ⁹ Ele nunca fez mal a ninguém. Ele nunca enganou ninguém. Mas ele foi sepultado ao lado de criminosos. Ele foi enterrado com os ricos.
> 
> ¹⁰ Mas o SENHOR quis que o seu humilde servo fosse esmagado e sofresse muito, para que a sua vida fosse um sacrifício pelo nosso pecado. Por isso o seu servo verá os seus descendentes e viverá por muito tempo. Ele será bem-sucedido fazendo a vontade do SENHOR.
> 
> ¹¹ Depois desse terrível sofrimento, ele verá a luz, e ficará satisfeito com o resultado daquilo que ele fez. O Senhor diz: “O meu servo, que é justo, vai receber o castigo das maldades de muitos para que eles não sejam condenados.
> 
> ¹² Eu irei aceitá-los, e eles serão para ele como a sua herança. Eles serão a sua recompensa pela sua vitória. Eu farei isso porque ele entregou voluntariamente a sua vida por eles e foi tratado como um criminoso ao levar sobre si o castigo de muitos. Agora ele está diante de mim e defende aqueles que tinham se rebelado contra mim”.

<p>&nbsp;</p>

> [!bible]- [[Pedro|2 Pedro]] 1:21 • VFL
> pois jamais qualquer profecia foi feita por vontade humana. Entretanto homens, guiados pelo Espírito Santo, falaram mensagens vindas de Deus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## **Dia 2:** Por que acreditar em Jesus?

<p>&nbsp;</p>

> [!question] Alguém já chamou você de louco por acreditar em Jesus Cristo?
> _“Espere, você realmente acredita que Deus nasceu de uma virgem, morreu em uma cruz, e ressuscitou dos mortos? Sério?”_

Se sim, veja como o apóstolo Paulo defendeu sua fé diante de dois políticos céticos durante seu julgamento em Cesareia: **“Festo interrompeu a defesa de Paulo e disse em alta voz: ‘Você está louco, Paulo! As muitas letras o estão levando à loucura.’ Respondeu Paulo: ‘Não estou louco, excelentíssimo Festo. O que estou dizendo é verdadeiro e de bom senso. O rei está familiarizado com essas coisas, e lhe posso falar abertamente. Estou certo de que nada disso escapou do seu conhecimento, pois nada se passou num lugar qualquer.’”** ([[Atos]] 26:24-26).

Eu amo essa lógica. Paulo apontou que o Cristianismo não acontecia num lugar qualquer. Havia testemunhas reais das coisas reais que aconteceram em um lugar real no nosso planeta. Os primeiros cristãos não se apoiavam nos próprios sentimentos ou no que acreditavam em seus corações. Em vez disso, colocavam sua fé no que haviam visto, ouvido e testemunhado — as palavras e obras do Salvador, visto vivo publicamente, depois de sua morte, por centenas de testemunhas. ([[Coríntios|1 Coríntios]] 15, [[Atos]] 2, [[Atos]] 13)!

A fé deles, assim como a nossa hoje, é verdadeira e sensata. Você não é tolo por confiar em Jesus.

<p>&nbsp;</p>

### Referência do dia 2

> [!bible]- [[Atos]] 26:24-26 • VFL
> ²⁴ Paulo estava dizendo estas coisas em sua defesa quando Festo gritou: — Você está louco, Paulo! Você estudou tanto que ficou maluco!
> 
> ²⁵ Mas Paulo disse: — Eu não estou maluco, Excelentíssimo Festo. As coisas que eu estou dizendo são verdade e de bom senso.
> 
> ²⁶ O próprio rei Agripa aqui presente sabe a respeito dessas coisas e eu tenho certeza de que nenhuma delas escapou ao conhecimento dele, pois nada foi feito às escondidas. É por isso que eu posso falar ao rei abertamente.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## **Dia 3:** Phasellus laoreet

Curabitur blandit tempus ardua ridiculus sed magna. Magna pars studiorum, prodita quaerimus.

<p>&nbsp;</p>

### Referência do dia 3

> [!bible]- Livro 0:00
> 

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## EDITOR

Gostaríamos de agradecer ao _“Time of Grace Ministry”_ por fornecer este plano. Para mais informações, visite: [timeofgrace.org/welcome-to-time-of-grace](https://timeofgrace.org/welcome-to-time-of-grace/?togipsrc=youversion&togpreselect=1-2).

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Por Que Acreditar na Bíblia?](http://bible.com/r/7kp)”_ | YouVersion  
- February 21, 2022

<p>&nbsp;</p>

<!-- HackMD
###### tags: `biblia` `crença` `acreditar` `ceticismo` `cetico` `contexto` `cristo` `cristianismo` `devocional` `jesus` `palavra` `youversion` -->

<!-- Obsidian -->
###### tags:

#BIBLIA #crenca #acreditar #ceticismo #cetico #contexto #cristo #RELIGIAO/Cristianismo #YouVersion/mike_novotny 

