---
title: "Um Guia de Oração para 7 Dias - Dia 1: O que é Oração | YouVersion"
author: "YouVersion"
dates:
  - created: "July 20, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["o que é oração"]
abstract: "O que é oração?"
link: "https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/1/segment/0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 1: O QUE É ORAÇÃO? ~ CRIADO EM 2022/07/20 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 1: O que é Oração?

<p>&nbsp;</p>

### O que é oração?

De acordo com o site [Dictionary.com](https://www.dictionary.com/browse/pray), oração é:

> [!abstract] Oração
> É uma petição devotada a Deus ou a um objeto de adoração.
> 
> Uma comunhão espiritual com Deus ou a um objeto de adoração.
> 
> O ato ou prática de orar a Deus ou a um objeto de adoração.

Quando a palavra _oração_ nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Talvez seja um pastor de igreja vestido em um roupão branco, de pé, atrás de um púlpito, com seus braços estendidos para a congregação. Poderia ser uma avó ajoelhada perto da cama, com as mãos entrelaçadas, conversando silenciosamente com Deus. Talvez seja um grupo de mulheres de mãos dadas em um círculo no salão de comunhão de uma igreja. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração.

Porém, se formos honestos, a maioria de nós se envergonha quando ouve alguém dizer algo sobre oração. Podemos até nos sentir como os escritores dos Salmos que imploravam a Deus para ouvir seu clamor e suas orações, mas nos questionamos se Ele realmente ouvirá. O raciocínio por trás desse pensamento negativo é provavelmente porque não achamos que estamos fazendo o suficiente. Então, quando alguém pergunta: _“você orou sobre isso?”_ ou _“como anda sua vida de oração?”_, normalmente nos sentimos bombardeados com desânimo. 

> [!quote] [[Billy Graham]] disse:
> A oração é simplesmente uma conversa de mão dupla entre você e Deus.

Simples, não é? Mesmo assim, fazemos dela um ato tão complexo. Então, vamos considerar a oração como uma comunicação com Deus. Claro e simples. Deus, nosso criador, nos encontra exatamente onde estamos. Não temos de nos consertar ou estampar uma carinha feliz antes de chegarmos a Ele. Podemos nos chegar a Ele quando precisamos de paz, direção e esperança, quando não sentimos paz, estamos completamente perdidos e temos pouca ou nenhuma esperança. Nada em nossa vida conturbada O assusta. Isso não é um alívio?

A oração é simplesmente:

- _Conhecer a Deus;_

- _Comunicar nossos desejos a Deus;_

- _Ouvir Deus falar a verdade ao nosso espírito;_

- _Expressar nossas necessidades para que Deus possa nos ajudar;_

- _Observar Deus na criação e louvá-Lo;_

- _Sentar com Deus em nossa dor e pedir a Ele que nos conforte;_

- _Expressar nossa gratidão, agradecendo a Deus;_

- _Uma conversa contínua com Deus;_

Nossas vidas de oração devem ser menos sobre um quadro em um programa matutino para _“começar bem o dia”_ e mais sobre uma conversa contínua, vibrante e aberta que temos ao longo do dia com Deus — nosso Pai Celestial.

Pelos próximos seis dias nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida.

<p>&nbsp;</p>

> [!todo] Reflexão
> 
> - [ ] Qual a sua lembrança mais antiga de oração?
>     - Pellentesque habitant morbi tristique senectus et netus.
> 
> - [ ] Escreva sobre um momento em que você foi surpreendido porque Deus respondeu sua oração.
>     - Pellentesque habitant morbi tristique senectus et netus.
> 
> - [ ] Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.
>     - Pellentesque habitant morbi tristique senectus et netus.

<p>&nbsp;</p>

### Versículos Base

> [!bible] [[Salmos|Salmo]] 55:1
> Ouça a minha oração, ó meu Deus, não ignore o meu pedido.

<p>&nbsp;</p>

> [!bible] [[Salmos|Salmo]] 130:2
> Escute a minha voz, ó Senhor, ouça o meu pedido de socorro.

<p>&nbsp;</p>

> [!bible] [[Romanos]] 12:12
> Alegrem-se na esperança que têm. Tenham paciência nas dificuldades e orem em todas as ocasiões.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/1/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

