---
title: "Um Guia de Oração para 7 Dias - Dia 2: Ore como Jesus | YouVersion"
author: "YouVersion"
dates:
  - created: "July 21, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["ore como Jesus"]
abstract: "Quando a palavra oração nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração. Neste plano de sete dias, nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida."
link: "https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/2/segment/0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 2: ORE COMO JESUS ~ CRIADO EM 2022/07/21 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 2: Ore como Jesus

<p>&nbsp;</p>

> [!quote] Anônimo
> A oração é a conversa mais importante do dia. Leve a situação a Deus antes de falar sobre ela com mais alguém.

<p>&nbsp;</p>

Passar algum tempo em oração com Deus é mesmo tão importante assim? Bem, no primeiro dia aprendemos que a oração é simplesmente uma conversa de mão dupla que temos com Deus a fim de crescermos. É passar tempo conversando, ouvindo, perguntando, e agradecendo a Deus, entre outras coisas. Orar ou ter uma conversa contínua com Deus é vital para o crescimento como seguidores de Cristo.

Você sabia que Jesus passava muito tempo orando a Deus? A Bíblia diz que Ele ficava sozinho e orava com frequência. Nos Evangelhos de Mateus, Marcos, Lucas e João lemos que Ele orou mais de 24 vezes, pelo menos! Ele orava…

<p>&nbsp;</p>

- _(…) quando Ele estava sozinho e quando estava com os outros._

- _(…) antes de curar alguém e depois de curar alguém._

- _(…) antes das refeições e antes de decisões importantes._

- _(…) de manhã e à noite._
  
- _(…) depois de ser pregado na cruz e enquanto morria na cruz._

<p>&nbsp;</p>

O que as orações de Jesus nos falam é que Ele dependia do Pai para tudo, e Ele era o Salvador do mundo! Se Ele precisava de tempo com Deus em oração, nós certamente precisamos. Por quê? Porque existimos em um mundo que está constantemente jogando coisas novas para nós e nos implora para desviar a atenção da caminhada com Deus. A oração permite nos relacionarmos com Deus e ser guiados por Ele. Nesses momentos de oração Jesus se aproximava do Pai Celestial. Seus momentos de oração mostravam como Ele ansiava pelo Pai Celestial.

O que podemos aprender com Jesus sobre oração?

<p>&nbsp;</p>

- _Ore em todas as situações e ore por todas as situações._

- Basicamente, busque a Deus sobre _tudo_.

- Desentendimentos no relacionamento? _Fale com Deus._

- Catástrofes financeiras? _Fale com Deus._

- Problemas mentais? _Fale com Deus._

- Dilemas físicos? _Fale com Deus._

- Desafios na carreira? _Fale com Deus._

- Crises emocionais? _Fale com Deus._

<p>&nbsp;</p>

Seja qual for a situação ou problema, fale com Ele. Foi isso que Jesus fez. Agora, isso não anula os seguintes passos que você assumirá para o papel que desempenhará neste ilimitado relacionamento de oração que você tem com Deus. Isso significa apenas que vamos a Ele primeiro.

Nós não devemos ser intimidados pela oração. Não precisamos perguntar se estamos fazendo isso _“da maneira certa”_ ou se estamos falando _“a coisa certa”_. Deus simplesmente quer que estejamos em um relacionamento íntimo com Ele, e sabe que manter um conversa contínua com Ele em nossos dias é o melhor plano.

Conforme caminharmos pelo restante deste plano de leitura, vamos mergulhar no que a oração se parece em nossas vidas como seguidores de Cristo.

<p>&nbsp;</p>

> [!todo] Reflexão
> - [ ] Quando você acha que é mais fácil orar?
>     - Inmensae subtilitatis, obscuris et malesuada fames.
> 
> - [ ] A oração mais difícil?
>     - Inmensae subtilitatis, obscuris et malesuada fames.
> 
> - [ ] Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.
>     - Inmensae subtilitatis, obscuris et malesuada fames.

<p>&nbsp;</p>

### Referências

> [!Bible] Marcos 7:33-35
> ³³ Jesus, pois, tirou-o de entre a multidão, à parte, meteu-lhe os dedos nos ouvidos e, cuspindo, tocou-lhe na língua;
> ³⁴ e erguendo os olhos ao céu, suspirou e disse-lhe: Efatá; isto é Abre-te.
> ³⁵ E abriram-se-lhe os ouvidos, a prisão da língua se desfez, e falava perfeitamente.
<!-- > > [Marcos 7:33-35 • ALMEIDA](https://bible-api.com/marcos+7:33-35?translation=almeida) -->

<p>&nbsp;</p>

> [!Bible] Lucas 5:16
> Mas ele se retirava para os desertos, e ali orava. 
<!-- > > [Lucas 5:16 • ALMEIDA](https://bible-api.com/lucas+5:16?translation=almeida) -->

<p>&nbsp;</p>

> [!Bible] João 17
> _Jesus ora pelos discípulos_
> ¹ Depois de assim falar, Jesus, levantando os olhos ao céu, disse: Pai, é chegada a hora; glorifica a teu Filho, para que também o Filho te glorifique;
> ² assim como lhe deste autoridade sobre toda [[O que é a carne|a carne]], para que dê a vida eterna a todos aqueles que lhe tens dado.
> ³ E a vida eterna é esta: que te conheçam a ti, como o único Deus verdadeiro, e a Jesus Cristo, aquele que tu enviaste.
> ⁴ Eu te glorifiquei na terra, completando a obra que me deste para fazer.
> ⁵ Agora, pois, glorifica-me tu, ó Pai, junto de ti mesmo, com aquela glória que eu tinha contigo antes que o mundo existisse.
> ⁶ Manifestei o teu nome aos homens que do mundo me deste. Eram teus, e tu mos deste; e guardaram a tua palavra.
> ⁷ Agora sabem que tudo quanto me deste provém de ti;
> ⁸ porque eu lhes dei as palavras que tu me deste, e eles as receberam, e verdadeiramente conheceram que saí de ti, e creram que tu me enviaste.
> ⁹ Eu rogo por eles; não rogo pelo mundo, mas por aqueles que me tens dado, porque são teus;
> ¹⁰ todas as minhas coisas são tuas, e as tuas coisas são minhas; e neles sou glorificado.
> ¹¹ Eu não estou mais no mundo; mas eles estão no mundo, e eu vou para ti. Pai santo, guarda-os no teu nome, o qual me deste, para que eles sejam um, assim como nós.
> ¹² Enquanto eu estava com eles, eu os guardava no teu nome que me deste; e os conservei, e nenhum deles se perdeu, senão o filho da [[Qual é a diferença entre 1 Crônicas “Caia eu, pois, nas mãos do Senhor” e Hebreus “Horrenda coisa é cair nas mãos do Deus vivo”|perdição]], para que se cumprisse a Escritura.
> ¹³ Mas agora vou para ti; e isto falo no mundo, para que eles tenham a minha alegria completa em si mesmos.
> ¹⁴ Eu lhes dei a tua palavra; e o mundo os odiou, porque não são do mundo, assim como eu não sou do mundo.
> ¹⁵ Não rogo que os tires do mundo, mas que os guardes do Maligno.
> ¹⁶ Eles não são do mundo, assim como eu não sou do mundo.
> ¹⁷ Santifica-os na verdade, a tua palavra é a verdade.
> ¹⁸ Assim como tu me enviaste ao mundo, também eu os enviarei ao mundo.
> ¹⁹ E por eles eu me santifico, para que também eles sejam santificados na verdade.
> ²⁰ E rogo não somente por estes, mas também por aqueles que pela sua palavra hão de crer em mim;
> ²¹ para que todos sejam um; assim como tu, ó Pai, és em mim, e eu em ti, que também eles sejam um em nós; para que o mundo creia que tu me enviaste.
> ²² E eu lhes dei a glória que a mim me deste, para que sejam um, como nós somos um;
> ²³ eu neles, e tu em mim, para que eles sejam perfeitos em unidade, a fim de que o mundo conheça que tu me enviaste, e que os amaste a eles, assim como me amaste a mim.
> ²⁴ Pai, desejo que onde eu estou, estejam comigo também aqueles que me tens dado, para verem a minha glória, a qual me deste; pois que me amaste antes da fundação do mundo.
> ²⁵ Pai justo, o mundo não te conheceu, mas eu te conheço; conheceram que tu me enviaste;
> ²⁶ e eu lhes fiz conhecer o teu nome, e lho farei conhecer ainda; para que haja neles aquele amor com que me amaste, e também eu neles esteja.
<!-- > > [João 17:1-26 • ALMEIDA](https://bible-api.com/john+17:1-26?translation=almeida) -->

<p>&nbsp;</p>

> [!Bible] Hebreus 5:7 
> O qual nos dias da sua carne, tendo oferecido, com grande clamor e lágrimas, orações e súplicas ao que podia livrar da morte, e tendo sido ouvido por causa da sua reverência,
<!-- > > [Hebreus 5:7 • ALMEIDA](https://bible-api.com/hebreus+5:7?translation=almeida) -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/2/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

