---
title: "Um Guia de Oração para 7 Dias - Dia 7: A oração funciona mesmo? | YouVersion"
author: "YouVersion"
dates:
  - created: "December 11, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["a oração funciona mesmo", "oração funciona"]
abstract: "Quando a palavra oração nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração. Neste plano de sete dias, nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida."
link: "https://bible.com/reading-plans/18141/day/7?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 7: A ORAÇÃO FUNCIONA MESMO? ~ CRIADO EM 2022/12/11 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 7: A oração funciona mesmo?

<p>&nbsp;</p>

> [!quote] [[Tim Keller|Timothy Keller]]
> A oração — embora muitas vezes seja desgastante e até uma agonia — é, a longo prazo, a maior fonte de poder possível.

<p>&nbsp;</p>

As pessoas costumam dizer: _“Deus não respondeu à minha oração”_. Na verdade, [[Deus ainda fala hoje|Deus realmente responde a todas as orações que fazemos.]] Às vezes, ele diz: _“Sim”_, às vezes, diz: _“Não”_ e outras vezes, diz: _“Espere”_. Mas, quando não temos as [[Oração Respondida|orações respondidas]] de acordo com nossos desejos, presumimos que _“Ele não respondeu a oração”_.

Esse padrão de pensamento tende a ser muito egoísta e, no entanto, se formos honestos, provavelmente já alimentamos esse tipo de pensamento em algum momento de nossas vidas. Pedimos a Deus que faça essa coisa boa por nós. Quando isso não acontece, ficamos com o coração machucado porque confiamos em Deus. E isso é simplesmente difícil de aceitar.

Mas [não podemos permitir que o desânimo com nossos planos não realizados nos faça duvidar](https://www.youtube.com/watch?v=UbLtmEQFYFo "“Lidando com o Desânimo” | Luciano Subirá • YouTube") do Deus que criou o mundo. Quando chegamos a um lugar em que realmente acreditamos que Deus tem nosso melhor interesses no coração, ficamos em paz. Muitas vezes, Suas respostas _“não”_ são para nos proteger.

Quando não temos a resposta que desejamos de Deus, em vez da primeira resposta ser _“Por que o Senhor deixou isso acontecer?”_, vamos começar a perguntar: _“Como o Senhor usará isso?”_. Vamos mudar nosso _“por que”_ para _“como”_. Ajustando nossa visão de Deus que mostra Ele como mesquinho e retentor, para um Deus que deseja o melhor para nós; realmente nos mudará de dentro para fora.

> [!question] Então, a oração funciona mesmo?

Bem, se a definição de oração é a de que fazemos uma oração e pedimos a Deus que nos conceda algo que estamos pedindo, e se Ele não atende, então não, a oração não funciona. Mas, se vemos a oração como uma conversa contínua e uma via de mão dupla com Deus, onde crescemos em intimidade com Ele, então sim, a oração funciona. Tudo depende da nossa definição.

A medida em que concluímos este plano de leitura, podemos estar mais perto de Deus do que antes. Alguém já disse uma vez:

> [!quote] Quando a vida estiver difícil, ore. Quando a vida estiver indo bem, ore.

E lembremos que o objetivo de orar não é parecer mais espiritual em uma reunião de oração ou conseguir todas as posses materiais que desejamos. **O objetivo é um relacionamento íntimo com Deus.**

Então, vamos continuar orando — nos dias bonitos, nos feios, nos gloriosos e nos melancólicos. Porque Ele está sempre lá; mais perto de nós do que a nossa respiração.

<p>&nbsp;</p>

> [!todo] Reflexão
> - [ ] Você já se perguntou: _“Deus não respondeu a minha oração?”_. Compartilhe essa experiência.
>     - Unam incolunt Belgae, aliam Aquitani, tertiam.
> 
> - [ ] Dedique algum tempo pedindo a Deus que lhe revele a verdade a respeito dessa experiência.
> 
> - [ ] Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.
>     - Unam incolunt Belgae, aliam Aquitani, tertiam.

<p>&nbsp;</p>

### Referências

> [!Bible] Isaías 40:8 • VFL
> A erva morre, as flores murcham e caem, mas a palavra do nosso Deus permanece para sempre.

<p>&nbsp;</p>

> [!Bible] [[Filipenses]] 4:6-7 • VFL
> ⁶ Não vivam ansiosos por causa de nada. Ao contrário, orem a Deus em qualquer circunstância e peçam a ele o que vocês precisam. E façam isso sempre com ações de graça.
> ⁷ E a paz de Deus, que está além da compreensão humana, guardará os corações e as mentes de vocês em Cristo Jesus.

<p>&nbsp;</p>

> [!Bible] 1 João 5:14-15 • VFL
> ¹⁴ Nós temos muita [[O que a Bíblia diz sobre o contentamento|confiança em Deus]], pois sabemos que, se lhe pedirmos alguma coisa e se o nosso pedido estiver de acordo com a sua vontade, ele nos atenderá.
> ¹⁵ E, se sabemos que ele atende a todos os nossos pedidos, temos a certeza de haver conseguido todas as coisas que lhe pedimos.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/7/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

