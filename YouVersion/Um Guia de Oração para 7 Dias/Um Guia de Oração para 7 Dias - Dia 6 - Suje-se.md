---
title: "Um Guia de Oração para 7 Dias - Dia 6: Suje-se | YouVersion"
author: "YouVersion"
dates:
  - created: "December 9, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["suje-se", "ajoelhe-se e ore", "ajoelhe-se para orar"]
abstract: "Quando a palavra oração nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração. Neste plano de sete dias, nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida."
link: "https://bible.com/reading-plans/18141/day/6?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 6: SUJE-SE ~ CRIADO EM 2022/12/09 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 6: Suje-se

<p>&nbsp;</p>

> [!quote] Craig Groeschel
> A oração nunca é a última linha de defesa. É a nossa primeira linha de ataque.

<p>&nbsp;</p>

A Bíblia nos diz para orar pelos outros. Uma coisa que as orações fazem é incentivar as pessoas. Quando recebemos uma mensagem de texto, uma nota ou um telefonema de alguém dizendo que orou por nós, isso nos encoraja.

Ao orar pelos outros, estamos na linha de frente de suas vidas. Nós podemos orar em nome deles. À medida que crescemos em nossa proximidade com Deus, orar pelos outros se tornará mais fácil, mais natural e até mais emocionante. Então, ao longo do dia, vamos sujar os joelhos e orar. Aqui estão três grupos pelos quais orar:

- **Nosso círculo interno.**  
    O menor círculo são as pessoas mais próximas em nossas vidas, inclusive a nós mesmos. Este grupo precisa de nós em suas vidas. É composto por membros da família e os amigos mais próximos. Orar por esse grupo será mais fácil porque o conhecemos melhor. A maioria das orações será dirigida às pessoas deste grupo porque elas estão na vanguarda de nossas mentes.

- **Nosso círculo do meio.**  
    Este círculo do meio será maior que o círculo interno. Este grupo é composto por [[Quantos bons amigos você de fato possui|bons amigos]], conhecidos e parentes. Nem sempre sabemos o que orar pelas pessoas deste grupo, mas mesmo assim oramos por elas. Um simples _“Deus, por favor, dê sabedoria ao João”_ ou _“Deus, oro para que ela venha a conhecer o Senhor”_ será suficiente.

- **Nosso círculo externo.**  
    Este grupo será o maior grupo em nossa vida. É composto por pessoas que não conhecemos ou com as quais tivemos interação uma única vez. Pode ser o líder do país,<small>[^1]</small> uma celebridade ou um garçom de um restaurante. As orações por essas pessoas podem ser muito mais genéricas simplesmente porque não conhecemos suas necessidades. Às vezes, essas orações não serão específicas, portanto, orações mais genéricas são apropriadas.

[^1]: No breve vídeo abaixo o [[CRISTIANISMO/Respostas Bíblicas/Igreja Presbiteriana|Pastor Presbiteriano]] Paulo Won aborda o tema _“Devo Orar Pelo Presidente?”_ que vale a pena ser conferido:
    <span class="text-center" align="center">![[DEVO ORAR PELO PRESIDENTE.mp4|“Devo Orar Pelo Presidente?” — Pr. Paulo Won • YouTube]]  
    <small>_“[Devo Orar Pelo Presidente?](https://www.youtube.com/watch?v=nKGyvkBlvBM)”_ — Pr. Paulo Won • YouTube</small></span><br /> 

<p>&nbsp;</p>

Aqui estão algumas ideias para começar:

- _Faça uma lista diária e ore por meio dela._

- _Sente-se em silêncio e permita que Deus traga à mente as pessoas._

- _Coloque um lembrete no telefone para incentivar você a orar._

- _Desligue a música enquanto estiver dirigindo e ore._

- _Ore no momento em que surgir a necessidade._

Para impactar os outros devemos interromper nossas agendas. As orações não precisam ser longas e não precisamos nos preocupar com o resultado. Fizemos a nossa parte. Podemos ter como verdade que toda oração que oramos será ouvida por um Deus bom e amoroso. As orações podem ser espontâneas e planejadas. Quando Deus diz que podemos procurá-Lo para tudo e qualquer coisa, Ele realmente quer dizer isso.

Vamos aceitá-Lo em Sua oferta.

<p>&nbsp;</p>

> [!todo] Reflexão
> - [ ] Dedique tempo organizando os diferentes círculos e planeje ir a Deus em nome deles regularmente.
> 
> - [ ] Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.
>     - Unam incolunt Belgae, aliam Aquitani, tertiam.

<p>&nbsp;</p>

### Referências

> [!Bible] Mateus 19:26 • VFL
> Mas Jesus, olhando para eles, lhes respondeu: — Para os homens isto é impossível, mas para Deus tudo é possível.

<p>&nbsp;</p>

> [!Bible] Romanos 8:26 • VFL
> Assim como nossa esperança nos ajuda, o Espírito também nos ajuda em nossas fraquezas. Não sabemos orar como devemos, mas o próprio Espírito, com gemidos que as palavras não podem explicar, pede a Deus em nosso favor.

<p>&nbsp;</p>

> [!Bible] Mateus 19:26 • VFL
> Mas Jesus, olhando para eles, lhes respondeu: — Para os homens isto é impossível, mas para Deus tudo é possível.

<p>&nbsp;</p>

> [!Bible] Romanos 8:26 • VFL
> Assim como nossa esperança nos ajuda, o Espírito também nos ajuda em nossas fraquezas. Não sabemos orar como devemos, mas o próprio Espírito, com gemidos que as palavras não podem explicar, pede a Deus em nosso favor.

<p>&nbsp;</p>

> [!Bible] Filemon 1:4-6 • VFL
> ⁴ Todas as vezes que eu oro, eu me lembro de você e agradeço ao meu Deus,
> ⁵ porque ouço falar da fé que você tem para com o Senhor Jesus e do amor que você tem para com todo o povo de Deus.
> ⁶ Peço a Deus que a sua generosidade, a qual provém da fé que você tem, faça com que as pessoas percebam as boas coisas que acontecem entre nós no serviço de Cristo.

<p>&nbsp;</p>

> [!Bible] Tiago 5:13-16 • VFL
> ¹³ Se alguém entre vocês está sofrendo, ele deve orar. Se alguém está alegre, deve cantar hinos de louvor.
> ¹⁴ Se alguém entre vocês está doente, chame os presbíteros da igreja para orarem por ele e para colocarem óleo sobre ele, em nome do Senhor.
> ¹⁵ E a oração feita com fé fará com que o doente sare, e o Senhor o levantará. E se ele tiver cometido pecados, o Senhor lhe perdoará.
> ¹⁶ Portanto, confessem os seus pecados uns aos outros e [[Dia a dia com Spurgeon - Parte 1 - Dia 2 - Orai uns pelos outros|orem uns pelos outros]] para que vocês sejam curados. A oração feita pelo justo é poderosa e eficaz.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/6/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

