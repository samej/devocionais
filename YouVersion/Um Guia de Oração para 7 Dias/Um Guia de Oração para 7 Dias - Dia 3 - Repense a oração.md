---
title: "Um Guia de Oração para 7 Dias - Dia 3: Repense a oração | YouVersion"
author: "YouVersion"
dates:
  - created: "December 7, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["repense a oração"]
abstract: "Quando a palavra oração nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração. Neste plano de sete dias, nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida."
link: "https://bible.com/reading-plans/18141/day/3?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 3: REPENSE A ORAÇÃO ~ CRIADO EM 2022/12/07 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 3: Repense a oração

<p>&nbsp;</p>

> [!quote] Madre Teresa
> A oração é colocar a si mesmo nas mãos de Deus.

<p>&nbsp;</p>

Muitos seguidores de Cristo lutam para ter uma vida de oração decente. Pode ser difícil para nós porque é fácil focar no próprio ato de orar em vez de focar em Deus. Ter uma vida de oração incrível não é o objetivo. [[Como orar a Deus segundo a Bíblia|Conectar-se com Deus e crescer em nossa intimidade com Ele é.]]

Talvez este cenário seja familiar: Começamos a orar quando a vida está difícil. Então a vida se torna mais fácil, e não oramos tanto porque não precisamos mais tanto de Deus. Mas, algo difícil cruza nosso caminho e sabemos que precisamos orar sobre isso. Nos sentimos culpados porque faz algum tempo que não oramos e presumimos que Deus está furioso conosco.

**É hora de repensarmos a oração.**

Vamos pensar na oração à luz dos nossos relacionamentos mais íntimos. Pode ser um irmão, o cônjuge, ou um melhor amigo. A comunicação com eles é fácil e natural. Podemos mandar uma mensagem de texto, fazer uma ligação ou visitá-los pessoalmente. Às vezes fazemos visitas longas e outras vezes, as mensagens são curtas. Nós geralmente não começamos a conversa com essa pessoa assim:

> [!example] _“Querida Jenny, obrigado por responder minha ligação hoje. Estou empolgado por falar com você. Vamos debater…”_

Em vez disso, temos essa conversa em andamento que tende a continuar até a próxima.

Se tratarmos a oração como tratamos o relacionamento com aqueles mais próximos de nós, a oração se entrelaçará em todas as áreas de nossas vidas. Ela conectará as partes que desconectamos de Deus. Será uma conversa contínua que teremos com Ele — [[5 conselhos da Bíblia que ensinam como mostrar gratidão|cheia de momentos de gratidão]], oração pelos outros, e mesmo o compartilhamento de nossas frustrações. E se essa conversa é contínua, então verdadeiramente, existe a necessidade de dizer _“amém”_?

Isso não significa que não mostramos a honra que Ele merece. Não nos dirigimos a Ele com, _“Ei cara… e aí?”_. Não, nós vivemos nossas vidas onde Ele é honrado e adorado e fazendo isso, os encontros com Ele são muito mais genuínos. Ele se importa menos com as palavras dirigidas a Ele, e mais com o lugar de onde as palavras vieram.

Nosso relacionamento com Deus crescerá na medida que lutarmos para ter uma conversa contínua com Ele. Tanto orações curtas como longas têm o seu lugar. Mas não podemos apenas esperar conhecê-Lo em um momento de cinco minutos toda manhã. Temos que trazê-Lo às nossas vidas diárias e permitir que Ele sopre Sua paz, esperança, conforto e orientação em cada aspecto dela.

<p>&nbsp;</p>

> [!hint] É hora de repensarmos a oração.

<p>&nbsp;</p>

> [!todo] Reflexão
> - [x] Você tende a orar uma vez ou você fala com Deus durante o dia?
>     - De uns dias pra cá, <!-- 2022/12/08 --> geralmente uma vez pela manhã. 😥
> 
> - [x] O que você pode começar a fazer hoje para ter um relacionamento contínuo e diário com Deus? (Lembretes, alarmes, orar por uma hora?)
>     - Colocarei lembretes no celular para ser notificado em ocasiões no decorrer do dia.
> 
> - [x] Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.
>     - Fiquei _negativamente_ impactado com a diferença entre o que estou habituado a fazer durante a oração quando em contraste com o que foi exposto aqui;<small>[^1]</small> é como se Deus estivesse me ensinando como é que Ele quer que eu me relacione com Ele.

[^1]: No sentido de que há anos venho orando de forma “errada”. 

<p>&nbsp;</p>

### Referências

> [!Bible] [[Efésios]] 6:18 • VFL
> Orem em todas as ocasiões com a ajuda do Espírito, com todo o tipo de oração e súplica. Para que possam fazer isto, vocês devem estar sempre vigiando com perseverança e orando por todo o povo santo de Deus.

<p>&nbsp;</p>

> [!Bible] [[Filipenses]] 3:10-11 • VFL
> ¹⁰ Tudo o que desejo é conhecer a Cristo, e experimentar o poder que foi manifesto quando ele ressuscitou dos mortos. Quero também participar dos seus sofrimentos e me tornar como ele na sua morte,
> ¹¹ com a esperança de que assim eu alcance a ressurreição dos mortos.

<p>&nbsp;</p>

> [!Bible] 1 Tessalonicenses 5:16-18 • VFL
> ¹⁶ Estejam sempre alegres.
> ¹⁷ Orem constantemente.
> ¹⁸ Deem sempre graças a Deus em todas as circunstâncias, pois isso é o que Deus quer que vocês façam em Cristo Jesus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/3/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

