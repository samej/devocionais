---
title: "Um Guia de Oração para 7 Dias - Dia 4: Venha mesmo que esteja em desordem | YouVersion"
author: "YouVersion"
dates:
  - created: "December 8, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["vida desordenada", "vida bagunçada", "venha como está", "pecados perdoados"]
abstract: "Quando a palavra oração nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração. Neste plano de sete dias, nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida."
link: "https://bible.com/reading-plans/18141/day/4?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 4: VENHA MESMO QUE ESTEJA EM DESORDEM ~ CRIADO EM 2022/12/08 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 4: Venha mesmo que esteja em desordem

<p>&nbsp;</p>

> [!quote] Lou Holtz
> Quando nos atrapalhamos devido ao pecado – e isso vai acontecer – a confissão nos coloca de volta no prumo.

<p>&nbsp;</p>

Queremos desesperadamente nos achegar a Deus quando achamos que tudo está em seu devido lugar. Mas Deus quer que venhamos a Ele no meio de nossa turbulência. Nós não temos que nos transformar para impressioná-Lo.

Nós temos, no entanto, uma necessidade de conversar sobre a condição de nosso coração com Ele. Parte da oração é confessar as coisas erradas que dissemos, fizemos e até pensamos. [[C.S. Lewis]] diz que a confissão é como o _ponto inicial da oração_. Significa que fazemos isso primeiro.

A confissão é difícil para nós porque não queremos admitir nossas falhas. Novidade: Ele já sabe. Ele sabe as coisas que tentamos esconder Dele. Aquilo que fizemos anos atrás e que juramos levar para o túmulo? Ele sabe. E adivinha? [[Como é o amor de Deus por nós|Ele nos ama mesmo assim]].

Mesmo que Ele saiba, ainda precisamos confessar os pecados. Na verdade, é algo que a maioria de nós não quer fazer ou acha que é desnecessário. E, certamente, não queremos nos chamar de pecadores. Isso significaria que [[O que é pecado segundo a Bíblia|estamos cheios de pecado]]. Outra novidade: somos pecadores. Mas a Bíblia é bem clara que…

> [!bible] [[1-3 João|1 João]] 1:9
> Se confessarmos os nossos pecados, Deus nos perdoará e nos purificará de toda injustiça, pois ele é fiel e justo.

De quanto seremos limpos? Tudo.

Quando confessamos os pecados, [[Melhores escolhas menos arrependimentos|fazemos a escolha de nos arrepender]]. Isso significa que fazemos uma “virada” e literalmente mudamos para o caminho oposto ao pecado que acabamos de confessar. Confessar o pecado e andar em arrependimento pode incluir esta simples oração.

> [!important] Deus, por favor me perdoe por fofocar sobre a Beth hoje. Eu escolho dizer palavras que não coloquem as pessoas para baixo daqui para frente.

Mas há momentos em que chegamos a Ele e temos muito mais a confessar. Pode haver anos de discrepâncias morais que nos pesam. Algumas coisas que não lembramos especificamente. Se for esse o caso, a oração pode ser:

> [!important] Deus, eu sei que pequei muito e não me lembro de tudo. Por favor, me perdoe onde eu errei?

E adivinhem? A Bíblia nos diz que Ele não se _lembrará mais dos nossos pecados e que eles serão removidos de nós do leste até o oeste_.<small>[^1]</small>

[^1]: > [!bible] [[Hebreus]] 8:12
    > E eu perdoarei as faltas que eles cometerem, e eu nunca mais me lembrarei dos seus pecados”.
    
    > [!bible] [[Hebreus]] 10:17
    > E depois ele diz: “Nunca mais me lembrarei dos seus pecados ou das suas maldades”.
    
    > [!bible] [[Salmos|Salmo]] 103:12
    > Assim como o Oriente está afastado do Ocidente, assim também Deus afasta os nossos pecados de nós.

Podemos pensar que somos um caso perdido e que as circunstâncias não tem jeito. E adivinha? Casos perdidos ou sem solução são as especialidades de Deus. Precisamos apenas levar nossas vidas bagunçadas para Ele.

<p>&nbsp;</p>

> [!todo] Reflexão
> - Peça a Deus para trazer à sua mente tudo o que você precisa confessar e, em seguida, simplesmente faça isso.
> 
> - Há algum problema que está sempre esbarrando em você, que precisa ajustar, remover ou acrescentar para ajudá-lo a virar na direção oposta?
> 
> - Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.

<p>&nbsp;</p>

### Referências

> [!Bible] Salmo 103:11-12 • VFL
> ¹¹ Pois assim como é grande a altura entre o céu e a terra, assim é grande o seu amor por aqueles que o temem.
> ¹² Assim como o Oriente está afastado do Ocidente, assim também Deus afasta os nossos pecados de nós.

> [!bible] [[Hebreus]] 8:12
> E eu perdoarei as faltas que eles cometerem, e eu nunca mais me lembrarei dos seus pecados”.

> [!bible] [[1-3 João|1 João]] 1:9
> Se confessarmos os nossos pecados, Deus nos perdoará e nos purificará de toda injustiça, pois ele é fiel e justo.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/4/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

