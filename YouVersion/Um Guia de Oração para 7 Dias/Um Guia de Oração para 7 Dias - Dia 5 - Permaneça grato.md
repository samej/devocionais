---
title: "Um Guia de Oração para 7 Dias - Dia 5: Permaneça grato | YouVersion"
author: "YouVersion"
dates:
  - created: "December 9, 2022"
  - modified: "December 12, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["permaneça grato", "ore agradecendo"]
abstract: "Quando a palavra oração nos vem à mente, nós provavelmente temos uma ideia do que ela quer dizer. Independente da sua formação, você provavelmente teve alguma experiência prévia com o conceito de oração. Neste plano de sete dias, nos aprofundaremos no aspecto da oração e como podemos incorporar práticas que nos ajudarão a começar uma conversa contínua com Deus que durará por toda a vida."
link: "https://bible.com/reading-plans/18141/day/5?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- UM GUIA DE ORAÇÃO PARA 7 DIAS - DIA 5: PERMANEÇA GRATO ~ CRIADO EM 2022/12/09 ~ ATUALIZADO EM 2022/12/12 -->

# Um Guia de Oração para 7 Dias

<p>&nbsp;</p>

## Dia 5: Permaneça grato

<p>&nbsp;</p>

> [!quote] Paul E. Miller
> Aprender a orar não nos oferece uma vida menos ocupada; oferece-nos um coração menos ocupado.

<p>&nbsp;</p>

Ouvimos dizer que _as pessoas felizes nem sempre são gratas, mas as pessoas agradecidas quase sempre são felizes_. Vamos pensar sobre isso. De todas as pessoas agradecidas que conhecemos, quantas são infelizes? Zero.

> [!question] Como permanecemos gratos durante a vida agitada que levamos?

> [!info] Sendo intencional.

As coisas mais gratificantes em nossas vidas exigem muita intenção. E permanecer em um estado de espírito agradecido a cada dia requer intencionalidade.

Uma das melhores maneiras de nosso coração permanecer em uma postura de gratidão é contar as bênçãos. Certo. Literalmente, comece a nomeá-las. Simplesmente parar e contar as bênçãos levará a uma mudança de atitude. Inicie uma lista no seu dispositivo, anote-as em lembretes ou defina um alarme no telefone que solicitará que você cite cinco coisas pelas quais você é grato.

Aqui estão algumas sugestões que nos ajudarão a ser intencionais em cultivar um coração agradecido:

- **Pare e cheire as rosas.**  
    Nosso Deus é um Criador incrível. Tudo o que vemos na natureza é obra de Deus. Quantas vezes ficamos sem palavras por causa do nascer do sol? Quando esses momentos chegarem, apenas diga: _“Obrigado, Deus!”_.

- **Vire essa carranca de cabeça para baixo.**  
    Quando as coisas não correm de acordo com o plano que tínhamos em mente, em vez de focar no que _não_ recebemos, vamos nos concentrar no que _temos_. Não há problema em sentir uma ponta de decepção, mas não vamos morar lá. Vamos expressar nossa gratidão a Deus pelo que temos.

- **Não pare de acreditar.**  
    Talvez a melhor maneira de agradecer como seguidores de Cristo seja lembrar o que foi feito em nosso favor. Jesus deixou o lugar ao lado do Pai Celestial para vir em nosso socorro. Por causa de Seu ato perfeito e altruísta, não precisamos temer a eternidade. Se realmente mergulharmos nisso, nossos corações ficarão agradecidos.

Então, seremos gratos em todas as coisas e de todas as maneiras. Pelo pão na despensa, pelo tanque cheio de gasolina no carro e pelos entes queridos de quem desfrutamos. Mas nunca devemos esquecer Aquele que depositou todo Seu poder e autoridade para chegar a uma terra quebrada, para que pudéssemos viver para sempre com Ele. Se acordamos todos os dias com essa visão em mente, as primeiras palavras que sairão de nossos lábios serão simplesmente _“obrigado”_. E honestamente, se tudo o que fizermos for agradecer a Deus, isso é uma coisa muito boa.

<p>&nbsp;</p>

> [!todo] Reflexão
> - [x] Em que áreas da vida você acha fácil ser grato? 
>     - Em ter pleno domínio mental, raciocínio e ser desapegado às coisas materiais.
> 
> - [ ] Nos próximos sete dias, comece o dia agradecendo a Deus pelas bênçãos em sua vida. Mantenha uma lista em execução.
>     - [ ] **Dia 1:** 2022/12/10 - sábado
>         + Magna pars studiorum, prodita quaerimus.
>     - [ ] **Dia 2:** 2022/12/11 - domingo
>         + Magna pars studiorum, prodita quaerimus.
>     - [ ] **Dia 3:** 2022/12/12 - segunda-feira
>         + Magna pars studiorum, prodita quaerimus.
>     - [ ] **Dia 4:** 2022/12/13 - terça-feira
>         + Magna pars studiorum, prodita quaerimus.
>     - [ ] **Dia 5:** 2022/12/14 - quarta-feira
>         + Magna pars studiorum, prodita quaerimus.
>     - [ ] **Dia 6:** 2022/12/15 - quinta-feira
>         + Magna pars studiorum, prodita quaerimus.
>     - [ ] **Dia 7:** 2022/12/16 - sexta-feira
>         + Magna pars studiorum, prodita quaerimus.
> 
> - [ ] Anote qualquer revelação que Deus tenha te dado durante a [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] ou devocional.
>     - Unam incolunt Belgae, aliam Aquitani, tertiam.

<p>&nbsp;</p>

### Referências

> [!Bible] João 3:16 • VFL
> Pois Deus amou o mundo de tal maneira que deu o seu Filho único, para que todo o que nele crê não morra, mas tenha a vida eterna.

<p>&nbsp;</p>

> [!Bible] Romanos 6:23 • VFL
> Porque o salário que o pecado paga é a morte, mas o presente gratuito dado por Deus é a vida eterna em Cristo Jesus, nosso Senhor.

<p>&nbsp;</p>

> [!Bible] [[Colossenses]] 3:15 • VFL
> Deixem que a paz que Cristo dá controle os seus corações. Foi para esta paz que Deus os chamou num só corpo. Sejam sempre agradecidos.

<p>&nbsp;</p>

> [!Bible] [[Colossenses]] 4:2 • VFL
> Dediquem-se à oração. Estejam sempre alerta ao orarem, com ações de graça.

<p>&nbsp;</p>

> [!Bible] Tiago 1:17 • VFL
> Tudo o que é bom e todo dom que é perfeito vem lá do céu, do Pai que criou as luzes celestiais. Nele não há variação nem qualquer mudança que produza sombra.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Um Guia de Oração para 7 Dias - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Um Guia de Oração para 7 Dias](https://my.bible.com/pt/users/SamejSpenser884/reading-plans/18141-um-guia-de-oracao-para-7-dias/subscription/817189923/day/5/segment/0)”_ | Plano de Leitura Devocional • [[YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

