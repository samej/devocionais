---
title: "As Coisas que Jesus Nunca Disse | YouVersion"
author: "Craig Groeschel"
dates:
  - created: "February 9, 2022"
  - modified: "May 25, 2023"
tags: [" biblia contexto cristo RELIGIAO/Cristianismo devocional jesus palavra youversion "]
abstract: "Se Jesus estivesse de acordo com todas as ideias que os seguidores compartilharam baseados em Suas afirmações, então, Ele seria um Filho de Deus incrivelmente confuso, sempre em desacordo consigo mesmo e excessivamente ocupado liderando a acusação de ambos os lados de cada debate. Felizmente, Jesus fala por si mesmo."
link: "https://my.bible.com/pt/reading-plans/14676-things-jesus-never-said"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- AS COISAS QUE JESUS NUNCA DISSE ~ CRIADO EM 2022/02/09 ~ ATUALIZADO EM 2023/05/25 -->

# As Coisas que Jesus Nunca Disse

<p>&nbsp;</p>

<!-- IMAGEM CENTRALIZADA -->

<div align="center" class="center meio">
    <figure>
        <img width="100%" height="auto" alt="IMAGEM: A figura de Jesus Cristo com a mão esquerda segurando parte do manto em seu tórax e a mão direita ligeiramente erguida, como quem abenmçoa. Ao lado, os dizeres (em Inglês): “THINGS JESUS NEVER SAID”." src="https://s3.amazonaws.com/yvplans/14676/720x405.jpg" />
    </figure>
</div>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

Se Jesus estivesse de acordo com todas as ideias que os seguidores compartilharam baseados em Suas afirmações, então, Ele seria um Filho de Deus incrivelmente confuso, sempre em desacordo Consigo mesmo e excessivamente ocupado liderando a acusação de ambos os lados de cada debate. Felizmente, Jesus fala por Si mesmo.

Você pode ler as palavras Dele neste novo plano bíblico da Life\.Church para acompanhar a série de mensagens do pastor Craig Groeschel \[em inglês\], _“Things Jesus Never Said”_ \[Coisas que Jesus Nunca Disse\].

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 1: Deixe Jesus Falar por Si Mesmo

<p>&nbsp;</p>

> [!quote] [[Abraham Lincoln]]
> O problema com as citações encontradas na internet é que elas geralmente não são verdadeiras.

<p>&nbsp;</p>

Entendeu? O honesto Abraham obviamente não disse isso, mas com certeza pode ser verdade. Por que existem tantas palavras falsas, imprecisas, fora de contexto ou completamente inventadas que são atribuídas a pessoas como Abraham Lincoln? Por um lado, ele é amplamente conhecido como sábio, confiável, bom e honesto. A pessoa perfeita para obter convicções convenientes ou profundas percebeu, espalhou e acreditou. Quem é mais sábio, mais conhecido, profundamente bom e confiável que Lincoln? Jesus é.

Se Jesus estivesse de acordo com todas as ideias que os seguidores compartilharam baseados em Suas afirmações, então, ele seria um Filho de Deus incrivelmente confuso, sempre em desacordo consigo mesmo e excessivamente ocupado liderando a acusação de ambos os lados de cada debate.

Mas Jesus não é apenas mal interpretado por outras pessoas, não é? Quantas partes da sua vida são formadas por palavras, ideias ou sentimentos que você tem, talvez de maneira imprecisa, provavelmente atribuída não intencionalmente a Jesus? Aqui está outra maneira de fazer a pergunta. Que coisas você está fazendo ou dizendo em nome de Jesus que Ele nunca fez ou disse?

Os próximos dias serão sobre o que Jesus disse para que possamos descobrir algumas das coisas que Jesus nunca disse. E no final, esperamos ter palavras, mentes, corações e vidas que se parecem e soam um pouco mais com Ele.

<p>&nbsp;</p>

**Considere:** Que partes da minha personalidade ou visão de mundo eu atribuí a Jesus que podem ter pouco a ver com ele?

<p>&nbsp;</p>

### Referência do dia 1

> [!bible] [[Mateus]] 5:1-16 • VFL
> ¹ Quando Jesus viu a grande multidão, subiu para o alto de um monte e se sentou. Os seus discípulos se aproximaram
> 
> ² e ele começou a ensiná-los, dizendo:
> 
> ³ — Felizes são os que reconhecem que precisam de Deus, pois é esse tipo de pessoa que fará parte do reino de Deus.
> 
> ⁴ — [[O sermão do monte — exposição de Mateus 5-7 — Dia 2|Felizes são os que choram]], pois Deus os consolará.
> 
> ⁵ — [[O sermão do monte — exposição de Mateus 5-7 — Dia 3|Felizes são os humildes]], pois eles herdarão a terra que Deus prometeu.
> 
> ⁶ — Felizes são os que têm fome e sede de justiça, pois Deus fará com que fiquem satisfeitos.
> 
> ⁷ — [[O sermão do monte — exposição de Mateus 5-7 — Dia 5|Felizes são os que têm misericórdia]] dos outros, pois Deus terá misericórdia deles.
> 
> ⁸ — Felizes são os que têm coração puro, pois verão a Deus.
> 
> ⁹ — Felizes são os que fazem a paz, pois Deus os chamará de “filhos”.
> 
> ¹⁰ — [[O sermão do monte — exposição de Mateus 5-7 — Dia 8|Felizes são aqueles que são perseguidos por fazerem a vontade de Deus]], pois a eles pertence o reino de Deus.
> 
> ¹¹ — Felizes serão vocês quando forem insultados, perseguidos e receberem todo tipo de calúnias pelo fato de me seguirem.
> 
> ¹² Alegrem-se e fiquem realmente muito felizes, pois grande é a recompensa que receberão no céu. Foi desta mesma maneira que os profetas que viveram antes de vocês também foram perseguidos.
> 
> **O sal e a luz**
> 
> ¹³ — Vocês são o sal da terra! Se o sal, porém, perder o seu sabor, não poderá voltar a ser salgado e não presta para mais nada. Só serve para ser jogado fora e pisado pelos que passam.
> 
> ¹⁴ — Vocês são a luz do mundo! Uma cidade situada no alto de uma montanha não pode ser escondida.
> 
> ¹⁵ Da mesma forma, ninguém acende um lampião para colocá-lo debaixo de uma bacia, mas sim para colocá-lo em cima de uma mesa, para que possa iluminar a todos os que estão na casa.
> 
> ¹⁶ Que a luz de vocês brilhe diante das pessoas de tal forma que, ao verem o bom comportamento de vocês, elas deem glória ao Pai que está no céu.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 2: Siga-me e você estará livre de problemas

Um garoto de oito anos normalmente barulhento chegou em casa da escola em lágrimas e foi direto para a cama. Em vez de tênis fazendo barulho pela casa, algo horrível estava ecoando em sua alma. Ele havia se metido em problemas novamente por conversar em sala de aula. Ele estava chorando como se tivesse perdido o cachorro da família, como se houvesse perdido a chance de tomar sorvete novamente em uma vida futura como um monge que só faria tarefas e tiraria cochilos enquanto permaneceria sentado em uma pequena mesa.

Brincadeiras à parte, foi muito ruim. Ele estava com o coração partido.

Depois de tirá-lo do travesseiro e depois de descobrir qual era o problema, seu pai estava pronto para ouvir o seu desabafo. “Eu não acho que estava pronto para ser batizado”, disse o menino de bom coração em lágrimas. “Pensei que, se seguisse a Jesus, não cometeria tantos erros.”

Pensei que, se seguisse Jesus, não teria problemas: _não lutaria contra o vício, não teria crises no casamento, não perderia a calma, não passaria por divórcio, nem por lutas financeiras, não sofreria a perda precoce de pessoas queridas, não teria mais dúvidas, não veria meus filhos abandonarem a fé, não seria tentado, não perderia a promoção tão desejada, ninguém falaria mal a meu respeito, eu seria sempre popular e não cometeria tantos erros._ Quais pensamentos você teve?

Essa não é uma história fofa e inventada sobre um garoto imaginário. Ele é tão real quanto você. Ele foi ferido pela mesma razão que alguém foi ferido: ele entendeu mal quem é Jesus e o que Jesus diz. Jesus nunca disse: “Siga-me e você estará livre de problemas”. Ele disse coisas como: “Siga-me e: farei de você pescador de homens; Não perturbe seu coração; Você experimentará problemas, mas eu venci o mundo; Negue a si mesmo; Vou te dar um descanso verdadeiro; Perca a sua vida por Minha causa e você a encontrará.” Essa é uma lista parafraseada, mas você pode ler as palavras de Jesus por si mesmo na leitura das Escrituras de hoje.

Então, o pai olhou nos olhos do menino e disse: “Seguir a Jesus não significa que você nunca terá problemas — significa que você já está perdoado. Significa que Ele está com você agora e da próxima vez que precisar de ajuda para fazer a coisa certa. Isso significa que Ele te ama mesmo quando você comete um erro maior do que falar na aula.”.

Jesus não nos convidou para uma vida fácil e sem problemas. Ele nos convida a dar tudo e ainda receber mais do que jamais poderíamos querer ou ganhar.

<p>&nbsp;</p>

**Ore:** _Jesus, você vai me mostrar como eu lhe entendi mal? Perdoe-me por tentar buscar uma vida mais fácil em vez de buscar a Sua presença e por tentar garantir minha perfeição em vez de aceitar a Sua graça. Obrigado por não apenas me perdoar, mas por estar comigo hoje. Amém._

<p>&nbsp;</p>

### Referência do dia 2

```ad-note
title: **Mateus 4:19** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>19</sup></small> quando Jesus lhes disse: — Sigam-me e eu tornarei vocês em pescadores de pessoas.
```

```ad-note
title: **João 14:1-6** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>1</sup></small> Jesus disse: — Não fiquem preocupados! Continuem confiando em Deus e continuem acreditando em mim.

<small><sup>2</sup></small> Na casa de meu Pai há muitos quartos. Se não fosse assim, eu já lhes teria dito, pois vou preparar um lugar para vocês.

<small><sup>3</sup></small> Depois de ir e preparar um lugar para vocês, eu voltarei. Então levarei vocês comigo, para que possam estar onde eu estiver.

<small><sup>4</sup></small> Vocês sabem como chegar ao lugar para onde eu vou.

<small><sup>5</sup></small> Tomé então disse a Jesus: — Senhor, não sabemos para onde vai! Como podemos saber o caminho?

<small><sup>6</sup></small> Jesus respondeu a ele: — Eu sou o Caminho, a Verdade e a Vida. Ninguém pode chegar até o Pai se não for por mim.
```

```ad-note
title: **João 16:33** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>33</sup></small> — Eu digo isto a vocês para que, por meu intermédio, vocês encontrem paz. Vocês sofrem neste mundo, mas sejam corajosos. Eu venci o mundo!
```

```ad-note
title: **Mateus 16:24-25** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>24</sup></small> E Jesus, então, disse aos seus discípulos: — Se alguém quiser vir comigo, tem que negar a si mesmo, pegar a sua cruz e me seguir.

<small><sup>25</sup></small> Digo isto pois todo aquele que quiser salvar a sua vida a todo custo, irá perdê-la; e todo aquele que perder a sua vida por minha causa, irá salvá-la.
```

```ad-note
title: **Mateus 11:28** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>28</sup></small> Venham a mim todos vocês que estão cansados ou sobrecarregados e eu lhes darei descanso.
```

```ad-note
title: **João 10:27** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>27</sup></small> As minhas ovelhas ouvem a minha voz. Eu as conheço e elas me seguem.
```

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 3: Siga o seu caminho e faça o que deixa você feliz

```ad-note
title: **Dr. Paul Tripp**

“Gerenciar o seu caminho para o controle pessoal de modo a garantir que você conseguirá o que pensa ser natural.”

```

<p>&nbsp;</p>

Assim como às vezes confundimos seguir Jesus como um plano de auto-salvação que um dia tornará nossa vida mais perfeita e sem problemas, outras vezes confundimos o caminho de Jesus com a busca da felicidade.

A matemática mental funciona assim: Deus é bom e a felicidade é boa, então Deus deve querer que eu descubra como ser feliz. Além disso, essa coisa, relacionamento ou modo de vida me faz feliz, então Deus deve querer que eu vá buscá-lo por mim mesmo.

Se você está agora se sentindo encurralado, não se sinta assim. Você não está sozinho. Esse tipo de pensamento é natural. No plano bíblico _“[Novo Ano, Novas Misericórdias](https://my.bible.com/pt/reading-plans/13613-new-year-new-mercies)”_, o Dr. Paul David Tripp relata: “Desejar que você seja mais soberano sobre pessoas, situações e locais do que jamais será é natural.” É natural desejar o próprio caminho. É natural tentar organizar a vida para que você não sinta desconforto. É natural querer que Jesus, religião ou [[A Bíblia é um projeto para toda a vida|leitura da Bíblia]] façam sua vida funcionar. É natural procurar entretenimento, coisas ou substância para silenciar o barulho quando a religião não dá certo. É natural tentar fazer o que for preciso para ser feliz.

É natural, mas está funcionando? Quando percebemos que, no final de cada tentativa humana de garantir a própria felicidade, há menos segurança e mais infelicidade, é quando finalmente podemos ver Jesus por quem Ele realmente é. Jesus é suficiente.

Jesus nunca disse: “Siga seu caminho e faça o que te faz feliz.” Ele disse que Ele é o Caminho, a Verdade e a Vida. Ele disse que ninguém vem ao Pai, exceto por meio Dele. Jesus é suficiente.

Jesus nunca disse que você não pode ser feliz. Ele, no entanto, sugeriu que você não pode encontrar verdadeira alegria, profunda felicidade, verdadeira restauração ou perceber o amor eterno de nosso Pai celestial sem passar por Ele como se somente Ele fosse suficiente. Porque Jesus _é_ suficiente.

Felizmente, Jesus o ama tanto que fica no caminho da busca pela felicidade. Ele entra no seu mundo e desiste da vida Dele, para que você possa finalmente desistir de tentar descobrir o que o faz feliz. Para citar o Dr. Tripp, “Ele está resgatando você de pensar que você pode viver a vida que deveria viver, enquanto confia nos recursos inadequados de sua sabedoria, experiência, retidão e força”.

Isso não é natural. Isso é sobrenatural. É quem Jesus é — Jesus é suficiente.

<p>&nbsp;</p>

**Considere:** Como tenho tentado organizar minha felicidade? Como minha vida ficaria diferente se eu vivesse como se Jesus bastasse?

<p>&nbsp;</p>

### Referência do dia 3

```ad-note
title: **João 10:1-18** • <abbr title="Nova Versão Internacional">NVI</abbr>
collapse: closed
icon: bible

<small><sup>1</sup></small> “Eu lhes digo a verdade: quem entra no curral das ovelhas às escondidas, por sobre a cerca, em vez de passar pela porta, é certamente ladrão e assaltante!

<small><sup>2</sup></small> Mas quem entra pela porta é o pastor das ovelhas.

<small><sup>3</sup></small> O porteiro lhe abre a porta, e as ovelhas reconhecem sua voz e se aproximam. Ele chama suas ovelhas pelo nome e as conduz para fora.

<small><sup>4</sup></small> Depois de reuni-las, vai adiante delas, e elas o seguem porque conhecem sua voz.

<small><sup>5</sup></small> Nunca seguirão um desconhecido; antes, fugirão dele, pois não reconhecem sua voz.”

<small><sup>6</sup></small> Os que ouviram Jesus usar essa ilustração não entenderam o que ele quis dizer,

<small><sup>7</sup></small> por isso ele a explicou: “Eu lhes digo a verdade: eu sou a porta das ovelhas.

<small><sup>8</sup></small> Todos que vieram antes de mim eram ladrões e assaltantes, mas as ovelhas não os ouviram.

<small><sup>9</sup></small> Sim, eu sou a porta. Quem entrar por mim será salvo. Entrará e sairá e encontrará pasto.<small><sup>[^1]</sup></small>

[^1]: Ou _encontrará segurança_. 

<small><sup>10</sup></small> O ladrão vem para roubar, matar e destruir. Eu vim para lhes dar vida, uma vida plena, que satisfaz.

<small><sup>11</sup></small> “Eu sou o [[A parábola do bom pastor|bom pastor]]. O bom pastor sacrifica sua vida pelas ovelhas.

<small><sup>12</sup></small> O empregado foge quando vê um lobo se aproximar. Abandona as ovelhas porque elas não lhe pertencem e ele não é seu pastor. Então o lobo as ataca e dispersa o rebanho.

<small><sup>13</sup></small> O empregado foge porque trabalha apenas por dinheiro e não se importa de fato com as ovelhas.

<small><sup>14</sup></small> “Eu sou o bom pastor. Conheço minhas ovelhas, e elas me conhecem,

<small><sup>15</sup></small> assim como meu Pai me conhece e eu o conheço; e eu sacrifico minha vida pelas ovelhas.

<small><sup>16</sup></small> Tenho outras ovelhas, que não estão neste curral. Devo trazê-las também. Elas ouvirão minha voz, e haverá um só rebanho e um só pastor.<small><sup>[^2]</sup></small>

[^2]: Jesus quer dizer que ele tem seguidores que não são judeus. Ver [João 11:52](https://bolls.life/VFL/43/11/52). 

<small><sup>17</sup></small> “O Pai me ama, pois sacrifico minha vida para tomá-la de volta.

<small><sup>18</sup></small> Ninguém a tira de mim, mas eu mesmo a dou. Tenho autoridade para entregá-la e também para tomá-la de volta, pois foi isso que meu Pai ordenou”.
```

```ad-note
title: **Salmos 16:1-11** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>1</sup></small> **Poema de Davi.** Proteja-me, ó meu Deus,  
pois no Senhor me refugio.

<small><sup>2</sup></small> Alguns disseram: “O SENHOR é o meu Senhor,  
todo o bem que tenho vem do Senhor”.<small><sup>[^1]</sup></small>

[^1]: ou “Disse ao SENHOR, meu Senhor: ‘Você é bom para mim. Não há outro melhor do que você’”. 

<small><sup>3</sup></small> E ao mesmo tempo disseram aos deuses do país:  
“Vocês também são fortes e muito agradáveis”.<small><sup>[^2]</sup></small>

[^2]: _deuses_ Literalmente, “santos”, referindo-se aos deuses falsos do país. 

<small><sup>4</sup></small> Os que seguem outros deuses sofrerão muito.  
Eu não participarei das ofertas sangrentas que eles oferecem aos seus ídolos.  
Nem sequer permitirei que os meus lábios pronunciem o nome desses deuses.

<small><sup>5</sup></small> O SENHOR é a porção que me foi dada,  
a minha felicidade.  
O meu futuro está nas suas mãos!

<small><sup>6</sup></small> Foi-me dado um lugar maravilhoso,  
a minha herança é a mais bela.

<small><sup>7</sup></small> Louvarei o SENHOR porque ele me guia;  
até de noite, ele dirige e guia os meus passos.

<small><sup>8</sup></small> Para mim o SENHOR é sempre o primeiro;  
nada pode me abalar, pois ele está ao meu lado.

<small><sup>9</sup></small> Por isso o meu coração e a minha alma estão cheios de alegria  
e o meu corpo viverá em segurança para sempre.

<small><sup>10</sup></small> Porque não me abandonará no lugar dos mortos,  
não deixará o seu santo ficar na cova.<small><sup>[^3]</sup></small>

[^3]: _ficar na cova_ [[O que é a septuaginta|A LXX]] tem: “sofrer decomposição”. 

<small><sup>11</sup></small> Ensine-me o caminho que conduz à vida,  
à alegria sem fim de estar na sua presença,  
às delícias de estar ao seu lado direito para sempre.
```

```ad-note
title: **[[João 14_6 não é teologia de adesivo de para-choque|João 14:6]]** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>6</sup></small> Jesus respondeu a ele: — Eu sou o Caminho, a Verdade e a Vida. Ninguém pode chegar até o Pai se não for por mim.
```

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 4: Eu te perdoarei, mesmo se você não perdoar os outros

Algo que você deveria saber sobre as palavras de Jesus é que Ele gastou muitas delas contando histórias. Ele escolheu compartilhar histórias por muitas razões, mas uma delas pode ter sido porque Ele sabia que as histórias são cheias de personagens que podemos nos enxergar — personagens que podem nos convidar para a história que Deus está contando através das nossas vidas.

Um das histórias de Jesus é contada em Lucas 15:8-10. Ele nos apresenta uma mulher que tinha 10 moedas de prata. As moedas que Ele menciona são dracmas gregas o que equivalia ao salário por um dia completo de trabalho cada uma. Então, a mulher perdeu uma moeda. Um dia completo de trabalho. Aí, Jesus nos pergunta, “Ela não procuraria a moeda por todos os lugares? Varreria a casa inteira procurando por ela?” Sim, Jesus. Ela procuraria, e nós procuraríamos.

Então, Jesus nos diz que ela encontrou a moeda. Oba! Novamente, Jesus fez uma pergunta. (Outra coisa em que Jesus gastou muitas palavras foi fazendo perguntas.) Ela não celebraria? Contaria até mesmo para os seus vizinhos e amigos? Quem sabe faria uma festinha? Talvez compraria um café para a próxima pessoa da fila? Daí Jesus nos conta algo realmente incrível. Assim como a mulher celebrou a sua moeda, Deus e todos os seus anjos celebram quando uma única pessoa acerta sua vida com Deus.

Essa é uma bela história sobre a [[O que a Bíblia diz sobre a bondade de Deus|graça de Deus]] por nós, certo? Vamos virar a moeda. Aqui está outra história. Você passa 10 dias com alguém com que se importa muito. Talvez você esteja visitando a família. Ou então sejam 10 dias quaisquer do seu casamento. Os primeiros nove dias são ótimos, mas no décimo dia alguém te aborrece profundamente. Você “perde uma moeda”. Pergunta: Você já planejou uma celebração de amor para o momento em que eles se acertam? Ou você está ensaiando as maneiras como você vai lembrá-los de seus erros?

Essa história trabalha dos dois jeitos. Porque Jesus nunca disse, “Eu te perdoarei, mesmo se você não perdoar os outros”. Ele disse coisas como, “Perdoe e você será perdoado; perdoe setenta vezes sete (quantas vezes for preciso); e perdoe _em qualquer momento_ que você tiver _qualquer coisa_, contra _qualquer pessoa_”. Enquanto você lê as palavras de Jesus hoje, peça a Ele que te dê força e coragem para lidar com qualquer falta de perdão que você esteja preso. Além disso, tire um tempo para celebrar. Você é a moeda que Ele encontrou.

<p>&nbsp;</p>

**Ore:** _Jesus, quem eu preciso perdoar? Como eu deveria perdoá-los? Ajude-me a expandir a mesma graça que Você mostrou a mim._

<p>&nbsp;</p>

### Referência do dia 4

```ad-note
title: **Lucas 15:8-10** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>8</sup></small> — Ou imaginem ainda uma mulher que tendo dez moedas de prata venha a perder uma. Será que ela não acenderá um lampião, varrerá a casa e a procurará cuidadosamente até achá-la?

<small><sup>9</sup></small> E quando encontrar a moeda, chamará as suas amigas e vizinhas, e dirá: “Alegrem-se comigo, pois encontrei a moeda que tinha perdido!”

<small><sup>10</sup></small> Pois eu lhes digo que, da mesma maneira, há alegria na presença dos anjos de Deus por causa de um pecador que muda a sua forma de pensar e de viver.
```

```ad-note
title: **Marcos 11:25** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>25</sup></small> E, quando vocês estiverem orando, se tiverem alguma coisa contra alguém, perdoem a essa pessoa. Dessa forma, o Pai de vocês, que está no céu, também perdoará os seus pecados.
```

```ad-note
title: **Mateus 18:21-35** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>21</sup></small> Pedro, então, se aproximou de Jesus e lhe perguntou: — Senhor, se meu irmão continuar pecando contra mim, até quantas vezes eu devo perdoar-lhe? Até sete vezes?

<small><sup>22</sup></small> Jesus, porém, lhe respondeu: — Eu não lhe digo que até sete vezes, mas até setenta vezes sete.

<small><sup>23</sup></small> — Por isso o reino de Deus pode ser comparado a um rei que decidiu acertar as contas com os seus servos.

<small><sup>24</sup></small> Assim que ele começou, um homem que lhe devia milhões de moedas de prata foi levado até ele.

<small><sup>25</sup></small> O homem, entretanto, não tinha como pagar a dívida. O rei, então, mandou que ele fosse vendido, juntamente com sua mulher, seus filhos e tudo o que possuía. Dessa forma a sua dívida seria paga.

<small><sup>26</sup></small> O devedor, porém, se ajoelhou aos pés do rei e lhe implorou: “Tenha paciência comigo e eu lhe pagarei tudo que lhe devo”.

<small><sup>27</sup></small> — O rei sentiu pena daquele servo e lhe perdoou a dívida, deixando-o ir embora.

<small><sup>28</sup></small> Quando o servo saiu, encontrou um outro servo que lhe devia cem moedas de prata. Ele agarrou este outro servo pelo pescoço e, sufocando-o, lhe dizia: “Pague-me o que você me deve”.

<small><sup>29</sup></small> Este outro servo, ajoelhando-se aos pés dele, lhe implorou: “Tenha paciência comigo e eu pagarei tudo o que lhe devo”.

<small><sup>30</sup></small> — Ele, entretanto, não concordou. Ao contrário, jogou o servo na prisão até que ele pagasse o que lhe devia.

<small><sup>31</sup></small> Quando os outros servos viram o que tinha acontecido, ficaram muito tristes e foram à procura do seu senhor para lhe contar o que tinha acontecido.

<small><sup>32</sup></small> Então o senhor chamou o primeiro servo e lhe disse: “Você é um mau servo! Você me implorou e eu perdoei toda a sua dívida para comigo.

<small><sup>33</sup></small> Por que você não teve pena do outro servo assim como eu tive pena de você?”

<small><sup>34</sup></small> O senhor ficou com muita raiva e mandou que aquele servo fosse castigado até que lhe pagasse toda a dívida.

<small><sup>35</sup></small> É assim que meu Pai que está no céu fará com vocês, se cada um não perdoar de coração ao seu irmão.
```

```ad-note
title: **Lucas 6:27-37** • <abbr title="Versão Fácil de Ler">VFL</abbr>
collapse: closed
icon: bible

<small><sup>27</sup></small> Mas eu digo a vocês que estão me escutando: Amem os seus inimigos e façam o bem a quem os odeiam.

<small><sup>28</sup></small> Falem bem daqueles que os amaldiçoam e orem por aqueles que maltratam vocês.

<small><sup>29</sup></small> Se alguém lhe bater num lado do rosto, vire-lhe também o outro lado. Se alguém pegar a sua capa, deixe que leve também a sua túnica.

<small><sup>30</sup></small> Dê para todo aquele que lhe pedir alguma coisa e, se alguém levar o que é seu, não peça de volta.

<small><sup>31</sup></small> Tratem as outras pessoas da mesma maneira que vocês gostariam de ser tratados por elas.

<small><sup>32</sup></small> — Pois, se vocês amarem somente aqueles que os amam, que louvor vocês esperam receber? Até mesmo os pecadores amam aqueles que os amam!

<small><sup>33</sup></small> E se vocês fizerem o bem somente para aqueles que fazem o bem para vocês, que louvor vocês esperam receber? Até mesmo os pecadores fazem isso.

<small><sup>34</sup></small> Se vocês emprestarem somente para aqueles que vocês acham que vão pagar, que méritos vocês esperam ganhar? Até os pecadores emprestam a pecadores para receberem de volta a mesma quantia.

<small><sup>35</sup></small> Ao contrário, amem os seus inimigos e façam o bem a eles. Emprestem e não esperem receber de volta o que emprestaram, pois assim a sua recompensa será grande e vocês serão chamados filhos do Altíssimo. Façam isto porque Deus também é bom para com os ingratos e maus.

<small><sup>36</sup></small> Sejam misericordiosos como o Pai de vocês é misericordioso.

<small><sup>37</sup></small> Não julguem os outros para que Deus não julgue vocês. Não condenem os outros para que Deus não condene vocês. Perdoem os outros para que Deus perdoe vocês.
```

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 5:

Magna pars studiorum, prodita quaerimus.

<p>&nbsp;</p>

### Referência do dia 5

```ad-note
title: **João 10:1-18** • <abbr title="Nova Versão Internacional">NVI</abbr>
collapse: closed
icon: bible

<small><sup>1</sup></small> 

```

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 6: 

Magna pars studiorum, prodita quaerimus.

<p>&nbsp;</p>

### Referência do dia 6

```ad-note
title: **João 10:1-18** • <abbr title="Nova Versão Internacional">NVI</abbr>
collapse: closed
icon: bible

<small><sup>1</sup></small> 

```



<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

[Encontre vídeos, guias de discussão e muito mais \[em inglês\] sobre "Things Jesus Never Said" \[As Coisas que Jesus Nunca Disse\]](https://www.life.church/media/things-jesus-never-said/).

## 

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[As Coisas que Jesus Nunca Disse](https://my.bible.com/pt/reading-plans/14676-things-jesus-never-said)”_ | YouVersion  
- February 9, 2022

<p>&nbsp;</p>

<!-- ###### tags: `biblia` `contexto` `cristo` `cristianismo` `devocional` `jesus` `palavra` `youversion` -->

###### tags:

#biblia #contexto #cristo #RELIGIAO/Cristianismo #devocional #jesus #palavra #youversion 



