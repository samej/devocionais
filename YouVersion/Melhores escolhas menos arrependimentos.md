---
title: "Melhores escolhas, menos arrependimentos | Devocional • YouVersion"
author: "Andy Stanley"
dates:
  - created: "November 15, 2022"
  - modified: "November 21, 2022"
tags: [" RELIGIAO/Cristianismo/devocionais YouVersion/andy_stanley devocional escolhas arrependimento "]
aliases: ["melhores escolhas", "menos arrependimentos", "devocionais YouVersion"]
abstract: "Suas escolhas irão determinar a direção e a qualidade da sua vida. Embora ninguém planeje complicar a vida ao fazer más escolhas, muitas pessoas não planejam fazer boas escolhas. Neste plano de leitura de cinco dias, você encontrará cinco perguntas revolucionárias a serem feitas sempre que precisar tomar uma decisão."
link: "https://bible.com/r/5zM"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- MELHORES ESCOLHAS, MENOS ARREPENDIMENTOS ~ CRIADO EM 2022/11/15 ~ ATUALIZADO EM 2022/11/21 -->

# Melhores escolhas, menos arrependimentos

<p>&nbsp;</p>

<span class="text-center" align="center">![[Melhores escolhas menos arrependimentos.png]]</span>

<p>&nbsp;</p>

## Informações do Plano

5 dias

Suas escolhas irão determinar a direção e a qualidade da sua vida. Embora ninguém planeje complicar a vida ao fazer más escolhas, muitas pessoas não planejam fazer boas escolhas. Neste plano de leitura de cinco dias, você encontrará cinco perguntas revolucionárias a serem feitas sempre que precisar tomar uma decisão.

<p>&nbsp;</p>

> [!info]- Sobre o Editor
> Gostaríamos de agradecer ao **North Point Publishing** por fornecer este plano.  
Para mais informações, visite: [andystanley.com/better-decisions-fewer-regrets/](https://andystanley.com/better-decisions-fewer-regrets/)

<!-- [Sobre o Editor](https://andystanley.com/better-decisions-fewer-regrets/) -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 1 de 5

<p>&nbsp;</p>

O coração engana. Ele pode ser perigoso. É por isso que somos tão facilmente _convencidos_ e, às vezes, somos tão _convincentes_. Nós não só mentimos para nós mesmos; nós nos enganamos.

Mas existe uma maneira de examinar o nosso coração enganoso. Seguem aqui três dicas:

<p>&nbsp;</p>

### 1\. Em primeiro lugar, _admita_.

Quanto mais cedo você aceitar este fato desagradável e alarmante a seu respeito, mais rápido será capaz de desenvolver e manter uma desconfiança saudável, mais aberto estará para receber opiniões e conselhos que entram em confronto com a direção que o seu coração está seguindo, mais cauteloso será quando o vendedor que existe dentro de você tentar “vender o seu peixe” e mais fácil será reconhecer que aquilo que você está tentando justificar pode ser apenas uma mentira que você está dizendo a si mesmo.

<p>&nbsp;</p>

### 2\. Em segundo lugar, _pergunte_.

> [!question] Faça a seguinte pergunta:
> _“Estou sendo honesto comigo mesmo… de verdade?”_

Tenha uma conversa franca consigo mesmo. Olhe-se no espelho. Olhe em seus próprios olhos. Sim, falando sério… fique parado na frente do espelho e pergunte-se em voz alta… e diga o seu nome.

> [!question] _“Andy, você está sendo honesto consigo mesmo… de verdade?

E, então, fale a verdade… mesmo que você não esteja planejando _agir_ de acordo com essa verdade.

Você deve a si mesmo esse direito de _saber_, mesmo que tal conhecimento revele uma direção que você não deseje seguir. Não vai doer _saber_. Você precisa ser honesto consigo mesmo… verdadeiramente.

<p>&nbsp;</p>

### 3\. Em terceiro lugar, _seja curioso_…

Quando superamos o nosso próprio desconforto e ficamos curiosos para descobrir _por que_ estamos sentindo o que estamos sentindo… por que estamos determinados a fazer algo, chegamos à verdade.

Mas a maioria das pessoas não faz isso.

<p>&nbsp;</p>

### Não seja a maioria.

Seja uma pessoa curiosa. A curiosidade vai fazer você perceber os limites da sua ignorância. É aí que aprendemos. É aí que passamos a compreender. É aí que conseguimos enxergar o nosso preconceito e como a nossa visão é estreita. Quando algo se torna desagradável… (e isso sempre acontecerá)… quando algo se torna desagradável e você fica tentado a se afastar… e a voltar para aquilo em que você sempre acreditou, saiba disso…  Esse sentimento é o medo falando. É a insegurança falando. Você aprenderá muito pouco com ambos. Então mude a sua atitude e seja curioso. Se você fizer isso, aprenderá algo. No mínimo, aprenderá algo sobre si mesmo…

<p>&nbsp;</p>

### Versículos Base

> [!bible] [[Jeremias]] 17:9
> “Não há nada mais enganoso do que o coração; não tem esperança. Quem o conhece?

> [!bible] [[Tiago]] 1:14
> Mas cada um é tentado pelos seus próprios maus desejos, quando estes desejos o atraem e o seduzem.

> [!bible] [[Provérbios]] 28:26
> Quem só confia em si mesmo é insensato, mas o sábio foge do perigo.

> [!bible] [[Provérbios]] 11:3
> Os justos são guiados pela honestidade, os desonestos são destruídos pela falsidade.

> [!bible] [[Salmos|Salmo]] 51:6
> O Senhor ama o coração sincero, e me ensina a ter sabedoria.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 2 de 5

<p>&nbsp;</p>

Um dia você vai querer se sentar ou se recostar bem pertinho dos seus filhos ou netos e contar a eles a sua história. Toda a sua história. E ao passo que eles forem envelhecendo, você vai querer incluir pormenores e detalhes em vez de desejar que eles não lhe façam mais perguntas.

Caso seja solteiro, você vai querer se sentar à mesa, frente a frente com alguém com quem espera passar o resto da sua vida e contar a sua história. A sua história toda. Você vai querer ser o herói da sua própria história. Todos nós queremos ser. E daqui para a frente, você poderá ser o seu próprio herói. Mas, antes de contar a sua história, você vai precisar parar por um momento e se perguntar:

> [!question] Qual história vou querer contar?
> - Quando essa crise, essa oportunidade, essa tentação fizer parte do passado… quando ela se tornar apenas mais uma lembrança… qual história vou querer contar?
> - Qual das opções disponíveis você quer que faça parte da sua história?

Cada escolha que você fizer entrará para a história da sua vida. Cada escolha referente aos seus relacionamentos, às suas finanças e à sua carreira, bem como as consequências acerca dessas escolhas, se tornará, — definitivamente — parte da sua história. Todos nós já vivemos o bastante para termos alguns capítulos que gostaríamos de apagar. Sem dúvida você tem algumas passagens da sua vida que gostaria de reescrever. Todos nós temos. Chamamos isso de _arrependimento_. Mas é bem provável que as escolhas que levaram você aos seus maiores arrependimentos poderiam ter sido evitadas se você tivesse parado para se perguntar:

> [!question] Qual história vou querer contar?

Daqui para a frente, escreva uma história que valha a pena contar. Escreva uma história a qual você se orgulhe de contar. Se você estiver prestes a tomar uma decisão agora, pare e pergunte a si mesmo:

> [!question] Das opções disponíveis, qual escolha desejo que entre definitivamente para a história da minha vida?

<p>&nbsp;</p>

### Versículos Base

> [!bible] [[Provérbios]] 13:22
> O homem bom deixa herança para os netos, mas a riqueza dos pecadores está reservada para os justos.

> [!bible] [[Provérbios]] 20:7
> O justo vive honestamente; felizes serão os seus filhos!

> [!bible] [[Deuteronômio]] 30:9-10
> ⁹ Então o SENHOR, seu Deus, fará com que tenham êxito em tudo o que fizerem. O SENHOR irá abençoar vocês com muitos filhos, os seus animais com muitas crias, e a sua terra com grandes colheitas. O SENHOR fará com que vocês tenham êxito em tudo e sejam felizes, assim como ele fez com os seus antepassados.
> ¹⁰ Serão abençoados por terem obedecido ao SENHOR, seu Deus, cumprindo os seus mandamentos e decretos que estão escritos neste Livro da Lei, e por terem voltado para o SENHOR, seu Deus, com todo o coração e com toda a alma.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 3 de 5

<p>&nbsp;</p>

Às vezes… na verdade, mais vezes do que gostaríamos de admitir, uma escolha que estamos considerando gera uma certa tensão dentro de nós. Algo sobre essa escolha nos desagrada. Algo sobre essa escolha parece não ser exatamente a melhor opção. Essa sensação nos faz pausar. Essa sensação nos incomoda. Essa sensação nos faz hesitar. E, muitas vezes, não fazemos ideia do porquê.

Por vezes, os especialistas chamam esse fenômeno de _sinal de alerta_, um sentimento interno de “não sei por quê, mas algo me diz que essa não é a melhor escolha”. Quando isso acontecer, faça uma pausa e preste bem atenção à tensão.

Não a ignore.

Não a menospreze.

Faça uma pausa e pergunte a si mesmo:

> [!question] O que está me incomodando acerca dessa escolha?

Essa hesitação interna, esse sinal de alerta, muitas vezes é a maneira de Deus de nos levar a uma outra direção. Pessoas obedientes a Deus não tentam brincar de Deus. Não tentam prever os resultados de suas ações. Em vez disso, elas se submetem. Obedecem. Seguem a vontade do Senhor. Por mais que desejem alguma coisa, elas querem colocar a cabeça no travesseiro à noite sabendo que estão em paz com o seu Pai celestial. E com razão. É como o meu pai gosta de dizer:

> [!quote] Deus assume total responsabilidade pela vida totalmente entregue a Ele.

Por isso, preste bem atenção à tensão. Caso contrário, você poderá se encontrar do outro lado, depois de já ter feito alguma escolha e querer mudar de ideia sobre a decisão tomada.

Portanto, se houver certa tensão no seu coração, algo que o incomoda, algo difícil de identificar, ou talvez algo que outra pessoa tenha identificado sobre uma escolha que você está pensando em fazer, pare e [[JPAV365 - 2022-12-28 - Dia 362 - Primeiro Ouça|preste atenção]]. Essa tensão pode muito bem ser a maneira como Deus protege você. Pode ser o meio que Ele está usando para ajudar você a não fazer uma escolha da qual você possa se arrepender mais tarde. Cada vez que você estiver prestes a tomar uma decisão, especialmente uma que o pegue de surpresa, pergunte-se:

> [!question] Há alguma tensão que mereça a minha atenção?

Não a ignore. Não a menospreze. Deixe que essa tensão o incomode até que você compreenda o por quê ela o incomoda.

<p>&nbsp;</p>

### Versículos Base

> [!bible] [[Isaías]] 30:21
> Quando você se desviar para a esquerda ou para a direita, ouvirá uma voz que lhe dirá: “Este é o caminho que deve seguir”.

> [!bible] [[Jeremias]] 33:3
> “Judá, me chame, que eu responderei e lhe contarei segredos grandiosos e inimagináveis que você não conhece.

> [!bible] [[1-2 Crônicas|1 Crônicas]] 16:11
> Busquem forças no SENHOR; procurem sempre pela sua ajuda.

> [!bible] [[Tiago]] 4:7
> Portanto, submetam-se a Deus. Resistam ao Diabo e ele fugirá de vocês.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 4 de 5

<p>&nbsp;</p>

O comando do apóstolo Paulo em [[Efésios]] 5:17 de “compreender qual é a vontade de Deus” é uma exortação para _enfrentarmos_ aquilo que sabemos, em nosso coração, que devemos fazer. ==Somos mestres na arte da autossabotagem.== Então Paulo, fazendo bom uso da gramática de sua época, salta das páginas do livro, pega a gente pelo colarinho, nos puxa para bem perto e grita:

> [!attention] Pare de brincar! Pare de fingir. Pare de racionalizar! Enfrente e faça aquilo que você sabe que deve fazer! Pergunte e aceite a resposta!

Essa foi a última tentativa de Paulo para nos fazer admitir e agir sobre o que sabemos que devemos fazer.

Por mais que essa atitude pareça simples, a verdade é que a maioria das pessoas não faz a escolha sábia a menos que ela seja a sua única opção. Quando isso acontece, elas já estão tentando recuperar algo que perderam ou que estão prestes a perder. Assim como você, tenho amigos que mudaram seus hábitos alimentares pouco saudáveis da noite para o dia depois de se depararem com o câncer ou com uma doença cardíaca. Um nódulo, uma obstrução ou uma falta de ar nos forçam a encarar aquilo que recusamos a reconhecer durante anos. Esses tipos de descoberta geralmente nos levam a uma disciplina que desafia a morte.

Mas por que esperamos?

Aproveitando as palavras de Paulo, esperamos porque não estamos dispostos a _enfrentar_ aquilo que sempre soubemos. Quando paramos de mentir para nós mesmos, os resultados são incríveis. Todos que conheço que passaram por uma transformação depois de levarem um susto relacionados à saúde, dizem a mesma coisa:

> [!tip] Eu deveria ter feito essas mudanças anos atrás.

Traduzindo: “Durante anos me recusei a enfrentar aquilo que eu sabia ser verdade em meu coração”.

A falência pode surtir o mesmo efeito. Ou o gatilho pode ser uma gravidez inesperada, uma carta do advogado de seu esposo ou esposa, uma penalidade por dirigir alcoolizado ou uma viagem com um de seus filhos para que ele possa fazer um detox. E talvez isso seja preciso. Existe algo em algum lugar que tem o poder de chamar a sua atenção, algo que forçará você a encarar certos fatos! Infelizmente, esse algo pode ser assustador ou até deixar cicatrizes. Você pode acabar ficando com opções limitadas e oportunidades reduzidas. Então…

> [!question] Por que deixar certas coisas irem tão longe? Por que não enfrentar e fazer agora aquilo que você sabe ser verdade em seu coração?

Por que não perguntar:

> [!question] _Qual é a coisa sábia a se fazer?_

<p>&nbsp;</p>

### Versículos Base

> [!bible] [[Efésios]] 5:17
> Por isso não sejam tolos, mas procurem entender qual é a vontade do Senhor.

> [!bible] [[Provérbios]] 19:20
> Ouça os conselhos e aceite a correção; assim aprenderá a ser sábio.

> [!bible] [[Provérbios]] 3:5-6
> ⁵ Confie no SENHOR de todo o seu coração e não se apoie na sua própria inteligência.
> ⁶ Em tudo o que fizer, procure a sabedoria, e ela o guiará durante toda a sua vida.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

## Dia 5 de 5

<p>&nbsp;</p>

> [!question] O que o amor _exige_ de mim?

Essa pergunta esclarecedora, mas aterrorizante, deve servir para vigiarmos a nossa consciência. Ela deve ser um guia, como uma placa sinalizadora, uma bússola, enquanto navegamos pelas complexidades inevitáveis próprias de cada relacionamento. Deve informar como devemos agir na hora de namorar, educar os filhos, liderar, gerenciar e treinar. Deve traçar um perímetro em torno do que dizemos e fazemos enquanto cônjuges, colegas de trabalho e vizinhos.

Essa pergunta dá voz à vontade de Deus para a nossa vida sobre as questões onde a Bíblia, assim como todas as outras literaturas religiosas, se torna silenciosa. Ela preenche as lacunas com uma precisão inquietadora. Ela funciona bem onde as concordâncias falham. Anula a justificativa insípida: – Mas a Bíblia não nos diz que é errado \_\_\_\_\_\_\_\_\_. — Fecha brechas. Expõe a hipocrisia. É juiz e júri. É uma pergunta tão simples. Mas é, de maneira inescapável, extremamente exigente.

A nossa última pergunta, inspirada pelo mandamento de Jesus, “como eu os amei”, transpassa todos os cenários relacionais imagináveis. De vez em quando, todos nós somos tentados a nos perguntar ou imaginar o quão pouco estamos conseguindo lidar com certos relacionamentos… exatamente o que não queremos que a pessoa que está do outro lado da mesa pense a nosso respeito. Esse assunto requer uma explicação. Quando apresento esse conceito publicamente, muitas vezes peço aos ouvintes que memorizem o seguinte:

> [!hint] Quando você não tiver certeza do que dizer ou fazer, pergunte o que o amor exige de você.

Temos esse mandamento universal, inevitável e simples de Jesus. Devemos fazer aos outros o que o nosso Pai celestial fez por meio de Cristo. Ele fez o que era melhor para nós. Nós, por nossa vez, devemos fazer o que é melhor para os outros, até mesmo quando darmos menos do que o nosso melhor for considerado aceitável pelos outros.

Ame.

O amor preenche lacunas. O amor reduz o atrito causado pela nossa falta de visão ampla, pelo nosso conhecimento limitado e pelas nossas experiências que acabam dificultando a nossa capacidade de discernir. Há muita coisa que não sei. Há muita coisa que nunca vou entender. Mas minha ignorância não impede a minha capacidade de colocar os outros em primeiro lugar.

Portanto, se por um lado nem sempre sei no que acreditar e, ao mesmo tempo, a minha opinião sobre uma série de coisas continua a amadurecer e a mudar, quase sempre sei o que o _amor exige de mim_.

Aposto que você também sabe.

<p>&nbsp;</p>

### Versículos Base

> [!bible] [[Mateus]] 7:12
> — Portanto, tratem as outras pessoas da mesma maneira que vocês gostariam de ser tratados por elas. Este é o real significado da lei de Moisés e do ensino dos profetas.

> [!bible] [[João]] 13:34-35
> ³⁴ Eu lhes dou um novo mandamento: Amem-se uns aos outros. Vocês devem se amar uns aos outros da mesma forma como eu amei a vocês.
> ³⁵ Nisto todas as pessoas saberão que vocês são meus discípulos, se vocês se amarem uns aos outros.

> [!bible] [[Gálatas]] 5:22-23
> ²² Mas o Espírito produz: amor, alegria, paz, paciência, ternura, bondade, fidelidade,
> ²³ humildade e [[Versículos sobre Domínio Próprio|domínio próprio]]. Contra coisas como estas não há lei.

> [!bible] [[Coríntios#1 Coríntios]] 13:1-13
> ¹ Ainda que eu fale as línguas dos homens, e até a dos anjos, se eu não tiver amor, serei como um sino que ressoa, ou como um tambor que faz barulho.
> ² Ainda que eu tenha o dom de profetizar e conheça todas as coisas secretas de Deus e tenha todo o conhecimento, ainda que eu tenha uma fé tão grande que possa deslocar montanhas, se não tiver amor, eu não serei nada.
> ³ Ainda que eu dê todos os meus bens para alimentar os pobres e ainda que eu ofereça o meu próprio corpo para ser queimado em sacrifício, se eu não tiver amor, nada disso terá significado.<small>[^1]</small>
> ⁴ O amor do qual eu falo é paciente e amável. O amor não é ciumento, não exalta a si mesmo, não é orgulhoso.
> ⁵ O amor não é malcriado, não procura seus interesses, não se irrita facilmente, não guarda mágoas.
> ⁶ O amor não se alegra com o mal, mas alegra-se com a verdade.
> ⁷ O amor aceita todas as coisas com paciência, tem sempre confiança e esperança, e se mantém sempre firme.
> ⁸ O amor jamais acaba. Há dons de profetizar, mas eles desaparecerão. Há dons de falar em várias línguas, mas eles cessarão. Há o conhecimento, mas ele desaparecerá.
> ⁹ Estas coisas acabarão, porque tanto o conhecimento que agora temos como o que recebemos por meio da profecia ainda não estão completos.
> ¹⁰ Porém, quando vier a perfeição, aquilo que não está completo acabará.
> ¹¹ Quando eu era criança, falava como uma criança, pensava como uma criança, raciocinava como uma criança. Quando cheguei a ser homem, deixei de lado as coisas de criança.
> ¹² Agora nós vemos como se estivéssemos olhando para um espelho escuro. Mas, quando a perfeição vier, então veremos claramente. Agora meu conhecimento é incompleto. Mas, quando aquele tempo vier, conhecerei completamente, assim como sou conhecido por Deus.
> ¹³ Agora, pois, permanecem estas três coisas: a fé, a esperança e o amor. Porém, a maior delas é o amor.

[^1]: _ainda que eu ofereça (…) queimado em sacrifício_ Os melhores e mais antigos manuscritos têm: “ainda que eu ofereça o meu próprio corpo para ter do que me gloriar”.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

**Observação:** Salvo quando indicado, os trechos bíblicos utilizados aqui são da VFL — Versão Fácil de Ler.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Melhores escolhas, menos arrependimentos](https://www.bible.com/)”_ | por Andy Stanley • [[YouVersion]]
- November 15, 2022

<p>&nbsp;</p>

###### tags:

#RELIGIAO/Cristianismo/devocionais #YouVersion/andy_stanley #devocional #escolhas #arrependimento 

<p>&nbsp;</p>

