---
title: "Famintos por Deus | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Samuel Rodriguez"
dates:
  - created: "December 13, 2022"
  - modified: "December 15, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo "]
aliases: ["famintos por Deus", "fome espiritual", "nutrição espiritual"]
abstract: "Este plano de leitura escrito pelo Rev. Samuel Rodriguez foca em nossa fome espiritual e reconhece que nossas almas precisam regularmente de nutrição. Independente de com que frequência oramos, quantas passagens da Bíblia estudamos e memorizamos, o quão frequentemente participamos de eventos da igreja ou servimos àqueles que precisam, sempre há espaço para nos aproximarmos mais de Deus."
links:
  - "https://www.bible.com/pt/reading-plans/22183-hungry-for-god"
  - "https://www.bible.com/pt/reading-plans/22183-hungry-for-godday/1?segment=0"
  - "https://www.bible.com/pt/reading-plans/22183-hungry-for-godday/2?segment=0"
  - "https://www.bible.com/pt/reading-plans/22183-hungry-for-godday/3?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- FAMINTOS POR DEUS ~ CRIADO EM 2022/12/13 ~ ATUALIZADO EM 2022/12/15 -->

# Famintos por Deus

<p>&nbsp;</p>

<span class="text-center" align="center">![[Famintos por Deus.jpg]]</span>

<p>&nbsp;</p>

### Apresentação

Este plano de leitura escrito pelo Rev. Samuel Rodriguez foca em nossa fome espiritual e reconhece que nossas almas precisam regularmente de nutrição. Independente de com que frequência oramos, quantas passagens da Bíblia estudamos e memorizamos, o quão frequentemente participamos de eventos da igreja ou servimos àqueles que precisam, sempre há espaço para nos aproximarmos mais de Deus.

<p>&nbsp;</p>

#### EDITOR

Gostaríamos de agradecer a **Charisma House** por fornecer esse plano. Para mais informações (em inglês), acesse: [samuelrodriguezbooks.com](https://samuelrodriguezbooks.com/)

<!-- [Sobre o Editor](https://samuelrodriguezbooks.com/) -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### **Dia 1:** Nossa Necessidade de Alimento Espiritual

A nossa alma precisa de alimento espiritual para se desenvolver, da mesma forma que o nosso corpo precisa de comida para sobreviver. Mas só existe uma única fonte de alimento que pode nos nutrir, satisfazer, fortalecer e sustentar completamente: um relacionamento com o Deus vivo.

Um aspecto significativo da fome natural é que ela ocorre regularmente. Nosso corpo necessita de nutrição e energia de forma regular e contínua. Só porque ontem você estava com fome e comeu uma farta refeição que satisfez o seu apetite, não significa que você nunca mais sentirá fome. O seu corpo digere a comida que você come, transforma essa comida em combustível para o seu corpo, elimina o que não precisa e começa o ciclo novamente.

Esse ciclo repetitivo de fome e saciedade realmente parece intencional. Quando Deus providenciou o maná para o Seu povo na jornada para a Terra Prometida, Ele lhes dava apenas o suficiente para cada dia. Quando Jesus ensinou Seus seguidores a orar, Ele nos instruiu a pedir ao nosso Pai: “Dá-nos hoje o alimento que precisamos” ([[Mateus]] 6:11). Cristo mesmo providenciou tal refeição, quando abençoou os pães e peixes do almoço de um menino, e os multiplicou para alimentar mais de cinco mil pessoas que vieram ouvir o Seu ensino ([[Mateus]] 14:13–21).

A maioria de nós não caça nem colhe nosso alimento todos os dias. Nossos trabalhos nos fornecem mais do que apenas o nosso [[pão diário]]. No nosso mundo moderno, em nações desenvolvidas, muitos de nós temos armários de cozinha completos, despensas, geladeiras e freezers cheios de comidas que foram conservadas para preservar seu frescor. Alguns alimentos como o feijão, as nozes, os grãos e o arroz, assim como os enlatados, são até considerados “não perecíveis”, pois podem ser armazenados por meses e anos.

Durante a pandemia de COVID-19, muitas pessoas estocaram alimentos, porque temiam que os mercados e os produtores não tivessem o suficiente. Alimentos não perecíveis estavam muito caros porque ninguém sabia por quanto tempo o suprimento de alimentos poderia ser interrompido. Sem poder ir ao mercado buscar ingredientes desejados ou jantar nos nossos restaurantes favoritos, tivemos que nos virar com o que tínhamos ao nosso alcance. Mais uma vez, muitos de nós nos demos conta de um luxo que estávamos acostumados a ignorar.

Quando consideramos nossa fome espiritual, reconhecemos que as nossas almas também necessitam de nutrição regularmente. Independente de quantas vezes oramos, quantas passagens da Bíblia estudamos e memorizamos, com que frequência participamos de eventos na igreja ou servimos pessoas necessitadas, sempre há espaço para nos aproximarmos mais de Deus. Poderíamos passar o dia todo, todos os dias, orando e louvando ao Senhor e ainda assim não seria o suficiente para preencher a nossa necessidade da presença de Deus em nossas vidas.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

#### Referências

> [!Bible]- Mateus 6:11 • VFL
> Dê-nos hoje o alimento que precisamos.

<p>&nbsp;</p>

> [!Bible]- Mateus 14:13-21 • VFL
> ¹³ Quando Jesus ficou sabendo o que tinha acontecido, saiu dali num barco e foi sozinho para um lugar onde não tinha ninguém. Quando a multidão soube disso, deixou os povoados e o seguiu por terra.
> ¹⁴ Quando Jesus saiu do barco e viu a grande multidão, teve muita pena do povo e curou os doentes.
> ¹⁵ Ao anoitecer, os discípulos de Jesus se aproximaram e lhe disseram: — Não tem ninguém neste lugar e já é tarde. Despeça estas pessoas para que elas possam ir até as vilas mais próximas e comprem comida para si.
> ¹⁶ Jesus, porém, lhes disse: — Essa gente não precisa ir embora; por que vocês mesmos não lhes dão alguma coisa para comer?
> ¹⁷ Eles, no entanto, lhe responderam: — Mas tudo o que temos são cinco pães e dois peixes!
> ¹⁸ Jesus, então, lhes disse: — Tragam os pães e os peixes aqui.
> ¹⁹ Depois mandou que a multidão se sentasse na grama. A seguir, Jesus pegou os cinco pães e os dois peixes, olhou para o céu e agradeceu a Deus pelo alimento. Então, partiu os pães e os deu aos discípulos, os quais os distribuíram entre a multidão.
> ²⁰ Todos comeram e ficaram satisfeitos e os discípulos recolheram ainda doze cestos cheios dos pedaços que sobraram.
> ²¹ Os que comeram foram mais ou menos cinco mil homens, sem contar as mulheres e as crianças.

<p>&nbsp;</p>

> [!Bible]- Salmo 42:1 • VFL
> Assim como a corça procura pelas águas do rio quando tem sede, também eu procuro pelo Senhor, ó meu Deus.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### **Dia 2:** Apenas Deus Pode Satisfazer

Como seres espirituais eternos em corpos mortais temporais, fomos feitos para adorar. Criados à [[O que significa dizer que o homem é feito à imagem e semelhança de Deus|imagem de Deus]], desejamos estar conectados com Aquele que nos criou e que melhor nos conhece, assim como as crianças desejam estar conectadas com seus pais. Queremos pertencer e ser parte de algo significativo e importante, de uma causa que transcende nosso próprio ego, status, riqueza e fama. Queremos cumprir o propósito para o qual Deus nos criou, entregando-nos ao serviço para que outros possam experimentar [[Quais são os atributos de Deus na Bíblia|Sua graça, misericórdia, paz, alegria e abundantes bênçãos]].

Buscar esse tipo de sustento para nossas almas não significa que nossas vidas serão fáceis, confortáveis ou sem sofrimento. Pelo contrário, quanto mais conscientes nos tornamos de nosso desejo de amar, servir e obedecer a Deus, mais frequentemente sofremos as consequências de viver em [[O que é pecado segundo a Bíblia|um mundo decaído de pessoas pecadoras]]. Na verdade, Jesus incluiu essas pessoas em Sua lista de bênçãos, que geralmente chamamos de Bem-aventuranças, parte de Seu [[O que é o Sermão da Montanha (estudo)|Sermão da Montanha]]: “Bem-aventurados os que têm fome e sede de justiça, porque serão fartos” ([[Mateus]] 5:6, <abbr title="Almeida Revista e Atualizada">ARA</abbr>).

Em vez de esperar, sofrer e confiar no Senhor, muitas vezes é tentador resolver o problema com as próprias mãos quando nos sentimos vazios por dentro. Em vez de seguir Jesus e achegar-nos a Deus para o alimento da alma, cedemos à tentação e buscamos gratificação instantânea. Suspeito que muitas de nossas lutas emocionais resultam de nossas tentativas de encontrar alimento espiritual em uma dieta mundana. Perseguimos objetivos, relacionamentos, posses e realizações que acreditamos que nos farão felizes e contentes, apenas para obtê-los e descobrirmos que [[Agradecer Pela Dor|a dor permanece dentro de nós]]. O [[Quem foi Isaías|profeta Isaías]] expressou de forma simples e poética nosso anseio espiritual e a frustração que sentimos quando nossas próprias tentativas de nos satisfazer falham:

<p>&nbsp;</p>

> [!bible] [[Isaías]] 55:1-2 • NVI
> ¹ “Venham, todos vocês que estão com sede, venham às águas; e, vocês que não possuem dinheiro algum, venham, comprem e comam! Venham, comprem vinho e leite sem dinheiro e sem custo.
> ² Por que gastar dinheiro naquilo que não é pão e o seu trabalho árduo naquilo que não satisfaz? Escutem, escutem-me, e comam o que é bom, e a alma de vocês se deliciará na mais fina refeição.

<p>&nbsp;</p>

Como o profeta aponta, nada que possamos comprar nos satisfaz da mesma forma com que Deus enche o nosso coração e nutre a nossa alma. Este é o tipo de refeição que toca todos os nossos sentidos enquanto _“nos deliciamos com a mais fina refeição”_, sabendo que a [[O que a Bíblia diz sobre a bondade de Deus|bondade de Deus]] realmente nos satisfaz.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

#### Referências

> [!Bible]- Mateus 5:6 • VFL
> — Felizes são os que têm fome e sede de justiça, pois Deus fará com que fiquem satisfeitos.

<p>&nbsp;</p>

> [!Bible]- Isaías 55:1-2 • VFL
> ¹ “Venham! Venham beber desta água todos os que têm sede! Venham os que não têm dinheiro, comprem trigo e comam! Comprem vinho e leite de graça, sem terem que pagar!
> ² Por que gastam dinheiro para comprar o que não é a verdadeira comida? Por que trabalham tanto naquilo que não satisfaz completamente? Mas se ouvirem o que eu digo, comerão o que é bom e ficarão satisfeitos.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

### **Dia 3:** Defina o Seu Padrão em Jesus

Acredito que nós não somos nós mesmos, (nosso eu verdadeiro), quando nos contentamos com menos que o melhor de Deus. Permanecemos frustrados, com raiva, pessoas egoístas com um apetite insaciável mesmo com tudo que consumimos. Quando nos alimentamos do alimento espiritual e temos uma experiência íntima com Deus, descobrimos uma profunda satisfação que nada mais pode tocar.

Apesar disso, constantemente resistimos em obedecer os mandamentos de Deus, seguir as Suas regras, e praticar os hábitos espirituais que satisfazem profundamente. Ao invés disso, retornamos a velhos [[Hábitos e Vícios|vícios]] e prazeres destrutivos esperando encontrar o que estamos procurando e nos frustramos cada vez mais quando nossas tentativas vazias produzem mais desapontamento, desespero e desencorajamento. Retornamos para o que costumávamos fazer antes de encontrar Jesus e convidar o [[A Bíblia realmente afirma que Jesus Cristo e o Espírito Santo são Deus|Seu Espírito]] para dentro de nossas vidas, para aquilo que não satisfaz. Olhamos para os esforços do nosso passado ao invés de olhar para as promessas futuras de Deus.

Talvez não haja uma ilustração melhor desse fenômeno do que quando [[Quem foram os 12 apóstolos|os discípulos de Jesus]] foram pescar uma noite, pouco tempo depois que seu Mestre havia ressuscitado dos mortos. (ver João 21:1-11.)

Eu acho interessante, — mas não surpreendente —, que depois que Jesus foi crucificado, enterrado, e ressuscitou, Seus discípulos foram fazer o que sabiam melhor: pescar. Tinha só um problema — eles não eram mais quem costumavam ser! Ao invés de aproveitarem a plenitude do Salvador ressuscitado, a alegria de serem novas criaturas em Cristo, eles voltaram para as suas configurações originais.

Todo nós temos configurações originais, mesmo que esse não seja o termo que usamos. No seu smartphone, notebook, no computador do carro, assim como tantos eletrodomésticos e sistemas em sua casa, você tem o kit operacional original instalado conhecido como configurações originais. Se um vírus malicioso ataca ou se algo dá errado, você sempre pode _resetar_ o aparelho para as configurações originais. Elas são básicas, familiares, e estavam ali desde que o sistema foi criado.

Os discípulos voltaram a fazer o que eles faziam antes de conhecer Jesus. Eles voltaram às suas configurações originais. E quais foram os resultados? Eles não conseguiram nada!

Eles experimentaram o que acontece quando voltamos para nosso estilo de vida antigo, quando retornamos para como vivíamos antes de conhecer a Cristo: _Não conseguimos nada._ Por que? É muito simples: não tem nada lá atrás!

Não há nada de grande valor quando você volta a fazer o que fazia antes de conhecer Jesus. Não há nada no seu comportamento antigo, não há nada no seu relacionamento anterior, não há nada no seu antigo trabalho. Não há nada no seu antigo modo de pensar.

Se Deus te tirou dali, por que você iria querer voltar?

Se você quer que a sua fome te ajude a prosperar no poder do Deus vivo, então é hora de mudar suas configurações originais para Jesus! Não volte às rotinas de pecado e aos seus velhos pensamentos. Não volte para uma dieta de prazeres do mundo e de posses vazias. _Mude suas configurações originais!_

A Bíblia nos avisa claramente, _“Não fiquem lembrando das coisas passadas, nem pensem nas coisas antigas”_ ([[Isaías]] 43:18).

Quando você tiver a real determinação de que nunca olhará para o passado, pensará no passado, ou falará do passado, então o futuro glorioso de Deus estará aberto para você! A sua fome é preenchida com alegria, paz, esperança, propósito, e amor incondicional.

Se você quer prosperar na mesa de abundância do Senhor, então faça essa declaração: Eu não voltarei atrás. Diga isso em alta voz e seja intencional: _**Eu não voltarei para às minhas configurações originais. Ao invés disso, eu continuarei a buscar Deus com todo o meu coração!**_

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

#### Referências

> [!bible]- João 21:1-11 • VFL
> ¹ Depois disso, Jesus apareceu outra vez aos seus discípulos perto do Mar de Tiberíades. Foi assim:
> ² Estavam juntos [[Quem foi Pedro|Simão Pedro]], Tomé (chamado Dídimo), Natanael (de Caná da Galileia), os dois filhos de Zebedeu e outros dois discípulos.
> ³ Simão Pedro disse aos outros: — Vou pescar. Os outros discípulos disseram: — Nós também vamos com você. Então eles foram e entraram no barco, mas naquela noite não pegaram nada.
> ⁴ De manhã, quando começou a clarear, Jesus estava na praia, mas os discípulos não sabiam que era ele.
> ⁵ Jesus perguntou a eles: — Amigos, vocês não pescaram nada, não é verdade? E eles responderam: — É verdade.
> ⁶ Então Jesus disse a eles: — Joguem a rede do lado direito do barco e vocês encontrarão alguma coisa. Eles jogaram a rede e logo depois já não podiam puxá-la para dentro do barco por causa da grande quantidade de peixes.
> ⁷ [[Quem foi o discípulo amado|O discípulo que Jesus amava]] disse a Pedro: — É o Senhor! Quando Pedro o ouviu dizer isto, amarrou o roupão à sua volta (pois o tinha tirado) e se jogou na água.
> ⁸ Os outros discípulos continuaram no barco, arrastando a rede cheia de peixes. Eles não estavam muito longe da praia, apenas a uns cem metros.
> ⁹ Quando os discípulos desceram do barco, viram uma fogueira com peixes nas brasas, e pão.
> ¹⁰ Então Jesus disse: — Tragam alguns dos peixes que vocês acabaram de pescar.
> ¹¹ Simão Pedro entrou no barco e puxou a rede até à margem. A rede estava cheia com cento e cinquenta e três grandes peixes e, mesmo assim, ela não arrebentou.

<p>&nbsp;</p>

> [!bible]- Isaías 43:18 • VFL
> Por isso não se lembrem mais do que aconteceu antes, nem pensem mais no passado.

<p>&nbsp;</p>

> [!bible]- [[Filipenses]] 3:13-14 • VFL
> ¹³ Irmãos, quanto a mim, eu não acho que já conquistei esse prêmio. Porém uma coisa eu faço: eu me esqueço das coisas que ficaram para trás, e me esforço em alcançar as que estão diante de mim.
> ¹⁴ E assim eu prossigo para o alvo, a fim de conquistar [[O Que é a Salvação Eterna Como Consegui-la|o prêmio que Deus nos chamou para receber em Cristo Jesus]].

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Famintos por Deus](https://www.bible.com/pt/reading-plans/22183-hungry-for-god)”_ | por [Samuel Rodriguez](https://samuelrodriguezbooks.com/) • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

