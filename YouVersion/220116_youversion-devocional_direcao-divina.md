---
title: "Direção Divina | YouVersion Devocional"  
author: "Craig Groeschel"  
create: "January 13, 2022"  
update: "January 23, 2022"  
tags: ["devocional", "YouVersion", "BIBLIA", "Direção Divina", "Craig Groeschel",]  
abstract: "Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a sabedoria de Deus para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar."  
links:
    - https://gist.github.com/24dac054c0b10999c47cfc06f60f272f  
    - https://gist.io/@SamejSpenser/24dac054c0b10999c47cfc06f60f272f  
    - https://hackmd.io/@SamejSpenser/HytTIUU6t  
    - https://my.bible.com/pt/reading-plans/3670-divine-direction  
    - https://samej.com.br/XXXXXXXXXXXXXXXX  
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"  
---

<!-- LINK EXTERNO PARA O CSS -->

<link rel="stylesheet" type="text/css" href="~/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="/home/samej/Dropbox/Draft/Gists-Privadas/CSS-geral/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://github.com/SamejSpenser/HPnews/blob/master/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="https://www.dropbox.com/s/migjxzkp82d3fue/stylesheet-geral.css" />

<link rel="stylesheet" type="text/css" href="../media/stylesheet-geral.css" />

<!-- LINK DO CSS NA PASTA DO EPSILON NO SMARTPHONE -->

<link rel="stylesheet" type="text/css" href="file:///storage/emulated/0/Epsilon/media/stylesheet-geral.css" />

<!-- DEVOCIONAL: “DIREÇÃO DIVINA” ~ CRIADO EM 2022/01/16 ~ ATUALIZADO EM 2022/01/23 -->

<center><span align="center meio">

# Direção Divina <br />YouVersion Devocional

![BANNER: “DIVINE DIRECTION - 7 DECISIONS/THAT WILL CHANGE YOUR LIFE | Craig Groeschel”](https://s3.amazonaws.com/yvplans/3670/720x405.jpg)

</span></center>

<p>&nbsp;</p>

> Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você se tornasse um especialista ao fazer essas escolhas? No Plano Bíblico Direção Divina, o autor best-seller do New York Times e Pastor Sênior da Life\.Church, Craig Groeschel, encoraja você com sete princípios de seu livro Direção Divina para ajudá-lo a encontrar a [[Entendendo a sabedoria de Deus|sabedoria de Deus]] para as decisões diárias. Descubra a direção espiritual que você precisa para viver uma história que honre a Deus e que você vai amar contar.

<p>&nbsp;</p>

## ÍNDICE

<!-- MarkdownTOC -->

- [DIA 1: COMEÇAR](#dia-1-come%C3%87ar)
    + [Dia 1: Referências](#dia-1-refer%C3%AAncias)
- [DIA 2: PARAR](#dia-2-parar)
    + [Dia 2: Referências](#dia-2-refer%C3%AAncias)
- [DIA 3: PERMANECER](#dia-3-permanecer)
    + [Dia 3: Referências](#dia-3-refer%C3%AAncias)
- [DIA 4: IR](#dia-4-ir)
    + [Dia 4: Referências](#dia-4-refer%C3%AAncias)
- [DIA 5: SERVIR](#dia-5-servir)
    + [Dia 5: Referências](#dia-5-refer%C3%AAncias)
- [DIA 6: CONECTAR-SE](#dia-6-conectar-se)
    + [Dia 6: Referências](#dia-6-refer%C3%AAncias)
- [DIA 7: CONFIAR](#dia-7-confiar)
    + [Dia 7: Referências](#dia-7-refer%C3%AAncias)
- [NOTAS](#notas)

<!-- /MarkdownTOC -->

<p>&nbsp;</p>

<a id="dia-1-come%C3%87ar"></a>
## DIA 1: COMEÇAR

Todos os dias fazemos escolhas que definem nossas vidas. Como seria a vida se você deixasse os princípios divinos guiarem essas escolhas? Na próxima semana, começaremos a explorar sete princípios do meu livro _Direção Divina_ para ajudá-lo a encontrar a [[Entendendo a sabedoria de Deus|sabedoria de Deus]] para as decisões diárias.

Se alguém pedisse a você para contar a história de sua vida, o que você diria?

Você pode começar com o lugar onde nasceu e como foi criado. Pode mencionar seu primeiro amor. Talvez você falasse sobre a grande mudança que sua família fez ou quando se formou na faculdade. Se você é casado, pode descrever como conheceu seu cônjuge. Se não é casado, pode descrever o motivo. Se você é pai ou mãe, pode buscar algumas fotos em seu telefone e mostrar sua família. Ou talvez você descreva sua carreira. O que há na sua história?

A maioria de nós têm capítulos que preferimos não compartilhar com ninguém. Talvez você tenha acabado em algum lugar onde nunca quis estar. Você não queria explodir, mas explodiu. Tomou decisões que te levaram mais longe do que pretendia ir. Você fez algumas coisas que lhe custaram mais do que pensou que teria que pagar. Você machucou as pessoas. Comprometeu seus valores. Quebrou promessas. Você fez coisas que acha que não pode desfazer.

**Sua história não acabou. Não é tarde demais para mudar a história que você contará um dia.**

Há boas notícias: sua história ainda não acabou. Não é tarde demais para mudar a história que você contará um dia. Independentemente do que você fez (ou não fez), seu futuro não está escrito. Você tem mais vitórias para vencer, mais amigos para conhecer, mais uma diferença a fazer, mais da [[O que a Bíblia diz sobre a bondade de Deus|bondade de Deus]] para experimentar. Quer goste ou não do enredo até agora, com a ajuda de Deus, você pode transformar sua história em algo que você tenha orgulho de compartilhar.

Aqui está uma maneira de mudar a sua história: _comece_ algo novo.

Não importa o quanto incerto, com medo ou preso você possa se sentir agora, sua história continua hoje. O que você vai começar hoje? Orar diariamente com seu cônjuge? Ler um plano bíblico YouVersion todos os dias? Ir para o aconselhamento para lidar com um problema não resolvido? Viver com mais generosidade? Servir na igreja ou na comunidade? Agora é um bom momento para escrever. Abra suas anotações e registre os pensamentos. Não pense demais. Mas reserve um momento para colocá-los no papel. Apenas uma frase ou duas.

**Pergunte a si mesmo:** O que preciso começar a fazer para avançar em direção a história de vida que quero contar?

<p>&nbsp;</p>

<a id="dia-1-refer%C3%AAncias"></a>
### Dia 1: Referências

> “<small>^17^</small> Portanto, se alguém está em Cristo, é nova criação. As coisas antigas já passaram; <abbr title="5:17 Vários manuscritos dizem “eis que tudo se fez novo!”.">eis que surgiram coisas novas!</abbr>”[^1]  
> [2 Coríntios 5:17](https://my.bible.com/pt/bible/129/2CO.5.17.NVI) • NVI.

> “<small>^4^</small> Não mais a chamarão abandonada, nem desamparada à sua terra. Você, porém, será chamada <abbr title="62:4 Hefzibá significa o meu prazer está nela.">Hefzibá</abbr>,[^2] e a sua terra, <abbr title="62:4 Beulá significa casada.">Beulá</abbr>,[^3] pois o S<small>ENHOR</small> terá prazer em você, e a sua terra estará casada.”  
> [Isaías 62:4](https://my.bible.com/pt/bible/129/ISA.62.4.NVI) • NVI.

> “<small>^22^</small> Quanto à antiga maneira de viver, vocês foram ensinados a despir-se do velho <abbr title="4:22 Isto é, da velha vida dos não regenerados.">homem</abbr>,[^4] que se corrompe por desejos enganosos,  
> <small>^23^</small> a serem renovados no modo de pensar e  
> <small>^24^</small> a revestir-se do novo homem, criado para ser semelhante a Deus em justiça e em santidade provenientes da verdade.”  
> [Efésios 4:22-24](https://my.bible.com/pt/bible/129/EPH.4.22-24.NVI) • NVI.

> “<small>^18^</small> Esqueçam o que se foi; não vivam no passado.  
> <small>^19^</small> Vejam, estou fazendo uma coisa nova! Ela já está surgindo! Vocês não a reconhecem? Até no deserto vou abrir um caminho e riachos no ermo.”  
> [Isaías 43:18-19](https://my.bible.com/pt/bible/129/ISA.43.18-19.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

<a id="dia-2-parar"></a>
## DIA 2: PARAR

Uma das melhores decisões que você pode tomar quando está sentindo um impulso ou enfrentando um dilema de alto risco, é a decisão de _parar_. Dê um tempo. Ore pedindo orientação. Pense mais um pouco. Obtenha alguma [[Entendendo a sabedoria de Deus|sabedoria divina]] de pessoas que você confia e imagine os possíveis resultados. Então, pergunte a si mesmo: “Isso é algo que eu deveria parar completamente?”.

A maioria de nós têm boas intenções ou pelo menos algum tipo de justificativa para o que fazemos. E, no entanto, muitos de nós ficamos surpresos quando nos encontramos muito longe da direção que queremos seguir. As grandes mudanças em nossas vidas – tanto negativas quanto positivas – raramente acontecem sem uma série de decisões que se acumulam umas sobre as outras como dominós sem fim.

Você vê como parar pode ser uma das coisas mais produtivas que fazemos? Quando você para e faz um balanço de onde está e para onde quer ir, _então_ você pode decidir como avançar para o seu destino.

**O que você pode parar a fim de se aproximar da direção divina de Deus?**

Você está fazendo algo que o está levando a uma direção que você não quer ir (ou que Deus não quer que você vá)? O que você precisa parar completamente? Um vício em mídias sociais, álcool, pornografia, aprovação ou trabalho? Um relacionamento doentio? Uma atitude de julgamento? O que você pode parar a fim de se aproximar da direção divina de Deus? Trate cada escolha como se fosse o próximo passo em direção ao seu destino.

Quando um comportamento ou relacionamento nos leva em uma direção que sabemos estar distante da história que queremos contar, precisamos pausar não apenas para considerar as consequências, mas também para escolher parar de viajar na direção errada. Você provavelmente já ouviu a palavra “arrepender-se”. Um de seus significados literais é virar-se. Quando você se arrepende, para de se dirigir em uma direção e retorna para Deus e o caminho Dele para você.

Nesse sentido, parar de fato, significa caminhar em uma nova direção. Você pode precisar dar um passo em direção à responsabilidade, ao perdão, aos amigos certos ou a um novo lugar para morar.

<p>&nbsp;</p>

**Pergunte a si mesmo:**

1. Se eu fizer a escolha que estou considerando onde ela pode me levar?

2. O que posso parar a fim de me aproximar da direção divina de Deus?

<p>&nbsp;</p>

<a id="dia-2-refer%C3%AAncias"></a>
### Dia 2: Referências

> “<small>^1^</small> Portanto, também nós, uma vez que estamos rodeados por tão grande nuvem de testemunhas, livremo-nos de tudo o que nos atrapalha e do pecado que nos envolve e corramos com perseverança a corrida que nos é proposta,  
> <small>^2^</small> tendo os olhos fitos em Jesus, autor e consumador da nossa fé. Ele, pela alegria que lhe fora proposta, suportou a cruz, desprezando a vergonha, e assentou-se à direita do trono de Deus.”  
> [Hebreus 12:1-2](https://my.bible.com/pt/bible/129/HEB.12.1-2.NVI) • NVI.

> “<small>^19^</small> Arrependam-se, pois, e voltem-se para Deus, para que os seus pecados sejam cancelados,  
> <small>^20^</small> para que venham tempos de descanso da parte do Senhor, e ele mande o Cristo, o qual lhes foi designado, Jesus.”  
> [Atos 3:19-20](https://my.bible.com/pt/bible/129/ACT.3.19-20.NVI) • NVI.

>“<small>^13^</small> Quem esconde os seus pecados não prospera, mas quem os confessa e os abandona encontra misericórdia.”  
> [Provérbios 28:13](https://my.bible.com/pt/bible/129/PRO.28.13.NVI) • NVI.

> “ <small>^3^</small> Os mestres da lei e os fariseus trouxeram-lhe uma mulher surpreendida em adultério. Fizeram-na ficar em pé diante de todos  
> <small>^4^</small> e disseram a Jesus: “Mestre, esta mulher foi surpreendida em ato de adultério.  
> <small>^5^</small> Na Lei, Moisés nos ordena apedrejar tais mulheres. E o senhor, que diz?”  
> <small>^6^</small> Eles estavam usando essa pergunta como armadilha, a fim de terem uma base para acusá-lo. Mas Jesus inclinou-se e começou a escrever no chão com o dedo.  
> <small>^7^</small> Visto que continuavam a interrogá-lo, ele se levantou e lhes disse: “Se algum de vocês estiver sem pecado, seja o primeiro a atirar pedra nela”.  
> <small>^8^</small> Inclinou-se novamente e continuou escrevendo no chão.  
> <small>^9^</small> Os que o ouviram foram saindo, um de cada vez, começando pelos mais velhos. Jesus ficou só, com a mulher em pé diante dele.  
> <small>^10^</small> Então Jesus pôs-se em pé e perguntou-lhe: “Mulher, onde estão eles?  
> <small>^11^</small> Ninguém a condenou?” “Ninguém, Senhor”, disse ela. Declarou Jesus: “Eu também não a condeno. Agora vá e abandone sua vida de pecado”.”  
> [João 8:3-11](https://my.bible.com/pt/bible/129/JHN.8.3-11.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

<a id="dia-3-permanecer"></a>
## DIA 3: PERMANECER

Pensei muitas vezes em como minha vida seria diferente se desistisse quando quisesse. Minha história poderia se tornar algo como: “Sim, eu costumava pensar que deveria ser pastor, mas dei uma chance e as coisas não deram certo. É assim que acontece”.

Tenho certeza de que você terá que lutar com desafios ocasionais em diferentes épocas de sua vida: um chefe que você não acha que pode aguentar nem mais um dia, um relacionamento que de repente está te machucando, um sonho que está ficando sem recursos, uma mudança que falhou em suas expectativas. Quando você enfrenta dificuldades, é natural reconsiderar as grandes decisões que alteram a vida. Você pode fazer perguntas como essas.

- _Eu deveria me arriscar, sair do emprego e procurar outra coisa?_
- _Depois de minha esposa ter um caso -- é hora de seguir em frente?_
- _Eu realmente sou capaz de administrar um negócio? Devo cortar o mal pela raiz antes que as coisas piorem?_

Em cada um desses exemplos -- e com a maioria das principais escolhas da vida -- você está numa bifurcação crucial na estrada, e é hora de decidir: eu deveria _permanecer_ ou ir embora?

**Estou escolhendo desistir porque é a coisa certa ou porque parece que sair seria mais fácil?**

Muitas vezes, a melhor e mais recompensadora decisão que você pode tomar é seguir a rota mesmo quando seria mais simples se virar e ir embora. Não estou dizendo que você nunca precisará ir embora. Mas antes de decidir, pergunte a si mesmo: “Estou escolhendo desistir porque é a coisa certa ou porque parece que sair seria mais fácil?” Às vezes, o maior ato de fé é a fidelidade, permanecer onde você está plantado. Daqui a alguns anos você pode olhar para trás e agradecer a Deus por ter decidido ficar quando seria mais fácil ir embora.

Lembre-se, Deus fez você à Sua imagem e semelhança e Ele é o autor e consumador de sua história. Você não é um desistente, é um finalizador.

**Ore:** Deus há alguma coisa que eu esteja tentando deixar de lado? O Senhor quer que eu permaneça e termine? O Senhor vai me dar forças para prosseguir? Amém.

<p>&nbsp;</p>

<a id="dia-3-refer%C3%AAncias"></a>
### Dia 3: Referências

> “<small>^16^</small> Rute, porém, respondeu: “Não insistas comigo que te deixe e que não mais te acompanhe. Aonde fores irei, onde ficares ficarei! O teu povo será o meu povo e o teu Deus será o meu Deus!”  
> [Rute 1:16](https://my.bible.com/pt/bible/129/RUT.1.16.NVI) • NVI.

>“<small>^30^</small> Até os jovens se cansam e ficam exaustos, e os moços tropeçam e caem;  
> <small>^31^</small> mas aqueles que esperam no Senhor renovam as suas forças. Voam alto como águias; correm e não ficam exaustos, andam e não se cansam.”  
> [Isaías 40:30-31](https://my.bible.com/pt/bible/129/ISA.40.30-31.NVI) • NVI.

> “<small>^7^</small> Portanto, irmãos, sejam pacientes até a vinda do Senhor. Vejam como o agricultor aguarda que a terra produza a preciosa colheita e como espera com paciência até virem as chuvas do outono e da primavera.  
> <small>^7^</small> Sejam também pacientes e fortaleçam o seu coração, pois a vinda do Senhor está próxima.”  
> [Tiago 5:7-8](https://my.bible.com/pt/bible/129/JAS.5.7-8.NVI) • NVI.

> “<small>^9^</small> E não nos cansemos de fazer o bem, pois no tempo próprio colheremos, se não desanimarmos.”  
> [Gálatas 6:9](https://my.bible.com/pt/bible/129/GAL.6.9.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

<a id="dia-4-ir"></a>
## DIA 4: IR

Você sente algo novo acontecendo em sua vida? Você quer algo diferente? Mesmo se não sentir isso agora, é sempre uma boa ideia manter seu coração preparado para a mudança, porque ela é uma garantia.

Nós conversamos sobre _começar_, _parar_, e _permanecer_. Às vezes, a melhor decisão que você pode tomar para encontrar a direção de Deus é _ir_.

Muitas vezes somos chamados a manter a posição quando a pressão aumenta, mas muitas vezes precisamos arriscar. Você se sente inquieto onde você está? Deus pode ter plantado um desejo divino em você para servi-Lo de uma maneira surpreendente. Talvez Ele tenha te inspirado com um grupo específico de pessoas, uma ideia, um problema ou um lugar. Talvez ele esteja chamando você para ir. Siga esse palpite e veja onde Ele levará você. Aceite a aventura. A melhor maneira de dar um salto de fé é ter um bom começo de corrida.

Há uma história ótima no [[O Que é o Antigo Testamento|Antigo Testamento]] sobre Abrão e Sarai (que mais tarde tiveram os nomes mudados para Abraão e Sara) que descreve o "ir" perfeitamente. Em [[Gênesis]] 12, Deus falou com Abrão. Na época, ele estava morando em uma cidade chamada Harã, mas ele era de uma cidade chamada Ur dos caldeus. Na cidade natal de Abrão, Ur, o povo adorava um falso deus da lua chamado Nannar.

O importante é que o único Deus verdadeiro escolheu revelar-se a Abrão. Deus deu a ele um comando muito simples e direto: afaste-se de tudo que você já conheceu _“**Saia** da sua terra, do meio dos seus parentes e da casa de seu pai, e **vá** para a terra que eu lhe mostrarei.”_ [[Gênesis]] 12:1 NVI, ênfase minha.

Saia e vá.

**Para avançar em direção ao seu destino, você pode ter que se afastar da segurança.**

Para ir a outro lugar, você tem que sair de onde você está. Você tem que deixar o que é conhecido, confortável, previsível e fácil. Para avançar em direção ao seu destino, você pode ter que se afastar da segurança.

Quem sabe onde Deus vai conduzir sua história se você simplesmente deixar Ele te conduzir? Um dia, daqui a alguns anos, você vai olhar para trás e ver toda a história. O que vai ser? "Eu senti como se Deus estivesse me chamando, mas estava com medo, então não fiz nada." Ou você terá uma aventura cheia de fé para contar? A diferença é se você vai ou não quando Deus disser: "Vá."

**Pergunte:** O que Deus está me chamando para deixar? Onde ele está me chamando para ir?

<p>&nbsp;</p>

<a id="dia-4-refer%C3%AAncias"></a>
### Dia 4: Referências

> “<small>^1^</small> Então o Senhor disse a Abrão: “Saia da sua terra, do meio dos seus parentes e da casa de seu pai, e vá para a terra que eu lhe mostrarei.  
> <small>^2^</small> “Farei de você um grande povo, e o abençoarei. Tornarei famoso o seu nome, e você será uma bênção.  
> <small>^3^</small> Abençoarei os que o abençoarem e amaldiçoarei os que o amaldiçoarem; e por meio de você todos os povos da terra serão abençoados”.  
> <small>^4^</small> Partiu Abrão, como lhe ordenara o Senhor, e Ló foi com ele. Abrão tinha setenta e cinco anos quando saiu de Harã.”  
> [Gênesis 12:1-4](https://my.bible.com/pt/bible/129/GEN.12.1-4.NVI) • NVI.

> “<small>^15^</small> E disse-lhes: “Vão pelo mundo todo e preguem o evangelho a todas as pessoas.”  
> [Marcos 16:15](https://my.bible.com/pt/bible/129/MRK.16.15.NVI) • NVI.

> “<small>^17^</small> Portanto, pensem nisto: Quem sabe que deve fazer o bem e não o faz comete pecado.”  
> [Tiago 4:17](https://my.bible.com/pt/bible/129/JAS.4.17.NVI) • NVI.

> “<small>^8^</small> Eu o instruirei e o ensinarei no caminho que você deve seguir; eu o aconselharei e cuidarei de você.”  
> [Salmos 32:8](https://my.bible.com/pt/bible/129/PSA.32.8.NVI) • NVI.

> “<small>^13^</small> Irmãos, não penso que eu mesmo já o tenha alcançado, mas uma coisa faço: esquecendo-me das coisas que ficaram para trás e avançando para as que estão adiante,  
> <small>^14^</small> prossigo para o alvo, a fim de ganhar o prêmio do chamado celestial de Deus em Cristo Jesus.”  
[Filipenses 3:13-14](https://my.bible.com/pt/bible/129/PHP.3.13-14.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

<a id="dia-5-servir"></a>
## DIA 5: SERVIR

Servir aos outros não é natural para mim. Eu sou uma pessoa egocêntrica. Gosto das coisas do meu jeito. Não é algo de que tenha orgulho, mas infelizmente, é verdade.

Eu não sou o único. Todos nós podemos ser um pouco egocêntricos. Por natureza, somos pessoas egoístas. Pense nisso: você não precisa ensinar uma criança a ser egoísta. De acordo com Jesus, a vida não é toda sobre nós, mas ainda assim tudo em nossa cultura tenta nos diz para fazermos as coisas do nosso jeito.

Uma das maneiras mais rápidas de esquecer sobre Deus é ser consumido com o “eu”. Jesus tinha palavras bem diretas para aqueles que queriam segui-Lo. Ele disse: _“Se alguém quiser acompanhar-me, negue-se a si mesmo, tome a sua cruz e siga-me.”_ Mateus 16:24 NVI.

Deus quer que trilhemos o _Seu_ caminho. E ele não está falando sobre um pedaço de carne extra, nem sobre alface.

Falando em comida, Jesus fez uma declaração que deveria nos fazer parar antes de pedirmos o próximo hambúrguer. _"A minha comida... é fazer a vontade daquele que me enviou e concluir a sua obra."_ João 4:34 NVI.

Imagine poder dizer: "A minha comida é servir a Deus. Minha comida é agradá-lo. Minha comida é completar a tarefa que Deus me deu para fazer. Minha comida é fazer a vontade do meu Pai e concluir a Sua obra”. Esse é um tipo diferente de alimento. Isso é viver com direção divina.

**Quando toda a cultura diz: "Encha-se", Deus nos diz para encher outras pessoas.**

Quando as pessoas ao nosso redor dizem: “Consiga tudo o que puder! É tudo sobre você”, Deus quer que contribuamos em vez de consumirmos. Quando toda a cultura diz: "Encha-se", Deus nos diz para encher os outros. Deus não nos criou para sermos compradores. Ele nos criou para sermos doadores. Em vez de nos concentrarmos nos desejos, somos chamados a nos concentrar nas necessidades dos outros. Em vez de furar a fila, somos chamados a esperar no final. Deus nos criou para servir.

Esse tipo de vida vai mudar a sua história.

Pense nisso. As histórias que você ama relembrar são aquelas em que você ajudou o vizinho, se envolveu na igreja ou doou alguma coisa. Isso é porque fomos feitos para servir como Jesus fez na Terra. A decisão de servir pode não parecer natural para você. Nem sempre foi para mim. Mas percebi que servir não é algo que _fazemos_. Somos chamados para _ser_ servos. Porque quando servimos, nos tornamos como Cristo.

**Ore:** Deus, como o Senhor está me chamando para servir? Quem o Senhor está me chamando para servir? Onde o Senhor está me chamando para servir?

<p>&nbsp;</p>

<a id="dia-5-refer%C3%AAncias"></a>
### Dia 5: Referências

> “<small>^24^</small> Então Jesus disse aos seus discípulos: “Se alguém quiser acompanhar-me, negue-se a si mesmo, tome a sua cruz e siga-me.”  
> [Mateus 16:24](https://my.bible.com/pt/bible/129/MAT.16.24.NVI) • NVI.

> “<small>^32^</small> Mas ele lhes disse: “Tenho algo para comer que vocês não conhecem”.  
> <small>^33^</small> Então os seus discípulos disseram uns aos outros: “Será que alguém lhe trouxe comida?”  
> <small>^34^</small> Disse Jesus: “A minha comida é fazer a vontade daquele que me enviou e concluir a sua obra.”  
> [João 4:32-34](https://my.bible.com/pt/bible/129/JHN.4.32-34.NVI) • NVI.

> “<small>^11^</small> O maior entre vocês deverá ser servo.”  
> [Mateus 23:11](https://my.bible.com/pt/bible/129/MAT.23.11.NVI) • NVI.

> “<small>^35^</small> Em tudo o que fiz, mostrei a vocês que mediante trabalho árduo devemos ajudar os fracos, lembrando as palavras do próprio Senhor Jesus, que disse: ‘Há maior felicidade em dar do que em receber’.”  
> [Atos 20:35](https://my.bible.com/pt/bible/129/ACT.20.35.NVI) • NVI.

> “<small>^10^</small> Cada um exerça o dom que recebeu para servir os outros, administrando fielmente a [[O que a Bíblia diz sobre a bondade de Deus|graça de Deus]] em suas múltiplas formas.  
> <small>^11^</small> Se alguém fala, faça-o como quem transmite a palavra de Deus. Se alguém serve, faça-o com a força que Deus provê, de forma que em todas as coisas Deus seja glorificado mediante Jesus Cristo, a quem sejam a glória e o poder para todo o sempre. Amém.”  
> [1 Pedro 4:10-11](https://my.bible.com/pt/bible/129/1PE.4.10-11.NVI) • NVI.

> “<small>^13^</small> Irmãos, vocês foram chamados para a liberdade. Mas não usem a liberdade para dar ocasião à vontade da carne; ao contrário, sirvam uns aos outros mediante o amor.  
> <small>^14^</small> Toda a Lei se resume num só mandamento: “Ame o seu próximo como a si mesmo”.  
> [Gálatas 5:13-14](https://my.bible.com/pt/bible/129/GAL.5.13-14.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

<a id="dia-6-conectar-se"></a>
## DIA 6: CONECTAR-SE

Quando percebi que fui feito não só para servir na igreja, mas para servir aos outros como igreja, a conexão com as pessoas tornou-se primordial. Você não pode servir sem se conectar. E suas conexões mudarão as histórias que você contará amanhã. Isso tem sido verdade ao longo da história. Basta considerar o homem que escreveu mais de um terço do [[Novo Testamento]], o apóstolo Paulo.

Paulo nem sempre foi cristão. Antes de ser um seguidor de Jesus, ele era Saulo de uma cidade chamada Tarso, um sujeito furioso que perseguia e matava cristãos. Se você não gosta dos seguidores de Jesus, teria amado Saulo. Mas depois de tirar a vida daqueles que acreditavam que Jesus foi ressuscitado dentre os mortos, Paulo tornou-se um deles.

Sua mudança foi tão grande, tão radical, tão transformadora que Saulo (que teve o nome mudado para Paulo) imediatamente quis contar aos outros sobre Jesus. O problema era que nenhum cristão confiava nele e por razões óbvias.

O livro de Atos coloca isso de forma simples: _Quando \[Saulo\] chegou a Jerusalém, tentou reunir-se aos discípulos, mas todos estavam com medo dele, não acreditando que fosse realmente um discípulo._ Atos 9:26 NVI. Você não pode culpar os discípulos por seu ceticismo. Eu não gostaria que o cara que matou os cristãos no mês passado liderasse meu estudo bíblico! Você gostaria?

Paulo tinha um problema. Ele não tinha credibilidade com os outros cristãos. Então, ele procurou alguém que lhe desse a chance de compartilhar sua nova paixão. A decisão de Paulo de se conectar não mudou apenas a história dele; mudou a história. Veja bem, Paulo só precisava de um amigo para alterar o curso de seu destino. E esse amigo era um homem chamado [[Barnabé]].

**Você só precisa de uma amizade para mudar o curso da sua história.**

Hoje você lerá sobre quando [[Barnabé]] arriscou levar Paulo aos apóstolos, que eram os líderes do povo que Paulo estava tentando matar anteriormente. O que aconteceu? O novo amigo de Paulo, [[Barnabé]], apostou sua reputação nele. E por causa de [[Barnabé]], os outros discípulos deram a Paulo uma chance. O resto é história. Você só precisa de uma amizade para mudar o curso da sua história.

Você só precisa de amizade para ter um casamento melhor. Você só precisa confessar para superar um vício. Você só precisa de uma conversa para ficar em melhor forma. Você só precisa de um mentor para entender seus talentos e tornar-se um líder melhor.

**Pergunte a si mesmo:** O que preciso fazer para me conectar com as pessoas certas? Existe alguém de quem eu precise me desconectar?

<p>&nbsp;</p>

<a id="dia-6-refer%C3%AAncias"></a>
### Dia 6: Referências

> “<small>^26^</small> Quando chegou a Jerusalém, tentou reunir-se aos discípulos, mas todos estavam com medo dele, não acreditando que fosse realmente um discípulo.  
> <small>^27^</small> Então [[Barnabé]] o levou aos apóstolos e lhes contou como, no caminho, Saulo vira o Senhor, que lhe falara, e como em Damasco ele havia pregado corajosamente em nome de Jesus.  
> <small>^28^</small> Assim, Saulo ficou com eles e andava com liberdade em Jerusalém, pregando corajosamente em nome do Senhor.”  
> [Atos 9:26-28](https://my.bible.com/pt/bible/129/ACT.9.26-28.NVI) • NVI.

> “<small>^24^</small> Quem tem muitos amigos pode chegar à ruína, mas existe amigo mais apegado que um irmão.”  
> [Provérbios 18:24](https://my.bible.com/pt/bible/129/PRO.18.24.NVI) • NVI.

> “<small>^20^</small> Aquele que anda com os sábios será cada vez mais sábio, mas o companheiro dos tolos acabará mal.”  
> [Provérbios 13:20](https://my.bible.com/pt/bible/129/PRO.13.20.NVI) • NVI.

> “<small>^42^</small> Eles se dedicavam ao ensino dos apóstolos e à comunhão, ao partir do pão e às orações.  
> <small>^43^</small> Todos estavam cheios de temor, e muitas maravilhas e sinais eram feitos pelos apóstolos.  
> <small>^44^</small> Os que criam mantinham-se unidos e tinham tudo em comum.  
> <small>^45^</small> Vendendo suas propriedades e bens, distribuíam a cada um conforme a sua necessidade.  
> <small>^46^</small> Todos os dias, continuavam a reunir-se no pátio do templo. Partiam o pão em casa e juntos participavam das refeições, com alegria e sinceridade de coração,  
> <small>^47^</small> louvando a Deus e tendo a simpatia de todo o povo. E o Senhor lhes acrescentava diariamente os que iam sendo salvos.”  
> [Atos 2:42-47](https://my.bible.com/pt/bible/129/ACT.2.42-47.NVI) • NVI.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

<a id="dia-7-confiar"></a>
## DIA 7: CONFIAR

Moro em Oklahoma onde o clima pode mudar muito rápido e drasticamente. Um ano em março, tivemos o mais belo dia ensolarado de primavera e um aumento de 28°C. No dia seguinte, nevou três centímetros. Por mais dramática que essa mudança tenha sido, não é nada comparado à temporada de tornados. As tempestades parecem rodopiar do nada.

Assim como elas fazem em nossas vidas.

Conversei recentemente com uma senhora no jogo de futebol do meu filho que tem lutado contra alguns problemas de saúde. Ela explicou como esteve perto de Deus anos atrás e costumava estar muito envolvida em nossa igreja. Mas quando ela começou a passar por algumas provações muito sérias, ela questionou por que Deus permitia isso. Ela lutou contra as lágrimas quando disse: “Como posso adorar um Deus em quem não posso confiar?”.

**Vamos confiar que Deus é bom, mesmo quando a vida não é?**

A pergunta desta senhora atinge o centro de uma das maiores decisões da vida. Vamos confiar que Deus é bom mesmo quando a vida não é? Nossa resposta à dor e aos desafios diz muito sobre o futuro.

Por sua própria natureza, a fé exige confiança em algo -- ou em alguém -- que nem sempre é previsível ou compreensível pelos padrões humanos. Se formos honestos, a maioria de nós quer uma prova irrefutável da presença benevolente de Deus em nossas vidas.

Isso não é novo. Lembra-se da dúvida de Tomé? Depois que Jesus morreu na cruz e ressuscitou dos mortos, Tomé disse que não acreditaria a menos que visse a prova. Em vez de ficar furioso e deixá-lo de lado pela [[O cristão pode ir ao psicólogo ou isso é falta de fé O que a Bíblia diz?|falta de fé]], Jesus graciosamente mostrou a Tomé Suas mãos perfuradas.

E lembra-se dos discípulos na tempestade? _Levantou-se um forte vendaval, e as ondas se lançavam sobre o barco, de forma que este ia se enchendo de água._ Marcos 4:37 NVI. No meio da tempestade, os discípulos não estavam sozinhos. Marcos nos lembra no versículo seguinte que Jesus estava dormindo na popa do barco.

**Com Jesus no barco, as tempestades ainda podem te sacudir, mas você não vai afundar.**

Pessoas como você e eu, a senhora no jogo de futebol, Tomé e os discípulos tendem a pensar que não estaríamos passando por uma tempestade se Deus estivesse realmente conosco. Mas não é assim. Com Jesus no barco, as tempestades ainda podem sacudir, mas você não vai afundar. Ele está com você, tanto em um banho de primavera quanto no pior tornado imaginável.

Ele não está apenas com você, Ele é por você. E se Ele está com você, quem pode ser contra você? Confie em Deus na batalha que você está travando. Confie a Ele o futuro cônjuge. Confie a Ele os filhos. Confie a Ele a sua carreira. Confie a Ele a saúde. Confie a Ele as finanças.

Confie Nele sem reservas.

Ponto final.

**Ore:** Pai Celestial confio em você com o que vou começar e parar. Confio em você com onde eu vou ficar e ir. Confio em você o suficiente para dar a minha vida para servir e me conectar com as pessoas. E confio que você está presente com propósito no meio das tempestades da minha vida. Obrigado por estar comigo, guiando meus passos e me dando orientação divina. Amém.

<p>&nbsp;</p>

<a id="dia-7-refer%C3%AAncias"></a>
### Dia 7: Referências

> “<small>^24^</small> Tomé, chamado Dídimo, um dos Doze, não estava com os discípulos quando Jesus apareceu.  
> <small>^25^</small> Os outros discípulos lhe disseram: “Vimos o Senhor!” Mas ele lhes disse: “Se eu não vir as marcas dos pregos nas suas mãos, não colocar o meu dedo onde estavam os pregos e não puser a minha mão no seu lado, não crerei”.  
> <small>^26^</small> Uma semana mais tarde, os seus discípulos estavam outra vez ali, e Tomé com eles. Apesar de estarem trancadas as portas, Jesus entrou, pôs-se no meio deles e disse: “Paz seja com vocês!”  
> <small>^27^</small> E Jesus disse a Tomé: “Coloque o seu dedo aqui; veja as minhas mãos. Estenda a mão e coloque-a no meu lado. Pare de duvidar e creia”.  
> <small>^28^</small> Disse-lhe Tomé: “Senhor meu e Deus meu!”  
> <small>^24^</small> Então Jesus lhe disse: “Porque me viu, você creu? Felizes os que não viram e creram”.”  
> [João 20:24-29](https://my.bible.com/pt/bible/129/JHN.20.24-29.NVI) • NVI.

> “<small>^35^</small> Naquele dia, ao anoitecer, disse ele aos seus discípulos: “Vamos para o outro lado”.  
> <small>^36^</small> Deixando a multidão, eles o levaram no barco, assim como estava. Outros barcos também o acompanhavam.  
> <small>^37^</small> Levantou-se um forte vendaval, e as ondas se lançavam sobre o barco, de forma que este ia se enchendo de água.  
> <small>^38^</small> Jesus estava na popa, dormindo com a cabeça sobre um travesseiro. Os discípulos o acordaram e clamaram: “Mestre, não te importas que morramos?”  
> <small>^39^</small> Ele se levantou, repreendeu o vento e disse ao mar: “Aquiete-se! Acalme-se!” O vento se aquietou, e fez-se completa bonança.  
> <small>^40^</small> Então perguntou aos seus discípulos: “Por que vocês estão com tanto medo? Ainda não têm fé?”  
> <small>^41^</small> Eles estavam apavorados e perguntavam uns aos outros: “Quem é este que até o vento e o mar lhe obedecem?”.”  
> [Marcos 4:35-41](https://my.bible.com/pt/bible/129/MRK.4.35-41.NVI) • NVI.

> “<small>^9^</small> Não fui eu que ordenei a você? Seja forte e corajoso! Não se apavore nem desanime, pois o S<small>ENHOR</small>, o seu Deus, estará com você por onde você andar”.”  
> [Josué 1:9](https://my.bible.com/pt/bible/129/JOS.1.9.NVI) • NVI.

> “<small>^31^</small> Que diremos, pois, diante dessas coisas? Se Deus é por nós, quem será contra nós?”  
> [Romanos 8:31](https://my.bible.com/pt/bible/129/ROM.8.31.NVI) • NVI.

> “<small>^25^</small> Portanto eu digo: Não se preocupem com sua própria vida, quanto ao que comer ou beber; nem com seu próprio corpo, quanto ao que vestir. Não é a vida mais importante que a comida, e o corpo mais importante que a roupa?  
> <small>^26^</small> Observem as aves do céu: não semeiam nem colhem nem armazenam em celeiros; contudo, o Pai celestial as alimenta. Não têm vocês muito mais valor do que elas?  
> <small>^27^</small> Quem de vocês, por mais que se preocupe, pode acrescentar uma hora que seja à sua vida?[^5]  
> <small>^28^</small> “Por que vocês se preocupam com roupas? Vejam como crescem os lírios do campo. Eles não trabalham nem tecem.  
> <small>^29^</small> Contudo, eu digo que nem Salomão, em todo o seu esplendor, vestiu-se como um deles.  
> <small>^30^</small> Se Deus veste assim a erva do campo, que hoje existe e amanhã é lançada ao fogo, não vestirá muito mais a vocês, homens de pequena fé?  
> <small>^31^</small> Portanto, não se preocupem, dizendo: ‘Que vamos comer?’ ou ‘Que vamos beber?’ ou ‘Que vamos vestir?’  
> <small>^32^</small> Pois os pagãos é que correm atrás dessas coisas; mas o Pai celestial sabe que vocês precisam delas.  
> <small>^33^</small> Busquem, pois, em primeiro lugar o Reino de Deus e a sua justiça, e todas essas coisas serão acrescentadas a vocês.”  
> [Mateus 6:25-33](https://my.bible.com/pt/bible/129/MAT.6.25-33.NVI) • NVI.

<p>&nbsp;</p>

Conheça mais sobre o meu livro, em inglês _“[Divine Direction](http://divinedirectionbook.com/)” \[Direção Divina\]_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

_Este plano bíblico é adaptado do livro “Divine Direction” \[Direção Divina\], com permissão de Zondervan. O conteúdo foi alterado para ser mais conciso._

Gostaríamos de agradecer ao Pastor Craig Groeschel e à Life\.Church por fornecerem este plano. Para mais informações \[em inglês\], acesse: [craiggroeschel.com](https://craiggroeschel.com/).

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr style="text-align: center; margin: auto;" width="30%" />

<p>&nbsp;</p>

- **Fonte:** _“[Direção Divina](https://my.bible.com/pt/reading-plans/3670-divine-direction)”_ | YouVersion  
- January 16, 2022

<p>&nbsp;</p>

###### tags: `devocional` `YouVersion` `Bíblia` `Direção Divina` `Craig Groeschel`

<a id="notas"></a>
## NOTAS

[^1]: 5:17 Vários manuscritos dizem _“eis que tudo se fez novo!”_. 

[^2]: 62:4 _Hefzibá_ significa _o meu prazer está nela_. 

[^3]: 62:4 _Beulá_ significa _casada_. 

[^4]: 4:22 Isto é, da velha vida dos não regenerados. 

[^5]: 6:27 Ou _um único côvado à sua altura?_ O côvado era uma medida linear de cerca de 45 centímetros. 