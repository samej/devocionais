---
title: "Dia a dia com Spurgeon - Parte 1 - Dia 5: Quando a cruz for trocada pela coroa | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 19, 2022"
  - modified: "December 19, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "Quando a cruz for trocada pela coroa"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/5?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON - PARTE 1 - DIA 5: QUANDO A CRUZ FOR TROCADA PELA COROA ~ CRIADO EM 2022/12/19 ~ ATUALIZADO EM 2022/12/19 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 5:** Quando a cruz for trocada pela coroa

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

Se formos cúmplices de Jesus em Sua vergonha, compartilharemos com Ele o brilho que o cercará quando aparecer novamente em glória. Está você, amado, com Cristo Jesus? Uma união vital o liga ao seu Senhor? Então, se está hoje com Ele em Sua vergonha; se leva a Sua cruz, e sai com Ele levando a Sua reprovação; sem dúvida estará com Ele quando a cruz for trocada pela coroa. Mas julgue-se a si mesmo agora<!-- esta noite -->; pois se você não estiver com Cristo na regeneração, não estará quando Ele voltar em glória. Se você se afastar deste lado sombrio da comunhão, não compreenderá seu brilho, seu momento feliz, quando o Rei voltar e Seus santos anjos com Ele.

O quê?! Há anjos com Ele?  
E ainda assim Ele não escolheu os anjos — Ele adotou a semente de Abraão.

Os santos anjos estão com Ele?  
Minh’alma, se é realmente Sua amada, não pode ficar longe dele. Se seus amigos e vizinhos são chamados a ver a Sua glória, o que pensa você que é casada com Ele? Deverá ficar distante? Embora seja um dia de julgamento, não pode ficar longe daquele coração que, tendo admitido anjos em Sua intimidade, admitiu você em união.

Ele não lhe disse, ó minh’alma: _“Desposar-te-ei comigo em justiça, e em juízo, e em benignidade, e em misericórdias?”_. Seus próprios lábios não disseram: _“Casei-me contigo, e minha alegria está em ti?”_.

Se os anjos, que são apenas amigos e próximos, deverão estar com Ele, é certo que Sua própria amada Hefzibá,<small>[^1]</small> em quem está toda a Sua alegria, estará perto dEle, e sentada à Sua mão direita. Eis uma estrela da manhã de esperança para você, de tamanho brilho, que pode iluminar a experiência mais sombria e desoladora.

[^1]: Em Isaías 62:4. Este nome quer dizer: _“meu prazer está nela”_.
    
    > [!bible] [[Isaías]] 62:4 • VFL
    > Não será mais chamada de “Cidade abandonada”, nem a sua terra, “Destruída”. O seu nome será “Meu prazer”, e a sua terra, “Minha esposa”, porque o SENHOR tem muito prazer em você, e a sua terra se casará com ele.
    
    <small>\[Nota do Editor\]</small> 

<p>&nbsp;</p>

### Referência

> [!bible]- Marcos 8:38 • VFL
> As pessoas de hoje em dia são pecadoras. Elas têm sido infiéis a Deus. Enquanto vocês estiverem morando no meio delas, não tenham vergonha de mim nem das coisas que ensino. Se acontecer isso, então eu, o Filho do Homem, também terei vergonha de vocês quando eu vier na glória de meu Pai com os santos anjos.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/5?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 19, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

