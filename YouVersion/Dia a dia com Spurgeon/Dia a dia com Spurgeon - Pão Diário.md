---
title: "Dia a dia com Spurgeon | Ministérios Pão Diário"
author: "Ministérios Pão Diário"
dates:
  - created: "December 15, 2022"
  - modified: "April 11, 2023"
tags: [" BIOGRAFIAS/Cristas/charles_spurgeon podcasts BIOGRAFIAS/Cristas/spurgeon "]
aliases: ["dia a dia com Spurgeon"]
abstract: "Apresentamos um podcast com alguns devocionais extraídos do livro “Dia a Dia com Spurgeon – Dia e Noite”."
link: "https://paodiario.org.br/podcasts/dia-a-dia-com-spurgeon/"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- PODCAST: DIA A DIA COM SPURGEON ~ CRIADO EM 2022/12/15 ~ ATUALIZADO EM 2023/04/11 -->

# Dia a dia com Spurgeon - Pão Diário

<p>&nbsp;</p>

<!-- ![](https://paodiario.org.br/wp-content/uploads/2020/11/00Spurgeon_Autor-do-mes.jpg) -->

<span class="text-center" align="center">![[00Spurgeon_Autor-do-mes.png]]</span>

<p>&nbsp;</p>

[[Charles Spurgeon|Charles H. Spurgeon]] é reconhecido como uma das mentes mais brilhantes de sua época, ele escreveu mais de 600 mensagens, e publicou 3.561 sermões e 135 livros. E para a sua apreciação, apresentamos um podcast com alguns devocionais extraídos do livro _“Dia a Dia com Spurgeon – Dia e Noite”_. Originalmente, o devocional contêm 732 mensagens, uma para a leitura na manhã e outra para noite.

<p>&nbsp;</p>

Prepare seu coração e ouça!

<p>&nbsp;</p>

#### Episódio 1: 1 Pedro 5-7

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 1 - 1 Pedro 5-7.mp3]]</span>

<p>&nbsp;</p>

#### Episódio 2: 1 Samuel 18-19

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 2 - 1 Samuel 18-19.mp3]]</span>

<p>&nbsp;</p>

#### Episódio 3: Hebreus 2-18

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 3 - Hebreus 2-18.mp3]]</span>

<p>&nbsp;</p>

#### Episódio 4: Isaías 7-14

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 4 - Isaías 7-14.mp3]]</span>

<p>&nbsp;</p>

#### Episódio 5: Jeremias 33-3

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 5 - Jeremias 33-3.mp3]]</span>

<p>&nbsp;</p>

#### Episódio 6: Lamentações 3-21

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 6 - Lamentações 3-21.mp3]]</span>

<p>&nbsp;</p>

#### Episódio 7: Salmo 30-6

<span class="text-center" align="center">![[Dia a Dia com Spurgeon - Episódio 7 - Salmo 30-6.mp3]]</span>

<p>&nbsp;</p>

<!-- ## Charles Spurgeon

[[Charles Spurgeon|Charles H. Spurgeon]], conhecido como “príncipe dos pregadores”, foi um dos maiores evangelistas do século 19.

## Adquira

[[Dia a dia com Spurgeon - Parte 1 - Dia 1 - A empatia de Cristo|Dia a dia com Spurgeon]] | Manhã & Noite

## Participe do plano de leitura

7 meditações -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon](https://paodiario.org.br/podcasts/dia-a-dia-com-spurgeon/)”_ | por [[Pão Diário|Ministérios Pão Diário]]
- December 15, 2022

<p>&nbsp;</p>

###### tags:

#BIOGRAFIAS/Cristas/charles_spurgeon #podcasts #BIOGRAFIAS/Cristas/spurgeon 

<p>&nbsp;</p>

