---
title: "Dia a dia com Spurgeon – Parte 1 - Apresentação | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Pão Diário"
dates:
  - created: "December 15, 2022"
  - modified: "December 22, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON – PARTE 1 ~ CRIADO EM 2022/12/15 ~ ATUALIZADO EM 2022/12/22 -->

### Índice

- [[Dia a dia com Spurgeon - Parte 1 - Apresentação|Apresentação]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 1 - A empatia de Cristo|Dia 1: A empatia de Cristo]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 2 - Orai uns pelos outros|Dia 2: Orai uns pelos outros]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 3 - Deus conforta os abatidos|Dia 3: Deus conforta os abatidos]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 4 - Seguindo as pegadas de Cristo|Dia 4: Seguindo as pegadas de Cristo]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 5 - Quando a cruz for trocada pela coroa|Dia 5: Quando a cruz for trocada pela coroa]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 6 - Correções em amor|Dia 6: Correções em amor]]
- [[Dia a dia com Spurgeon - Parte 1 - Dia 7 - Eu te ajudo, diz o Senhor|Dia 7: Eu te ajudo, diz o Senhor]]

<p>&nbsp;</p>

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - Parte 1 - cover.png]]</span>

<p>&nbsp;</p>

7 Dias

[[Charles Spurgeon|Charles H. Spurgeon]], aclamado como _“O Príncipe dos pregadores”_ do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente!

<p>&nbsp;</p>

### Editor

[Pão Diário](https://paodiario.org.br/autores-classicos/charles-spurgeon/?utm_source=youversion&utm_campaign=youversion)

<!-- [Sobre o Editor](https://paodiario.org.br/autores-classicos/charles-spurgeon/?utm_source=youversion&utm_campaign=youversion) -->

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 15, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

