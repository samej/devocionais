---
title: "Dia a dia com Spurgeon – Parte 2 - Dia 1 | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 22, 2022"
  - modified: "December 22, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33609-dia-a-dia-com-spurgeon-parte-2/day/1?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON – PARTE 2 - DIA 1 ~ CRIADO EM 2022/12/22 ~ ATUALIZADO EM 2022/12/22 -->

# Dia a dia com Spurgeon – Parte 2

<p>&nbsp;</p>

## **Dia 1**

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - Parte 2 - cover.png]]</span>

<p>&nbsp;</p>

Todas as coisas sobre a Terra precisam ser renovadas. Nada que foi criado segue sozinho. _“E, assim, renovas a face da terra”_ foi a expressão do salmista. Mesmo as árvores, que não se vestem com cuidado, nem encurtam sua vida com o trabalho, precisam beber a chuva do céu e sugar os tesouros escondidos do solo. Os cedros do Líbano, plantados por Deus, vivem apenas porque diariamente se enchem da fresca seiva da terra.

Da mesma forma, a vida do homem não se sustenta sem a renovação divina. Assim como é necessário reparar o desgaste do corpo com refeições frequentes, devemos renovar o desgaste da alma alimentando-a com o [[Bíblia — Palavra de Deus|Livro de Deus]], com a Palavra pregada, ou pela mesa farta da comunhão.

Como nossas graças são depreciadas quando suas fontes são negligenciadas! Como são famintos alguns pobres cristãos que vivem sem o uso diligente da Palavra de Deus e da oração secreta! Se a nossa devoção pode viver sem Deus, não é divina; não passa de um sonho; se assim fosse, seria esperar por Ele como as flores esperam pelo orvalho. Sem renovação constante, não estamos prontos para os insistentes ataques do maligno, ou para as sérias aflições do céu, ou até mesmo para as lutas interiores.

Quando o vendaval chegar, ai da árvore que não tiver sugado a seiva fresca e se prendido à rocha com muitas raízes entrelaçadas. Quando surgem as tempestades, ai do marinheiro que não tiver reforçado o mastro, lançado a âncora nem procurado refúgio.

Se a bondade cresce fraca, o maligno certamente reunirá forças e lutará desesperadamente para obter o controle sobre nós; e então, talvez, uma sofrida desolação ou uma desgraça lamentável aconteça.

Aproximemo-nos aos pés da misericórdia divina, em humilde súplica, e perceberemos o cumprimento da promessa: _“Os que esperam no Senhor renovam as suas forças”_.

<p>&nbsp;</p>

### Referência

> [!bible]- [[Isaías]] 41:1 • VFL
> “Silêncio, diante de mim, ó povos distantes. Que as nações renovem as suas forças! Venham apresentar o seu caso, vamos nos reunir para decidir a questão.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon – Parte 2 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 2](https://www.bible.com/pt/reading-plans/33609-dia-a-dia-com-spurgeon-parte-2/day/1?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 16, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

