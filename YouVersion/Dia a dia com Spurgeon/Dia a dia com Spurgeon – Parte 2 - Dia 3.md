---
title: "Dia a dia com Spurgeon – Parte 2 - Dia 3 | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 24, 2022"
  - modified: "December 24, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33609-dia-a-dia-com-spurgeon-parte-2/day/3?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON – PARTE 2 - DIA 3 ~ CRIADO EM 2022/12/24 ~ ATUALIZADO EM 2022/12/24 -->

# Dia a dia com Spurgeon – Parte 2

<p>&nbsp;</p>

## **Dia 3**

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - Parte 2 - cover.png]]</span>

<p>&nbsp;</p>

Bendito é o fato que cristãos podem alegrar-se mesmo na mais profunda agonia; [[Alegrai-vos no Sofrimento|ainda que a dificuldade os cerque]], eles cantam; e, como muitos pássaros, cantam melhor dentro de suas gaiolas. As ondas podem cobri-los, mas suas almas logo surgem na superfície e veem a luz do semblante de Deus; eles possuem certa leveza que mantém suas cabeças sempre acima do nível da água e que os ajuda a cantar em meio à tempestade: _“Deus continua comigo”_.

> [!question] A quem deve ser dada a glória?
> Oh! A Jesus — tudo é por Jesus.

A dificuldade não necessariamente traz consigo consolação ao cristão, mas a presença do Filho de Deus na fornalha ardente com ele, enche seu coração de alegria. Ele está doente, sofrendo, mas Jesus o visita e prepara sua cama. Ele está morrendo e as frias águas do Jordão sobem até seu pescoço, mas Jesus o envolve em Seus braços e proclama: _“Não tema, meu amado; morrer é ser abençoado. As fontes principais da água da morte estão no céu; não são amargas, são doces como néctar, pois fluem do trono de Deus”_.

Conforme o santo parte vadeando pela corrente, e as vagas o envolvem e o coração e [[O que é a carne|a carne]] minguam, a mesma voz soa em seu ouvido: _“Não temas, Eu sou contigo; não te assombres, porque Eu sou o Teu Deus”_. Ao aproximar-se das fronteiras do infinito desconhecido, quase com medo de entrar no reino das sombras, Jesus diz: _“Não temais, porque vosso Pai Se agradou em dar-vos o Seu reino”_.

O cristão, então fortalecido e consolado, não teme morrer. Não! Ele está inclusive disposto a partir, pois dado que viu Jesus como a Estrela da Manhã, ele anseia olhá-Lo fixamente como para o sol em sua força. Verdadeiramente, a presença de Jesus é todo o céu que desejamos. Ele é ao mesmo tempo _“a glória de nossos dias mais reluzentes; o consolo de nossas noites”_.

<p>&nbsp;</p>

### Referência

> [!bible]- [[Salmos|Salmo]] 33:21 • VFL
> Ele é quem nos dá toda a alegria; confiamos nele, porque ele é verdadeiro.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon – Parte 2 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 2](https://www.bible.com/pt/reading-plans/33609-dia-a-dia-com-spurgeon-parte-2/day/3?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 24, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

