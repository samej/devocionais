---
title: "Dia a dia com Spurgeon - Parte 1 - Dia 6: Correções em amor | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 20, 2022"
  - modified: "December 20, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "correções em amor"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/6?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON - PARTE 1 - DIA 6: CORREÇÕES EM AMOR ~ CRIADO EM 2022/12/20 ~ ATUALIZADO EM 2022/12/20 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 6:** Correções em amor

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

Há murmúrios entre cristãos hoje, como havia no acampamento de Israel na antiguidade. Há aqueles que, quando a vara desce, clamam contra a dispensação aflitiva. Eles perguntam: _“Por que sou afligido assim? O que eu fiz para ser castigado desta maneira?”_ Uma palavra para você, ó murmurador: por que deveria murmurar contra as dispensações de seu Pai celeste? Ele pode tratá-lo com mais rigor do que você merece? Considere que você um dia foi rebelde, mas Ele o perdoou! Certamente se Ele, em Sua sabedoria, vir que deve castigá-lo agora, você não deveria reclamar.

Afinal, você está sendo atingido tanto quanto seus pecados merecem? Considere a corrupção que há em seu peito, e então você acha que precisa de tanta vara para atingi-la? Pese a si mesmo e perceba quanto lixo está misturado com seu ouro; e ainda acha que o fogo é quente demais para limpar tanto lixo que está aí? O seu orgulhoso espírito rebelde não provou que seu coração não é completamente santificado? Não são essas palavras de murmúrio contrárias à santa natureza submissa dos filhos de Deus? A correção não é necessária?

Mas se você vai murmurar contra a disciplina, tome cuidado, pois ela será mais rígida com os murmuradores. Deus sempre castiga Seus filhos duas vezes se eles não suportarem o primeiro golpe pacientemente. Mas saiba de uma coisa — Ele _“não aflige, nem entristece de bom grado os filhos dos homens”_. Todas as Suas correções são enviadas em amor, para purificá-lo e levá-lo para mais perto dEle.

Certamente o ajudará a suportar o castigo com resignação, se você for capaz de reconhecer a mão de seu Pai. Pois _“o Senhor corrige a quem ama e açoita a todo filho a quem recebe. \[…\] Mas, se estais sem correção, de que todos se têm tornado participantes, logo, sois bastardos e não filhos.”_, _“Nem murmureis, como alguns deles murmuraram e foram destruídos pelo exterminador.”_.

<p>&nbsp;</p>

### Referência

> [!bible]- [[Números]] 14:2 • VFL
> Todos os israelitas falavam mal de Moisés e de Aarão e diziam: — Melhor tivesse sido morrer no Egito, ou morrer de uma só vez neste deserto!

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/6?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 19, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

