---
title: "Dia a dia com Spurgeon - Parte 1 - Dia 3: Deus conforta os abatidos | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 18, 2022"
  - modified: "December 18, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "Deus conforta os abatidos"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/3?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON - PARTE 1 - DIA 3: DEUS CONFORTA OS ABATIDOS ~ CRIADO EM 2022/12/18 ~ ATUALIZADO EM 2022/12/18 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 3:** Deus conforta os abatidos

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

E quem conforta como Ele? Vá até algum pobre, melancólico, aflito filho de Deus; fale a ele sobre as doces promessas e sussurre ao seu ouvido palavras escolhidas de conforto; ele é como a víbora surda, que não ouve a voz do encantador, que nunca a encanta tão sabiamente. Ele está bebendo veneno e fel, conforte-o o quanto quiser, e obterá dele apenas uma nota ou duas de resignação triste; não ouvirá salmos de louvor, aleluias ou sonetos de júbilo.

Mas deixe Deus chegar a Seu filho, deixe que Ele eleve seu semblante e os olhos do [[Por que Salomão disse que é melhor ir ao velório do que em festas se isso não é verdade|enlutado]] brilhem com esperança. Você não o ouve cantar:

> — Este é o paraíso, se estás aqui; se vais embora, isto é o inferno?
> — Você pode não tê-lo alegrado: mas o Senhor o fez; Ele é o “Deus de toda consolação.”
> — Não há bálsamo em Gileade, mas há bálsamo em Deus.
> — Não há médico entre as criaturas, mas o Criador é [[Quais os nomes de Deus na Bíblia|Jeová-Raphá]], que o cura.

É maravilhoso como uma doce [[Bíblia — Palavra de Deus|palavra de Deus]] comporá canções inteiras para os cristãos.

Uma palavra do Senhor é como um pedaço de ouro, e o cristão é o batedor do ouro e pode martelar aquela promessa por semanas inteiras. Então, pobre cristão, você não precisa sentar-se em desespero. Vá até o Consolador e peça a Ele que lhe dê consolo. Você é um pobre poço seco. Já foi dito que, quando uma bomba d’água está seca, é preciso derramar primeiro água sobre ela, para que se volte a obter água.

Cristão, quando estiver seco, vá até Deus, peça que lance Sua alegria em seu coração, e seu júbilo será completo. Não vá a companheiros terrenos, pois encontrará neles os consoladores de [[Quem foi Jó|Jó]]; mas vá primeiro e principalmente ao seu “Deus, que conforta os abatidos”, e logo dirá: “Na multidão dos meus pensamentos, as Tuas consolações recreiam a minha alma”.

<p>&nbsp;</p>

### Referência

> [!bible]- 2 Coríntios 7:16 • VFL
> Alegro-me porque posso confiar inteiramente em vocês.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/3?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 18, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

