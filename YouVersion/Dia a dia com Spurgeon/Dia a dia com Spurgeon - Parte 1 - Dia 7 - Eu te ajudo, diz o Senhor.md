---
title: "Dia a dia com Spurgeon - Parte 1 - Dia 7: Eu te ajudo, diz o Senhor | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 21, 2022"
  - modified: "December 21, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "eu te ajudo, diz o Senhor"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/7?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON - PARTE 1 - DIA 7: EU TE AJUDO, DIZ O SENHOR ~ CRIADO EM 2022/12/20 ~ ATUALIZADO EM 2022/12/21 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 7:** Eu te ajudo, diz o Senhor

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

Hoje, vamos ouvir o Senhor Jesus dizer a cada um de nós:

> [!info] “Eu o ajudo”.
> “É algo simplesmente pequeno para mim, seu Deus, ajudá-lo. Pense no que Eu já fiz.
> 
> O quê! Não o ajudei?
> 
> [[Cenas e Explicações da Crucificação de Jesus|Pois o comprei com o meu sangue.]]
> 
> O quê! Não o ajudei?
> 
> Eu morri por você e, se fiz o maior, não faria o menor? Ajudar você! É o mínimo que farei; Eu fiz mais, e farei mais.
> 
> Antes do início do mundo, o escolhi. Fiz o pacto por você. Deixei minha glória de lado e me tornei homem por você; abri mão da minha vida por você; e se fiz isso tudo, certamente o ajudarei agora. Ao ajudá-lo, estarei lhe dando o que Eu já comprei para você. Se precisar de ajuda mil vezes, Eu darei; você pede pouco comparado ao que estou pronto para dar. Esse muito que você precisa não é nada para Eu lhe dar.
> 
> ‘Eu o ajudo?’
> 
> Não tema! Se houvesse uma formiga à porta do seu celeiro pedindo ajuda, não o arruinaria lhe dar um punhado do seu trigo; e você não é nada além de um pequeno inseto à porta da minha plena suficiência. ‘Eu o ajudo.’”

Ó minh’alma, isso não é suficiente? Você precisa de mais força do que a onipotência da [[Santíssima Trindade na Bíblia - Estudo|Santa Trindade]]? Quer mais sabedoria do que existe no Pai, mais amor do que se mostra no Filho, ou mais poder do que é manifesto na influência do Espírito?

Traga o seu jarro vazio! Certamente esse poço o encherá. Venha depressa, reúna seus desejos e traga-os para cá — seu vazio, seus problemas, suas necessidades. Eis que esse rio de Deus está cheio para abastecer você; o que mais poderá desejar? Vá em frente, minh’alma, nessa sua força. O Deus Eterno é o seu socorro!

<p>&nbsp;</p>

> [!info] Não temas, Eu sou contigo; ah, não te assombres! Eu, Eu sou teu Deus; Eu ainda te darei ajuda.

<p>&nbsp;</p>

### Referência

> [!bible]- Isaías 41:14 • VFL
> “Povo de Israel, descendentes de Jacó, vocês são fracos e insignificantes, mas não tenham medo. Eu mesmo ajudarei vocês”. Assim diz o SENHOR. “Eu sou seu salvador, sou o Santo de Israel.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/7?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 19, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

