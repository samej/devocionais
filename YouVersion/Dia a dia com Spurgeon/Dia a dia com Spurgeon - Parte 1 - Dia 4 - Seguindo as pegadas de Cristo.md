---
title: "Dia a dia com Spurgeon - Parte 1 - Dia 4: Seguindo as pegadas de Cristo | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 18, 2022"
  - modified: "December 18, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "Seguindo as pegadas de Cristo"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/4?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON - PARTE 1 - DIA 4: SEGUINDO AS PEGADAS DE CRISTO ~ CRIADO EM 2022/12/18 ~ ATUALIZADO EM 2022/12/18 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 4:** Seguindo as pegadas de Cristo

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

Amarás o teu próximo. Talvez ele nade em riquezas e você seja pobre e, vivendo em sua casinha humilde ao lado da mansão imponente dele, veja todos os dias suas posses, seu linho fino e seus suntuosos banquetes. Deus concedeu a ele esses presentes, não cobice sua riqueza e não tenha pensamentos ruins sobre ele. Fique satisfeito com sua porção; se não puder ter mais e melhor, não olhe para o seu próximo desejando que ele seja como você. Ame-o, e então, não o invejará.

Talvez, por outro lado, você seja rico e perto de você morem os pobres. Não se abstenha de chamá-los próximos. Você deve amá-los. O mundo os chama de inferiores. São inferiores em quê? São muito mais parecidos a você do que imagina, pois Deus “de um só fez toda a raça humana para habitar sobre toda a face da terra”. Seu casaco é melhor do que o deles, mas isso não significa que você seja melhor. São homens, e o que você é mais do que isso? Esteja atento em amar seu próximo, mesmo que ele esteja coberto de trapos ou mergulhado em profunda pobreza.

Mas, talvez você diga: “Não posso amar meu próximo porque, apesar de tudo o que faço, ele retribui com ingratidão e menosprezo”. Então há mais espaço para o heroísmo do amor. Você seria capaz de ser um guerreiro em cama de penas em vez de suportar a dura batalha do amor? Aquele que ousar mais, deverá ganhar mais; e se o seu caminho de amor é difícil, trilhe-o ousadamente, ainda assim, ame a seu próximo no bem e no mal. Acumule brasas vivas sobre sua cabeça e, se ele for difícil de ser agradado, procure não agradá-lo, mas agradar o seu Mestre. Lembre-se de que se ele desprezar o seu amor, o seu Mestre não o desprezará, e sua obra será tão aceitável a Ele como se tivesse sido aceita por seu próximo. Ame seu próximo, pois ao fazer isso, estará seguindo as pegadas de Cristo.

<p>&nbsp;</p>

### Referência

> [!bible]- Mateus 5:43 • VFL
> — Vocês também ouviram que foi dito: “Ame o seu próximo e odeie os seus inimigos”.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/4?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 18, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

