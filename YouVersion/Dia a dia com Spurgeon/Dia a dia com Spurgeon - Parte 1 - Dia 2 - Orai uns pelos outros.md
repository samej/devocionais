---
title: "Dia a dia com Spurgeon – Parte 1 - Dia 2: Orai uns pelos outros | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 16, 2022"
  - modified: "December 16, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "orem uns pelos outros"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/2?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON – PARTE 1 - DIA 2: ORAI UNS PELOS OUTROS ~ CRIADO EM 2022/12/16 ~ ATUALIZADO EM 2022/12/16 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 2:** Orai uns pelos outros

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

Como uma motivação para oferecer oração intercessora, lembre-se de que tal oração é a mais doce aos ouvidos de Deus, pois a oração de Cristo tem esse caráter. Em todo o incenso que nosso Grande Sumo Sacerdote coloca agora no incensário, não há um único grão por si próprio. Sua intercessão deve ser a mais aceitável de todas as súplicas — e quanto mais nossa [[Como orar a Deus segundo a Bíblia|oração for parecida com a de Cristo]], mais doce ela será; assim, enquanto serão aceitas petições por nós mesmos, nossas súplicas por outros, tendo neles mais do [[O Fruto do Espírito Santo — Cultivando uma Vida Transformada|fruto do Espírito]], mais amor, mais fé, mais bondade fraternal, serão, por meio do precioso mérito de Jesus, a mais doce oferta que podemos dar a Deus, a própria gordura do nosso sacrifício.

Lembre-se, mais uma vez, que oração intercessória é a que mais prevalece. Que maravilhas ela tem feito! A Palavra de Deus está recheada com seus feitos maravilhosos. Cristão, você tem uma ferramenta poderosa nas mãos: use-a bem, use-a constantemente, use-a com fé e certamente será um benfeitor para seus irmãos.

Quando estiver aos ouvidos do Rei, fale com Ele sobre o sofrimento dos membros de Seu corpo. Quando você for chamado para perto de Seu trono e o Rei disser: “Peça, que eu darei o que quiser”, que suas petições sejam, não apenas por si mesmo, mas pelos muitos que precisam de Sua ajuda.

Se você tiver alguma graça e não for um intercessor, essa graça deve ser tão pequena quanto um grão de mostarda; você tem graça suficiente para deixar sua alma flutuar na areia movediça; mas você não tem esse fluir profundo da graça, caso contrário levaria em si mesmo a carga pesada dos desejos dos outros, e voltaria para eles com ricas bênçãos do seu Senhor; bênçãos que talvez não tivessem obtido se não fosse por seu intermédio:

> [!important] — Ah, que minhas mãos esqueçam sua função, minha língua fique fria, silenciosa e parada, este coração limitado esqueça de bater, se eu esquecer o trono da misericórdia!

<p>&nbsp;</p>

### Referência

> [!Bible]- Tiago 5:16 • VFL
> Portanto, confessem os seus pecados uns aos outros e orem uns pelos outros para que vocês sejam curados. A oração feita pelo justo é poderosa e eficaz.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/2?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 16, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

