---
title: "Dia a dia com Spurgeon – Parte 1 - Dia 1: A empatia de Cristo | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 16, 2022"
  - modified: "December 16, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon", "empatia de Cristo"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/1?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON – PARTE 1 - DIA 1: A EMPATIA DE CRISTO ~ CRIADO EM 2022/12/16 ~ ATUALIZADO EM 2022/12/16 -->

# Dia a dia com Spurgeon – Parte 1

<p>&nbsp;</p>

## **Dia 1:** A empatia de Cristo

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - cover.png]]</span>

<p>&nbsp;</p>

É um pensamento comum, mas ele é doce como néctar para o coração cansado — Jesus foi tentado como eu sou. Você já ouviu essa verdade muitas vezes; mas já a compreendeu? Ele foi tentado nos mesmíssimos pecados em que nós caímos. Não dissocie Jesus de nossa humanidade comum. Se você está preso em um quarto escuro, Jesus aí esteve antes. Você está travando uma luta acirrada, Jesus esteve frente a frente com o mesmo inimigo.

Tenhamos bom ânimo, Cristo carregou nossos fardos antes de nós, e os passos manchados de sangue do Rei da glória podem ser vistos pela estrada pela qual passamos nessa hora. Há ainda algo mais doce — Jesus foi tentado, mas nunca pecou.

Minh’alma, não há necessidade de pecar, pois Jesus foi homem e se um homem suportou estas tentações e não pecou, então, em Seu poder, Seus membros também podem deixar de pecar. Alguns iniciantes na vida espiritual acreditam que não podem ser tentados sem que caiam no pecado, mas se enganam; não há pecado algum em ser _tentado_, mas _há_ pecado em _ceder à tentação_.

Aqui está o consolo para os violentamente tentados. Há ainda mais para encorajá-los se refletirem no fato de que o Senhor Jesus, ainda que tentado, triunfou gloriosamente e assim como Ele venceu, certamente vencerão os Seus seguidores, pois Jesus é o homem representante do Seu povo; o Cabeça triunfou e os membros compartilham dessa vitória.

Medos são desnecessários, pois Cristo está conosco, munido para nossa defesa. Nosso lugar de segurança é o colo do Salvador. Talvez sejamos tentados agora, para que isso nos aproxime dEle. Abençoado é o vento que nos sopra para o ancoradouro do amor de nosso Salvador! Venturosas feridas são as que nos fazem buscar o amado Médico. Vocês, quando tentados, venham ao seu Salvador que foi tentado, pois Ele pode ser tocado com compaixão por nossas enfermidades e socorrerá todo aquele que é provado e tentado.

<p>&nbsp;</p>

### Referência

> [!bible]- Hebreus 2:18 • VFL
> Agora ele pode socorrer aqueles que são tentados, pois ele mesmo sofreu e foi tentado.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon - Parte 1 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 1](https://www.bible.com/pt/reading-plans/33607-dia-a-dia-com-spurgeon-parte-1/day/1?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 16, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

