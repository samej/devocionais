---
title: "Dia a dia com Spurgeon – Parte 2 - Dia 2 | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "Charles Spurgeon"
dates:
  - created: "December 23, 2022"
  - modified: "December 24, 2022"
tags: [" DEVOCIONAIS/YouVersion DEVOCIONAIS/YouVersion/spurgeon RELIGIAO/Cristianismo "]
aliases: ["dia a dia com Spurgeon", "devocionais de Charles Spurgeon"]
abstract: "Charles H. Spurgeon, aclamado como “O Príncipe dos pregadores” do século 19, conduz o leitor em uma jornada de aprofundamento no relacionamento com Deus e no conhecimento de Sua Palavra. Deleite-se com devocionais edificantes e aproxime-se do seu Criador diariamente."
link: "https://www.bible.com/pt/reading-plans/33609-dia-a-dia-com-spurgeon-parte-2/day/2?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- DIA A DIA COM SPURGEON – PARTE 2 - DIA 2 ~ CRIADO EM 2022/12/23 ~ ATUALIZADO EM 2022/12/24 -->

# Dia a dia com Spurgeon – Parte 2

<p>&nbsp;</p>

## **Dia 2**

<p>&nbsp;</p>

<span class="text-center" align="center">![[Dia a dia com Spurgeon - Parte 2 - cover.png]]</span>

<p>&nbsp;</p>

Note, minh’alma, o cuidado que Jesus manifestou pelas ovelhas de Seu rebanho, mesmo na hora de Seu julgamento! A paixão dominante é forte na morte. Ele Se entrega ao inimigo, mas interpõe uma palavra de poder para livrar [[Quem foram os 12 apóstolos|Seus discípulos]]. Quanto a Si mesmo, como uma ovelha perante seus tosquiadores, Ele emudece e não abre a boca, mas pelo bem de Seus discípulos, fala com grande vigor.

Aqui há amor, constante, misericordioso e fiel. Mas não há muito mais aqui do que o que é visto na superfície? Não temos nós a alma e o espírito da expiação nessas palavras? O [[A parábola do bom pastor|Bom Pastor]] dá Sua vida pelas ovelhas e defende que elas devam ser libertadas. O Fiador se apresenta e a justiça determina que aqueles por quem Ele se apresenta como substituto, devem ser libertados. Em meio ao cativeiro do Egito, aquela voz soa como uma palavra de poder: _“Deixai ir estes”_. O redimido deve sair da escravidão do pecado e de Satanás.

Em cada cela da masmorra de “[[O Peregrino#Gigante Desespero{.text-dir}|Desespero]]”,<small>[^1]</small> o som ecoa, _“Deixai ir estes”_, e para cá vêm “Desconfiança” e “Temeroso”. Satanás ouve a voz bem conhecida e levanta seu pé do pescoço dos caídos; a morte a escuta, e o túmulo abre seus portões para deixar os mortos se levantarem. Seu caminho é progresso, santidade, triunfo e glória, e ninguém ousará atrasá-los.{.text-esqu}

[^1]: Referência ao livro [[O Peregrino]] de [[John Bunyan]] \(Publicações Pão Diário, 2014) • \[Nota do Editor\] 

Nenhum leão se colocará em seu caminho, nenhum outro animal feroz subirá. _“A corça da manhã”_ atraiu os caçadores cruéis para Si, e agora as mais tímidas gazelas e corças do campo podem pastar em perfeita paz entre os lírios de Seu amor.

> [!important] O trovão rugiu sobre a Cruz no Calvário, e os peregrinos de Sião nunca mais serão feridos pelos pregos da vingança. Meu coração, alegre-se na imunidade assegurada pelo seu Redentor, e bendiga Seu nome durante o dia todo, e todos os dias.

<p>&nbsp;</p>

### Referência

> [!bible]- [[João]] 18:8 • VFL
> Jesus disse a eles: — Já lhes disse que sou eu. Se vocês estão procurando por mim, então deixem estes homens irem embora.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Dia a dia com Spurgeon – Parte 2 - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Dia a dia com Spurgeon – Parte 2](https://www.bible.com/pt/reading-plans/33609-dia-a-dia-com-spurgeon-parte-2/day/2?segment=0)”_ | por [[Pão Diário|Ministérios Pão Diário]] • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 23, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #DEVOCIONAIS/YouVersion/spurgeon #RELIGIAO/Cristianismo 

<p>&nbsp;</p>

