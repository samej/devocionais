---
title: "Aprendendo a Orar Como Jesus Fazia - Dia 2: Aprendendo a Orar de Maneira Equilibrada | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "We Are Zion"
date: "December 13, 2022"
  - modified: "December 13, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["aprendendo a orar", "orar como Jesus", "porque devemos orar"]
abstract: "A oração é com frequência negligenciada em nossa vida cristã porque assumimos que, como Deus já sabe tudo, não precisamos falar com Ele. Este plano o ajudará a reordenar sua vida para que você intencionalmente arranje tempo para buscar a vontade de Deus em sua vida e ore até ver as coisas acontecerem. A oração não deve mais ser nossa opção de apoio, mas nossa primeira resposta para tudo."
link: "https://www.bible.com/pt/reading-plans/27136/day/2?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- APRENDENDO A ORAR COMO JESUS FAZIA - DIA 2: APRENDENDO A ORAR DE MANEIRA EQUILIBRADA ~ CRIADO EM 2022/12/13 ~ ATUALIZADO EM 2022/12/13 -->

# Aprendendo a Orar Como Jesus Fazia

<p>&nbsp;</p>

## **Dia 2:** Aprendendo a Orar de Maneira Equilibrada

<p>&nbsp;</p>

<span class="text-center" align="center">![[Aprendendo a Orar Como Jesus Fazia.png]]</span>

<p>&nbsp;</p>

Na ciência aprendemos sobre um ponto pivô chamado fulcro que, quando algo é posicionado sobre ele, vai movê-lo com facilidade. Pense em uma gangorra ou uma porta com dobradiças. Esses objetos cotidianos se movem suavemente e facilmente por causa de seu pivô. Um pivô cria uma virada com pouca força. Uma oração equilibrada faz a mesma coisa. É o tipo de oração que muda a atmosfera espiritual e traz a vontade de Deus diretamente em foco. É o tipo de oração que honra a Deus e é benéfica ao Seu Reino. Quando os discípulos pediram a Jesus para ensiná-los a orar, Ele respondeu ensinando-lhes a agora conhecida oração do “[[A Oração do Pai Nosso explicada|Pai Nosso]]”. Esta oração é curta e doce, mas é bem abrangente. É assim que uma oração equilibrada se parece. Tim Elmore define a oração equilibrada como _“aprender a orar em níveis mais altos”_. Ele também diz que essas orações são _“orientadas para a missão e não para a manutenção”_.

Quando analisamos a [[Oração do Pai Nosso — significado e explicação passo a passo|oração do Pai Nosso]], você verá uma oração equilibrada escondida à vista de todos. Ela começa abordando a Deus, o Pai e honrando Seu nome. Ela continua com Jesus pedindo que o Reino de Deus venha à Terra, para fornecer tudo o que for necessário para aquele dia e depois para o perdão àquele que orar. Não para por aí, mas garante que o perdão recebido se torne o perdão demonstrado, caso esqueçamos isso, e então fechamos com um pedido de proteção contra o mal.

Uma oração como esta muda a equação de nós para o Reino de Deus. Não é errado orar por nós mesmos; na verdade, é imperativo que o façamos. E se ao invés disso orarmos por nós mesmos com uma perspectiva eterna? E se nossas orações forem sobre pedir a Deus que Seu Reino invada nossas vidas para que muitos mais encontrem salvação em nome de Jesus?

Isso pode revolucionar as nossas vidas e fazer de nós canais poderosos para o trabalho de Deus em nosso mundo.

<p>&nbsp;</p>

### Referências

> [!Bible]- Lucas 11:2-4 • VFL
> ² Então Jesus lhes disse: — Quando vocês orarem, digam: “Pai, que o seu nome sempre permaneça santo. Que o seu reino venha a nós.
> ³ Dê-nos todos os dias o alimento que precisamos.
> ⁴ Perdoe os nossos pecados, assim como nós também perdoamos aos que pecam contra nós. Não nos deixe cair em tentação”.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Aprendendo a Orar Como Jesus Fazia - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Aprendendo a Orar Como Jesus Fazia](https://www.bible.com/pt/reading-plans/27136/day/1?segment=0)”_ | por [We Are Zion](https://www.wearezion.co/bible-plan) • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

