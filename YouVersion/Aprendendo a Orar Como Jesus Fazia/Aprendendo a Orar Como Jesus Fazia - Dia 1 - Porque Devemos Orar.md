---
title: "Aprendendo a Orar Como Jesus Fazia - Dia 1: Porque Devemos Orar? | Plano de Leitura Devocional • Bíblia da YouVersion"
author: "We Are Zion"
date: "December 12, 2022"
  - modified: "December 13, 2022"
tags: [" DEVOCIONAIS/YouVersion ORACAO/orar RELIGIAO/Cristianismo RELIGIAO/Cristianismo/Jesus "]
aliases: ["aprendendo a orar", "orar como Jesus", "porque devemos orar"]
abstract: "A oração é com frequência negligenciada em nossa vida cristã porque assumimos que, como Deus já sabe tudo, não precisamos falar com Ele. Este plano o ajudará a reordenar sua vida para que você intencionalmente arranje tempo para buscar a vontade de Deus em sua vida e ore até ver as coisas acontecerem. A oração não deve mais ser nossa opção de apoio, mas nossa primeira resposta para tudo."
link: "https://www.bible.com/pt/reading-plans/27136/day/1?segment=0"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- APRENDENDO A ORAR COMO JESUS FAZIA - DIA 1: PORQUE DEVEMOS ORAR? ~ CRIADO EM 2022/12/12 ~ ATUALIZADO EM 2022/12/13 -->

# Aprendendo a Orar Como Jesus Fazia

<p>&nbsp;</p>

## **Dia 1:** Porque Devemos Orar?

<p>&nbsp;</p>

<span class="text-center" align="center">![[Aprendendo a Orar Como Jesus Fazia.png]]</span>

<p>&nbsp;</p>

Jesus, mesmo sendo Deus em carne e osso, Ele próprio precisava orar. Ele conseguia arrumar tempo e um local para orar. Ele orou quando alimentou e curou as pessoas. Ele orou quando estava isolado, maltratado e deixado para morrer. A oração era o “molho secreto” do ministério de Jesus na Terra. Ele não se limitava a orar como modelo, mas Ele também nos ensinou a orar.

A oração não é uma forma de arte, mas é um modo de vida. É comunicação e está entranhada em um desejo maior de conhecer a Deus e de ouvir a palavra d’Ele. Podemos orar já ou talvez você esteja tentando entender [[A resposta de Deus é melhor do que o que pedimos|como orar]]. Independentemente da nossa formação ou experiência, a oração deve ser tecida no próprio tecido da vida cristã.

Se a oração é comunicação, é importante saber que ela não é apenas uma forma de comunicação. É uma conversa entre o seu Criador e você. É algo íntimo e pessoal, o que significa que na oração ambos os lados têm a chance de falar e ser ouvidos. Jeremias, o profeta, nos instiga a invocar a Deus para que Ele nos diga coisas grandes e misteriosas. Não é uma grande honra e privilégio? O Criador do mundo, um rei todo-poderoso querendo confiar em nós e falar conosco.

Também é importante lembrar que podemos pedir qualquer coisa a Deus. Isso não se traduz automaticamente para Ele nos dando tudo. Significa que nenhum pedido é muito pequeno ou grande demais para ser apresentado a Ele. Alguém disse uma vez que a maior tragédia seria se todas as orações fossem ignoradas. O próprio Jesus pediu aos seus discípulos que “pedissem qualquer coisa” em Seu nome!

Cada oração que fazemos é como um incenso com uma doce fragrância ao nosso Deus eterno. Por isso não deixe de fazer suas orações. Ore sobre qualquer coisa, desde as maiores até as menores coisas da sua vida. A oração inclui Deus em todos os momentos do cotidiano.

<p>&nbsp;</p>

### Referências

> [!Bible]- Jeremias 33:3 • VFL
> “Judá, me chame, que eu responderei e lhe contarei segredos grandiosos e inimagináveis que você não conhece.

<p>&nbsp;</p>

> [!Bible]- João 16:23-24 • VFL
> ²³ E quando chegar esse dia, vocês não vão me perguntar nada. Digo a verdade a vocês: O meu Pai lhes dará tudo o que vocês pedirem em meu nome.
> ²⁴ Até agora vocês não pediram nada em meu nome. Se vocês pedirem, vocês receberão, para que a alegria de vocês seja completa.

<p>&nbsp;</p>

> [!Bible]- Apocalipse 8:3-4 • VFL
> ³ Depois veio outro anjo e ficou de pé junto ao altar. Ele estava com um incensário de ouro e foi-lhe dado muito incenso para ser oferecido com as orações de todo o povo de Deus sobre o altar de ouro que se encontra diante do trono.
> ⁴ E a fumaça do incenso, juntamente com as orações do povo de Deus, subiu da mão do anjo à presença de Deus.

<p>&nbsp;</p>

> [!Bible]- Mateus 6:5-8 • VFL
> ⁵ — E quando vocês orarem, não façam como os hipócritas, que gostam de orar de pé nas sinagogas e nas esquinas para poderem ser vistos pelo povo. Digo a verdade a vocês: Eles já receberam a recompensa deles.
> ⁶ Você, entretanto, quando orar, vá para o quarto, feche a porta e ore ao seu Pai, que não pode ser visto. E seu Pai, que vê o que você faz em segredo, lhe dará a recompensa.
> ⁷ — Quando vocês orarem, não repitam palavras que não significam nada, como os que não creem no nosso Deus; pois eles pensam que por causa das suas muitas palavras Deus os ouvirá.
> ⁸ Portanto, não sejam como eles, pois o Pai de vocês sabe o que vocês precisam antes mesmo de vocês pedirem.

<p>&nbsp;</p>

> [!Bible]- Marcos 11:24-25 • VFL
> ²⁴ Por isso eu lhes digo que tudo quanto vocês pedirem em oração, acreditem que já receberam e será de vocês.
> ²⁴ E, quando vocês estiverem orando, se tiverem alguma coisa contra alguém, perdoem a essa pessoa. Dessa forma, o Pai de vocês, que está no céu, também perdoará os seus pecados.

<p>&nbsp;</p>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<!-- ÍNDICE EMBEDADO -->

![[Aprendendo a Orar Como Jesus Fazia - Apresentação#Índice]]

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[Aprendendo a Orar Como Jesus Fazia](https://www.bible.com/pt/reading-plans/27136/day/1?segment=0)”_ | por [We Are Zion](https://www.wearezion.co/bible-plan) • Plano de Leitura Devocional | [[YouVersion|Bíblia da YouVersion]]
- December 12, 2022

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/YouVersion #ORACAO/orar #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/Jesus 

<p>&nbsp;</p>

