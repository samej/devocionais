---
title: "TEMPO NA PRESENÇA DE DEUS | World Challenge • Portuguese Devotional"
author: "Jim Cymbala"
dates:
  - created: "December 27, 2022"
  - modified: "December 27, 2022"
tags: [" Devocional/WC-pt RELIGIAO/Cristianismo RELIGIAO/Cristianismo/devocional "]
aliases: ["tempo na presença de Deus", "cheio do Espírito", "cheio do Espírito Santo", "cheios de sabedoria", "graduação num seminário"]
abstract: "Dia 362 de 365 do Devocional “World Challenge • Portuguese Devotional”."
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- TEMPO NA PRESENÇA DE DEUS (DIA 361) • WC-PT ~ CRIADO EM 2022/12/27 ~ ATUALIZADO EM 2022/12/27 -->

# World Challenge <br />Portuguese Devotional

<p>&nbsp;</p>

## Tempo na presença de Deus

<span class="text-dir" align="right">_Dia 362_  
<small>28 de dezembro</small></span>

<p>&nbsp;</p>

Vemos em [[Atos]] 6 que [[Quem foram os 12 apóstolos|os apóstolos]] tiveram que escolher o que muitos chamam de primeiros diáconos. Havia uma disputa entre os Judeus gregos e os Judeus hebraicos sobre a adequação do sistema de distribuição de comida. Os apóstolos decidiram apontar alguns homens para administrar a tarefa, de forma que a distribuição de comida tivesse a atenção necessária, enquanto os apóstolos continuavam focados na “oração e ministração da palavra” (v.4).

Os apóstolos disseram: “Irmãos, escolham entre vocês sete homens de bom testemunho, _cheios do Espírito_ e de sabedoria” (v.3, ênfase minha). Distribuir comida era uma tarefa servil. Ainda assim, os apóstolos perceberam que ser cheio ou controlado pelo Espírito era uma qualificação necessária para administrar sabiamente essa tarefa simples.

Compare isto com algumas práticas de seleção de nossa igreja contemporânea. Ao escolher as pessoas para ocupar posições profissionais no ministério, usualmente observamos primeiro a formação educacional. Pessoas que têm uma graduação num seminário tornam-se os principais candidatos para conduzirem o povo de Cristo, frequentemente, antes de ter o discernimento se esses líderes potenciais apresentam evidências de serem controlados pelo Espírito. Então, os candidatos devem passar por uma bateria de testes psicológicos para observar se são compatíveis com o ministério, como se a ciência fosse decisiva para a sabedoria. Mas, na igreja do [[Novo Testamento]], mesmo a tarefa de distribuir comida para viúvas requeria líderes que fossem controlados pelo Espírito e cheios de sabedoria.

Se todos os cristãos fossem [[Como receber o Espírito Santo|cheios do Espírito Santo]], se todos na comunidade fossem controlados pelo Espírito, os apóstolos não teriam definido tal qualificação. De fato, seria totalmente tolo. Imagine-os dizendo: _“Escolham sete pessoas que estão respirando”_. Entretanto, ser um cristão não garante, necessariamente, que a pessoa viva [[4 coisas que o Espírito Santo faz em sua vida|uma vida controlada pelo Espírito]]. A pessoa precisa gastar tempo na presença de Deus até “serem revestidos do poder do alto” ([[Lucas]] 24:49).

<p>&nbsp;</p>

> [!info] **Jim Cymbala** iniciou a _Brooklyn Tabernacle_ com menos de vinte membros, num prédio pequeno e degradado, num bairro difícil da cidade. Nativo do Brooklyn, é um amigo de longa data de David e Gary Wilkerson.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“TEMPO NA PRESENÇA DE DEUS”_ | por Jim Cymbala • World Challenge • Portuguese Devotional
- December 27, 2022

<p>&nbsp;</p>

##### World Challenge • Portuguese Devotional

- **Ontem:** [[WC-pt - 2022-12-27 - Se você soubesse|Dia 361]]
- **Amanhã:** [[WC-pt - 2022-12-29 - Esperando em Jesus|Dia 363]]

<p>&nbsp;</p>

###### tags:

#Devocional/WC-pt #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/devocional 

<p>&nbsp;</p>

