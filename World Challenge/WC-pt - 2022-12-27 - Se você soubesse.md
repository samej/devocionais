---
title: "SE VOCÊ SOUBESSE! | World Challenge • Portuguese Devotional"
author: "David Wilkerson"
dates:
  - created: "December 27, 2022"
  - modified: "December 27, 2022"
tags: [" Devocional/WC-pt RELIGIAO/Cristianismo RELIGIAO/Cristianismo/devocional "]
aliases: ["Jesus nosso porto seguro", "segurança em Cristo", "descanso em Cristo", "Jesus verdadeira liberdade"]
abstract: "Dia 361 de 365 do Devocional “World Challenge • Portuguese Devotional”."
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- SE VOCÊ SOUBESSE! (DIA 361) • WC-PT ~ CRIADO EM 2022/12/27 ~ ATUALIZADO EM 2022/12/27 -->

# World Challenge <br />Portuguese Devotional

<p>&nbsp;</p>

## Se você soubesse!

<span class="text-dir" align="right">_Dia 361_  
<small>27 de dezembro</small></span>

<p>&nbsp;</p>

Como cristãos, sabemos que Jesus é a única esperança para o mundo.

[[Paulo de Tarso - quem foi o apóstolo na Bíblia|Paulo]] fala sobre essa esperança quando escreve: “Lembrem-se… que naquela época vocês estavam sem Cristo, separados da comunidade de Israel, sendo estrangeiros quanto às alianças da promessa, SEM ESPERANÇA e sem Deus no mundo. Mas agora… vocês… foram aproximados mediante o sangue de Cristo. POIS ELE É A NOSSA PAZ” ([[Efésios]] 2:11-14, minha ênfase).

A geração perdida de hoje é como a multidão em Jerusalém sobre quem Jesus chorou. As pessoas do tempo de Cristo perderam o que Ele queria dar para eles. Perderam a verdadeira liberdade. Perderam a paz que vem da segurança de ter [[O que é pecado segundo a Bíblia|todos os pecados perdoados]]. Perderam o toque de cura de Jesus. Perderam um abrigo para a tempestade. Perderam a presença do [[4 coisas que o Espírito Santo faz em sua vida|Espírito Santo]] permanente, confortante e orientativa.

<!-- Foi sobre essa multidão de perdidos que Jesus chorou e clamou: “_Se vocês soubessem!_ Se soubessem o que eu queria para suas vidas. Se aceitassem o que eu ofereci a vocês. Eu quis protege-los, recebe-los nos meus braços de conforto. Se tivessem ouvido. Se tivessem conhecido meu amor e graça por vocês” (parafraseando Lucas 19:41-42). -->

Foi sobre essa multidão de perdidos que Jesus chorou e clamou: 

> [!bible] Parafraseando [[Lucas]] 19:41-42
> _Se vocês soubessem!_ Se soubessem o que eu queria para suas vidas. Se aceitassem o que eu ofereci a vocês. Eu quis protege-los, recebe-los nos meus braços de conforto. Se tivessem ouvido. Se tivessem conhecido meu amor e graça por vocês.

Cristo estava dizendo: _“Se você tivesse conhecido a provisão dada pelo meu Pai celestial para você, conheceria a paz que excede todo o entendimento”_. A Bíblia mostra esse mesmo clamor: _“Se você soubesse!”_ de capa à capa. Hoje, nós que cremos, temos esse refúgio, alguém para nos voltarmos em nossas lutas mais difíceis. Certos de que o mesmo poder que passou por aqueles que o rejeitaram é dado de graça para nós que recebemos a oferta de Jesus pela fé.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“SE VOCÊ SOUBESSE!”_ | por [[David Wilkerson|David Wilkerson]] • World Challenge • Portuguese Devotional
- December 27, 2022

<p>&nbsp;</p>

##### World Challenge • Portuguese Devotional

- **Amanhã:** [[WC-pt - 2022-12-28 - Tempo na presença de Deus|Dia 362]]

<p>&nbsp;</p>

###### tags:

#Devocional/WC-pt #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/devocional 

<p>&nbsp;</p>

