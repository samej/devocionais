# Devocionais

<p>&nbsp;</p>

Repositório criado para compartilhar com amigos os devocionais que li, estudei e concluí.

Para melhor visualização, clone/copie os diretórios e arquivos para um cofre no [Obsidian.md](https://obsidian.md/) (pode ser um já existente ou um novo cofre, a escolha é sua).

É possível que você encontre backlinks quebrados, pois há vários deles que apontam para outras notas dos meus arquivos pessoais. Sinta-se livre para remover esses backlinks quebrados.

Conforme eu complete e adicione novos devocionais ao meu cofre no Obsidian, farei o upload aqui no GitLab.

### Recomendações e explicações

Um simples plugin que recomendo terem instalado para melhor visualização dos versículos bíblicos adicionados aos textos e às notas de fim contidas nos arquivos deste repositório é o “[Obsidian Bible Reference](https://github.com/tim-hub/obsidian-bible-reference)”.

Todas as vezes que você se deparar com algum livro bíblico neste formato: “`[[Mateus]]`”, “`[[1-3 João|1 João]]`”, ou “`[[Samuel#2 Samuel]]`”, saiba que é um backlink para uma nota contendo a descrição geral do livro bíblico com seu referido vídeo explicativo. Esses vídeos explicativos são produzidos e publicados pelo “[BibleProject Português](https://bibleproject.com/portugues/)” no [YouTube](https://www.youtube.com/channel/UCRyMEBm9qrb4vEdJOPhRd1w).

Os nomes de personagens bíblicos neste formato: “`[[Quem foi Davi|Davi]]`”, “`[[Quem foram Adão e Eva|Adão e Eva]]`” e demais parecidos com estes são backlinks para a história e/ou descrição dos personagens que contenham esse nome. As fontes desses textos podem ser os sites [Apologeta](https://www.apologeta.com.br/), [Estilo Adoração](https://estiloadoracao.com/personagens-biblicos/), [Respostas Bíblicas](https://www.respostas.com.br/) e outros.

As Bíblias Online que utilizo são duas, nessa ordem:

- [Bolls Bible](https://bolls.life/)

- [YouVersion](https://my.bible.com/pt/bible)

A **[Bolls](https://t.me/bbbible "Bolls Bible on Telegram")** você tanto pode utilizar em seu navegador favorito quanto instalá-la em seu dispositivo (mobile e desktop); é gratuita, sem propagandas e funciona off-line.

A **YouVersion** tem muitos recursos além da Bíblia online, como devocionais, vídeos, etc. Mas não tem versão nativa para desktop (além do navegador), apenas para mobile.

Caso tenha alguma dúvida, sinta-se livre para entrar em contato comigo via [e-mail](mailto:samej@duck.com) ou [Telegram](https://t.me/SamejSpenser) (lembre-se de apresentar-se adequadamente e explicar o motivo do contato para que eu evite me confundir com algum tipo de spam e te bloquear/denunciar por spam).

<p>&nbsp;</p>

***

<p>&nbsp;</p>

**© Todos os direitos reservados a seus respectivos autores. Os autores estão devidamente informados e mencionados em cada devocional.**

<p>&nbsp;</p>