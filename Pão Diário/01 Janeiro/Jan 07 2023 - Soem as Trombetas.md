---
title: "07 de Janeiro - Soem as Trombetas | Pão Diário"
author: "Lisa M. Samra"
dates:
  - created: "January 7, 2023"
  - modified: "January 8, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/lisa_samra "]
aliases: ["", ""]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://anchor.fm/ministerios-pao-diario/episodes/Devocional-soem-as-trombetas---0701-e1srpvd"
  - "https://t.me/paodiariooficial/5689"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 07 DE JANEIRO - SOEM AS TROMBETAS ~ CRIADO EM 2023/01/07 ~ ATUALIZADO EM 2023/01/08 -->

# 07 DE JANEIRO

<p>&nbsp;</p>

## Soem as Trombetas

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Números]] 10:8-10
- **Plano de leitura anual:** [[Gênesis]] 18–19; [[Mateus]] 6:1-18

<p>&nbsp;</p>

> [!bible]- [[Números]] 10:8-10 • VFL
> ⁸ Os filhos de Aarão, os sacerdotes, serão os encarregados de tocar as trombetas. Esta será uma lei que sempre obedecerão todos vocês e também as gerações por vir.
> 
> ⁹ — Quando estiverem na sua terra e o inimigo os atacar, vocês terão que sair para lutar. Nessas ocasiões toquem as trombetas e gritem forte, assim eu, o SENHOR, seu Deus, prestarei atenção e os salvarei dos seus inimigos.
> 
> ¹⁰ Também deverão tocar as trombetas em ocasiões de alegria, como nos dias de festa e em suas festas de Lua Nova. Também tocarão quando oferecerem suas ofertas que devem ser queimadas completamente e suas ofertas para festejar. Assim, eu me lembrarei de vocês porque eu sou o SENHOR, seu Deus.

<p>&nbsp;</p>

<span class="text-center" align="center">![[Min. Pão Diário - 07 de Janeiro - Soem as Trombetas.mp3]]  
<small>**Devocional:** _“[Soem as Trombetas](https://anchor.fm/ministerios-pao-diario/episodes/Devocional-soem-as-trombetas---0701-e1srpvd)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

O “Toque do silêncio” é uma chamada de trompete tocada pelos militares no final do dia e em funerais. Fiquei surpresa quando li as diversas letras e descobri que muitos versos terminam com a frase _“Deus está próximo”_. Seja antes que o escurecer se instale ou enquanto se lamenta a perda de um ente querido, suas palavras oferecem aos soldados a reconfortante garantia de que Deus está próximo.

No [[Antigo Testamento]], as trombetas também lembravam os israelitas de que Deus estava próximo. No meio da celebração das festas e festivais que faziam parte da aliança entre Deus e a nação de Israel, os judeus deveriam “soar as trombetas” ([[Números]] 10:10). Fazer soar a trombeta era um lembrete da presença de Deus, e de que o Senhor estava disponível quando os israelitas mais precisavam dEle, e desejava ajudá-los.

Ainda hoje, precisamos dos lembretes de que Deus está próximo. E em nosso estilo de adoração também podemos invocar a Deus em oração e louvor. Talvez as nossas orações possam soar como as trombetas clamando a Deus para nos ajudar. E o belo incentivo é que os ouvidos de Deus estão abertos às nossas orações ([[Pedro#1 Pedro|1 Pedro]] 3:12).<small>[^1]</small> A cada um de nossos apelos, Ele responde com a certeza de Sua presença que nos fortalece e nos conforta nas dificuldades e tristezas da vida.

[^1]: > [!bible] [[Pedro#1 Pedro|1 Pedro]] 3:12 • VFL
    > Porque o Senhor cuida dos justos e ouve as suas orações. Mas o Senhor está contra aqueles que fazem o mal”.

<p>&nbsp;</p>

<span class="text-dir" align="right">**Lisa M. Samra**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 06 2023 - Profundezas do Amor|« Jan 6 2023]] • [[Jan 08 2023 - Deus dos Invisíveis|Jan 8 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[07 DE JANEIRO - SOEM AS TROMBETAS](https://t.me/paodiariooficial/5689)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 7, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/lisa_samra 

<p>&nbsp;</p>
