---
title: "04 de Janeiro - Adoração como Estilo de Vida | Pão Diário"
author: "Xochitl E. Dixon"
dates:
  - created: "January 4, 2023"
  - modified: "May 13, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/xochitl_dixon estilo_de_vida RELIGIAO/Cristianismo/adoracao "]
aliases: ["estilo de vida", "adoração como estilo de vida"]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://podcastaddict.com/episode/150951881"
  - "https://t.me/paodiariooficial/5662"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 04 DE JANEIRO - ADORAÇÃO COMO ESTILO DE VIDA ~ CRIADO EM 2023/01/04 ~ ATUALIZADO EM 2023/05/13 -->

# 04 DE JANEIRO

<p>&nbsp;</p>

## Adoração como Estilo de Vida

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Salmos|Salmo]] 100
- **Plano de leitura anual:** [[Gênesis]] 10–12; [[Mateus]] 4

<p>&nbsp;</p>

> [!bible]- [[Salmos|Salmo]] 100
> ¹ Salmo de agradecimento. Cantem com alegria ao SENHOR, ó habitantes de toda a terra!
> 
> ² Adorem o SENHOR com alegria e entrem na sua presença com cânticos alegres.
> 
> ³ Reconheçam que o SENHOR é Deus. Foi ele que nos criou e é a ele que pertencemos. Nós somos o seu povo, e ovelhas do seu rebanho.
> 
> ⁴ Entrem pelas portas do seu templo com cânticos de agradecimento, e nos seus pátios com cânticos de louvor. Agradeçam-lhe e louvem o seu nome!
> 
> ⁵ Pois o SENHOR é bom, o seu amor dura para sempre, e ele é fiel por todas as gerações.

<p>&nbsp;</p>

<span class="text-center" align="center">![Link do episódio em .mp3](https://d3ctxlq1ktw2nl.cloudfront.net/staging/2022-11-29/c1227eb2-ef2f-8b18-f97d-544565cd4c16.mp3)  
<small>**Devocional:** _“[Adoração como Estilo de Vida](https://podcastaddict.com/episode/150951881)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

Eu estava num centro de conferências e esperava na fila pelo café da manhã. Um grupo de mulheres entrou no refeitório. Eu sorri, cumprimentando a pessoa que estava na fila, atrás de mim, que correspondeu dizendo: _“Eu te conheço”_. Nós nos servimos e tentamos descobrir de onde nos conhecíamos, mas eu tinha a certeza de que ela me confundira com alguém.

No almoço, ela me perguntou: _“Você dirige um carro branco?”_. Concordei. _“Eu costumava, alguns anos atrás”_. Ela riu e disse: _“Parávamos no mesmo semáforo quase todas as manhãs. Você cantava com alegria e mãos levantadas e sempre pensei que você estivesse adorando a Deus. Isso me fazia querer cantar, mesmo em dias difíceis”_. Louvamos a Deus, oramos e almoçamos juntas.

As pessoas percebem como os cristãos se comportam, mesmo quando pensamos que ninguém nos observa. À medida que adotamos um estilo de vida de adoração, podemos entrar na presença do nosso Criador a qualquer hora e em qualquer lugar. Quando [[Como é o amor de Deus por nós|reconhecemos o Seu amor e fidelidade]], podemos usufruir da comunhão íntima com Ele e agradecê-Lo pelos Seus cuidados contínuos ([[Salmos|Salmo]] 100). Podemos inspirar outros, louvando-O em nosso carro, orando em público ou espalhando o Seu amor por meio de atos gentis para que todos _“louvem o seu nome”_ (v. 4). [[Adoração que Agrada ao Senhor|Adorar a Deus]] é mais do que um compromisso no domingo de manhã.

<p>&nbsp;</p>

<span class="text-dir" align="right"> — por _Xochitl E. Dixon_</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center meio" align="center">**[[Jan 03 2023 - Movendo-se na Velocidade de Jesus|« Jan 03 2023]] • [[Jan 05 2023 - Um Processo de Amadurecimento|Jan 05 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[04 DE JANEIRO - ADORAÇÃO COMO ESTILO DE VIDA](https://t.me/paodiariooficial/5662)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 4, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/xochitl_dixon #estilo_de_vida #RELIGIAO/Cristianismo/adoracao 

<p>&nbsp;</p>
