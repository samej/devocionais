---
title: "08 de Janeiro - Deus dos Invisíveis | Pão Diário"
author: "James Banks"
dates:
  - created: "January 8, 2023"
  - modified: "January 8, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/james_banks "]
aliases: ["Deus do invisível", "Deus trabalha nos bastidores", "ação invisível de Deus"]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://anchor.fm/ministerios-pao-diario/episodes/Devocional-Deus-dos-invisveis---0801-e1srq0r"
  - "https://t.me/paodiariooficial/5698"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 08 DE JANEIRO - SOEM AS TROMBETAS ~ CRIADO EM 2023/01/08 ~ ATUALIZADO EM 2023/01/08 -->

# 08 DE JANEIRO

<p>&nbsp;</p>

## Deus dos Invisíveis

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[João]] 1:35-42
- **Plano de leitura anual:** [[Gênesis]] 20–22; [[Mateus]] 6:19-34

<p>&nbsp;</p>

> [!bible]- [[João]] 1:35-42 • VFL
> ³⁵ No dia seguinte, João estava no mesmo local e com ele estavam dois dos homens que o seguiam.
> 
> ³⁶ Ao ver Jesus passar por ali, João disse: — Olhem o Cordeiro de Deus!
> 
> ³⁷ Os dois homens que seguiam a João ouviram o que ele disse e foram atrás de Jesus.
> 
> ³⁸ Jesus, virando-se para trás, viu que eles o seguiam e disse: — O que vocês querem? Eles perguntaram: — Rabi, onde é que o senhor mora? (Rabi quer dizer “Mestre”.)
> 
> ³⁹ Jesus respondeu: — Venham comigo e verão. Então os dois homens foram com ele, viram o lugar onde Jesus morava e ficaram lá com ele nesse dia. Eram mais ou menos quatro horas da tarde.
> 
> ⁴⁰ [[Quem foi André|André, irmão de Simão]] Pedro, era um dos dois homens que tinham ido atrás de Jesus depois de terem ouvido João falar dele.
> 
> ⁴¹ Logo depois, André foi procurar seu irmão Simão e lhe disse: — Encontramos o Messias! (Messias quer dizer “o Rei Escolhido por Deus”.)
> 
> ⁴² Depois André levou Simão a Jesus. Jesus olhou para Simão e disse: — Você é Simão, filho de João; de agora em diante será chamado Cefas. (Cefas quer dizer “Pedro”.)

<p>&nbsp;</p>

<span class="text-center" align="center">![[Min. Pão Diário - 08 de Janeiro - Deus dos Invisíveis.mp3]]  
<small>**Devocional:** _“[Deus dos Invisíveis](https://anchor.fm/ministerios-pao-diario/episodes/Devocional-Deus-dos-invisveis---0801-e1srq0r)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

> [!quote] Às vezes me sinto como se fosse invisível. Mas quero tanto que Deus me use.

<p>&nbsp;</p>

Ana limpava a academia do hotel quando começamos a conversar. Descobri que ela tinha uma história incrível quando a ouvi: _“Eu era uma prostituta viciada em crack e vivia nas ruas. Mas sabia que Deus queria que abandonasse aquela vida e andasse em Sua presença. Um dia, anos atrás, ajoelhei-me aos pés de Jesus, e Ele me libertou”_. Agradeci por compartilhar o que o Senhor tinha feito por ela, pois Ele a usou em nossa conversa de maneira tão linda para me relembrar do Seu poder para transformar vidas.

Deus gosta de usar pessoas que outros tendem a ignorar. O [[Biografia de Santo André - eBiografia|apóstolo André]] não é tão conhecido como o seu irmão [[História de Pedro — Os altos e baixos na vida do apóstolo Pedro|Pedro]], mas a Bíblia relata que a primeira coisa que André fez ao encontrar seu irmão Simão \[Pedro\] foi dizer-lhe: _“‘Encontramos o Messias’ […] Então André levou Simão para conhecer Jesus”_ ([[João]] 1:41-42).

Pedro conheceu Jesus por meio de André. Quando André, um dos discípulos de [[Biografia de João Batista - eBiografia|João Batista]], soube sobre Jesus por João, ele O seguiu, creu nEle e imediatamente contou ao seu irmão. A silenciosa fidelidade de André teve um impacto que abalaria o mundo. Deus valoriza o serviço fiel acima da fama. Ele pode nos usar poderosamente onde quer que estejamos, mesmo quando ninguém nos observa.

<p>&nbsp;</p>

<span class="text-dir" align="right">**James Banks**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 07 2023 - Soem as Trombetas|« Jan 7 2023]] • [[Jan 09 2023 - Desenterre|Jan 9 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[08 DE JANEIRO - DEUS DOS INVISÍVEIS](https://t.me/paodiariooficial/5698)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 8, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/james_banks 

<p>&nbsp;</p>
