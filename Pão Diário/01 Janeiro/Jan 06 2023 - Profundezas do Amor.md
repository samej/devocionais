---
title: "06 de Janeiro - Profundezas do Amor | Pão Diário"
author: "Elisa Morgan"
dates:
  - created: "January 6, 2023"
  - modified: "January 6, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/elisa_morgan RELIGIAO/Cristianismo/amor_de_Deus "]
aliases: ["amor de Deus", ""]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://podcastaddict.com/episode/151095720"
  - "https://t.me/paodiariooficial/5685"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 06 DE JANEIRO - PROFUNDEZAS DO AMOR ~ CRIADO EM 2023/01/06 ~ ATUALIZADO EM 2023/01/06 -->

# 06 de Janeiro

<p>&nbsp;</p>

## Profundezas do Amor

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[João]] 3:1-6
- **Plano de leitura anual:** [[Gênesis]] 16–17; [[Mateus]] 5:27-48

<p>&nbsp;</p>

> [!bible]- [[João]] 3:1-6
> ¹ Havia entre os fariseus um homem chamado Nicodemos. Ele era um dos líderes do povo judeu.
> 
> ² Uma noite Nicodemos foi até Jesus e disse: — Mestre, nós sabemos que o senhor foi enviado por Deus para nos ensinar, pois ninguém pode fazer os [[Milagres do Antigo Testamento|milagres]] que o senhor faz a não ser que Deus esteja com ele.
> 
> ³ Jesus respondeu: — Digo-lhe a verdade: Se uma pessoa não nascer de novo, ela não pode fazer parte do reino de Deus.
> 
> ⁴ Nicodemos disse: — Mas se um homem já é velho, como pode ele nascer de novo? Ele não pode entrar outra vez no ventre de sua mãe. Portanto, como pode nascer pela segunda vez?
> 
> ⁵ Jesus respondeu: — Digo-lhe a verdade: Se uma pessoa não nascer da água e do Espírito, ela não pode entrar no reino de Deus.
> 
> ⁶ A pessoa que nasce de pais humanos é um ser de este mundo. Mas a pessoa que nasce do Espírito é um ser do mundo espiritual.

<p>&nbsp;</p>

<!-- <span class="text-center" align="center">![Link do episódio em .mp3](https://anchor.fm/s/c830ed0/podcast/play/62825844/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-11-29%2F6dbd95e8-abe6-09e0-d68d-207551f37c47.mp3)  
<small>**Devocional:** _“[Profundezas do Amor](https://podcastaddict.com/episode/151095720)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span> -->

<span class="text-center" align="center">![[Ministérios_Pão_Diário_20230106_Devocional_profundezas_do_amor.mp3]]  
<small>**Devocional:** _“[Profundezas do Amor](https://podcastaddict.com/episode/151095720)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

Daniel, de 3 anos, tinha acabado de aprender a nadar quando pisou numa madeira podre e caiu num poço de 12 metros de profundidade no quintal de seu avô. Daniel conseguiu boiar, apesar dos 3m de água abaixo dele, até que seu pai desceu para resgatá-lo. Os bombeiros trouxeram cordas para resgatar o menino, mas o pai estava tão preocupado com o filho que ele já havia descido pelas pedras escorregadias para certificar-se de que Daniel estava seguro. Até onde vai o amor de um pai pelos filhos!

Quando [[Quem foi João Evangelista|João]] escreveu aos cristãos da [[Estudo Sobre a História da Igreja Primitiva|Igreja Primitiva]], que lutavam para encontrar o fundamento de sua fé enquanto o falso ensino corria solto entre eles, o apóstolo lançou estas palavras como verdadeiro salva-vidas: _“Vejam como é grande o amor do Pai por nós, pois ele nos chama de filhos, o que de fato somos!”_ ([[1-3 João|1 João]] 3:1). Afirmar que os crentes em Jesus são “filhos” de Deus expressou a comunhão íntima e legal que validou o relacionamento de todos que confiam no Senhor. A que distâncias e profundezas Deus irá por Seus filhos!

Há ações que apenas um pai tomará por seu filho, como descer num poço para resgatá-lo. Assim foi o supremo ato de nosso Pai celestial, que enviou o Seu único Filho para nos trazer [[O Plano de Deus para o Homem|a salvação]] e nos aproximar do Seu coração, restaurando-nos à vida com Ele (vv. 5-6).

<p>&nbsp;</p>

<span class="text-dir" align="right">**Elisa Morgan**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 05 2023 - Um Processo de Amadurecimento|« Jan 5 2023]] • [[Jan 07 2023 - Soem as Trombetas|Jan 7 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[06 DE JANEIRO - PROFUNDEZAS DO AMOR](https://t.me/paodiariooficial/5685)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 6, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/elisa_morgan #RELIGIAO/Cristianismo/amor_de_Deus 

<p>&nbsp;</p>

