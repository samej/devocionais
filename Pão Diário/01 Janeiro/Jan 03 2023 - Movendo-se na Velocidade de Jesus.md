---
title: "03 de Janeiro - Movendo-se na Velocidade de Jesus | Pão Diário"
author: "Adam Holz"
dates:
  - created: "January 3, 2023"
  - modified: "January 6, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/adam_holz "]
aliases: ["por que Deus permite o sofrimento", ""]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://podcastaddict.com/episode/150911016"
  - "https://t.me/paodiariooficial/5651"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 03 DE JANEIRO - MOVENDO-SE NA VELOCIDADE DE JESUS ~ CRIADO EM 2023/01/03 ~ ATUALIZADO EM 2023/01/06 -->

# 03 DE JANEIRO

<p>&nbsp;</p>

## MOVENDO-SE NA VELOCIDADE DE JESUS

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[João]] 11:14-27
- **Plano de leitura anual:** [[Gênesis]] 7–9; [[Mateus]] 3

<p>&nbsp;</p>

> [!bible]- [[João]] 11:14-27 • VFL
> ¹⁴ Então Jesus lhes disse claramente: — Lázaro morreu,
> 
> ¹⁵ e eu estou contente por não ter estado lá, pois assim vocês podem crer. Vamos até lá para vê-lo.
> 
> ¹⁶ Então Tomé (o que se chama Dídimo) disse aos outros discípulos: — Vamos nós também, para morrermos com Jesus.
> 
> ¹⁷ Quando Jesus chegou em Betânia, soube que Lázaro já tinha sido enterrado há quatro dias.
> 
> ¹⁸ (Betânia ficava cerca de três quilômetros de Jerusalém
> 
> ¹⁹ e muitos dos judeus tinham ido para lá confortar [[Marta e Maria — estudo bíblico sobre a prioridade da vida cristã|Marta e Maria]] pela morte de seu irmão.)
> 
> ²⁰ Quando Marta ouviu dizer que Jesus estava chegando, foi ao seu encontro, mas Maria ficou em casa, sentada.
> 
> ²¹ Marta disse a Jesus: — Senhor, se tivesse estado aqui o meu irmão não teria morrido.
> 
> ²² Mas eu sei que, mesmo agora, Deus dará tudo o que lhe pedir.
> 
> ²³ Jesus disse a ela: — O seu irmão se levantará e viverá outra vez.
> 
> ²⁴ Marta respondeu: — Eu sei que ele se levantará e viverá outra vez na ressurreição, no último dia.
> 
> ²⁵ Jesus lhe disse: — Eu sou a ressurreição e a vida. Quem crê em mim, mesmo que esteja morto, viverá;
> 
> ²⁶ e todo aquele que vive e crê em mim nunca morrerá. Você crê nisto?
> 
> ²⁷ Marta respondeu: — Sim, Senhor! Eu creio que o senhor é o Messias, o Filho de Deus, que estava para vir ao mundo.

<p>&nbsp;</p>

<span class="text-center" align="center">![Link do episódio em .mp3](https://anchor.fm/s/c830ed0/podcast/play/62825695/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-11-29%2F7ca8539c-16fa-acf0-8170-2eae3c5509ce.mp3)  
<small>**Devocional:** _“[Movendo-se na Velocidade de Jesus](https://podcastaddict.com/episode/150911016)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

Quando meu carro parou de funcionar, eu decidi ir a pé para casa, pois moro perto de uma oficina mecânica. Passei por uma via movimentada e notei que todos se moviam com enorme rapidez. Não é mistério que os carros sejam mais velozes que os pedestres, e estamos tão acostumados a nos movermos sempre rápido. Muitas vezes, espero que Deus também aja assim, e que os planos dEle se encaixem no meu ritmo.

Quando Jesus vivia na Terra, às vezes, o Seu ritmo aparentemente lento desapontava os Seus amigos. Quando [[Marta e Maria — estudo bíblico sobre a prioridade da vida cristã|Marta e Maria]] avisaram Jesus que o irmão delas, Lázaro, estava doente, ambas sabiam que Jesus podia ajudar ([[João]] 11:1-3), porém Ele chegou quatro dias após Lázaro morrer (v. 17). Marta disse a Jesus _“Se o Senhor estivesse aqui, meu irmão não teria morrido”_ (v. 21). Ou seja, Jesus não se movera rápido o suficiente. No entanto, Ele tinha outros planos: ressuscitar Lázaro (vv.38-44).

Você consegue entender o desespero da Marta? Eu consigo. Às vezes, anseio que Jesus Se apresse para responder certa oração. Outras, parece que Ele está atrasado, contudo a Sua agenda soberana é diferente da nossa. Jesus cumpre a Sua vontade [[O que significa o tempo de Deus|em Seu tempo, não no nosso]]. E, ao final, Ele demonstra a [[O que a Bíblia diz sobre a bondade de Deus|Sua glória e bondade]] de maneiras que são muito maiores do que os nossos planos.

<p>&nbsp;</p>

<span class="text-dir dire" align="right">**Adam Holz**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 02 2023 - Águas Profundas|« Jan 02 2023]] • [[Jan 04 2023 - Adoração como Estilo de Vida|Jan 04 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[03 DE JANEIRO - MOVENDO-SE NA VELOCIDADE DE JESUS](https://t.me/paodiariooficial/5651)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 3, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/adam_holz 

<p>&nbsp;</p>
