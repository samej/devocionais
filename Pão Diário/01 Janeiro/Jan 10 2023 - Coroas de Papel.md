---
title: "10 de Janeiro - Coroas de Papel | Pão Diário"
author: "Jennifer Benson Schuldt"
dates:
  - created: "January 10, 2023"
  - modified: "January 10, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/jennifer_benson_schuldt "]
aliases: ["disputas judiciais", "julgaremos o mundo", "julgaremos os anjos", "julgar esta sociedade injusta", "processos judiciais"]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://anchor.fm/ministerios-pao-diario/episodes/Devocional-coroas-de-papel---1001-e1srq3d"
  - "https://podcastaddict.com/episode/XXXX"
  - "https://t.me/paodiariooficial/5790"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 10 DE JANEIRO - COROAS DE PAPEL ~ CRIADO EM 2023/01/10 ~ ATUALIZADO EM 2023/01/10 -->

# 10 DE JANEIRO

<p>&nbsp;</p>

## COROAS DE PAPEL

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Coríntios|1 Coríntios]] 6:1-6
- **Plano de leitura anual:** [[Gênesis]] 25–26; [[Mateus]] 8:1-17

<p>&nbsp;</p>

> [!bible]- [[Coríntios|1 Coríntios]] 6:1-6 • VFL
> ¹ Quando algum de vocês tem uma queixa contra um irmão ou uma irmã na fé, por que vai a juízes que não são do povo de Deus? Por que não deixa o povo de Deus decidir quem tem razão?
> 
> ² Vocês não sabem que o povo de Deus vai julgar esta sociedade injusta? E se vocês vão julgar esta sociedade injusta, não são por acaso capazes de julgar sequer estes processos judiciais?
> 
> ³ Não sabem que julgaremos até os anjos? Quanto mais, então, os assuntos do dia a dia!
> 
> ⁴ Entretanto, quando vocês têm desacordos para serem julgados, por que vocês levam estas coisas para homens que não têm nenhuma aceitação na igreja?
> 
> ⁵ Digo isto para deixar vocês envergonhados. Será que não há pelo menos um entre vocês, que tenha bastante sabedoria para julgar desacordos entre irmãos?
> 
> ⁶ Mas um irmão leva ao tribunal outro irmão, e vocês deixam que pessoas que não acreditam em Cristo os julguem!

<p>&nbsp;</p>

<span class="text-center" align="center">![[Min. Pão Diário - 10 de Janeiro - Coroas de Papel.mp3]]  
<small>**Devocional:** _“[Coroas de Papel](https://anchor.fm/ministerios-pao-diario/episodes/Devocional-coroas-de-papel---1001-e1srq3d)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<!-- <span class="text-center" align="center">![Link do episódio em .mp3](link)  
<small>**Devocional:** _“[Coroas de Papel](https://podcastaddict.com/episode/XXXX)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span> -->

<p>&nbsp;</p>

Depois de uma refeição festiva em minha casa, todos abriram seus pacotes com doces, lembrancinhas e confetes. Mas havia algo mais: uma coroa de papel para cada um. Não resistimos e as colocamos enquanto sorríamos uns para os outros, sentados ao redor da mesa. Por um momento, éramos reis e rainhas, mesmo que nosso reino fosse de sobras na sala de jantar.

Lembrei-me então de uma promessa bíblica: na próxima vida, todos os cristãos governarão com Jesus. Paulo menciona isso em [[Coríntios|1 Coríntios]] 6 e questiona: _“Vocês não sabem que um dia nós, os santos, julgaremos o mundo?”_ (v. 2). Ele mencionou esse privilégio futuro para inspirar os cristãos a resolverem suas disputas de maneira pacífica, pois muitas eram as discussões jurídicas entre eles. Consequentemente, isso prejudicava a reputação de outros cristãos da comunidade.

Tornamo-nos melhores na resolução de conflitos à medida que o [[O que é o Espírito Santo na Bíblia|Espírito Santo]] produz em nós [[Versículos sobre Domínio Próprio|o autocontrole]], [[O que são e quais são os dons do Espírito Santo#O que são os 7 dons do Espírito Santo|a gentileza e a paciência]]. Quando Jesus voltar e completar o trabalho do Espírito em nós ([[1-3 João|1 João]] 3:2-3), estaremos prontos para o nosso eventual papel de _“sacerdotes para nosso Deus”_ ([[Apocalipse]] 5:10).<small>[^1]</small> Firmemo-nos nessa promessa que brilha nas Escrituras como um diamante numa coroa de ouro.

[^1]: > [!bible] [[1-3 João|1 João]] 3:2-3 • VFL
    > ² Queridos amigos, agora somos filhos de Deus, mas ainda não sabemos o que vamos ser. Nós sabemos que, quando Cristo voltar, seremos iguais a ele, pois o veremos como ele é.
    > 
    > ³ E todo aquele que tem esta esperança em Cristo, purifica a si mesmo, assim como ele é puro.
    
    > [!bible] [[Apocalipse]] 5:10 • VFL
    > Fez delas um reino. O Senhor também fez delas sacerdotes para o nosso Deus e elas reinarão sobre toda a terra”.

<p>&nbsp;</p>

<span class="text-dir" align="right">**Jennifer Benson Schuldt**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 09 2023 - Desenterre|« Jan 09 2023]] • [[Jan 11 2023 - Amanhã|Jan 11 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[10 DE JANEIRO - COROAS DE PAPEL](https://t.me/paodiariooficial/5790)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 10, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/jennifer_benson_schuldt 

<p>&nbsp;</p>

