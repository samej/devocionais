---
title: "01 de Janeiro - Por que Deus permite o sofrimento? | Pão Diário"
author: "Paschoal Piragine Jr."
dates:
  - created: "January 1, 2023"
  - modified: "January 6, 2023"
tags: [" bondade DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/Paschoal_Piragine_Jr justiça paschoal_piragine_jr sofrimento "]
aliases: ["por que Deus permite o sofrimento", ""]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://t.me/paodiariooficial/5615"
  - "https://podcastaddict.com/episode/150863729"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 01 DE JANEIRO - POR QUE DEUS PERMITE O SOFRIMENTO? ~ CRIADO EM 2023/01/01 ~ ATUALIZADO EM 2023/01/06 -->

# 01 DE JANEIRO

<p>&nbsp;</p>

## POR QUE DEUS PERMITE O SOFRIMENTO?

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Apocalipse]] 21
- **Plano de leitura anual:** [[Gênesis]] 1–3; [[Mateus]] 1

<p>&nbsp;</p>

> [!bible]- [[Apocalipse]] 21
> ¹ Depois vi um novo céu e uma nova terra. O primeiro céu e a primeira terra tinham desaparecido e o mar já não existia mais.
> 
> ² Vi também a cidade santa, a nova Jerusalém, que descia do céu, da parte de Deus. Ela estava vestida como uma noiva enfeitada para o seu marido.
> 
> ³ Então ouvi uma voz forte que vinha do trono, dizendo: — Agora, a morada de Deus vai ser com os homens. Deus habitará com eles e eles serão povos de Deus. Então, o próprio Deus estará com eles e Ele lhes será por Deus.
> 
> ⁴ Deus enxugará todas as lágrimas de seus olhos e a morte já não existirá mais. Não haverá mais luto, nem choro e nem dor, porque as coisas velhas já passaram.
> 
> ⁵ E aquele que estava sentado no trono disse: — Olhem, Eu estou fazendo tudo novo! E acrescentou: — Escreva isto, porque estas palavras são verdadeiras e dignas de confiança.
> 
> ⁶ E Ele ainda me disse: — Tudo está feito! Eu sou o Alfa e o Ômega, o Princípio e o Fim. A todos os que têm sede eu darei de beber, de graça, da fonte da água da vida.
> 
> ⁷ Aquele que vencer herdará todas estas coisas e eu serei o seu Deus e ele será o meu filho.
> 
> ⁸ Entretanto, os covardes, os incrédulos, os pervertidos, os assassinos, os que praticam [[Explorando a Perspectiva Bíblica sobre a Imoralidade|imoralidade sexual]], os que praticam a feitiçaria, os que adoram ídolos e todos os mentirosos terão a sua parte no lago que queima com fogo e enxofre. E essa é a segunda morte.
> 
> ⁹ Depois, um dos sete anjos que tinham as sete taças com as últimas sete pragas aproximou-se de mim e disse: — Venha, eu vou lhe mostrar a noiva, a esposa do Cordeiro.
> 
> ¹⁰ Enquanto eu estava sendo guiado pelo Espírito, o anjo me levou até uma montanha grande e alta e me mostrou a santa cidade de Jerusalém, que descia do céu, da parte de Deus.
> 
> ¹¹ Ela tinha a glória de Deus e o seu brilho parecia o brilho de uma pedra preciosíssima, assim como a pedra cristalina de jaspe.
> 
> ¹² A cidade tinha uma muralha muito grande e alta com doze portas. Havia um anjo em cada porta e, nas portas, estavam escritos os nomes das doze tribos de Israel.
> 
> ¹³ Havia três portas no leste, três no norte, três no sul e três no oeste.
> 
> ¹⁴ A muralha da cidade tinha doze alicerces e, em cada um deles, estava escrito o nome de um dos doze apóstolos do Cordeiro.
> 
> ¹⁵ O anjo que falava comigo tinha uma vara de ouro para medir a cidade, as suas portas e a sua muralha.
> 
> ¹⁶ A cidade era perfeitamente quadrada; o seu comprimento era igual à sua largura. O anjo mediu a cidade com a vara e deu 2.200 quilômetros de comprimento. O seu comprimento, a sua largura e a sua altura eram iguais.
> 
> ¹⁷ O anjo também mediu a muralha e deu 65 metros, medindo com o braço de um homem, isto é, de um anjo.
> 
> ¹⁸ A estrutura da muralha era de jaspe, e a cidade era feita de ouro puro, brilhante como o cristal.
> 
> ¹⁹ Os alicerces da muralha da cidade eram decorados com todo tipo de pedras preciosas. O primeiro alicerce era de jaspe; o segundo, de safira; o terceiro, de ágata; o quarto, de esmeralda;
> 
> ²⁰ o quinto, de ônix; o sexto, de sárdio; o sétimo, de crisólito; o oitavo, de berilo; o nono, de topázio; o décimo, de crisópraso; o décimo primeiro, de jacinto; e o décimo segundo de ametista.
> 
> ²¹ As doze portas eram doze pérolas e cada uma dessas portas era feita de uma só pérola. As ruas da cidade eram feitas de ouro puro e brilhavam como cristal.
> 
> ²² Não vi nenhum templo na cidade, porque o Senhor Deus Todo-Poderoso e o Cordeiro são o seu templo.
> 
> ²³ A cidade não precisa nem do sol nem da lua para iluminá-la, pois a glória de Deus a ilumina e o Cordeiro é a sua lâmpada.
> 
> ²⁴ As nações serão guiadas pela luz dessa lâmpada e os reis do mundo trarão a sua glória para a cidade.
> 
> ²⁵ As suas portas jamais se fecharão de dia e nela não haverá noite.
> 
> ²⁶ Das nações serão trazidas riqueza e glória para aquela cidade,
> 
> ²⁷ e nela jamais entrará nem coisa impura nem ninguém que faça coisas vergonhosas ou que seja mentiroso. Só entrarão aqueles cujos nomes estão escritos no Livro da Vida que pertence ao Cordeiro.

<p>&nbsp;</p>

<span class="text-center" align="center">![Link do episódio em .mp3](https://anchor.fm/s/c830ed0/podcast/play/62824996/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-11-29%2Fede79b7f-32ea-4714-dee9-b21a04f72262.mp3)  
<small>**Devocional:** _“[Por que Deus permite o sofrimento?](https://podcastaddict.com/episode/150863729)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

Por que Deus, sendo bom, permite o sofrimento? Vários teólogos têm tentado responder a essa pergunta. Mas, quando sofremos, não são as teorias que respondem à nossa dor. Alguns [[Princípios bíblicos de ética cristã|princípios bíblicos]] nos ajudam a superar as dificuldades.

Primeiro, [[Como é o amor de Deus por nós|Deus nos ama]] e, se Ele pudesse nos tiraria todo o sofrimento. Você diz: mas Deus não pode tudo? Por que Ele não pode acabar com o sofrimento? Porque o Senhor só fará isso no dia do julgamento final. A partir desse dia, “não haverá mais morte, nem tristeza, nem choro, nem dor” ([[Apocalipse]] 21:4). Se Deus resolvesse tudo agora, por causa da [[O que é a Justiça de Deus|Sua justiça]] e [[O que a Bíblia diz sobre a bondade de Deus|bondade]], Ele teria de nos julgar imediatamente, e a oportunidade de salvação se perderia para muitos.

Hoje Deus coloca os meios da graça a nosso favor. Quais são eles? No meio da luta, Ele é nosso companheiro; no sofrimento, Ele é nossa consolação; no vale de sombras escuras, Ele é nosso Pastor ([[Salmos|Salmo]] 23); na injustiça, Ele nos coroa com a [[O que significa a minha graça te basta|Sua graça]]. Enquanto estivermos neste corpo, haverá doença, dor, morte; porém um dia, na eternidade, este corpo será transformado e o mal terá passado.

Quando estou sofrendo, vejo apenas as nuvens escuras, mas o meu Deus, o meu sol, está lá atrás delas, e Ele não se esqueceu de mim. Creia que, um dia, toda a injustiça será julgada, e toda lágrima, enxuta.

<p>&nbsp;</p>

<span class="text-dir" align="right">**Paschoal Piragine Jr.**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 02 2023 - Águas Profundas|Jan 02 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[01 DE JANEIRO - POR QUE DEUS PERMITE O SOFRIMENTO?](https://t.me/paodiariooficial/5615)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 1, 2023

<p>&nbsp;</p>

###### tags:

#bondade #DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/Paschoal_Piragine_Jr justiça #justiça #paschoal_piragine_jr #sofrimento 

<p>&nbsp;</p>
