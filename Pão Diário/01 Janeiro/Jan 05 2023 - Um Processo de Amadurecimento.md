---
title: "05 de Janeiro - Um Processo de Amadurecimento | Pão Diário"
author: "Amy Boucher Pye"
dates:
  - created: "January 5, 2023"
  - modified: "January 6, 2023"
tags: [" amadurecimento DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/amy_boucher_pye "]
aliases: ["processo de amadurecimento", ""]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://podcastaddict.com/episode/150951881"
  - "https://t.me/paodiariooficial/5674"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 05 DE JANEIRO - UM PROCESSO DE AMADURECIMENTO ~ CRIADO EM 2023/01/05 ~ ATUALIZADO EM 2023/01/06 -->

# 05 DE JANEIRO

<p>&nbsp;</p>

## Um Processo de Amadurecimento

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Jeremias]] 15:15-18
- **Plano de leitura anual:** [[Gênesis]] 13–15; [[Mateus]] 5:1-26

<p>&nbsp;</p>

> [!bible]- [[Jeremias]] 15:15-18 • VFL
> ¹⁵ O SENHOR sabe o que está acontecendo. Lembre-se de mim, proteja-me e vingue-me daqueles que me perseguem. Não tenha tanta paciência com eles e aceite-me. Compreenda que pelo Senhor suporto a afronta.
> 
> ¹⁶ Quando eu recebia as suas palavras, eu as devorava. Elas eram a minha felicidade e a alegria do meu coração porque o Senhor me escolheu, SENHOR, Deus Todo-Poderoso.
> 
> ¹⁷ Não me assentei para celebrar com os que andam em festas. Me assentei sozinho porque lhe pertenço. O Senhor me encheu de indignação contra eles.
> 
> ¹⁸ Por que não acaba a minha dor? Por que é tão grave e incurável a minha ferida que não quer se curar? O Senhor será como uma miragem para mim ou como uma fonte que não tem água?

<p>&nbsp;</p>

<span class="text-center" align="center">![Link do episódio em .mp3](https://anchor.fm/s/c830ed0/podcast/play/62825749/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-11-29%2Fc1227eb2-ef2f-8b18-f97d-544565cd4c16.mp3)  
<small>**Devocional:** _“[Um Processo de Amadurecimento](https://podcastaddict.com/episode/150951881)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

No início de seu ministério de 50 anos na Inglaterra, Charles Simeon (1759–1836) conheceu seu vizinho, o pastor Henry Venn e suas filhas. Após uma visita, as filhas deste pastor comentaram sobre o quanto Simeon parecia rude e arrogante. O pai lhes pediu que buscassem um pêssego da árvore. Elas lhe questionaram o motivo de querer um fruto ainda imaturo e ouviram: _“Bem, é verde agora, e devemos esperar por um pouco mais de sol e algumas chuvas, logo o pêssego amadurecerá e ficará doce. Assim será com o senhor Simeon”_.

Simeon, ao longo dos anos, foi transformado pela [[O que significa a minha graça te basta|graça de Deus]] e por seu compromisso em ler a Bíblia e [[Necessidade de Oração|orar todos os dias]]. Um amigo que conviveu com ele por alguns meses testemunhou essa sua prática e comentou: _“Esse era o segredo de tanta graça e força espiritual”_.

Simeon, em seu tempo diário com Deus, seguiu a prática do profeta Jeremias, que ouvia fielmente as [[Bíblia — Palavra de Deus|palavras de Deus]] e dependia delas a ponto de dizer: _“Quando descobri tuas palavras, devorei-as”_. O profeta meditava profundamente sobre as palavras de Deus que eram a sua alegria e o prazer do seu coração ([[Jeremias]] 15:16). Se também nós somos semelhantes a uma fruta verde azeda podemos confiar que Deus nos ajudará a amadurecer por meio de Seu Espírito, à medida que o conhecemos [[Quais os benefícios de ler a Bíblia todos os dias|através da leitura]] e obediência à Sua Palavra.

<p>&nbsp;</p>

<span class="text-dir" align="right">**Amy Boucher Pye**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center meio" align="center">**[[Jan 04 2023 - Adoração como Estilo de Vida|« Jan 04 2023]] • [[Jan 06 2023 - Profundezas do Amor|Jan 06 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[05 DE JANEIRO - UM PROCESSO DE AMADURECIMENTO](https://t.me/paodiariooficial/5674)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 5, 2023

<p>&nbsp;</p>

###### tags:

#amadurecimento #DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/amy_boucher_pye 

<p>&nbsp;</p>
