---
title: "09 de Janeiro - Desenterre | Pão Diário"
author: "Anne Cetas"
dates:
  - created: "January 9, 2023"
  - modified: "January 9, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/anne_cetas amargura divorcio ressentimento frustracao "]
aliases: ["raiz de amargura", "raízes de amargura", "expectativa frustrada"]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://anchor.fm/ministerios-pao-diario/episodes/Devocional-desenterre---0901-e1srq1s"
  - "https://podcastaddict.com/episode/XXXX"
  - "https://t.me/paodiariooficial/5781"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 09 DE JANEIRO - DESENTERRE ~ CRIADO 2023/01/09 ~ ATUALIZADO 2023/01/09 -->

# 09 DE JANEIRO

<p>&nbsp;</p>

## DESENTERRE

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Rute]] 1:3-5,20-21
- **Plano de leitura anual:** [[Gênesis]] 23–24; [[Mateus]] 7

<p>&nbsp;</p>

> [!bible]- [[Rute]] 1:3-5,20-21
> ³ Passado algum tempo, Elimeleque, o marido de Noemi, morreu e Noemi ficou sozinha com os seus dois filhos.
> 
> ⁴ Eles se casaram com mulheres moabitas: uma se chamava Orfa e a outra, Rute. Dez anos depois,
> 
> ⁵ Malom e Quiliom também morreram e Noemi ficou sozinha, sem marido e sem filhos.
> 
> ²⁰ Mas ela dizia: — Não me chamem mais de Noemi. A partir de agora devem me chamar de Mara, porque o Deus Todo-Poderoso me fez muito infeliz.
> 
> ²¹ Quando saí daqui, tinha muito, mas agora o SENHOR me fez regressar sem nada. Por isso não me chamem Noemi, porque o SENHOR Todo-Poderoso me fez sofrer muito.

<p>&nbsp;</p>

<span class="text-center" align="center">![[Min. Pão Diário - 09 de Janeiro - desenterre.mp3]]  
<small>**Devocional:** _“[Desenterre](https://anchor.fm/ministerios-pao-diario/episodes/Devocional-desenterre---0901-e1srq1s)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<!-- <span class="text-center" align="center">![Link do episódio em .mp3](link)  
<small>**Devocional:** _“[Desenterre](https://anchor.fm/ministerios-pao-diario/episodes/Devocional-desenterre---0901-e1srq1s)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span> -->

<p>&nbsp;</p>

Quando o seu irmão e a cunhada tiveram problemas matrimoniais, Rebeca orou pela reconciliação, mas eles se divorciaram. Sem os protestos do pai, a mãe levou as filhas para outro estado e Rebeca distanciou-se das amadas sobrinhas. Anos depois, ela disse: _“Por lidar sozinha com essa tristeza, uma raiz de amargura cresceu em meu coração e começou a espalhar-se à família e amigos”_.

Noemi também lutou contra a amargura. Seu marido morreu numa terra estrangeira, e 10 anos depois seus dois filhos morreram. Ela ficou desamparada com as noras, Rute e Orfa ([[Rute]] 1:3-5). [[Quem foi Rute|Quando Rute e sua sogra voltaram para o país natal de Noemi]], todos na cidade se alegraram por vê-las. Mas Noemi lhes disse: _“o Todo-poderoso tornou minha vida muito amarga”_ (v. 20). Ela até lhes pediu para que a chamassem de “Mara”, ou seja, amarga.

Quem de nós nunca enfrentou decepção, foi tentado pela amargura quando alguém disse algo doloroso, teve uma expectativa frustrada ou as exigências dos outros nos deixaram ressentidos? Quando reconhecemos isso diante de Deus e lhe entregamos o que acontece no profundo do nosso coração, nosso afetuoso Jardineiro pode nos ajudar a desenterrar quaisquer raízes de amargura, estejam elas ainda pequenas ou crescendo há anos. Ele pode substituí-las por um espírito doce e alegre.

<p>&nbsp;</p>

<span class="text-dir" align="right">**Anne Cetas**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 08 2023 - Deus dos Invisíveis|« Jan 8 2023]] • [[Jan 10 2023 - Coroas de Papel|Jan 10 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[09 DE JANEIRO - DESENTERRE](https://t.me/paodiariooficial/5781)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 9, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/anne_cetas 

<p>&nbsp;</p>

