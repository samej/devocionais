---
title: "02 de Janeiro - Águas Profundas | Pão Diário"
author: "Sheridan Voysey"
dates:
  - created: "January 2, 2023"
  - modified: "January 6, 2023"
tags: [" DEVOCIONAIS/Pao_Diario DEVOCIONAIS/Pao_Diario/sheridan_voysey "]
aliases: ["águas profundas", ""]
abstract: "Devocional extraído do Grupo no Telegram e episódio do podcast Pão Diário."
links:
  - "https://podcastaddict.com/episode/150863812"
  - "https://t.me/paodiariooficial/5633"
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- 02 DE JANEIRO - ÁGUAS PROFUNDAS ~ CRIADO EM 2023/01/02 ~ ATUALIZADO EM 2023/01/06 -->

# 02 DE JANEIRO

<p>&nbsp;</p>

## ÁGUAS PROFUNDAS

<p>&nbsp;</p>

- **Leitura bíblica do dia:** [[Isaías]] 43:1-7
- **Plano de leitura anual:** [[Gênesis]] 4–6; [[Mateus]] 2

<p>&nbsp;</p>

> [!bible]- [[Isaías]] 43:1-7
> ¹ Assim diz o SENHOR, aquele que criou você, ó Jacó, aquele que o formou, ó Israel: “Não tenha medo, porque eu o resgatei. Eu dei a você seu nome, e por isso me pertence.
> 
> ² Quando você atravessar as águas, estarei com você. Quando você atravessar os rios, não se afogará. Quando você passar pelo fogo, não se queimará, as chamas não o alcançarão.
> 
> ³ Porque eu sou o SENHOR, seu Deus, o Santo de Israel, o seu Salvador. Dou o Egito como resgate pela sua libertação, entrego a Etiópia e Sebá em pagamento por você.
> 
> ⁴ Para mim, você é muito precioso, digno de estima e eu o amo muito. Troco outros por você, e nações pela sua vida.
> 
> ⁵ Não tenha medo, pois eu estou com você. Trarei os seus filhos desde o leste, e desde o oeste os juntarei a você.
> 
> ⁶ Darei esta ordem ao norte: ‘Soltem-nos!’ e ao sul: ‘Deixem eles saírem!’ Tragam os meus filhos de lá longe, tragam as minhas filhas dos confins da terra.
> 
> ⁷ Tragam a mim todos os que são meus, as pessoas que têm o meu nome. Eu fiz todos eles para minha glória, Eu formei todos eles, e eles são meus.

<p>&nbsp;</p>

<span class="text-center" align="center">![Link do episódio em .mp3](https://anchor.fm/s/c830ed0/podcast/play/62825063/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2022-11-29%2F0891026c-8241-19fb-c5b5-78ae9d99604f.mp3)  
<small>**Devocional:** _“[Águas Profundas](https://podcastaddict.com/episode/150863812)”_ • by [[Pão Diário|Ministérios Pão Diário]]</small></span>

<p>&nbsp;</p>

Em Nova Iorque, a bola cai. Em Londres, badala o Big Ben. Na Austrália e no Rio de Janeiro, queimam-se os fogos de artifício. Cada cidade tem sua celebração e recebe, com emoção, o novo ano e o recomeço que ele traz. No Dia de Ano-Novo, somos impelidos para novas águas. Quais amizades e oportunidades encontraremos?

Isso é empolgante, mas pode ser perturbador, pois não sabemos o que o futuro pode nos trazer. Muitas tradições de Ano-Novo refletem tal preocupação: os fogos de artifício foram inventados na China para, supostamente, afastar espíritos malignos e trazer prosperidade. Os babilônios faziam resoluções e votos de Ano-Novo para apaziguar seus deuses, pois queriam tornar o futuro ainda desconhecido em algo mais seguro.

Quando os babilônios não estavam fazendo os seus votos, estavam conquistando povos, inclusive Israel. Porém, Deus enviou aos judeus escravizados a mensagem: _“Não tema […] Quando passar por águas profundas, estarei a seu lado”_ ([[Isaías]] 43:1-2).<small>[^1]</small> Jesus disse algo semelhante quando Ele e os discípulos navegaram numa violenta tempestade. Antes de ordenar que as águas se acalmassem, Jesus lhes perguntou: _“Por que vocês estão com medo?”_ ([[Mateus]] 8:23-27).<small>[^2]</small>

[^1]: > [!bible] [[Isaías]] 43:1-2 • VFL
    > ¹ Assim diz o SENHOR, aquele que criou você, ó Jacó, aquele que o formou, ó Israel: “Não tenha medo, porque eu o resgatei. Eu dei a você seu nome, e por isso me pertence.
    > ² Quando você atravessar as águas, estarei com você. Quando você atravessar os rios, não se afogará. Quando você passar pelo fogo, não se queimará, as chamas não o alcançarão.

[^2]: > [!bible] [[Mateus]] 8:23-27 • VFL
    > ²³ Jesus entrou num barco e os seus discípulos o acompanharam.
    > ²⁴ De repente, uma grande tempestade agitou o lago e as suas ondas eram tão grandes que cobriam o barco. Entretanto, Jesus dormia.
    > ²⁵ Mas os discípulos foram acordá-lo e lhe disseram: — Salve-nos, Senhor, pois estamos prestes a morrer!
    > ²⁶ Jesus, porém, lhes disse: — Por que vocês estão com tanto medo, homens de pouca fé? E, levantando-se, repreendeu o vento e o lago e tudo ficou calmo.
    > ²⁷ Os discípulos ficaram muito espantados e diziam entre si: — Que tipo de homem é este que até o vento e as águas lhe obedecem?

Iniciamos uma nova jornada em direção à águas novas e profundas. O que quer que enfrentemos, Ele está conosco e tem o poder de acalmar as ondas.

<p>&nbsp;</p>

<span class="text-dir dire" align="right">**Sheridan Voysey**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

<span class="text-center" align="center">**[[Jan 01 2023 - Por que Deus permite o sofrimento|« Jan 01 2023]] • [[Jan 03 2023 - Movendo-se na Velocidade de Jesus|Jan 03 2023 »]]**</span>

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“[02 DE JANEIRO - ÁGUAS PROFUNDAS](https://t.me/paodiariooficial/5633)”_ | [[Pão Diário|Ministérios Pão Diário]]
- January 2, 2023

<p>&nbsp;</p>

###### tags:

#DEVOCIONAIS/Pao_Diario #DEVOCIONAIS/Pao_Diario/sheridan_voysey 

<p>&nbsp;</p>
