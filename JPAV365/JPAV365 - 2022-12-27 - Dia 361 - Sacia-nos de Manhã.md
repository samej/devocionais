---
title: "SACIA-NOS DE MANHÃ | JPAV365 — Janelas Para A Vida"
author: "Alejandro Bullón"
dates:
  - created: "December 27, 2022"
  - modified: "December 27, 2022"
tags: [" Devocional/JPAV365 RELIGIAO/Cristianismo RELIGIAO/Cristianismo/devocional "]
aliases: ["saciedade em Deus", "júbilo de gratidão", "salmo de Moisés", "salmos de Moisés"]
abstract: "Dia 361 de 365 do Devocional “Janela Para A Vida”."
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- SACIA-NOS DE MANHÃ (DIA 361) • JPAV365 ~ CRIADO EM 2022/12/27 ~ ATUALIZADO EM 2022/12/27 -->

# Janelas Para A Vida — 365

<p>&nbsp;</p>

## SACIA-NOS DE MANHÃ

<span class="text-dir" align="right">_Dia 361_  
<small>27 de dezembro</small></span>

<p>&nbsp;</p>

<!-- "Sacia-nos de manhã com a Tua benignidade, para que cantemos de júbilo e nos alegremos todos os nossos dias."
([[Salmos|Sl]] 90:14) -->

> [!bible] [[Salmos|Salmo]] 90:14 • VFL
> Encha-nos todas as manhãs com o seu amor fiel e cantaremos de alegria toda a nossa vida.

É madrugada em Brasília. Acordei cedo. Mais cedo que de costume. Precisava fazê-lo. Tenho um grande desafio diante de mim e aprendi que a melhor maneira de enfrentar desafios é buscando ao Senhor. _“Encha-nos todas as manhãs com o seu amor fiel”_, aconselha [[Quem foi Moisés — Respostas Bíblicas|Moisés]]. Este salmo foi escrito por ele. É uma oração que ele fez diante da enorme responsabilidade de conduzir Israel à terra prometida e perceber que tinha pouco tempo para cumprir sua missão.

<!-- "Os dias da nossa vida sobem a setenta anos ou, em havendo vigor, a oitenta; neste caso, o melhor deles é canseira e enfado, porque tudo passa rapidamente, e nós voamos.""
([[Salmos|Sl]] 90:10) -->

> [!bible] [[Salmos|Salmo]] 90:10 • VFL
> Vivemos uns setenta anos, talvez oitenta para os mais fortes. Vivemos sempre trabalhando, sofrendo e, de repente, a nossa vida termina e nós desaparecemos.

A vida passa rapidamente. Repentinamente, você abre os olhos diante do espelho e percebe cabelo branco na cabeça e rugas no rosto. Ainda ontem você era um jovem com a vida desabrochando. O que aconteceu? “Nós desaparecemos”, é a resposta de Moisés.

Se temos tanto para fazer e tão pouco tempo para realizar, precisamos saciar-nos de manhã com a benignidade do Senhor.

No Brasil, temos um ditado: _“Saco vazio não para em pé”_. Seria a versão popular do conselho de Moisés. Gastar tempo com Deus é inversão. Experimente. O dia que tiver mais dificuldades, quando você sentir que os desafios são maiores do que suas forças, esqueça tudo e, de manhã, fique a sós com Jesus. Abra-Lhe o coração. Diga-Lhe que está com medo, que precisa de forças, que talvez não sabe qual é a melhor saída.

Você verá. Não é possível explicar com palavras. Você simplesmente verá como as coisas se descomplicam naturalmente. A luz entra na mente confusa. O temor desaparece. O céu fica azul e você está pronto a viver o mais extraordinário dia de sua vida.

Faça dessa experiência uma realidade, hoje. Moisés afirma que o resultado será um cântico de júbilo no coração. Júbilo é a explosão de gratidão e de louvor. Endorfina pura. Um coquetel vitamínico para a alma agoniada pelos pesares da vida.

Parta agora para a luta do dia, conservando esta oração: _“Sacia-nos de manhã com a Tua benignidade, para que cantemos de júbilo e nos alegremos todos os nossos dias”_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“SACIA-NOS DE MANHÃ”_ | por Alejandro Bullón • JPAV365 — Janelas Para A Vida
- December 27, 2022

<p>&nbsp;</p>

##### JPAV365 — Janelas Para A Vida

- **Amanhã:** [[JPAV365 - 2022-12-28 - Dia 362 - Primeiro Ouça|Dia 362]]

<p>&nbsp;</p>

###### tags:

#Devocional/JPAV365 #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/devocional 

<p>&nbsp;</p>

