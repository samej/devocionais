---
title: "PRIMEIRO OUÇA | JPAV365 — Janelas Para A Vida"
author: "Alejandro Bullón"
dates:
  - created: "December 28, 2022"
  - modified: "December 28, 2022"
tags: [" Devocional/JPAV365 RELIGIAO/Cristianismo RELIGIAO/Cristianismo/devocional "]
aliases: ["preste atenção", "responder antes de ouvir", "tolice e vergonha", "estultícia e vergonha", "a arte de ouvir", "falar antes de ouvir", "aprenda a ouvir", "ouvir a voz de Deus"]
abstract: "Dia 362 de 365 do Devocional “Janela Para A Vida”."
breaks: "false"
GA: "UA-21920826-1"
lang: "pt-BR"
---

<!-- PRIMEIRO OUÇA (DIA 362) • JPAV365 ~ CRIADO EM 2022/12/28 ~ ATUALIZADO EM 2022/12/28 -->

# Janelas Para A Vida — 365

<p>&nbsp;</p>

## PRIMEIRO OUÇA

<span class="text-dir" align="right">_Dia 362_  
<small>28 de dezembro</small></span>

<p>&nbsp;</p>

<!-- "Responder antes de ouvir é estultícia e vergonha."
([[Provérbios|Pv]] 18:13) -->

> [!bible] Provérbios 18:13 • VFL
> Quem responde antes de ouvir mostra que é tolo e fica envergonhado.

<p>&nbsp;</p>

> [!quote] A razão pela qual temos dois ouvidos e uma boca é porque devemos ouvir mais do que falar.

Certamente você já ouviu essa frase. Pertence a Zenon, que viveu três séculos antes de Cristo. Era um filósofo grego que valorizava a arte de ouvir.

É triste ver que hoje pouca gente gosta de ouvir. Talvez por isso os psicanalistas têm uma carreira lucrativa. Milhares de pessoas recorrem aos consultórios para serem ouvidas. Todos falam, o tempo todo e em todo lugar.

O conselho de [[Salomão]] hoje é com relação ao perigo de falar antes de ouvir. Não seja apressado, preste atenção. Não existe substituto para a atenção. Se você não sabe qual é a pergunta completa, o que vai responder? Se você não ouvir a ordem, o que vai obedecer?

Aprenda a ouvir. Essa é uma das necessidades prioritárias do ser humano. [[4 formas de ouvir a voz de Deus|Ouça a voz de Deus]]. Pare, medite, analise porque as coisas estão indo do jeito que estão indo. Se todo dia você parar para ouvir a voz de Deus por quinze minutos, o dia será mais produtivo. A força que vem do Senhor o ajudará a enfrentar os desafios do dia com coragem.

Coragem não é ausência de medo. O medo sempre estará presente na natureza humana, mas você o encarará e o dominará com a força que recebe de Deus nesses quinze minutos diários de meditação.

Se, ao contrário, você sair de manhã correndo para cumprir seus compromissos sem ouvir a voz de Deus, o resultado, segundo o verso de hoje, será _“tolice e vergonha”_.

Tolice é insensatez, incapacidade de fazer as coisas certas. De que vale correr como um louco, de um lado para outro, tentando fazer o máximo, se por causa da tolice estraga tudo que realiza?

Faça de hoje um dia de realizações. Não se contente com fazer o seu melhor. Deus está sempre disposto a ajudá-lo a realizar mais do que o seu melhor. A excelência é o alvo daqueles que aprendem a desconfiar de suas próprias forças e depositar toda sua confiança em Jesus.

Que Deus lhe dê um dia de muita atenção e extraordinária produtividade, e não se esqueça de que _“responder antes de ouvir é tolice e vergonha”_.

<!-- SEPARADOR -->

<p>&nbsp;</p>

<hr class="style-two" />

<p>&nbsp;</p>

- **Fonte:** _“PRIMEIRO OUÇA”_ | por Alejandro Bullón • JPAV365 — Janelas Para A Vida
- December 27, 2022

<p>&nbsp;</p>

###### tags:

#Devocional/JPAV365 #RELIGIAO/Cristianismo #RELIGIAO/Cristianismo/devocional 

<p>&nbsp;</p>

